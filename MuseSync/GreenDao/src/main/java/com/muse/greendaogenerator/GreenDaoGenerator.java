package com.muse.greendaogenerator;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;
import org.greenrobot.greendao.generator.ToMany;
import org.atteo.evo.inflector.English;

public class GreenDaoGenerator {
    public static void main(String[] args) {
        Schema schema = new Schema(1, "com.sabaq.muse.database"); // Your app package name and the (.db) is the folder where the DAO files will be generated into.
        schema.enableKeepSectionsByDefault();

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema, "./app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables(final Schema schema) {
        addEntities(schema);
        for (int i = 0; i < schema.getEntities().size(); i++) {
            Entity entity = schema.getEntities().get(i);
            entity.setTableName(English.plural(entity.getClassName()));
        }

        // addPhonesEntities(schema);
    }

    // This is use to describe the colums of your table
    private static void addEntities(final Schema schema) {

        //region Exercise Entity
        Entity exercise = schema.addEntity("Exercise");
        exercise.setTableName("Exercise");
        //Primary Key
        addBaseProperty(false, exercise);
        addBaseAuditProperties(exercise);

        // Properties
        exercise.addStringProperty("ExerciseTitle").columnName("ExerciseTitle");
        exercise.addStringProperty("UrduTitle").columnName("UrduTitle");
        exercise.addStringProperty("UrduTitleImagePath").columnName("UrduTitleImagePath");
        exercise.addStringProperty("SindhiTitle").columnName("SindhiTitle");
        exercise.addStringProperty("Instruction").columnName("Instruction");
        exercise.addStringProperty("InstructionSoundPath").columnName("InstructionSoundPath");
        exercise.addStringProperty("TranslationImagePath").columnName("TranslationImagePath");
        exercise.addStringProperty("UrduInstructionImage").columnName("UrduInstructionImage");
        exercise.addStringProperty("Path").columnName("Path");
        exercise.addFloatProperty("UrduTitleImageSize").columnName("UrduTitleImageSize");
        exercise.addFloatProperty("InstructionSoundSize").columnName("InstructionSoundSize");
        exercise.addFloatProperty("TranslationImageSize").columnName("TranslationImageSize");
        exercise.addFloatProperty("UrduInstructionImageSize").columnName("UrduInstructionImageSize");
        exercise.addIntProperty("Order").columnName("Order");
        exercise.addIntProperty("TotalMarks").columnName("TotalMarks");
        exercise.addIntProperty("Timer").columnName("Timer");
        exercise.addBooleanProperty("Reviewed").columnName("Reviewed");
        exercise.addBooleanProperty("IsZoom").columnName("IsZoom");
        exercise.addBooleanProperty("IsSquare").columnName("IsSquare");
        exercise.addBooleanProperty("IsQuiz").columnName("IsQuiz");
        exercise.addBooleanProperty("IsCompleted").columnName("IsCompleted");

        // Foreign Keys
        Property exerciseGradeId = exercise.addStringProperty("GradeId").columnName("GradeId").getProperty();
        Property exerciseSubjectId = exercise.addStringProperty("SubjectId").columnName("SubjectId").getProperty();
        Property exerciseTopicId = exercise.addLongProperty("TopicId").columnName("TopicId").getProperty();
        Property exerciseSubTopicId = exercise.addLongProperty("SubTopicId").columnName("SubTopicId").getProperty();
        Property ExerciseId = exercise.addLongProperty("ExerciseId").columnName("ExerciseId").getProperty();
        Property ReadaloudId = exercise.addLongProperty("ReadaloudId").columnName("ReadaloudId").getProperty();
        Property exerciseTypeId = exercise.addStringProperty("ExerciseTypeId").columnName("ExerciseTypeId").getProperty();
        //endregion

        //region Question Entity
        Entity exerciseQuestion = schema.addEntity("ExerciseQuestion");
        exerciseQuestion.setTableName("ExerciseQuestion");
        //Primary Key
        addBaseProperty(false, exerciseQuestion);
        addBaseAuditProperties(exerciseQuestion);
        //Foreign Keys
        Property questionExerciseId = exerciseQuestion.addLongProperty("ExerciseId").columnName("ExerciseId").getProperty();
        exerciseQuestion.addStringProperty("ExerciseTypeId").columnName("ExerciseTypeId");
        // Properties
        exerciseQuestion.addStringProperty("TranslationImagePath").columnName("TranslationImagePath");
        exerciseQuestion.addStringProperty("Question").columnName("Question");
        exerciseQuestion.addStringProperty("UrduTitleImagePath").columnName("UrduTitleImagePath");
        exerciseQuestion.addStringProperty("UrduQuestion").columnName("UrduQuestion");
        exerciseQuestion.addStringProperty("SindhiQuestion").columnName("SindhiQuestion");
        exerciseQuestion.addStringProperty("ImagePath").columnName("ImagePath");
        exerciseQuestion.addStringProperty("UrduImagePath").columnName("UrduImagePath");
        exerciseQuestion.addStringProperty("AudioPath").columnName("AudioPath");
        exerciseQuestion.addStringProperty("Instruction").columnName("Instruction");
        exerciseQuestion.addStringProperty("InstructionAudioPath").columnName("InstructionAudioPath");
        exerciseQuestion.addStringProperty("InstructionImagePath").columnName("InstructionImagePath");
        exerciseQuestion.addStringProperty("ApplicationUserId").columnName("ApplicationUserId");

        exerciseQuestion.addFloatProperty("TranslationImageSize").columnName("TranslationImageSize");
        exerciseQuestion.addFloatProperty("ImageSize").columnName("ImageSize");
        exerciseQuestion.addFloatProperty("UrduImageSize").columnName("UrduImageSize");
        exerciseQuestion.addFloatProperty("AudioSize").columnName("AudioSize");
        exerciseQuestion.addFloatProperty("InstructionAudioSize").columnName("InstructionAudioSize");
        exerciseQuestion.addFloatProperty("InstructionImageSize").columnName("InstructionImageSize");

        exerciseQuestion.addIntProperty("Order").columnName("Order");
        exerciseQuestion.addBooleanProperty("TextboxShow").columnName("TextboxShow");
        //endregion

        //region Exercise Answer Entity
        Entity exerciseAnswer = schema.addEntity("ExerciseAnswer");
        exerciseAnswer.setTableName("ExerciseAnswer");
        //Primary Key
        addBaseProperty(false, exerciseAnswer);
        //Foreign Keys
        Property answerQuestionId = exerciseAnswer.addLongProperty("ExerciseQuestionId").columnName("ExerciseQuestionId").getProperty();
        // Properties
        exerciseAnswer.addStringProperty("Answer").columnName("Answer");
        exerciseAnswer.addStringProperty("ImagePath").columnName("ImagePath");
        exerciseAnswer.addStringProperty("AudioPath").columnName("AudioPath");
        exerciseAnswer.addStringProperty("SindhiAnswer").columnName("SindhiAnswer");
        exerciseAnswer.addStringProperty("UrduAnswer").columnName("UrduAnswer");
        exerciseAnswer.addStringProperty("MatchingAnswer").columnName("MatchingAnswer");
        exerciseAnswer.addStringProperty("MatchingImagePath").columnName("MatchingImagePath");
        exerciseAnswer.addStringProperty("MatchingAudioPath").columnName("MatchingAudioPath");

        exerciseAnswer.addFloatProperty("ImageSize").columnName("ImageSize");
        exerciseAnswer.addFloatProperty("AudioSize").columnName("AudioSize");
        exerciseAnswer.addFloatProperty("MatchingImageSize").columnName("MatchingImageSize");
        exerciseAnswer.addFloatProperty("MatchingAudioSize").columnName("MatchingAudioSize");

        exerciseAnswer.addIntProperty("CorrectOrder").columnName("CorrectOrder");
        exerciseAnswer.addBooleanProperty("CorrectAnswer").columnName("CorrectAnswer");
        //endregion

        //region Grade Entity
        Entity grade = schema.addEntity("Grade");
        grade.setTableName("Grade");
        //Primary Key
        addBaseProperty(true, grade);
        addBaseAuditProperties(grade);
        grade.addStringProperty("IconPath").columnName("IconPath");
        grade.addFloatProperty("IconSize").columnName("IconSize");

        //endregion

        //region Subject Entity
        Entity subject = schema.addEntity("Subject");
        subject.setTableName("Subject");
        //Primary Key
        addBaseProperty(true, subject);
        addBaseAuditProperties(subject);
        subject.addStringProperty("Description").columnName("Description");
        subject.addStringProperty("IconPath").columnName("IconPath");
        subject.addFloatProperty("IconSize").columnName("IconSize");

        //endregion

        //region Topic Entity
        Entity topic = schema.addEntity("Topic");
        topic.setTableName("Topic");
        //Primary Key
        addBaseProperty(false, topic);
        addBaseAuditProperties(topic);
        topic.addStringProperty("TopicName").columnName("TopicName");
        topic.addStringProperty("UrduTopicName").columnName("UrduTopicName");
        topic.addStringProperty("Description").columnName("Description");
        topic.addIntProperty("Order").columnName("Order");
        //Foreign Keys
        Property topicGradeId = topic.addStringProperty("GradeId").columnName("GradeId").getProperty();
        Property topicSubjectId = topic.addStringProperty("SubjectId").columnName("SubjectId").getProperty();

        topic.addStringProperty("IconPath").columnName("IconPath");
        topic.addStringProperty("UrduIconPath").columnName("UrduIconPath");
        topic.addFloatProperty("IconSize").columnName("IconSize");
        topic.addFloatProperty("UrduIconSize").columnName("UrduIconSize");

        //endregion

        //region SubTopic Entity
        Entity subTopic = schema.addEntity("SubTopic");
        subTopic.setTableName("SubTopic");
        //Primary Key
        addBaseProperty(false, subTopic);
        addBaseAuditProperties(subTopic);
        subTopic.addStringProperty("SubTopicName").columnName("SubTopicName");
        subTopic.addStringProperty("Description").columnName("Description");
        subTopic.addIntProperty("Order").columnName("Order");
        subTopic.addStringProperty("IconPath").columnName("IconPath");
        subTopic.addFloatProperty("IconSize").columnName("IconSize");

        //Foreign Keys
        Property subTopicTopicId = subTopic.addLongProperty("TopicId").columnName("TopicId").getProperty();
        //endregion

        //region Video Entity
        Entity video = schema.addEntity("Video");
        video.setTableName("Video");
        //Primary Key
        addBaseProperty(false, video);
        addBaseAuditProperties(video);
        video.addStringProperty("Title").columnName("Title");
        video.addStringProperty("UrduTitleImagePath").columnName("UrduTitleImagePath");
        video.addStringProperty("Description").columnName("Description");
        video.addStringProperty("Link").columnName("Link");
        video.addStringProperty("UrduPath").columnName("UrduPath");
        video.addStringProperty("SindhiPath").columnName("SindhiPath");
        video.addStringProperty("FileType").columnName("FileType");
        video.addStringProperty("Path").columnName("Path");
        video.addStringProperty("FileName").columnName("FileName");
        video.addIntProperty("Order").columnName("Order");

        video.addFloatProperty("UrduTitleImageSize").columnName("UrduTitleImageSize");
        video.addFloatProperty("UrduFileSize").columnName("UrduFileSize");
        video.addFloatProperty("SindhiFileSize").columnName("SindhiFileSize");
        video.addFloatProperty("FileSize").columnName("FileSize");
        video.addBooleanProperty("IsCompleted").columnName("IsCompleted");
        //Foreign Keys
        Property videoGradeId = video.addStringProperty("GradeId").columnName("GradeId").getProperty();
        Property videoSubjectId = video.addStringProperty("SubjectId").columnName("SubjectId").getProperty();
        Property videoTopicId = video.addLongProperty("TopicId").columnName("TopicId").getProperty();
        Property videoSubTopicId = video.addLongProperty("SubTopicId").columnName("SubTopicId").getProperty();
        Property videoTypeId = video.addStringProperty("VideoTypeId").columnName("VideoTypeId").getProperty();
        //endregion

        //region VideoQuestion

        Entity videoQuestion = schema.addEntity("VideoQuestion");
        videoQuestion.setTableName("VideoQuestion");
        //Primary Key
        addBaseProperty(false, videoQuestion);
        addBaseAuditProperties(videoQuestion);
        videoQuestion.addBooleanProperty("IsAnswerCompulsory").columnName("IsAnswerCompulsory");
        videoQuestion.addIntProperty("Order").columnName("Order");
        videoQuestion.addIntProperty("PlayTime").columnName("PlayTime");
        videoQuestion.addIntProperty("ForwardTime").columnName("ForwardTime");
        Property video_Id_videoQuestion = videoQuestion.addLongProperty("VideoId").columnName("VideoId").getProperty();

        //endregion

        //region VideoExternalLink

        Entity videoExternalLink = schema.addEntity("VideoExternalLink");
        videoExternalLink.setTableName("VideoExternalLink");
        //Primary Key
        addBaseProperty(false, videoExternalLink);
        addBaseAuditProperties(videoExternalLink);
        videoExternalLink.addBooleanProperty("IsAnswerCompulsory").columnName("IsAnswerCompulsory");
        videoExternalLink.addIntProperty("Order").columnName("Order");
        videoExternalLink.addStringProperty("Text").columnName("Text");
        videoExternalLink.addIntProperty("PlayTime").columnName("PlayTime");
        Property video_Id_videoExternalLink = videoExternalLink.addLongProperty("VideoId").columnName("VideoId").getProperty();

        //endregion

        //region VideoDocument

        Entity videoDocument = schema.addEntity("VideoDocument");
        videoDocument.setTableName("VideoDocument");
        //Primary Key
        addBaseProperty(false, videoDocument);
        addBaseAuditProperties(videoDocument);
        videoDocument.addBooleanProperty("IsAnswerCompulsory").columnName("IsAnswerCompulsory");
        videoDocument.addIntProperty("Order").columnName("Order");
        videoDocument.addStringProperty("Text").columnName("Text");
        videoDocument.addIntProperty("PlayTime").columnName("PlayTime");
        Property video_Id_videoDocument = videoDocument.addLongProperty("VideoId").columnName("VideoId").getProperty();

        //endregion


        //region Document Entity
        Entity document = schema.addEntity("Document");
        document.setTableName("Document");
        //Primary Key
        addBaseProperty(false, document);
        addBaseAuditProperties(document);
        document.addStringProperty("Title").columnName("Title");
        document.addStringProperty("UrduTitleImagePath").columnName("UrduTitleImagePath");
        document.addStringProperty("FileType").columnName("FileType");
        document.addStringProperty("Path").columnName("Path");
        document.addStringProperty("FileName").columnName("FileName");
        document.addIntProperty("Order").columnName("Order");

        document.addFloatProperty("UrduTitleImageSize").columnName("UrduTitleImageSize");
        document.addFloatProperty("FileSize").columnName("FileSize");
        //Foreign Keys
        Property documentGradeId = document.addStringProperty("GradeId").columnName("GradeId").getProperty();
        Property documentSubjectId = document.addStringProperty("SubjectId").columnName("SubjectId").getProperty();
        Property documentTopicId = document.addLongProperty("TopicId").columnName("TopicId").getProperty();
        Property documentTypeId = document.addStringProperty("DocumentTypeId").columnName("DocumentTypeId").getProperty();
        //endregion

        //region Readaloud Entity
        Entity readaloud = schema.addEntity("Readaloud");
        readaloud.setTableName("Readaloud");
        //Primary Key
        addBaseProperty(false, readaloud);
        addBaseAuditProperties(readaloud);
        readaloud.addStringProperty("Title").columnName("Title");
        readaloud.addStringProperty("UrduTitleImagePath").columnName("UrduTitleImagePath");
        readaloud.addStringProperty("Description").columnName("Description");
        readaloud.addStringProperty("UrduTitle").columnName("UrduTitle");
        readaloud.addStringProperty("UrduPath").columnName("UrduPath");
        readaloud.addBooleanProperty("CompleteConvention").columnName("CompleteConvention");
        readaloud.addBooleanProperty("Reviewed").columnName("Reviewed");
        readaloud.addIntProperty("Order").columnName("Order");

        readaloud.addFloatProperty("UrduTitleImageSize").columnName("UrduTitleImageSize");
        readaloud.addFloatProperty("UrduFileSize").columnName("UrduFileSize");
        readaloud.addFloatProperty("SindhiFileSize").columnName("SindhiFileSize");
        readaloud.addFloatProperty("FileSize").columnName("FileSize");

        readaloud.addBooleanProperty("IsCompleted").columnName("IsCompleted");
        //Foreign Keys
        Property readaloudGradeId = readaloud.addStringProperty("GradeId").columnName("GradeId").getProperty();
        Property readaloudSubjectId = readaloud.addStringProperty("SubjectId").columnName("SubjectId").getProperty();
        Property readaloudTopicId = readaloud.addLongProperty("TopicId").columnName("TopicId").getProperty();
        Property readaloudSubTopicId = readaloud.addLongProperty("SubTopicId").columnName("SubTopicId").getProperty();
        Property readaloudId = readaloud.addLongProperty("ReadaloudId").columnName("ReadaloudId").getProperty();
        Property readlaoudExerciseId = readaloud.addLongProperty("ExerciseId").columnName("ExerciseId").getProperty();
        Property readaloudTypeId = readaloud.addStringProperty("ReadaloudTypeId").columnName("ReadaloudTypeId").getProperty();
        //endregion

        //region  ReadaloudSlide Entity
        Entity readaloudSlide = schema.addEntity("ReadaloudSlide");
        readaloudSlide.setTableName("ReadaloudSlide");
        //Primary Key
        addBaseProperty(false, readaloudSlide);
        addBaseAuditProperties(readaloudSlide);
        readaloudSlide.addStringProperty("ImagePath").columnName("ImagePath");
        readaloudSlide.addFloatProperty("ImageSize").columnName("ImageSize");
        readaloudSlide.addStringProperty("UrduImagePath").columnName("UrduImagePath");
        readaloudSlide.addFloatProperty("UrduImageSize").columnName("UrduImageSize");
        readaloudSlide.addStringProperty("SindhiImagePath").columnName("SindhiImagePath");
        readaloudSlide.addFloatProperty("SindhiImageSize").columnName("SindhiImageSize");
        readaloudSlide.addStringProperty("AudioPath").columnName("AudioPath");
        readaloudSlide.addFloatProperty("AudioSize").columnName("AudioSize");
        readaloudSlide.addStringProperty("UrduAudioPath").columnName("UrduAudioPath");
        readaloudSlide.addFloatProperty("UrduAudioSize").columnName("UrduAudioSize");
        readaloudSlide.addStringProperty("SindhiAudioPath").columnName("SindhiAudioPath");
        readaloudSlide.addFloatProperty("SindhiAudioSize").columnName("SindhiAudioSize");
        readaloudSlide.addStringProperty("TextImageFilePath").columnName("TextImageFilePath");
        readaloudSlide.addFloatProperty("TextImageFileSize").columnName("TextImageFileSize");
        readaloudSlide.addStringProperty("Text").columnName("Text");
        readaloudSlide.addIntProperty("Order").columnName("Order");
        readaloudSlide.addBooleanProperty("Autoplay").columnName("Autoplay");
        readaloudSlide.addBooleanProperty("ReadWithMe").columnName("ReadWithMe");
        readaloudSlide.addBooleanProperty("ReadFluently").columnName("ReadFluently");
        readaloudSlide.addBooleanProperty("HasDictionary").columnName("HasDictionary");
        readaloudSlide.addBooleanProperty("HasQuestion").columnName("HasQuestion");
        //Foreign Keys
        Property readAloudSlideReadaloudId = readaloudSlide.addLongProperty("ReadaloudId").columnName("ReadaloudId").getProperty();
        //endregion

        //region ReadAloud Slide Audio
        Entity readaloudSlideAudio = schema.addEntity("ReadaloudSlideAudio");
        readaloudSlideAudio.setTableName("ReadaloudSlideAudio");
        //Primary Key
        addBaseProperty(false, readaloudSlideAudio);
        addBaseAuditProperties(readaloudSlideAudio);
        readaloudSlideAudio.addStringProperty("AudioPath").columnName("AudioPath");
        readaloudSlideAudio.addFloatProperty("AudioFileSize").columnName("AudioFileSize");
        readaloudSlideAudio.addStringProperty("Word").columnName("Word");
        Property readaloudSlideAudioReadaloudSlideId = readaloudSlideAudio.addLongProperty("ReadaloudSlideId").columnName("ReadaloudSlideId").getProperty();
        //endregion

        //region Readaloud Dictionary
        Entity readaloudDictionary = schema.addEntity("ReadaloudDictionary");
        readaloudDictionary.setTableName("ReadaloudDictionary");
        //Primary Key
        addBaseProperty(false, readaloudDictionary);
        addBaseAuditProperties(readaloudDictionary);
        readaloudDictionary.addStringProperty("Word").columnName("Word");
        readaloudDictionary.addFloatProperty("Description").columnName("Description");

        Property readaloudDictionaryReadaloudSlideId = readaloudDictionary.addLongProperty("ReadaloudSlideId").columnName("ReadaloudSlideId").getProperty();
        //endregion

        //region School
        Entity school = schema.addEntity("School");
        school.setTableName("School");
        //Primary Key
        addBaseProperty(false, school);


        // Properties
        school.addStringProperty("Name").columnName("Name");
        school.addStringProperty("SchoolTypeId").columnName("SchoolTypeId");
        school.addStringProperty("SchoolCode").columnName("SchoolCode");
        //endregion

        //region Student
        Entity student = schema.addEntity("Student");
        student.setTableName("Student");
        //Primary Key
        addBaseProperty(false, student);
        addBaseAuditProperties(student);
        // Foreign Keys
        Property studentSchoolId = student.addLongProperty("SchoolId").columnName("SchoolId").getProperty();
        // Properties
        student.addStringProperty("FirstName").columnName("FirstName");
        student.addStringProperty("LastName").columnName("LastName");
        student.addStringProperty("StudentCode").columnName("StudentCode");
        //endregion

        //region Relations

        //Topic Relation
        addOneToManyRelation(topic, subTopic, subTopicTopicId); //Exercise
        addOneToManyRelation(topic, exercise, exerciseTopicId); //Exercise
        addOneToManyRelation(topic, document, documentTopicId); //Document
        addOneToManyRelation(topic, video, videoTopicId); // Video
        addOneToManyRelation(topic, readaloud, readaloudTopicId); // Video

        //SubTopic Relation
        addOneToManyRelation(subTopic, exercise, exerciseSubTopicId);
        addOneToManyRelation(subTopic, readaloud, readaloudSubTopicId);
        addOneToManyRelation(subTopic, video, videoSubTopicId);

        //Exercise Relations
        addOneToManyRelation(exercise, exerciseQuestion, questionExerciseId);

        //Exercise Question Relations
        addOneToManyRelation(exerciseQuestion, exerciseAnswer, answerQuestionId);

        //Readaloud Relation
        addOneToManyRelation(readaloud, readaloudSlide, readAloudSlideReadaloudId);

        //Readaloud Slide Relation
        addOneToManyRelation(readaloudSlide, readaloudDictionary, readaloudDictionaryReadaloudSlideId);
        addOneToManyRelation(readaloudSlide, readaloudSlideAudio, readaloudSlideAudioReadaloudSlideId);

        //School Relation
        addOneToManyRelation(school, student, studentSchoolId);

        //Video Relation
        addOneToManyRelation(video, videoExternalLink, video_Id_videoExternalLink); //videoExternalLink
        addOneToManyRelation(video, videoQuestion, video_Id_videoQuestion); //videoQuestion
        addOneToManyRelation(video, videoDocument, video_Id_videoDocument); //videoDocument

        //endregion


    }

    private static void addBaseProperty(boolean isIdString, Entity entity) {
        //Id set to Long as it Only allows Long property to be foreign keys
        if (isIdString)
            entity.addStringProperty("Id").columnName("Id").primaryKey();
        else entity.addLongProperty("Id").columnName("Id").primaryKey().autoincrement();
        entity.addBooleanProperty("Active").columnName("Active");
    }


    private static void addBaseAuditProperties(Entity entity) {
        entity.addDateProperty("CreatedDate").columnName("CreatedDate");
        entity.addStringProperty("CreatedBy").columnName("CreatedBy");
        entity.addDateProperty("UpdatedDate").columnName("UpdatedDate");
        entity.addStringProperty("UpdatedBy").columnName("UpdatedBy");
    }

    private static void addOneToManyRelation(Entity parentEntity, Entity childEntity, Property parentIdProperty) {
        childEntity.addToOne(parentEntity, parentIdProperty);
        ToMany manyProperty = parentEntity.addToMany(childEntity, parentIdProperty);
        manyProperty.setName(English.plural(Character.toLowerCase(childEntity.getClassName().charAt(0)) + childEntity.getClassName().substring(1, childEntity.getClassName().length())));
    }
}
