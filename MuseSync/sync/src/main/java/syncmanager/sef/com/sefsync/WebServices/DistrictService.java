package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.District;


public interface DistrictService {
    @GET("/District")
    public void getDistrict(Callback<List<District>> callback);

    //Get Order record base on ID
    @GET("/District/{id}")
    public void getDistrictById(@Path("id") Integer id, Callback<District> callback);

    //Delete Order record base on ID
    @DELETE("/District/{id}")
    public void deleteDistrictById(@Path("id") Integer id, Callback<District> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/District/{id}")
    public void updateDistrictById(@Path("id") Integer id, @Body District district, Callback<District> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/District")
    public void addDistrict(@Body District district, Callback<District> callback);

}
