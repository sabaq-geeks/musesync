package syncmanager.sef.com.sefsync.Helper;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import static android.content.ContentValues.TAG;

/**
 * Created by Hammad Ali Khan on 4/28/2017.
 */

public class GlobalHelper {

    // Sync Adapter ke Attributes
    public long SECONDS_PER_MINUTE = 60L;
    public long SYNC_INTERVAL_IN_MINUTES = 2L;
    public long SYNC_INTERVAL =
            SYNC_INTERVAL_IN_MINUTES *
                    SECONDS_PER_MINUTE;

    //******** create account
    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "com.example.android.sefdatasync123.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "123sef.com";
    // The account name
    public static final String ACCOUNT = "sefaccount123";
    public static final String URL = "http://sef.sabaq.edu.pk/api/";
    public static final String DOWNLOAD_URL = "http://sef.sabaq.edu.pk";

    public  static boolean isStoragePermissionGranted(Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (context.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                // ActivityCompat.requestPermissions(MainService.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }
}
