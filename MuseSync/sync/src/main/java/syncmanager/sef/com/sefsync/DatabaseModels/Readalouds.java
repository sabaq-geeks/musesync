package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/26/2017.
 */
@DatabaseTable
public class Readalouds extends BaseModel {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

   /* @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "GradeId")
    private Grades grades;*/

    @DatabaseField(columnName = "GradeId")
    private String grades;

    @DatabaseField( columnName = "SubjectId")
    private String subjects;




    @DatabaseField(columnName = "TopicId")
    private String topics;




    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "ReadaloudTypeId")
    private ReadaloudTypes readaloudTypes;

    @DatabaseField
    private String Title;

    @ForeignCollectionField
    private Collection<ReadaloudSlides> readaloudSlideList;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    /*public Grades getGrades() {
        return grades;
    }

    public void setGrades(Grades grades) {
        this.grades = grades;
    }

    public Subjects getSubjects() {
        return subjects;
    }

    public void setSubjects(Subjects subjects) {
        this.subjects = subjects;
    }
*/
   /* public String getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

    public Topics getTopics() {
        return topics;
    }

    public void setTopics(Topics topics) {
        this.topics = topics;
    }

    public String getTopicId() {
        return TopicId;
    }

    public void setTopicId(String topicId) {
        TopicId = topicId;
    }

*/

    public ReadaloudTypes getReadaloudTypes() {
        return readaloudTypes;
    }

    public void setReadaloudTypes(ReadaloudTypes readaloudTypes) {
        this.readaloudTypes = readaloudTypes;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Collection<ReadaloudSlides> getReadaloudSlideList() {
        return readaloudSlideList;
    }

    public void setReadaloudSlideList(Collection<ReadaloudSlides> readaloudSlideList) {
        this.readaloudSlideList = readaloudSlideList;
    }
}
