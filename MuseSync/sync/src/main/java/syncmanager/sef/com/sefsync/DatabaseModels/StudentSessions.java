package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class StudentSessions  {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "StudentId")
    private Students student;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "AcademicSessionId")
    private AcademicSessions academicSessions;

    @DatabaseField
    private Boolean Active;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Students getStudent() {
        return student;
    }

    public void setStudent(Students student) {
        this.student = student;
    }

    public AcademicSessions getAcademicSessions() {
        return academicSessions;
    }

    public void setAcademicSessions(AcademicSessions academicSessions) {
        this.academicSessions = academicSessions;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }
}
