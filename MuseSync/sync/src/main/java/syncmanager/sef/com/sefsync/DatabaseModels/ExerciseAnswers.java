package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class ExerciseAnswers {

    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "ExerciseQuestionId")
    private ExerciseQuestions exerciseQuestions;

    private int exerciseQuestionid;

    @DatabaseField
    private String Answer;

    @DatabaseField
    private String UrduAnswer;

    @DatabaseField
    private String SindhiAnswer;

    @DatabaseField
    private Boolean CorrectAnswer;

    @DatabaseField
    private int CorrectOrder;

    @DatabaseField
    private String ImagePath;

    @DatabaseField
    private String AudioPath;

    @DatabaseField
    private Boolean Active;



    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public ExerciseQuestions getExerciseQuestions() {
        return exerciseQuestions;
    }

    public void setExerciseQuestions(ExerciseQuestions exerciseQuestions) {
        this.exerciseQuestions = exerciseQuestions;
    }

    public int getExerciseQuestionId() {
        return exerciseQuestionid;
    }

    public void setExerciseQuestionId(int exerciseQuestionId) {
        exerciseQuestionid = exerciseQuestionId;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public Boolean getCorrectAnswer() {
        return CorrectAnswer;
    }

    public void setCorrectAnswer(Boolean correctAnswer) {
        CorrectAnswer = correctAnswer;
    }

    public int getCorrectOrder() {
        return CorrectOrder;
    }

    public void setCorrectOrder(int correctOrder) {
        CorrectOrder = correctOrder;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getAudioPath() {
        return AudioPath;
    }

    public void setAudioPath(String audioPath) {
        AudioPath = audioPath;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }

    public int getRemoteId() {
        return RemoteId;
    }

    public void setRemoteId(int remoteId) {
        RemoteId = remoteId;
    }

    public int getExerciseQuestionid() {
        return exerciseQuestionid;
    }

    public void setExerciseQuestionid(int exerciseQuestionid) {
        this.exerciseQuestionid = exerciseQuestionid;
    }

    public String getUrduAnswer() {
        return UrduAnswer;
    }

    public void setUrduAnswer(String urduAnswer) {
        UrduAnswer = urduAnswer;
    }

    public String getSindhiAnswer() {
        return SindhiAnswer;
    }

    public void setSindhiAnswer(String sindhiAnswer) {
        SindhiAnswer = sindhiAnswer;
    }


}
