package syncmanager.sef.com.sefsync.syncmanager;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.io.File;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import syncmanager.sef.com.sefsync.Database.Constants;
import syncmanager.sef.com.sefsync.Database.DatabaseManager;
import syncmanager.sef.com.sefsync.DatabaseModels.ExerciseAnswers;
import syncmanager.sef.com.sefsync.DatabaseModels.ExerciseQuestions;
import syncmanager.sef.com.sefsync.DatabaseModels.Exercises;
import syncmanager.sef.com.sefsync.DatabaseModels.Grades;
import syncmanager.sef.com.sefsync.DatabaseModels.Subjects;
import syncmanager.sef.com.sefsync.DatabaseModels.Topics;
import syncmanager.sef.com.sefsync.Helper.GlobalHelper;
import syncmanager.sef.com.sefsync.Helper.SharedPreference;
import syncmanager.sef.com.sefsync.WebServices.RetrofitRestClient;

import static android.content.ContentValues.TAG;

/**
 * Created by Hammad Ali Khan on 4/28/2017.
 */

public class SyncMethods {
    Context context;
    DatabaseManager databaseManager;
    private static final String[] AUDIO = {"http://songpk.mobi/downlod2.php?filep=dgmob/downloadfile/124794/Bound_It_(Akon)(www.SongPK.mobi).mp3",
            "http://songpk.mobi/downlod2.php?filep=dgmob/downloadfile/43121/Believe_Me_(Mastered_Version)_(Lil_Wayne_Ft._Drake)_(www.SongPK.mobi).mp3",
            "http://songpk.mobi/downlod2.php?filep=dgmob/downloadfile/41094/Twerking_Cyrus_(Magazeen_Ft._Wale)_(www.SongPK.mobi).mp3",
            "http://songpk.mobi/downlod2.php?filep=dgmob/downloadfile/41560/Till_I_Find_You_(CDQ)_(Austin_Mahone)_(www.SongPK.mobi).mp3",
            "http://songpk.mobi/downlod2.php?filep=dgmob/downloadfile/43077/Clarity_(FULL)_(Zedd_Ft._Medina)_(www.SongPK.mobi).mp3",
            "http://songpk.mobi/downlod2.php?filep=dgmob/downloadfile/108008/Imagine_(Kid_Ink)(www.SongPK.mobi).mp3"

    };
    private static final String[] FILE1 = {"http://smart.sabaq.edu.pk/Content/Exercises/KG-1/English/First%20Topic/46Categorization%20Demo/business_400x400.png",
            "http://smart.sabaq.edu.pk/Content/Exercises/KG-1/English/First%20Topic/46Categorization%20Demo/436.jpg",
            "http://smart.sabaq.edu.pk/Content/Exercises/KG-1/English/First%20Topic/46Categorization%20Demo/2Aan_aSU.jpg",
            "http://smart.sabaq.edu.pk/Content/Exercises/KG-1/English/First%20Topic/46Categorization%20Demo//Q3Sgp7.jpg"
            , "https://cdn.pixabay.com/photo/2016/07/29/18/24/water-1555170_960_720.jpg",
            "https://extension.unh.edu/sites/default/files/images/4HAnSci/inquisitive-cat300.png",
            "https://cdn.pixabay.com/photo/2013/06/23/09/12/waves-140681_960_720.jpg",
            "https://nu.aeon.co/images/04efb9b6-46fe-423f-b049-59ac1f7377ab/header_Southern-Ocean.jpg"
    };

    public SyncMethods(Context context) {
        this.context = context;
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        this.context.registerReceiver(downloadReceiver, filter);
        databaseManager = new DatabaseManager(this.context);
    }

    /**
     * This method get max date(created or updated date) of exercise table,sends the date
     * to api and in returns get the list of exercises which have to be saved in local db.
     */
    public void getExercisesAndSaveInDb_ByMaxDate() {
        RetrofitRestClient retrofitRestClient = new RetrofitRestClient(GlobalHelper.URL);
        final Dao<Exercises, Integer> exercisesDao = new DatabaseManager(context).getHelper().getexercisesDao();

        String date = getTableMaxDate(exercisesDao, "exercises");
        Log.d("Yo", "getExercisesAndSaveInDb_ByMaxDate: " + date);
        retrofitRestClient.getExerciseService().getExercisesByDate(date, new Callback<List<Exercises>>() {
            public static final String TAG = "Subjects:";

            @Override
            public void success(List<Exercises> exercises, Response response) {
                saveExercisesInDb(exercises);
               // Gson gson = new Gson();
              // List<Exercises> exerciseSharedPref=  getSharedPref(Constants.TABLE_EXERCISE);


               // String s = gson.toJson(exercises);
                //saveInSharedPref(s,Constants.TABLE_EXERCISE);

                Log.d("Bydate", "success: " + exercises.size());
                Log.d("Bydate", "success: title " + exercises.get(0).getExerciseTitle());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "failure() called with: error = [" + error.getMessage() + "]");
            }
        });
    }

    private String getSharedPref(String key) {
        SharedPreference sharedPreference=new SharedPreference(context);
       return sharedPreference.getStringValue(key);
    }

    private void saveInSharedPref(String s, String key) {
        SharedPreference sharedPreference=new SharedPreference(context);
        sharedPreference.saveValueInSharedPreference(key,s);
    }


    /**
     * This method takes exercise list as param and save it in database in a loop.
     * It also calls the method which saves exercise questions of each exercise.
     *
     * @param exercises list to be saved in db
     */
    private void saveExercisesInDb(List<Exercises> exercises) {
        final Dao<Exercises, Integer> exercisesDao = databaseManager.getHelper().getexercisesDao();
        ArrayList<Exercises> exercisesList = new ArrayList<Exercises>(exercises);
        int j = 0;
        for (int i = 0; i < exercisesList.size(); i++) {
            try {
                Exercises exercise = exercisesList.get(i);
              /*  if (j != AUDIO.length - 1) {

                    exercise.setInstructionSoundPath(AUDIO[j]);
                    j++;
                }*/
                exercisesDao.createOrUpdate(exercise);
                Log.i("Question List:", "save exercise in db size:" + exercisesList.get(i).getExerciseQuestionList().size());
                saveExerciseQuestionsInDb(exercise, exercise.getExerciseQuestionList());
                if(i+1==exercisesList.size()){
                    checkAllFilesPathAndDownloadifNotThere();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }


    /**
     * This method saves Exercisequestion list in its table.
     *
     * @param exercise             to which exercise questions belongs
     * @param exerciseQuestionList exercise question list to be saved in db
     */
    private void saveExerciseQuestionsInDb(Exercises exercise, Collection<ExerciseQuestions> exerciseQuestionList) {
        final Dao<ExerciseQuestions, Integer> exercisesQuesDao = databaseManager.getHelper().getexercisequestionsDao();
        ArrayList<ExerciseQuestions> exerciseQuestionsList = new ArrayList(exerciseQuestionList);
        int j = 0;
        for (int i = 0; i < exerciseQuestionList.size(); i++) {
            try {
                ExerciseQuestions exerciseQuestion = exerciseQuestionsList.get(i);
                exerciseQuestion.setExercise(exercise);
               /* if (j != FILE1.length / 2 - 1) {
                    exerciseQuestion.setImagePath(FILE1[j]);
                    exerciseQuestion.setAudioPath(AUDIO[j]);
                    j++;
                }*/
                exercisesQuesDao.createOrUpdate(exerciseQuestion);
                saveExerciseAnswersInDb(exerciseQuestion, exerciseQuestion.getExerciseAnswersList(), exercise);
            } catch (SQLException e) {

            }
        }

    }

    private String ROOT_DIRECTORY = "/dm/";


    /**
     * This method return local path of exercise
     *
     * @param exercises
     * @return
     */
    private String getExerciseLocalPath(Exercises exercises) {
        String exercisePath = exercises.getId() + "-" + exercises.getExerciseTitle() + "/";
        exercisePath = Environment.getExternalStorageDirectory() + ROOT_DIRECTORY + exercisePath;
        Log.d(TAG, "getExerciseLocalPath: " + exercisePath);
        File Directory = new File(exercisePath);
        Directory.mkdirs();
        return exercisePath;
    }

    /**
     * This method saves exercise answers list in db.
     *
     * @param exerciseQuestion
     * @param exerciseAnswersCollection
     * @param exercises
     */

    private void saveExerciseAnswersInDb(ExerciseQuestions exerciseQuestion, Collection<ExerciseAnswers> exerciseAnswersCollection, Exercises exercises) {
        final Dao<ExerciseAnswers, Integer> exercisesAnsDao = databaseManager.getHelper().getexerciseanswersDao();
        ArrayList<ExerciseAnswers> exerciseAnsList = new ArrayList<ExerciseAnswers>(exerciseAnswersCollection);
        int k = FILE1.length / 2;
        for (int i = 0; i < exerciseAnsList.size(); i++) {
            try {
                ExerciseAnswers exerciseAnswer = exerciseAnsList.get(i);
                exerciseAnswer.setExerciseQuestions(exerciseQuestion);
               /* if (k != FILE1.length - 1) {
                    exerciseAnswer.setImagePath(FILE1[k]);
                    k++;
                }*/
                exercisesAnsDao.createOrUpdate(exerciseAnswer);
                // downloadExerciseAnswersImageFiles(exerciseAnswer, exercises);
            } catch (SQLException e) {

            }
        }
    }

    /**
     * This methods gets the list of Subjects by calling api and save it in db.
     * If api returns list of subjects then it first delete all subjects from db
     * and save the new subject list.
     */
    public void getSubjectsAndSaveInDb() {
        RetrofitRestClient retrofitRestClient = new RetrofitRestClient(GlobalHelper.URL);
        final Dao<Subjects, Integer> subjectsDao = databaseManager.getHelper().getsubjectsDao();
        retrofitRestClient.getSubjectsService().getSubjects(new Callback<List<Subjects>>() {
            public static final String TAG = "Subjects:";

            @Override
            public void success(List<Subjects> subjects, Response response) {
                Log.d("Subject FirstTime", "success: " + subjects.size());
                if (subjects.size() > 0) {
                    deleteTable(subjectsDao);
                    saveSubjectsInDb(subjects);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "failure() called with: error = [" + error.getMessage() + "]");
            }
        });

    }

    /**
     * This methods gets the list of Topics by calling api and save it in db.
     * If api returns list of topics then it first delete all topics from db
     * and save the new topic list.
     */

    public void getTopicsAndSaveInDb() {
        RetrofitRestClient retrofitRestClient = new RetrofitRestClient(GlobalHelper.URL);
        final Dao<Topics, Integer> topicsDao = databaseManager.getHelper().gettopicsDao();

        retrofitRestClient.getTopicsService().getTopics(new Callback<List<Topics>>() {
            public static final String TAG = "topics:";

            @Override
            public void success(List<Topics> topics, Response response) {
                Log.d("topics FirstTime", "success: " + topics.size());
                if (topics.size() > 0) {
                    deleteTable(topicsDao);
                    saveTopicsInDb(topics);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "failure() called with: error = [" + error.getMessage() + "]");
            }
        });
    }

    /**
     * This method takes topic list and save it in db.
     *
     * @param topics
     */

    private void saveTopicsInDb(List<Topics> topics) {
        final Dao<Topics, Integer> topicsDao = databaseManager.getHelper().gettopicsDao();
        ArrayList<Topics> topicsList = new ArrayList<Topics>(topics);

        for (int i = 0; i < topicsList.size(); i++) {
            try {
                topicsDao.createOrUpdate(topicsList.get(i));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * This method deletes old grades if api successfully returns list of grades and
     * calls the method to save grades in db.
     */
    public void getGradesAndSaveInDb() {
        RetrofitRestClient retrofitRestClient = new RetrofitRestClient(GlobalHelper.URL);
        final Dao<Grades, Integer> GradesDao = databaseManager.getHelper().getgradesDao();

        retrofitRestClient.getGradesService().getGrades(new Callback<List<Grades>>() {
            public static final String TAG = "grades";

            @Override
            public void success(List<Grades> grades, Response response) {
                if (grades.size() > 0) {
                    deleteTable(GradesDao);
                    saveGradesInDb(grades, GradesDao);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, "failure: " + error.getMessage());
            }
        });
    }

    /**
     * This method save grades in db.
     *
     * @param grades list to be saved in db
     * @param dao
     */
    private void saveGradesInDb(List<Grades> grades, Dao dao) {
        ArrayList<Grades> gradesList = new ArrayList<Grades>(grades);
        for (int i = 0; i < gradesList.size(); i++) {
            try {
                dao.create(gradesList.get(i));
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * This method get dao of the table and delete the whole table.
     *
     * @param dao
     */
    private void deleteTable(Dao dao) {

        try {
            Log.d(":size", ":size before delete:" + dao.countOf());
            dao.deleteBuilder().delete();

            Log.d(":size", ":size after delete:" + dao.countOf());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Saving subjects in local database
     *
     * @param subjects subject list to be saved
     */
    private void saveSubjectsInDb(List<Subjects> subjects) {
        final Dao<Subjects, Integer> subjectsDao = new DatabaseManager(context).getHelper().getsubjectsDao();
        ArrayList<Subjects> subjectList = new ArrayList<Subjects>(subjects);

        for (int i = 0; i < subjectList.size(); i++) {
            try {
                //subjectList.get(i).setExerciseQuestions(exerciseQuestion);
                subjectsDao.create(subjectList.get(i));

            } catch (SQLException e) {

            }
        }

    }

    /**
     * This method takes dao and return max date
     *
     * @param dao
     * @param tableName
     * @return Max date
     */

    private String getTableMaxDate(Dao dao, String tableName) {
        String obj = null;
        String updatedDate = null;
        String createdDate = null;
        String datetoBeSend = null;
        try {
           /* final Dao<Exercises, Integer> getexercisequestionsDao = new DatabaseManager(context).getHelper().getexercisesDao();
            UpdateBuilder<Exercises, Integer> updateBuilder = getexercisequestionsDao.updateBuilder();
            try {


                updateBuilder.updateColumnValue("UpdatedDate", date);
                updateBuilder.where().eq("Id", 3);
                getexercisequestionsDao.update(updateBuilder.prepare());
            } catch (SQLException e) {
                e.printStackTrace();
            }*/

            QueryBuilder<Exercises, Integer> qb = dao.queryBuilder();
            qb.selectRaw("MAX(UpdatedDate)", "MAX(CreatedDate)");
            GenericRawResults<String[]> reslts = dao.queryRaw(qb.prepareStatementString());
            String[] values = reslts.getFirstResult();
            if (values != null && values.length != 0) {
                if (values[0] != null) {
                    updatedDate = values[0];
                    createdDate = values[1];
                    datetoBeSend = compareDate(updatedDate, createdDate);
                    Log.d("2nd", "getTableMaxDate:UpdatedDate " + values[0]);
                    return datetoBeSend;

                } else {
                    Log.d("2nd", "getTableMaxDate:CreatedDate " + values[1]);
                    datetoBeSend = values[1];
                    return datetoBeSend;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return datetoBeSend;
    }

    /**
     * compares both dates and returns the max date.
     *
     * @param updatedDate
     * @param createdDate
     * @return max date
     */

    @Nullable
    private String compareDate(String updatedDate, String createdDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");

        Date date1 = null;
        try {
            date1 = format.parse(updatedDate);
            Date date2 = format.parse(createdDate);
            if (date1.compareTo(date2) <= 0) {
                System.out.println("updated Date is an earlier date");
                return createdDate;

            } else {
                return updatedDate;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    ///////////////////////**************************************************
    //////////////Download Manager methods////////////////////////////////////


    /**
     * This method replaces remote path to local path
     *
     * @param fileInfo object which contains info about path,tablename,id,isaudio
     */

    private void saveFileStatus(FileInfo fileInfo) {
        //checking which table to update
        if (fileInfo.getTableName().equals(Constants.TABLE_EXERCISE_QUESTION)) {
            final Dao<ExerciseQuestions, Integer> getexercisequestionsDao = new DatabaseManager(context).getHelper().getexercisequestionsDao();
            UpdateBuilder<ExerciseQuestions, Integer> updateBuilder = getexercisequestionsDao.updateBuilder();
            try {
                //check if it is audio or not
                if (!fileInfo.getIsAudio())
                    updateBuilder.updateColumnValue(Constants.COLUMN_EXERCISEQUESTION_IMAGE_PATH, fileInfo.getPath());
                else
                    updateBuilder.updateColumnValue(Constants.COLUMN_EXERCISEQUESTION_SOUND_PATH, fileInfo.getPath());
                updateBuilder.where().eq(Constants.COLUMN_EXERCISE_QUESTION_ID, fileInfo.getDbId());
                getexercisequestionsDao.update(updateBuilder.prepare());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (fileInfo.getTableName().equals(Constants.TABLE_EXERCISE_ANSWER)) {
            final Dao<ExerciseAnswers, Integer> getexerciseanswersDao = new DatabaseManager(context).getHelper().getexerciseanswersDao();
            UpdateBuilder<ExerciseAnswers, Integer> updateBuilder = getexerciseanswersDao.updateBuilder();
            try {

                if (!fileInfo.getIsAudio())
                    updateBuilder.updateColumnValue(Constants.COLUMN_EXERCISEANSWER_IMAGE_PATH, fileInfo.getPath());
                else
                    updateBuilder.updateColumnValue(Constants.COLUMN_EXERCISEANSWER_SOUND_PATH, fileInfo.getPath());
                updateBuilder.where().eq("Id", fileInfo.getDbId());
                getexerciseanswersDao.update(updateBuilder.prepare());
            } catch (SQLException e) {
                e.printStackTrace();
            }

        } else if (fileInfo.getTableName().equals(Constants.TABLE_EXERCISE)) {
            final Dao<Exercises, Integer> getexercisesDao = databaseManager.getHelper().getexercisesDao();
            UpdateBuilder<Exercises, Integer> updateBuilder = getexercisesDao.updateBuilder();
            try {

                if (fileInfo.getIsAudio())
                    updateBuilder.updateColumnValue(Constants.COLUMN_EXERCISE_INSTRUCTIONSOUND_PATH, fileInfo.getPath());

                updateBuilder.where().eq("Id", fileInfo.getDbId());
                getexercisesDao.update(updateBuilder.prepare());
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }

    /**
     * This methods calls methods of downloading media Files from Exercises,Exercise Questions and Answers.
     */

    public void checkAllFilesPathAndDownloadifNotThere() {
        //for images 0 and 1 for sounds
        Log.d(TAG, "checkAllFilesPathAndDownloadifNotThere: ");
        new Thread(new Runnable() {
            @Override
            public void run() {
                downloadExerciseQuestionsMediaFilesIfNotPresent(0);

            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {

                downloadExerciseQuestionsMediaFilesIfNotPresent(1);
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                downloadExerciseAnswersMediaFilesIfNotPresent(0);

            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {

                downloadExerciseAnswersMediaFilesIfNotPresent(1);
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                downloadExerciseMediaFilesIfNotPresent(1);
            }
        }).start();


    }

    /**
     * Download All Exercise Media Files by requesting to server path saved in local db.
     *
     * @param filetype 0 for Image and 1 for Audio
     */

    private void downloadExerciseMediaFilesIfNotPresent(int filetype) {
        final Dao<Exercises, Integer> exercisesDao = databaseManager.getHelper().getexercisesDao();

        try {
            String remoteImagePath;
            ArrayList<Exercises> exerciseList = new ArrayList(exercisesDao.queryBuilder().query());
            if (exerciseList.size() != 0) {
                for (int i = 0; i < exerciseList.size(); i++) {
                    Exercises exercise = exerciseList.get(i);

                    remoteImagePath = exercise.getInstructionSoundPath();
                    if (remoteImagePath != null && remoteImagePath.startsWith(Constants.REMOTEPATH_PREFIX)) {

                        String filename = remoteImagePath.substring(remoteImagePath.lastIndexOf("/") + 1);
                        String extension = filename.substring(filename.lastIndexOf("."));

                        FileInfo f = new FileInfo();
                        f.setTableName(Constants.TABLE_EXERCISE);
                        f.setDbId(exercise.getId());

                        if (filetype == 0) {
                            f.setIsAudio(false);
                        } else {
                            f.setIsAudio(true);
                            filename = exercise.getId() + "-" + "InstructionAudio" + extension;
                        }
                        String pathWithFileName = getExerciseLocalPath(exercise) + filename;
                        f.setPath(pathWithFileName);
                        File file = new File(pathWithFileName);
                        if (file.exists()) {
                            Log.i("fileexist", "downloadExerciseQuestionsMediaFilesIfNotPresent: " + pathWithFileName);
                            // saveFileStatus(f);
                            file.delete();

                        }
                        addDMRequest(remoteImagePath, f);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * download Exercise Questions Media Files
     *
     * @param filetype 0 for Image and 1 for Audio
     */
    private void downloadExerciseQuestionsMediaFilesIfNotPresent(int filetype) {
        final Dao<ExerciseQuestions, Integer> exercisesQuesDao = databaseManager.getHelper().getexercisequestionsDao();
        try {
            String remoteImagePath;
            ArrayList<ExerciseQuestions> exerciseQuestionsList = new ArrayList(exercisesQuesDao.queryBuilder().query());
            if (exerciseQuestionsList.size() != 0) {
                for (int i = 0; i < exerciseQuestionsList.size(); i++) {
                    ExerciseQuestions exerciseQuestion = exerciseQuestionsList.get(i);
                    if (filetype == 0)
                        remoteImagePath = exerciseQuestion.getImagePath();
                    else
                        remoteImagePath = exerciseQuestion.getAudioPath();
                    if (remoteImagePath != null && remoteImagePath.startsWith(Constants.REMOTEPATH_PREFIX)) {

                        String filename = remoteImagePath.substring(remoteImagePath.lastIndexOf("/") + 1);
                        String extension = filename.substring(filename.lastIndexOf("."));
                        FileInfo f = new FileInfo();
                        f.setTableName(Constants.TABLE_EXERCISE_QUESTION);
                        f.setDbId(exerciseQuestion.getId());

                        if (filetype == 0) {
                            f.setIsAudio(false);

                            filename = exerciseQuestion.getId() + "-" + "ExQuesImage" + extension;
                        } else {
                            f.setIsAudio(true);
                            filename = exerciseQuestion.getId() + "-" + "ExQuesAudio" + extension;

                        }
                        String pathWithFileName = getExerciseLocalPath(exerciseQuestion.getExercise()) + filename;
                        f.setPath(pathWithFileName);
                        File file = new File(pathWithFileName);
                        if (file.exists()) {
                            Log.i("fileexist", "downloadExerciseQuestionsMediaFilesIfNotPresent: " + pathWithFileName);
                            file.delete();

                        }
                        addDMRequest(remoteImagePath, f);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * download Exercise Answers Media Files
     *
     * @param filetype 0 for Image and 1 for Audio
     */
    private void downloadExerciseAnswersMediaFilesIfNotPresent(int filetype) {
        final Dao<ExerciseAnswers, Integer> ExerciseAnswersDao = databaseManager.getHelper().getexerciseanswersDao();

        try {
            String remoteImagePath;
            ArrayList<ExerciseAnswers> ExerciseAnswersList = new ArrayList(ExerciseAnswersDao.queryBuilder().query());
            if (ExerciseAnswersList.size() != 0) {
                for (int i = 0; i < ExerciseAnswersList.size(); i++) {
                    ExerciseAnswers exerciseAnswers = ExerciseAnswersList.get(i);
                    if (filetype == 0)
                        remoteImagePath = exerciseAnswers.getImagePath();
                    else
                        remoteImagePath = exerciseAnswers.getAudioPath();
                    if (remoteImagePath != null && remoteImagePath.startsWith(Constants.REMOTEPATH_PREFIX)) {

                        String filename = remoteImagePath.substring(remoteImagePath.lastIndexOf("/") + 1);
                        String extension = filename.substring(filename.lastIndexOf("."));
                        FileInfo f = new FileInfo();
                        f.setTableName(Constants.TABLE_EXERCISE_ANSWER);
                        f.setDbId(exerciseAnswers.getId());

                        if (filetype == 0) {
                            f.setIsAudio(false);
                            filename = exerciseAnswers.getId() + "-" + "ExAnswerImage" + extension;
                        } else {
                            f.setIsAudio(true);
                            filename = exerciseAnswers.getId() + "-" + "ExAnswerAudio" + extension;

                        }
                        String pathWithFileName = getExerciseLocalPath(exerciseAnswers.getExerciseQuestions().getExercise()) + filename;
                        f.setPath(pathWithFileName);
                        File file = new File(pathWithFileName);
                        if (file.exists()) {
                            Log.i("fileexist", "downloadExerciseAnswersMediaFilesIfNotPresent: " + pathWithFileName);
                            file.delete();

                        }
                        addDMRequest(remoteImagePath, f);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    HashMap<Long, FileInfo> downloadIds = new HashMap<>();

    /**
     * It requests download to the download manager
     *
     * @param serverPath download link
     * @param fileInfo   it contains file info to be downloaded
     */

    private void addDMRequest(String serverPath, final FileInfo fileInfo) {
        String fullpath = GlobalHelper.DOWNLOAD_URL + serverPath;
        fullpath = fullpath.replaceAll(" ", "%20");
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(fullpath));
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        String filename = serverPath.substring(serverPath.lastIndexOf("/") + 1);
        request.setTitle("" + filename);
        request.setDescription("Downloading");
        // we just want to download silently
        request.setVisibleInDownloadsUi(false);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
        Uri destinationUri = Uri.fromFile(new File(fileInfo.getPath()));
        request.setDestinationUri(destinationUri);
        Log.d(TAG, "addDMRequest() called with:" + GlobalHelper.DOWNLOAD_URL + " serverPath = [" + serverPath + "] uri:" + fullpath);
        // enqueue this request
        final DownloadManager downloadManager = (DownloadManager) context.getSystemService(context.DOWNLOAD_SERVICE);

        // if (!checkIfFileCurrentlyInDownloadQueue(downloadManager, fileInfo.getPath())) {
        final long dId = downloadManager.enqueue(request);
        downloadIds.put(dId, fileInfo);
       /* } else {
            Log.d(TAG, "addDMRequest: Path already in queue");
        }*/
        /*new Thread(new Runnable() {

            @Override
            public void run() {
                Boolean isDownloading=true;
                FileInfo f=fileInfo;
                try {
                    while (isDownloading) {

                        DownloadManager.Query q = new DownloadManager.Query();
                        //set the query filter to our previously Enqueued download
                        q.setFilterById(dId);
                        //Query the download manager about downloads that have been requested.
                        Cursor cursor = downloadManager.query(q);
                        cursor.moveToFirst();
                        int bytes_downloaded = cursor.getInt(cursor
                                .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                        int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                        if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                            //downloading = false;
                            saveFileStatus(f);
                            isDownloading=false;
                        }

                        //  dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);

                        if(!statusMessage(cursor).equals("Download in progress!") )
                            Log.d("Download", statusMessage(cursor)+dId);
                        //if(statusMessage(cursor).equals("Download failed!"))

                        cursor.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();*/

    }

    /**
     * BroadcastReceiver to check Download complete
     * It checks if download successful and it has hashmap value of fileinfo then save local path.
     */

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //check if the broadcast message is for our enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            DownloadManager mgr = (DownloadManager)
                    context.getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(referenceId);
            Cursor cur = mgr.query(query);
            int status = 0;
            if (cur.moveToFirst()) {
                int col = cur.getColumnIndex(
                        DownloadManager.COLUMN_LOCAL_URI);
                int index = cur.getColumnIndex(DownloadManager.COLUMN_STATUS);
                if (index != -1)
                    status = cur.getInt(index);
                if (cur.getInt(index) == DownloadManager.STATUS_SUCCESSFUL) {

//                    Log.d(TAG, "DownloadonReceive: " + fileInfo.getPath());

                    //**************
                    Uri uri = Uri.parse(cur.getString(col));
                    String TAG = "Path";

                    File file = new File("" + uri);
                    String fileName = file.getName();

                    String id = "";
                    if (uri.getPath().contains("/dm/")) {
                        // if downloaded file path contain dm
                        Log.d(TAG, " path from uri:" + uri.getPath());
                        Log.d(TAG, "onReceive: " + cur.getString(col));
                        //removing Extension
                        String fileNameWithOutExt = fileName.replaceFirst("[.][^.]+$", "");
                        String[] splitFileName = fileNameWithOutExt.split("-");
                        //Splitting file name and getting id on first index
                        id = splitFileName[0];
                        Log.d(TAG, " contains dm:" + uri.getPath().contains("/dm/"));
                        // This checks if file does not contain integer in the end of name it means if it is duplicate
                        if (Character.isDigit(fileNameWithOutExt.charAt(fileNameWithOutExt.length() - 1))) {
                            // deletes the duplicate file
                            Boolean b = new File(uri.getPath()).delete();
                            Log.d(TAG, "onReceive: deleteFile:" + b + " " + fileNameWithOutExt);
                        } else {

                            FileInfo fileInfo = new FileInfo();
                            fileInfo.setPath(uri.getPath());
                            fileInfo.setDbId(Integer.parseInt(id));
                            if (splitFileName[1].startsWith("ExQues"))
                                fileInfo.setTableName(Constants.TABLE_EXERCISE_QUESTION);
                            else if (splitFileName[1].startsWith("ExAns"))
                                fileInfo.setTableName(Constants.TABLE_EXERCISE_ANSWER);
                            else if (splitFileName[1].startsWith("Instr"))
                                fileInfo.setTableName(Constants.TABLE_EXERCISE);

                            if (splitFileName[1].contains("Audio"))
                                fileInfo.setIsAudio(true);
                            else
                                fileInfo.setIsAudio(false);
                            saveFileStatus(fileInfo);
                            Log.d(TAG, "onReceive: unique");
                        }
                    }
                    //****************
                  /*  if (fileInfo != null) {

                        Uri u = Uri.parse(cur.getString(col));

                        File file = new File("" + u);
                        String fileName = file.getName();
                        String fileNameWithOutExt = fileName.replaceFirst("[.][^.]+$", "");
                        if (Character.isDigit(fileNameWithOutExt.charAt(fileNameWithOutExt.length()-1)) ) {
                            File fileTobeDeleted = new File(u.getPath());
                            Boolean b = fileTobeDeleted.delete();
                            Log.d(TAG, "onReceive: path to be deleted:" + fileNameWithOutExt + " deleted:" + b);
                        } else {
                            saveFileStatus(fileInfo);
                        }

                    }
                    else {
                        Log.d(TAG, "DownloadonReceiveWithoutFileInfo: ");
                    }*/
                } else {
                    int reason = cur.getInt(cur.getColumnIndex(DownloadManager.COLUMN_REASON));
                    Log.d(TAG, "Download not correct, status [" + status + "] reason [" + reason + "]");

                }
            }
            cur.close();

        }
    };

    public void unregReceiver() {
        if (downloadReceiver != null)
            context.unregisterReceiver(downloadReceiver);
    }

}
