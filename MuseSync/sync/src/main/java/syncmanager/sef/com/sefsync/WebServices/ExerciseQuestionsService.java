package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.ExerciseQuestions;


public interface ExerciseQuestionsService {
    @GET("/ExerciseQuestions")
    public void getExerciseQuestions(Callback<List<ExerciseQuestions>> callback);

    //Get Order record base on ID
    @GET("/ExerciseQuestions/{id}")
    public void getExerciseQuestionsById(@Path("id") Integer id, Callback<ExerciseQuestions> callback);

    //Delete Order record base on ID
    @DELETE("/ExerciseQuestions/{id}")
    public void deleteExerciseQuestionsById(@Path("id") Integer id, Callback<ExerciseQuestions> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/ExerciseQuestions/{id}")
    public void updateExerciseQuestionsById(@Path("id") Integer id, @Body ExerciseQuestions exercisequestions, Callback<ExerciseQuestions> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/ExerciseQuestions")
    public void addExerciseQuestions(@Body ExerciseQuestions exercisequestions, Callback<ExerciseQuestions> callback);

}
