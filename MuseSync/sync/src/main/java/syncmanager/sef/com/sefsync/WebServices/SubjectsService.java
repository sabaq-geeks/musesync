package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import syncmanager.sef.com.sefsync.DatabaseModels.Subjects;


public interface SubjectsService {
    @GET("/subjectsapi/getdetailedsubjects")
    public void getSubjects(Callback<List<Subjects>> callback);

    @GET("/Subjectsapi/getdetailedsubjects")
    public void getSubjectsByDate(@Query("Date") String updatedDate, Callback<List<Subjects>> callback);

    //Get Order record base on ID
    @GET("/Subjects/{id}")
    public void getSubjectsById(@Path("id") Integer id, Callback<Subjects> callback);

    //Delete Order record base on ID
    @DELETE("/Subjects/{id}")
    public void deleteSubjectsById(@Path("id") Integer id, Callback<Subjects> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Subjects/{id}")
    public void updateSubjectsById(@Path("id") Integer id, @Body Subjects subjects, Callback<Subjects> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Subjects")
    public void addSubjects(@Body Subjects subjects, Callback<Subjects> callback);

}
