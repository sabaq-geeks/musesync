package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class Quizs extends BaseModel {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "SchoolId")
    private Schools school;

    private int SchoolId;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "QuizTypeId")
    private QuizTypes quizType;

    private String QuizTypeId;

  /*  @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "GradeId")
    private Grades grade;
*/
@DatabaseField
    private int GradeId;

  /*  @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "TopicId")
    private Topics topic;*/
@DatabaseField
    private String TopicId;



    @DatabaseField( columnName = "SubjectId")
    private String subjects;

    @DatabaseField
    private String Name;

    @DatabaseField
    private int MaxMarks;

    @DatabaseField
    private int PassingMarks;

    @DatabaseField
    private String Date;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "TeacherId")
    private Teachers teacher;

    private int TeacherId;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "AcademicSessionId")
    private AcademicSessions academicSession;

    private int AcademicSessionId;

    /*public Subjects getSubjects() {
        return subjects;
    }

    public void setSubjects(Subjects subjects) {
        this.subjects = subjects;
    }
*/
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Schools getSchool() {
        return school;
    }

    public void setSchool(Schools school) {
        this.school = school;
    }

    public int getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(int schoolId) {
        SchoolId = schoolId;
    }

    public QuizTypes getQuizType() {
        return quizType;
    }

    public void setQuizType(QuizTypes quizType) {
        this.quizType = quizType;
    }

    public String getQuizTypeId() {
        return QuizTypeId;
    }

    public void setQuizTypeId(String quizTypeId) {
        QuizTypeId = quizTypeId;
    }

  /*  public Grades getGrade() {
        return grade;
    }

    public void setGrade(Grades grade) {
        this.grade = grade;
    }*/

    public int getGradeId() {
        return GradeId;
    }

    public void setGradeId(int gradeId) {
        GradeId = gradeId;
    }

   /* public Topics getTopic() {
        return topic;
    }

    public void setTopic(Topics topic) {
        this.topic = topic;
    }
*/
    public String getTopicId() {
        return TopicId;
    }

    public void setTopicId(String topicId) {
        TopicId = topicId;
    }



    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getMaxMarks() {
        return MaxMarks;
    }

    public void setMaxMarks(int maxMarks) {
        MaxMarks = maxMarks;
    }

    public int getPassingMarks() {
        return PassingMarks;
    }

    public void setPassingMarks(int passingMarks) {
        PassingMarks = passingMarks;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public Teachers getTeacher() {
        return teacher;
    }

    public void setTeacher(Teachers teacher) {
        this.teacher = teacher;
    }

    public int getTeacherId() {
        return TeacherId;
    }

    public void setTeacherId(int teacherId) {
        TeacherId = teacherId;
    }

    public AcademicSessions getAcademicSession() {
        return academicSession;
    }

    public void setAcademicSession(AcademicSessions academicSession) {
        this.academicSession = academicSession;
    }

    public int getAcademicSessionId() {
        return AcademicSessionId;
    }

    public void setAcademicSessionId(int academicSessionId) {
        AcademicSessionId = academicSessionId;
    }
}
