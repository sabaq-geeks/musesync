package syncmanager.sef.com.sefsync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Hammad Ali Khan on 6/5/2017.
 */

public class ServiceStarter extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent("syncmanager.sef.com.sefsync.MainService");
        i.setClass(context, MainService.class);
        context.startService(i);
       /* Intent i=new Intent(context,MainService.class);
        context.startService(i);*/
    }
}
