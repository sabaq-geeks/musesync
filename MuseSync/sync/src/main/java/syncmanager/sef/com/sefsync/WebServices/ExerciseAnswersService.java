package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.ExerciseAnswers;


public interface ExerciseAnswersService {
    @GET("/ExerciseAnswers")
    public void getExerciseAnswers(Callback<List<ExerciseAnswers>> callback);

    //Get Order record base on ID
    @GET("/ExerciseAnswers/{id}")
    public void getExerciseAnswersById(@Path("id") Integer id, Callback<ExerciseAnswers> callback);

    //Delete Order record base on ID
    @DELETE("/ExerciseAnswers/{id}")
    public void deleteExerciseAnswersById(@Path("id") Integer id, Callback<ExerciseAnswers> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/ExerciseAnswers/{id}")
    public void updateExerciseAnswersById(@Path("id") Integer id, @Body ExerciseAnswers exerciseanswers, Callback<ExerciseAnswers> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/ExerciseAnswers")
    public void addExerciseAnswers(@Body ExerciseAnswers exerciseanswers, Callback<ExerciseAnswers> callback);

}
