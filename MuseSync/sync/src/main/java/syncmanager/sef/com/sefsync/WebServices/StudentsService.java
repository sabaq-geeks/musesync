package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Students;


public interface StudentsService {
    @GET("/Students")
    public void getStudents(Callback<List<Students>> callback);

    //Get Order record base on ID
    @GET("/Students/{id}")
    public void getStudentsById(@Path("id") Integer id, Callback<Students> callback);

    //Delete Order record base on ID
    @DELETE("/Students/{id}")
    public void deleteStudentsById(@Path("id") Integer id, Callback<Students> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Students/{id}")
    public void updateStudentsById(@Path("id") Integer id, @Body Students students, Callback<Students> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Students")
    public void addStudents(@Body Students students, Callback<Students> callback);

}
