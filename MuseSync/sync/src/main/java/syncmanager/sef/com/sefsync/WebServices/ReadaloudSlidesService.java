package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.ReadaloudSlides;


public interface ReadaloudSlidesService {
    @GET("/ReadaloudSlides")
    public void getReadaloudSlides(Callback<List<ReadaloudSlides>> callback);

    //Get Order record base on ID
    @GET("/ReadaloudSlides/{id}")
    public void getReadaloudSlidesById(@Path("id") Integer id, Callback<ReadaloudSlides> callback);

    //Delete Order record base on ID
    @DELETE("/ReadaloudSlides/{id}")
    public void deleteReadaloudSlidesById(@Path("id") Integer id, Callback<ReadaloudSlides> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/ReadaloudSlides/{id}")
    public void updateReadaloudSlidesById(@Path("id") Integer id, @Body ReadaloudSlides readaloudslides, Callback<ReadaloudSlides> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/ReadaloudSlides")
    public void addReadaloudSlides(@Body ReadaloudSlides readaloudslides, Callback<ReadaloudSlides> callback);

}
