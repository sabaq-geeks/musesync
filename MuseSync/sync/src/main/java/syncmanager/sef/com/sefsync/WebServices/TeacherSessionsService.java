package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.TeacherSessions;


public interface TeacherSessionsService {
    @GET("/TeacherSessions")
    public void getTeacherSessions(Callback<List<TeacherSessions>> callback);

    //Get Order record base on ID
    @GET("/TeacherSessions/{id}")
    public void getTeacherSessionsById(@Path("id") Integer id, Callback<TeacherSessions> callback);

    //Delete Order record base on ID
    @DELETE("/TeacherSessions/{id}")
    public void deleteTeacherSessionsById(@Path("id") Integer id, Callback<TeacherSessions> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/TeacherSessions/{id}")
    public void updateTeacherSessionsById(@Path("id") Integer id, @Body TeacherSessions teachersessions, Callback<TeacherSessions> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/TeacherSessions")
    public void addTeacherSessions(@Body TeacherSessions teachersessions, Callback<TeacherSessions> callback);

}
