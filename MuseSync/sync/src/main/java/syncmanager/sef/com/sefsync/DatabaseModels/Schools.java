package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class Schools extends BaseModel {
    @DatabaseField
    public int RemoteId;
    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField
    private String SemisCode;

    @DatabaseField
    private String LegacyCode;

    @DatabaseField
    private String Name;

    @DatabaseField
    private String SchoolCode;

    @DatabaseField
    private String RegistrationNumber;

    @DatabaseField
    private String NtnNo;

    @DatabaseField
    private String OrganizationTypeId;

    @DatabaseField
    private String PreferredCommunication;

    @DatabaseField
    private String Address;


    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "DistrictId")
    private District districts;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "UnionCouncilId")
    private UnionCouncil unionCouncils;

    @DatabaseField
    private String Village;

    @DatabaseField
    private String Phone;

    @DatabaseField
    private String Email;

    @DatabaseField
    private String StakeholderId;

    @DatabaseField
    private String VendorAccountNumber;

    @DatabaseField
    private String ContactNo;

    @DatabaseField
    private String Fax;

    @DatabaseField
    private String Website;

    @DatabaseField
    private Boolean Registered;

    @DatabaseField
    private String BankBranchId;

    @DatabaseField
    private String AccountNo;

    @DatabaseField
    private String AccountTitle;

    @DatabaseField
    private String ProgramId;

    @DatabaseField
    private String PhaseId;

    @DatabaseField
    private String Comments;

    @DatabaseField
    private String Location;

    @DatabaseField
    private int Stakeholder_Id;

    @DatabaseField
    private String OrganizationId;

    @DatabaseField
    private String NearestFamousPlace;

    @DatabaseField
    private  String SubsidyModelId;


    @ForeignCollectionField
    private Collection<Teachers> teacherList;

    @ForeignCollectionField
    private Collection<Students> studentList;

    @ForeignCollectionField
    private Collection<AcademicSessions> academicSessionsList;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getSemisCode() {
        return SemisCode;
    }

    public void setSemisCode(String semisCode) {
        SemisCode = semisCode;
    }

    public String getLegacyCode() {
        return LegacyCode;
    }

    public void setLegacyCode(String legacyCode) {
        LegacyCode = legacyCode;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSchoolCode() {
        return SchoolCode;
    }

    public void setSchoolCode(String schoolCode) {
        SchoolCode = schoolCode;
    }

    public String getRegistrationNumber() {
        return RegistrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        RegistrationNumber = registrationNumber;
    }

    public String getNtnNo() {
        return NtnNo;
    }

    public void setNtnNo(String ntnNo) {
        NtnNo = ntnNo;
    }

    public String getOrganizationTypeId() {
        return OrganizationTypeId;
    }

    public void setOrganizationTypeId(String organizationTypeId) {
        OrganizationTypeId = organizationTypeId;
    }

    public String getPreferredCommunication() {
        return PreferredCommunication;
    }

    public void setPreferredCommunication(String preferredCommunication) {
        PreferredCommunication = preferredCommunication;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public District getDistricts() {
        return districts;
    }

    public void setDistricts(District districts) {
        this.districts = districts;
    }

    public UnionCouncil getUnionCouncils() {
        return unionCouncils;
    }

    public void setUnionCouncils(UnionCouncil unionCouncils) {
        this.unionCouncils = unionCouncils;
    }

    public String getVillage() {
        return Village;
    }

    public void setVillage(String village) {
        Village = village;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getStakeholderId() {
        return StakeholderId;
    }

    public void setStakeholderId(String stakeholderId) {
        StakeholderId = stakeholderId;
    }

    public String getVendorAccountNumber() {
        return VendorAccountNumber;
    }

    public void setVendorAccountNumber(String vendorAccountNumber) {
        VendorAccountNumber = vendorAccountNumber;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String website) {
        Website = website;
    }

    public Boolean getRegistered() {
        return Registered;
    }

    public void setRegistered(Boolean registered) {
        Registered = registered;
    }

    public String getBankBranchId() {
        return BankBranchId;
    }

    public void setBankBranchId(String bankBranchId) {
        BankBranchId = bankBranchId;
    }

    public String getAccountNo() {
        return AccountNo;
    }

    public void setAccountNo(String accountNo) {
        AccountNo = accountNo;
    }

    public String getAccountTitle() {
        return AccountTitle;
    }

    public void setAccountTitle(String accountTitle) {
        AccountTitle = accountTitle;
    }

    public String getProgramId() {
        return ProgramId;
    }

    public void setProgramId(String programId) {
        ProgramId = programId;
    }

    public String getPhaseId() {
        return PhaseId;
    }

    public void setPhaseId(String phaseId) {
        PhaseId = phaseId;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public int getStakeholder_Id() {
        return Stakeholder_Id;
    }

    public void setStakeholder_Id(int stakeholder_Id) {
        Stakeholder_Id = stakeholder_Id;
    }

    public String getOrganizationId() {
        return OrganizationId;
    }

    public void setOrganizationId(String organizationId) {
        OrganizationId = organizationId;
    }

    public String getNearestFamousPlace() {
        return NearestFamousPlace;
    }

    public void setNearestFamousPlace(String nearestFamousPlace) {
        NearestFamousPlace = nearestFamousPlace;
    }

    public String getSubsidyModelId() {
        return SubsidyModelId;
    }

    public void setSubsidyModelId(String subsidyModelId) {
        SubsidyModelId = subsidyModelId;
    }

    public Collection<Teachers> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(Collection<Teachers> teacherList) {
        this.teacherList = teacherList;
    }

    public Collection<Students> getStudentList() {
        return studentList;
    }

    public void setStudentList(Collection<Students> studentList) {
        this.studentList = studentList;
    }

    public Collection<AcademicSessions> getAcademicSessionsList() {
        return academicSessionsList;
    }

    public void setAcademicSessionsList(Collection<AcademicSessions> academicSessionsList) {
        this.academicSessionsList = academicSessionsList;
    }
}
