package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Images;


public interface ImagesService {
    @GET("/Images")
    public void getImages(Callback<List<Images>> callback);

    //Get Order record base on ID
    @GET("/Images/{id}")
    public void getImagesById(@Path("id") Integer id, Callback<Images> callback);

    //Delete Order record base on ID
    @DELETE("/Images/{id}")
    public void deleteImagesById(@Path("id") Integer id, Callback<Images> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Images/{id}")
    public void updateImagesById(@Path("id") Integer id, @Body Images images, Callback<Images> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Images")
    public void addImages(@Body Images images, Callback<Images> callback);

}
