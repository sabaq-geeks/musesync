package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.ImageGroups;


public interface ImageGroupsService {
    @GET("/ImageGroups")
    public void getImageGroups(Callback<List<ImageGroups>> callback);

    //Get Order record base on ID
    @GET("/ImageGroups/{id}")
    public void getImageGroupsById(@Path("id") Integer id, Callback<ImageGroups> callback);

    //Delete Order record base on ID
    @DELETE("/ImageGroups/{id}")
    public void deleteImageGroupsById(@Path("id") Integer id, Callback<ImageGroups> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/ImageGroups/{id}")
    public void updateImageGroupsById(@Path("id") Integer id, @Body ImageGroups imagegroups, Callback<ImageGroups> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/ImageGroups")
    public void addImageGroups(@Body ImageGroups imagegroups, Callback<ImageGroups> callback);

}
