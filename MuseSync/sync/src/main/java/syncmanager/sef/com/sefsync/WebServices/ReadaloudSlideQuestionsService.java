package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.ReadaloudSlideQuestions;


public interface ReadaloudSlideQuestionsService {
    @GET("/ReadaloudSlideQuestions")
    public void getReadaloudSlideQuestions(Callback<List<ReadaloudSlideQuestions>> callback);

    //Get Order record base on ID
    @GET("/ReadaloudSlideQuestions/{id}")
    public void getReadaloudSlideQuestionsById(@Path("id") Integer id, Callback<ReadaloudSlideQuestions> callback);

    //Delete Order record base on ID
    @DELETE("/ReadaloudSlideQuestions/{id}")
    public void deleteReadaloudSlideQuestionsById(@Path("id") Integer id, Callback<ReadaloudSlideQuestions> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/ReadaloudSlideQuestions/{id}")
    public void updateReadaloudSlideQuestionsById(@Path("id") Integer id, @Body ReadaloudSlideQuestions readaloudslidequestions, Callback<ReadaloudSlideQuestions> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/ReadaloudSlideQuestions")
    public void addReadaloudSlideQuestions(@Body ReadaloudSlideQuestions readaloudslidequestions, Callback<ReadaloudSlideQuestions> callback);

}
