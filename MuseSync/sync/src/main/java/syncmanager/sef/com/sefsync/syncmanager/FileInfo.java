package syncmanager.sef.com.sefsync.syncmanager;

/**
 * Created by Hammad Ali Khan on 5/3/2017.
 */

public class FileInfo {


    private int dbId;
    private String tableName;
    private String Path;
    private int exerciseId;
    private Boolean isAudio;

    public Boolean getIsAudio() {
        return isAudio;
    }

    public void setIsAudio(Boolean audio) {
        isAudio = audio;
    }

    public int getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(int exerciseId) {
        this.exerciseId = exerciseId;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String imagePath) {
        this.Path = imagePath;
    }

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


}
