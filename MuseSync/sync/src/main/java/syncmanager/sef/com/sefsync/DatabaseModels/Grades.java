package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class Grades extends BaseModel{
    @DatabaseField
    public int RemoteId;
    @DatabaseField(canBeNull = false, id = true)
    private String Id;

   /* @ForeignCollectionField
    private Collection<AcademicSessions> academicSessionsList;*/

//    @ForeignCollectionField
//    private Collection<Contents> contentList;



//    @ForeignCollectionField
//    private Collection<Quizs> quizsList;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    /*public Collection<AcademicSessions> getAcademicSessionsList() {
        return academicSessionsList;
    }

    public void setAcademicSessionsList(Collection<AcademicSessions> academicSessionsList) {
        this.academicSessionsList = academicSessionsList;
    }*/

   /* public Collection<Contents> getContentList() {
        return contentList;
    }

    public void setContentList(Collection<Contents> contentList) {
        this.contentList = contentList;
    }
*/
   /* public Collection<Quizs> getQuizsList() {
        return quizsList;
    }

    public void setQuizsList(Collection<Quizs> quizsList) {
        this.quizsList = quizsList;
    }*/
}
