package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/26/2017.
 */
@DatabaseTable
public class Images {

    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;


    @ForeignCollectionField
    private Collection<ContentDetails> contentDetailList;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Collection<ContentDetails> getContentDetailList() {
        return contentDetailList;
    }

    public void setContentDetailList(Collection<ContentDetails> contentDetailList) {
        this.contentDetailList = contentDetailList;
    }
}
