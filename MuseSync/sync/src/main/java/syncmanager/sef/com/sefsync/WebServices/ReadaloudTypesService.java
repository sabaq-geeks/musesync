package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.ReadaloudTypes;


public interface ReadaloudTypesService {
    @GET("/ReadaloudTypes")
    public void getReadaloudTypes(Callback<List<ReadaloudTypes>> callback);

    //Get Order record base on ID
    @GET("/ReadaloudTypes/{id}")
    public void getReadaloudTypesById(@Path("id") Integer id, Callback<ReadaloudTypes> callback);

    //Delete Order record base on ID
    @DELETE("/ReadaloudTypes/{id}")
    public void deleteReadaloudTypesById(@Path("id") Integer id, Callback<ReadaloudTypes> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/ReadaloudTypes/{id}")
    public void updateReadaloudTypesById(@Path("id") Integer id, @Body ReadaloudTypes readaloudtypes, Callback<ReadaloudTypes> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/ReadaloudTypes")
    public void addReadaloudTypes(@Body ReadaloudTypes readaloudtypes, Callback<ReadaloudTypes> callback);

}
