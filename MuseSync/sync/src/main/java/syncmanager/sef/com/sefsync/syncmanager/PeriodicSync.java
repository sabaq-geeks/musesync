package syncmanager.sef.com.sefsync.syncmanager;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import syncmanager.sef.com.sefsync.R;


public class PeriodicSync extends AppCompatActivity {


    //    public static final String AUTHORITY = "com.example.android.datasync.provider";
//    // Account
//    public static final String ACCOUNT = "dummyaccount";
//    String ACCOUNT_TYPE="example.com";
//    Account mAccount;
    // Sync interval constants
    public static final long SECONDS_PER_MINUTE = 4L;
    public static final long SYNC_INTERVAL_IN_MINUTES = 2L;
    public static final long SYNC_INTERVAL =
            SYNC_INTERVAL_IN_MINUTES *
                    SECONDS_PER_MINUTE;
    // Global variables
    // A content resolver for accessing the provider
    ContentResolver mResolver;

    //******** create account
    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "com.example.android.sefdatasync123.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "123sef.com";
    // The account name
    public static final String ACCOUNT = "sefaccount123";
    // Instance fields
    Account mAccount;

    Button createAccount,RequestSync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_periodic_sync);
        createAccount = (Button) findViewById(R.id.btnCreateAccount);
        RequestSync=(Button)findViewById(R.id.btnRequestSync);
        // this will create account - Account should be made one time
        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAccount = CreateSyncAccount(PeriodicSync.this);
            }
        });

        //********************************************************
        // Extras for running sync adapter on button click
        // Pass the settings flags by inserting them in a bundle
        final Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
           /*
         * Turn on periodic syncing
         *
         * SECONDS_PER_MINUTE: 60 SECONDS * MINUTES
         */
        mAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        ContentResolver.setSyncAutomatically(mAccount, AUTHORITY, true);
        ContentResolver.addPeriodicSync(
                mAccount,
                AUTHORITY,
                Bundle.EMPTY,
                SECONDS_PER_MINUTE);

        //This will run syncadapter on button click
        RequestSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentResolver.requestSync(mAccount,AUTHORITY,settingsBundle);
            }
        });
//********************************************************

        mResolver = getContentResolver();



    }

    /**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            Log.i("Successful:", "Add Account");
            Toast.makeText(context, "Successful", Toast.LENGTH_SHORT).show();
        } else {
            Log.i("Error", "Add Account");
            Toast.makeText(context, "The account exists or some other error occurred", Toast.LENGTH_SHORT).show();
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
        }
        return newAccount;
    }


}
