package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class Teachers {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField
    private String DateOfBirth;

    @DatabaseField
    private String Gender;

    @DatabaseField
    private String CnicNo;

    @DatabaseField
    private String Address;
    @DatabaseField
    private  String LegacyCode;

    @ForeignCollectionField
    private Collection<TeacherSessions> teacherSessionList;

    @ForeignCollectionField
    private Collection<Quizs> quizsList;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "SchoolId")
    private Schools schools;

    private int SchoolId;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "UnionCouncilId")
    private UnionCouncil unionCouncils;

    private String UnionCouncilId;

    @DatabaseField
    private String TeacherName;

    @DatabaseField
    private String FatherName;

    @DatabaseField
    private String GuardianName;

    @DatabaseField
    private String TeacherTypeId;

    @DatabaseField
    private String SubsidyCategoryId;

    @DatabaseField
    private String MaritalStatusId;

    @DatabaseField
    private String DateOfJoiningProgram;

    @DatabaseField
    private Boolean TeacherLeft;

    @DatabaseField
    private String ReasonForLeaving;

    @DatabaseField
    private int CurrentSalary;

    @DatabaseField
    private int StartingSalary;

    @DatabaseField
    private String DateOfJoining;

    @DatabaseField
    private String DesignationId;

    @DatabaseField
    private String Qualification;

    @DatabaseField
    private  String ProfessionalQualification;

    @DatabaseField
    private String TeachingExperience;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getCnicNo() {
        return CnicNo;
    }

    public void setCnicNo(String cnicNo) {
        CnicNo = cnicNo;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getLegacyCode() {
        return LegacyCode;
    }

    public void setLegacyCode(String legacyCode) {
        LegacyCode = legacyCode;
    }

    public Collection<TeacherSessions> getTeacherSessionList() {
        return teacherSessionList;
    }

    public void setTeacherSessionList(Collection<TeacherSessions> teacherSessionList) {
        this.teacherSessionList = teacherSessionList;
    }

    public Collection<Quizs> getQuizsList() {
        return quizsList;
    }

    public void setQuizsList(Collection<Quizs> quizsList) {
        this.quizsList = quizsList;
    }

    public Schools getSchools() {
        return schools;
    }

    public void setSchools(Schools schools) {
        this.schools = schools;
    }

    public int getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(int schoolId) {
        SchoolId = schoolId;
    }

    public UnionCouncil getUnionCouncils() {
        return unionCouncils;
    }

    public void setUnionCouncils(UnionCouncil unionCouncils) {
        this.unionCouncils = unionCouncils;
    }

    public String getUnionCouncilId() {
        return UnionCouncilId;
    }

    public void setUnionCouncilId(String unionCouncilId) {
        UnionCouncilId = unionCouncilId;
    }

    public String getTeacherName() {
        return TeacherName;
    }

    public void setTeacherName(String teacherName) {
        TeacherName = teacherName;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getGuardianName() {
        return GuardianName;
    }

    public void setGuardianName(String guardianName) {
        GuardianName = guardianName;
    }

    public String getTeacherTypeId() {
        return TeacherTypeId;
    }

    public void setTeacherTypeId(String teacherTypeId) {
        TeacherTypeId = teacherTypeId;
    }

    public String getSubsidyCategoryId() {
        return SubsidyCategoryId;
    }

    public void setSubsidyCategoryId(String subsidyCategoryId) {
        SubsidyCategoryId = subsidyCategoryId;
    }

    public String getMaritalStatusId() {
        return MaritalStatusId;
    }

    public void setMaritalStatusId(String maritalStatusId) {
        MaritalStatusId = maritalStatusId;
    }

    public String getDateOfJoiningProgram() {
        return DateOfJoiningProgram;
    }

    public void setDateOfJoiningProgram(String dateOfJoiningProgram) {
        DateOfJoiningProgram = dateOfJoiningProgram;
    }

    public Boolean getTeacherLeft() {
        return TeacherLeft;
    }

    public void setTeacherLeft(Boolean teacherLeft) {
        TeacherLeft = teacherLeft;
    }

    public String getReasonForLeaving() {
        return ReasonForLeaving;
    }

    public void setReasonForLeaving(String reasonForLeaving) {
        ReasonForLeaving = reasonForLeaving;
    }

    public int getCurrentSalary() {
        return CurrentSalary;
    }

    public void setCurrentSalary(int currentSalary) {
        CurrentSalary = currentSalary;
    }

    public int getStartingSalary() {
        return StartingSalary;
    }

    public void setStartingSalary(int startingSalary) {
        StartingSalary = startingSalary;
    }

    public String getDateOfJoining() {
        return DateOfJoining;
    }

    public void setDateOfJoining(String dateOfJoining) {
        DateOfJoining = dateOfJoining;
    }

    public String getDesignationId() {
        return DesignationId;
    }

    public void setDesignationId(String designationId) {
        DesignationId = designationId;
    }

    public String getQualification() {
        return Qualification;
    }

    public void setQualification(String qualification) {
        Qualification = qualification;
    }

    public String getProfessionalQualification() {
        return ProfessionalQualification;
    }

    public void setProfessionalQualification(String professionalQualification) {
        ProfessionalQualification = professionalQualification;
    }

    public String getTeachingExperience() {
        return TeachingExperience;
    }

    public void setTeachingExperience(String teachingExperience) {
        TeachingExperience = teachingExperience;
    }
}
