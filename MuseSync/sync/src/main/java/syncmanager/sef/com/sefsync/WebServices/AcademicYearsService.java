package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.AcademicYears;


public interface AcademicYearsService {
    @GET("/AcademicYears")
    public void getAcademicYears(Callback<List<AcademicYears>> callback);

    //Get Order record base on ID
    @GET("/AcademicYears/{id}")
    public void getAcademicYearsById(@Path("id") Integer id, Callback<AcademicYears> callback);

    //Delete Order record base on ID
    @DELETE("/AcademicYears/{id}")
    public void deleteAcademicYearsById(@Path("id") Integer id, Callback<AcademicYears> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/AcademicYears/{id}")
    public void updateAcademicYearsById(@Path("id") Integer id, @Body AcademicYears academicyears, Callback<AcademicYears> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/AcademicYears")
    public void addAcademicYears(@Body AcademicYears academicyears, Callback<AcademicYears> callback);

}
