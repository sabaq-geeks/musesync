package syncmanager.sef.com.sefsync.WebServices;



/**
 * Created by Kashir on 2/3/2017.
 */

public class RestService {
    private static final String URL = "http://103.31.80.102:2019/api/";
    private static final String URLCustomService = "http://103.31.80.102:2019/";

    private retrofit.RestAdapter restAdapter;
    /*private UserVehicleService apiService;
    private UserVehicleService apiService1;*/

    public RestService()
    {
        restAdapter = new retrofit.RestAdapter.Builder()
                .setEndpoint(URL)
             /*       .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)*/
                .build();
    }

    public RestService(Boolean Custom)
    {
        restAdapter = new retrofit.RestAdapter.Builder()
                .setEndpoint(URLCustomService)
                .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)
                .build();
    }

    public  ExercisesService getUserVehicleService()
    {
       return restAdapter.create(ExercisesService.class);
    }


}
