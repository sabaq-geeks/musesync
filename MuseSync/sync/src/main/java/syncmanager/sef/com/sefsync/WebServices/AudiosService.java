package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Audios;


public interface AudiosService {
    @GET("/Audios")
    public void getAudios(Callback<List<Audios>> callback);

    //Get Order record base on ID
    @GET("/Audios/{id}")
    public void getAudiosById(@Path("id") Integer id, Callback<Audios> callback);

    //Delete Order record base on ID
    @DELETE("/Audios/{id}")
    public void deleteAudiosById(@Path("id") Integer id, Callback<Audios> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Audios/{id}")
    public void updateAudiosById(@Path("id") Integer id, @Body Audios audios, Callback<Audios> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Audios")
    public void addAudios(@Body Audios audios, Callback<Audios> callback);

}
