package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.ContentDetails;


public interface ContentDetailsService {
    @GET("/ContentDetails")
    public void getContentDetails(Callback<List<ContentDetails>> callback);

    //Get Order record base on ID
    @GET("/ContentDetails/{id}")
    public void getContentDetailsById(@Path("id") Integer id, Callback<ContentDetails> callback);

    //Delete Order record base on ID
    @DELETE("/ContentDetails/{id}")
    public void deleteContentDetailsById(@Path("id") Integer id, Callback<ContentDetails> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/ContentDetails/{id}")
    public void updateContentDetailsById(@Path("id") Integer id, @Body ContentDetails contentdetails, Callback<ContentDetails> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/ContentDetails")
    public void addContentDetails(@Body ContentDetails contentdetails, Callback<ContentDetails> callback);

}
