package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

import javax.security.auth.Subject;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class Exercises extends BaseModel{
    @DatabaseField
    public int RemoteId;
    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField(columnName = "GradeId")
    private String GradeId;

    @DatabaseField
    private String InstructionSoundPath;

    @DatabaseField
    private int TotalMarks;

    @DatabaseField
    private int Timer;
   /* @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "SubjectId")
    private Subjects subjects;*/

    @DatabaseField
    private String SubjectId;

    /* @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "TopicId")
     private Topics topics;*/
    @DatabaseField
    private int TopicId;

    @DatabaseField
    private String UrduTitle;

    @DatabaseField
    private String SindhiTitle;




   /* @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "SubTopicId")
    private SubTopics subTopics;*/

//    private String SubTopicId;
/*
    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "ExerciseTypeId")
    private ExerciseTypes exerciseTypes;*/

    @DatabaseField
    private String ExerciseTypeId;

    @DatabaseField
    private String ExerciseTitle;

    @ForeignCollectionField
    private Collection<ExerciseQuestions> ExerciseQuestions;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }



    public String getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

   /* public Topics getTopics() {
        return topics;
    }

    public void setTopics(Topics topics) {
        this.topics = topics;
    }*/

    public int getTopicId() {
        return TopicId;
    }

    public void setTopicId(int topicId) {
        TopicId = topicId;
    }

/*

    public ExerciseTypes getExerciseTypes() {
        return exerciseTypes;
    }

    public void setExerciseTypes(ExerciseTypes exerciseTypes) {
        this.exerciseTypes = exerciseTypes;
    }
*/

    public String getExerciseTypeId() {
        return ExerciseTypeId;
    }

    public void setExerciseTypeId(String exerciseTypeId) {
        ExerciseTypeId = exerciseTypeId;
    }

    public String getExerciseTitle() {
        return ExerciseTitle;
    }

    public void setExerciseTitle(String exerciseTitle) {
        ExerciseTitle = exerciseTitle;
    }

    public Collection<ExerciseQuestions> getExerciseQuestionList() {
        return ExerciseQuestions;
    }

    public void setExerciseQuestionList(Collection<ExerciseQuestions> ExerciseQuestions) {
        this.ExerciseQuestions = ExerciseQuestions;
    }

    public int getRemoteId() {
        return RemoteId;
    }

    public void setRemoteId(int remoteId) {
        RemoteId = remoteId;
    }

    public String getGradeId() {
        return GradeId;
    }

    public void setGradeId(String gradeId) {
        GradeId = gradeId;
    }

    public String getInstructionSoundPath() {
        return InstructionSoundPath;
    }

    public void setInstructionSoundPath(String instructionSoundPath) {
        InstructionSoundPath = instructionSoundPath;
    }

    public int getTotalMarks() {
        return TotalMarks;
    }

    public void setTotalMarks(int totalMarks) {
        TotalMarks = totalMarks;
    }

    public int getTimer() {
        return Timer;
    }

    public void setTimer(int timer) {
        Timer = timer;
    }

    public String getUrduTitle() {
        return UrduTitle;
    }

    public void setUrduTitle(String urduTitle) {
        UrduTitle = urduTitle;
    }

    public String getSindhiTitle() {
        return SindhiTitle;
    }

    public void setSindhiTitle(String sindhiTitle) {
        SindhiTitle = sindhiTitle;
    }

    public Collection<syncmanager.sef.com.sefsync.DatabaseModels.ExerciseQuestions> getExerciseQuestions() {
        return ExerciseQuestions;
    }

    public void setExerciseQuestions(Collection<syncmanager.sef.com.sefsync.DatabaseModels.ExerciseQuestions> exerciseQuestions) {
        ExerciseQuestions = exerciseQuestions;
    }
}
