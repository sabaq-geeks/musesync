package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */

public class BaseModel {

    @DatabaseField
    public String CreatedDate;

    @DatabaseField
    public String CreatedBy;

    @DatabaseField
    public String UpdatedDate;

    @DatabaseField
    public String UpdatedBy;

    @DatabaseField
    public Boolean Active;


    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        UpdatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        UpdatedBy = updatedBy;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }
}
