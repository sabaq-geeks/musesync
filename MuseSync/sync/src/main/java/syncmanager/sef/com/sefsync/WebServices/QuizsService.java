package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Quizs;


public interface QuizsService {
    @GET("/Quizs")
    public void getQuizs(Callback<List<Quizs>> callback);

    //Get Order record base on ID
    @GET("/Quizs/{id}")
    public void getQuizsById(@Path("id") Integer id, Callback<Quizs> callback);

    //Delete Order record base on ID
    @DELETE("/Quizs/{id}")
    public void deleteQuizsById(@Path("id") Integer id, Callback<Quizs> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Quizs/{id}")
    public void updateQuizsById(@Path("id") Integer id, @Body Quizs quizs, Callback<Quizs> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Quizs")
    public void addQuizs(@Body Quizs quizs, Callback<Quizs> callback);

}
