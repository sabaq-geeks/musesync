package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Grades;


public interface GradesService {
    @GET("/Gradesapi/getdetailedgrades")
    public void getGrades(Callback<List<Grades>> callback);

    //Get Order record base on ID
    @GET("/Grades/{id}")
    public void getGradesById(@Path("id") Integer id, Callback<Grades> callback);

    //Delete Order record base on ID
    @DELETE("/Grades/{id}")
    public void deleteGradesById(@Path("id") Integer id, Callback<Grades> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Grades/{id}")
    public void updateGradesById(@Path("id") Integer id, @Body Grades grades, Callback<Grades> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Grades")
    public void addGrades(@Body Grades grades, Callback<Grades> callback);

}
