package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class ContentTags  {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "Content_Id")
    private Contents contents;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "Tag_Id")
    private Tags tags;

    public String Tag_Id;

    public int Content_Id;


    public Contents getContents() {
        return contents;
    }

    public int getRemoteId() {
        return RemoteId;
    }

    public void setRemoteId(int remoteId) {
        RemoteId = remoteId;
    }

    public void setContents(Contents contents) {
        this.contents = contents;
    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }

    public String getTag_Id() {
        return Tag_Id;
    }

    public void setTag_Id(String tag_Id) {
        Tag_Id = tag_Id;
    }

    public int getContent_Id() {
        return Content_Id;
    }

    public void setContent_Id(int content_Id) {
        Content_Id = content_Id;
    }
}
