package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Topics;


public interface TopicsService {
    @GET("/Topicsapi/getdetailedtopics")
    public void getTopics(Callback<List<Topics>> callback);

    //Get Order record base on ID
    @GET("/Topics/{id}")
    public void getTopicsById(@Path("id") Integer id, Callback<Topics> callback);

    //Delete Order record base on ID
    @DELETE("/Topics/{id}")
    public void deleteTopicsById(@Path("id") Integer id, Callback<Topics> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Topics/{id}")
    public void updateTopicsById(@Path("id") Integer id, @Body Topics topics, Callback<Topics> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Topics")
    public void addTopics(@Body Topics topics, Callback<Topics> callback);

}
