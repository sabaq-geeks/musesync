package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Teachers;


public interface TeachersService {
    @GET("/Teachers")
    public void getTeachers(Callback<List<Teachers>> callback);

    //Get Order record base on ID
    @GET("/Teachers/{id}")
    public void getTeachersById(@Path("id") Integer id, Callback<Teachers> callback);

    //Delete Order record base on ID
    @DELETE("/Teachers/{id}")
    public void deleteTeachersById(@Path("id") Integer id, Callback<Teachers> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Teachers/{id}")
    public void updateTeachersById(@Path("id") Integer id, @Body Teachers teachers, Callback<Teachers> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Teachers")
    public void addTeachers(@Body Teachers teachers, Callback<Teachers> callback);

}
