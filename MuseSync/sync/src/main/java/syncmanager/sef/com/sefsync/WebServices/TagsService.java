package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Tags;


public interface TagsService {
    @GET("/Tags")
    public void getTags(Callback<List<Tags>> callback);

    //Get Order record base on ID
    @GET("/Tags/{id}")
    public void getTagsById(@Path("id") Integer id, Callback<Tags> callback);

    //Delete Order record base on ID
    @DELETE("/Tags/{id}")
    public void deleteTagsById(@Path("id") Integer id, Callback<Tags> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Tags/{id}")
    public void updateTagsById(@Path("id") Integer id, @Body Tags tags, Callback<Tags> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Tags")
    public void addTags(@Body Tags tags, Callback<Tags> callback);

}
