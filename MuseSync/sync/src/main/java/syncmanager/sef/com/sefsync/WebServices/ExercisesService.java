package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import syncmanager.sef.com.sefsync.DatabaseModels.Exercises;


public interface ExercisesService {
    @GET("/exercisesapi/getdetailedexercises")
    public void getExercises(Callback<List<Exercises>> callback);

    @GET("/exercisesapi/getdetailedexercises")
    public void getExercisesByDate( @Query("Date") String updatedDate,Callback<List<Exercises>> callback);

    //Get Order record base on ID
    @GET("/Exercises/{id}")
    public void getExercisesById(@Path("id") Integer id, Callback<Exercises> callback);

    //Delete Order record base on ID
    @DELETE("/Exercises/{id}")
    public void deleteExercisesById(@Path("id") Integer id, Callback<Exercises> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Exercises/{id}")
    public void updateExercisesById(@Path("id") Integer id, @Body Exercises exercises, Callback<Exercises> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Exercises")
    public void addExercises(@Body Exercises exercises, Callback<Exercises> callback);

}
