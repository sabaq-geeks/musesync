package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class Topics extends BaseModel{

    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField
    public String TopicName;

    @DatabaseField
    public String GradeId;

    @DatabaseField
    public String SubjectId;

    @DatabaseField
    public String IconPath;

    @DatabaseField
    public int Order;



    /*@ForeignCollectionField
    private Collection<SubTopics> subTopicList;
*/
 /*   @ForeignCollectionField
    private Collection<Contents> contentList;*/

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

   /* public Collection<SubTopics> getSubTopicList() {
        return subTopicList;
    }

    public void setSubTopicList(Collection<SubTopics> subTopicList) {
        this.subTopicList = subTopicList;
    }*/

//    public Collection<Contents> getContentList() {
//        return contentList;
//    }
//
//    public void setContentList(Collection<Contents> contentList) {
//        this.contentList = contentList;
//    }
}
