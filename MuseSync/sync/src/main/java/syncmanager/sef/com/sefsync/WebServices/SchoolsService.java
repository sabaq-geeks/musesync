package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Schools;


public interface SchoolsService {
    @GET("/Schools")
    public void getSchools(Callback<List<Schools>> callback);

    //Get Order record base on ID
    @GET("/Schools/{id}")
    public void getSchoolsById(@Path("id") Integer id, Callback<Schools> callback);

    //Delete Order record base on ID
    @DELETE("/Schools/{id}")
    public void deleteSchoolsById(@Path("id") Integer id, Callback<Schools> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Schools/{id}")
    public void updateSchoolsById(@Path("id") Integer id, @Body Schools schools, Callback<Schools> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Schools")
    public void addSchools(@Body Schools schools, Callback<Schools> callback);

}
