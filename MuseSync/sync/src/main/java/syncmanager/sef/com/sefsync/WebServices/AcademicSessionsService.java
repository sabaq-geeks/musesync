package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.AcademicSessions;


public interface AcademicSessionsService {
    @GET("/AcademicSessions")
    public void getAcademicSessions(Callback<List<AcademicSessions>> callback);

    //Get Order record base on ID
    @GET("/AcademicSessions/{id}")
    public void getAcademicSessionsById(@Path("id") Integer id, Callback<AcademicSessions> callback);

    //Delete Order record base on ID
    @DELETE("/AcademicSessions/{id}")
    public void deleteAcademicSessionsById(@Path("id") Integer id, Callback<AcademicSessions> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/AcademicSessions/{id}")
    public void updateAcademicSessionsById(@Path("id") Integer id, @Body AcademicSessions academicsessions, Callback<AcademicSessions> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/AcademicSessions")
    public void addAcademicSessions(@Body AcademicSessions academicsessions, Callback<AcademicSessions> callback);

}
