package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Ishaq Ahmed Khan on 4/20/2017.
 */
@DatabaseTable
public class District extends BaseModel {
    @DatabaseField
    public int RemoteId;

    @ForeignCollectionField
    private Collection<TalukaTehsil> TalukaTehsil;

    @DatabaseField(canBeNull = false, id = true)
    public String Id;

    private String ProvinceId;

    public Collection<TalukaTehsil> getTalukaTehsil() {
        return TalukaTehsil;
    }

    public void setTalukaTehsil(Collection<TalukaTehsil> talukaTehsil) {
        TalukaTehsil = talukaTehsil;
    }

    public int getRemoteId() {
        return RemoteId;
    }

    public void setRemoteId(int remoteId) {
        RemoteId = remoteId;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getProvinceId() {
        return ProvinceId;
    }

    public void setProvinceId(String provinceId) {
        ProvinceId = provinceId;
    }
}
