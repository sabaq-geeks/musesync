package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.QuizTypes;


public interface QuizTypesService {
    @GET("/QuizTypes")
    public void getQuizTypes(Callback<List<QuizTypes>> callback);

    //Get Order record base on ID
    @GET("/QuizTypes/{id}")
    public void getQuizTypesById(@Path("id") Integer id, Callback<QuizTypes> callback);

    //Delete Order record base on ID
    @DELETE("/QuizTypes/{id}")
    public void deleteQuizTypesById(@Path("id") Integer id, Callback<QuizTypes> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/QuizTypes/{id}")
    public void updateQuizTypesById(@Path("id") Integer id, @Body QuizTypes quiztypes, Callback<QuizTypes> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/QuizTypes")
    public void addQuizTypes(@Body QuizTypes quiztypes, Callback<QuizTypes> callback);

}
