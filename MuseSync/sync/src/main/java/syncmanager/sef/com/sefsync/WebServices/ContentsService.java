package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Contents;


public interface ContentsService {
    @GET("/Contents")
    public void getContents(Callback<List<Contents>> callback);

    //Get Order record base on ID
    @GET("/Contents/{id}")
    public void getContentsById(@Path("id") Integer id, Callback<Contents> callback);

    //Delete Order record base on ID
    @DELETE("/Contents/{id}")
    public void deleteContentsById(@Path("id") Integer id, Callback<Contents> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Contents/{id}")
    public void updateContentsById(@Path("id") Integer id, @Body Contents contents, Callback<Contents> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Contents")
    public void addContents(@Body Contents contents, Callback<Contents> callback);

}
