package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Ishaq Ahmed Khan on 4/20/2017.
 */
@DatabaseTable
public class TalukaTehsil extends BaseModel {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "DistrictId")
    private District District;

    @DatabaseField(canBeNull = false, id = true)
    public String Id;

    @ForeignCollectionField
    private Collection<UnionCouncil> UnionCouncil;

    public District getDistrict() {
        return District;
    }

    public void setDistrict(District district) {
        District = district;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public Collection<UnionCouncil> getUnionCouncil() {
        return UnionCouncil;
    }

    public void setUnionCouncil(Collection<UnionCouncil> unionCouncil) {
        UnionCouncil = unionCouncil;
    }
}
