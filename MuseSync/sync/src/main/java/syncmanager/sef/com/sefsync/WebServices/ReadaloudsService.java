package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Readalouds;


public interface ReadaloudsService {
    @GET("/Readalouds")
    public void getReadalouds(Callback<List<Readalouds>> callback);

    //Get Order record base on ID
    @GET("/Readalouds/{id}")
    public void getReadaloudsById(@Path("id") Integer id, Callback<Readalouds> callback);

    //Delete Order record base on ID
    @DELETE("/Readalouds/{id}")
    public void deleteReadaloudsById(@Path("id") Integer id, Callback<Readalouds> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Readalouds/{id}")
    public void updateReadaloudsById(@Path("id") Integer id, @Body Readalouds readalouds, Callback<Readalouds> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Readalouds")
    public void addReadalouds(@Body Readalouds readalouds, Callback<Readalouds> callback);

}
