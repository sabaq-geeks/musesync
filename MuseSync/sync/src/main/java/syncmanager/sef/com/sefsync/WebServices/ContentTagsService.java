package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.ContentTags;


public interface ContentTagsService {
    @GET("/ContentTags")
    public void getContentTags(Callback<List<ContentTags>> callback);

    //Get Order record base on ID
    @GET("/ContentTags/{id}")
    public void getContentTagsById(@Path("id") Integer id, Callback<ContentTags> callback);

    //Delete Order record base on ID
    @DELETE("/ContentTags/{id}")
    public void deleteContentTagsById(@Path("id") Integer id, Callback<ContentTags> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/ContentTags/{id}")
    public void updateContentTagsById(@Path("id") Integer id, @Body ContentTags contenttags, Callback<ContentTags> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/ContentTags")
    public void addContentTags(@Body ContentTags contenttags, Callback<ContentTags> callback);

}
