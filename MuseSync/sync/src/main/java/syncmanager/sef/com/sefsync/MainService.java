package syncmanager.sef.com.sefsync;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import syncmanager.sef.com.sefsync.Helper.GlobalHelper;
import syncmanager.sef.com.sefsync.syncmanager.SyncMethods;

import static android.content.ContentValues.TAG;

public class MainService extends Service {
    SyncMethods syncMethod1;
    SyncMethods syncMethod2;
    Context context;
    public MainService() {
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context=this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: ");

        if(GlobalHelper.isStoragePermissionGranted(MainService.this)) {

        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
        try {
            syncMethod2.unregReceiver();
        }
        catch (Exception e){
            Log.d(TAG, "onDestroy: "+e.getMessage());
        }
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
               // ActivityCompat.requestPermissions(MainService.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }
}
