package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.ExerciseTypes;


public interface ExerciseTypesService {
    @GET("/exercisetypesapi/getdetailedexercisetypes")
    public void getExerciseTypes(Callback<List<ExerciseTypes>> callback);

    //Get Order record base on ID
    @GET("/ExerciseTypes/{id}")
    public void getExerciseTypesById(@Path("id") Integer id, Callback<ExerciseTypes> callback);

    //Delete Order record base on ID
    @DELETE("/ExerciseTypes/{id}")
    public void deleteExerciseTypesById(@Path("id") Integer id, Callback<ExerciseTypes> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/ExerciseTypes/{id}")
    public void updateExerciseTypesById(@Path("id") Integer id, @Body ExerciseTypes exercisetypes, Callback<ExerciseTypes> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/ExerciseTypes")
    public void addExerciseTypes(@Body ExerciseTypes exercisetypes, Callback<ExerciseTypes> callback);

}
