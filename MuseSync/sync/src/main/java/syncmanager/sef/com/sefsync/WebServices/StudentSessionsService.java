package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.StudentSessions;


public interface StudentSessionsService {
    @GET("/StudentSessions")
    public void getStudentSessions(Callback<List<StudentSessions>> callback);

    //Get Order record base on ID
    @GET("/StudentSessions/{id}")
    public void getStudentSessionsById(@Path("id") Integer id, Callback<StudentSessions> callback);

    //Delete Order record base on ID
    @DELETE("/StudentSessions/{id}")
    public void deleteStudentSessionsById(@Path("id") Integer id, Callback<StudentSessions> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/StudentSessions/{id}")
    public void updateStudentSessionsById(@Path("id") Integer id, @Body StudentSessions studentsessions, Callback<StudentSessions> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/StudentSessions")
    public void addStudentSessions(@Body StudentSessions studentsessions, Callback<StudentSessions> callback);

}
