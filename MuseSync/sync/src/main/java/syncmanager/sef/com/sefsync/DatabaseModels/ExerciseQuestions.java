package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class ExerciseQuestions {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "ExerciseId")
    private Exercises exercise;

    private int ExerciseId;

    @DatabaseField
    private String Question;

    @DatabaseField
    private String UrduQuestion;

    @DatabaseField
    private String SindhiQuestion;

    @DatabaseField
    private String ImagePath;

    @DatabaseField
    private String AudioPath;

    @DatabaseField
    private Boolean Active;



    @ForeignCollectionField
    private Collection<ExerciseAnswers> ExerciseAnswers;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Exercises getExercise() {
        return exercise;
    }

    public void setExercise(Exercises exercise) {
        this.exercise = exercise;
    }

    public int getExerciseId() {
        return ExerciseId;
    }

    public void setExerciseId(int exerciseId) {
        ExerciseId = exerciseId;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getAudioPath() {
        return AudioPath;
    }

    public void setAudioPath(String audioPath) {
        AudioPath = audioPath;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }

    public Collection<ExerciseAnswers> getExerciseAnswersList() {
        return ExerciseAnswers;
    }

    public void setExerciseAnswersList(Collection<ExerciseAnswers> ExerciseAnswers) {
        this.ExerciseAnswers = ExerciseAnswers;
    }

    public int getRemoteId() {
        return RemoteId;
    }

    public void setRemoteId(int remoteId) {
        RemoteId = remoteId;
    }

    public String getUrduQuestion() {
        return UrduQuestion;
    }

    public void setUrduQuestion(String urduQuestion) {
        UrduQuestion = urduQuestion;
    }

    public String getSindhiQuestion() {
        return SindhiQuestion;
    }

    public void setSindhiQuestion(String sindhiQuestion) {
        SindhiQuestion = sindhiQuestion;
    }



    public Collection<syncmanager.sef.com.sefsync.DatabaseModels.ExerciseAnswers> getExerciseAnswers() {
        return ExerciseAnswers;
    }

    public void setExerciseAnswers(Collection<syncmanager.sef.com.sefsync.DatabaseModels.ExerciseAnswers> exerciseAnswers) {
        ExerciseAnswers = exerciseAnswers;
    }
}
