package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/26/2017.
 */
@DatabaseTable
public class ReadaloudTypes extends BaseModel{
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "ReadaloudId")
    private Readalouds readalouds;

    @DatabaseField
    private String ImagePath;

    @DatabaseField
    private String AudioPath;

    @DatabaseField
    private int Order;

    @ForeignCollectionField
    private Collection<Readalouds> readaloudList;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Readalouds getReadalouds() {
        return readalouds;
    }

    public void setReadalouds(Readalouds readalouds) {
        this.readalouds = readalouds;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getAudioPath() {
        return AudioPath;
    }

    public void setAudioPath(String audioPath) {
        AudioPath = audioPath;
    }

    public int getOrder() {
        return Order;
    }

    public void setOrder(int order) {
        Order = order;
    }

    public Collection<Readalouds> getReadaloudList() {
        return readaloudList;
    }

    public void setReadaloudList(Collection<Readalouds> readaloudList) {
        this.readaloudList = readaloudList;
    }
}
