package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/26/2017.
 */
@DatabaseTable
public class ReadaloudSlides extends BaseModel {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @ForeignCollectionField
    private Collection<ReadaloudSlideQuestions> readaloudSlideQuestionList;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "ReadaloudId")
    private Readalouds readalouds;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Collection<ReadaloudSlideQuestions> getReadaloudSlideQuestionList() {
        return readaloudSlideQuestionList;
    }

    public void setReadaloudSlideQuestionList(Collection<ReadaloudSlideQuestions> readaloudSlideQuestionList) {
        this.readaloudSlideQuestionList = readaloudSlideQuestionList;
    }
}
