package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */

@DatabaseTable
public class QuizTypes {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private String Id;

    @ForeignCollectionField
    private Collection<Quizs> quizses;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public Collection<Quizs> getQuizses() {
        return quizses;
    }

    public void setQuizses(Collection<Quizs> quizses) {
        this.quizses = quizses;
    }
}
