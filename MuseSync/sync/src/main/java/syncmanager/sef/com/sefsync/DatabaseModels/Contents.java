package syncmanager.sef.com.sefsync.DatabaseModels;

import android.renderscript.RenderScript;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

import javax.security.auth.Subject;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class Contents extends BaseModel{

    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private String Id;

    /*@DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "GradeId")
    private Grades grade;*/

    public int GradeId;

   /* @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "SubjectId")
    private Subjects subject;



    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "TopicId")
    private Topics topics;*/

    @DatabaseField(columnName = "SubjectId")
    private String subject;

    @DatabaseField(columnName = "TopicId")
    private String topics;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "ContentTypeId")
    private ContentTypes contentTypes;

    @DatabaseField
    private int Downloadable;



    @ForeignCollectionField
    private Collection<ContentDetails> contentDetailList;

    public int getRemoteId() {
        return RemoteId;
    }

    public void setRemoteId(int remoteId) {
        RemoteId = remoteId;
    }





    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }



    public int getGradeId() {
        return GradeId;
    }

    public void setGradeId(int gradeId) {
        GradeId = gradeId;
    }



    public ContentTypes getContentTypes() {
        return contentTypes;
    }

    public void setContentTypes(ContentTypes contentTypes) {
        this.contentTypes = contentTypes;
    }

    public int getDownloadable() {
        return Downloadable;
    }

    public void setDownloadable(int downloadable) {
        Downloadable = downloadable;
    }

    public Collection<ContentDetails> getContentDetailList() {
        return contentDetailList;
    }

    public void setContentDetailList(Collection<ContentDetails> contentDetailList) {
        this.contentDetailList = contentDetailList;
    }
}
