package syncmanager.sef.com.sefsync.WebServices;


import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.client.OkClient;
import syncmanager.sef.com.sefsync.DatabaseModels.ExerciseAnswers;
import syncmanager.sef.com.sefsync.DatabaseModels.ExerciseQuestions;

/**
 * Created by Ishaq on 2/3/2017.
 */

public class RetrofitRestClient {
    private retrofit.RestAdapter restAdapter;

    public RetrofitRestClient(String url) {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(5, TimeUnit.MINUTES);
        client.setReadTimeout(5, TimeUnit.MINUTES);


        restAdapter = new retrofit.RestAdapter.Builder()
                .setEndpoint(url)
                .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(client)).build();
    }

//    public RetrofitRestClient(Boolean Custom) {
//        restAdapter = new retrofit.RestAdapter.Builder()
//                .setEndpoint(URLCustomService)
//                .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)
//                .build();
//    }

    public ExercisesService getExerciseService() {
        return restAdapter.create(ExercisesService.class);
    }





    public ImagesService getImagesServiceService() {
        return restAdapter.create(ImagesService.class);
    }

    public AudiosService getAudiosServiceService() {
        return restAdapter.create(AudiosService.class);
    }

    public AcademicSessionsService getAcademicSessionsService() {
        return restAdapter.create(AcademicSessionsService.class);
    }
    public AcademicYearsService getAcademicYearsService() {
        return restAdapter.create(AcademicYearsService.class);
    }
    public ContentDetailsService getContentDetailsService() {
        return restAdapter.create(ContentDetailsService.class);
    }
    public ContentsService getContentsService() {
        return restAdapter.create(ContentsService.class);
    }
    public ContentTagsService getContentTagsService() {
        return restAdapter.create(ContentTagsService.class);
    }
    public ContentTypesService getContentTypesService() {
        return restAdapter.create(ContentTypesService.class);
    }
    public DistrictService getDistrictService() {
        return restAdapter.create(DistrictService.class);
    }
    public DocumentsService getDocumentsService() {
        return restAdapter.create(DocumentsService.class);
    }
    public ExerciseAnswersService getExerciseAnswersService() {
        return restAdapter.create(ExerciseAnswersService.class);
    }
    public ExerciseQuestionsService getExerciseQuestionsService() {
        return restAdapter.create(ExerciseQuestionsService.class);
    }
    public ExercisesService getExercisesService() {
        return restAdapter.create(ExercisesService.class);
    }
    public ExerciseTypesService getExerciseTypesService() {
        return restAdapter.create(ExerciseTypesService.class);
    }
    public GradesService getGradesService() {
        return restAdapter.create(GradesService.class);
    }
    public ImageGroupsService getImageGroupsService() {
        return restAdapter.create(ImageGroupsService.class);
    }
    public ImagesService getImagesService() {
        return restAdapter.create(ImagesService.class);
    }
    public QuizsService getQuizsService() {
        return restAdapter.create(QuizsService.class);
    }
    public QuizTypesService getQuizTypesService() {
        return restAdapter.create(QuizTypesService.class);
    }
    public ReadaloudsService getReadaloudsService() {
        return restAdapter.create(ReadaloudsService.class);
    }
    public ReadaloudSlideQuestionsService getReadaloudSlideQuestionsService() {
        return restAdapter.create(ReadaloudSlideQuestionsService.class);
    }
    public ReadaloudSlidesService getReadaloudSlidesService() {
        return restAdapter.create(ReadaloudSlidesService.class);
    }
    public ReadaloudTypesService getReadaloudTypesService() {
        return restAdapter.create(ReadaloudTypesService.class);
    }
    public SchoolsService getSchoolsService() {
        return restAdapter.create(SchoolsService.class);
    }
    public StudentsService getStudentsService() {
        return restAdapter.create(StudentsService.class);
    }
    public StudentSessionsService getStudentSessionsService() {
        return restAdapter.create(StudentSessionsService.class);
    }
    public SubjectsService getSubjectsService() {
        return restAdapter.create(SubjectsService.class);
    }
/*    public SubTopicsService getSubTopicsService() {
        return restAdapter.create(SubTopicsService.class);
    }*/
    public TagsService getTagsService() {
        return restAdapter.create(TagsService.class);
    }
    public TalukaTehsilService getTalukaTehsilService() {
        return restAdapter.create(TalukaTehsilService.class);
    }
    public TeachersService getTeachersService() {
        return restAdapter.create(TeachersService.class);
    }
    public TeacherSessionsService getTeacherSessionsService() {
        return restAdapter.create(TeacherSessionsService.class);
    }
    public TopicsService getTopicsService() {
        return restAdapter.create(TopicsService.class);
    }
    public UnionCouncilService getUnionCouncilService() {
        return restAdapter.create(UnionCouncilService.class);
    }


}
