package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.UnionCouncil;


public interface UnionCouncilService {
    @GET("/UnionCouncil")
    public void getUnionCouncil(Callback<List<UnionCouncil>> callback);

    //Get Order record base on ID
    @GET("/UnionCouncil/{id}")
    public void getUnionCouncilById(@Path("id") Integer id, Callback<UnionCouncil> callback);

    //Delete Order record base on ID
    @DELETE("/UnionCouncil/{id}")
    public void deleteUnionCouncilById(@Path("id") Integer id, Callback<UnionCouncil> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/UnionCouncil/{id}")
    public void updateUnionCouncilById(@Path("id") Integer id, @Body UnionCouncil unioncouncil, Callback<UnionCouncil> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/UnionCouncil")
    public void addUnionCouncil(@Body UnionCouncil unioncouncil, Callback<UnionCouncil> callback);

}
