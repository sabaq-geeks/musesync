package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Ishaq Ahmed Khan on 4/20/2017.
 */
@DatabaseTable
public class UnionCouncil extends BaseModel {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    public String Id;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "TalukaTehsilId")
    private TalukaTehsil TalukaTehsil;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public TalukaTehsil getTalukaTehsil() {
        return TalukaTehsil;
    }

    public void setTalukaTehsil(TalukaTehsil talukaTehsil) {
        TalukaTehsil = talukaTehsil;
    }
}
