package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.ContentTypes;


public interface ContentTypesService {
    @GET("/ContentTypes")
    public void getContentTypes(Callback<List<ContentTypes>> callback);

    //Get Order record base on ID
    @GET("/ContentTypes/{id}")
    public void getContentTypesById(@Path("id") Integer id, Callback<ContentTypes> callback);

    //Delete Order record base on ID
    @DELETE("/ContentTypes/{id}")
    public void deleteContentTypesById(@Path("id") Integer id, Callback<ContentTypes> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/ContentTypes/{id}")
    public void updateContentTypesById(@Path("id") Integer id, @Body ContentTypes contenttypes, Callback<ContentTypes> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/ContentTypes")
    public void addContentTypes(@Body ContentTypes contenttypes, Callback<ContentTypes> callback);

}
