package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Hammad Ali Khan on 4/26/2017.
 */
@DatabaseTable
public class ContentDetails extends BaseModel{

    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "ContentId")
    private Contents contents;


    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "AudioId")
    private Audios audios;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "ImageId")
    private Images images;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "VideoId")
    private Videos videos;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "DocumentId")
    private Documents documents;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "ImageGroupId")
    private ImageGroups imageGroups;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Contents getContents() {
        return contents;
    }

    public void setContents(Contents contents) {
        this.contents = contents;
    }

    public Audios getAudios() {
        return audios;
    }

    public void setAudios(Audios audios) {
        this.audios = audios;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public Videos getVideos() {
        return videos;
    }

    public void setVideos(Videos videos) {
        this.videos = videos;
    }

    public Documents getDocuments() {
        return documents;
    }

    public void setDocuments(Documents documents) {
        this.documents = documents;
    }

    public ImageGroups getImageGroups() {
        return imageGroups;
    }

    public void setImageGroups(ImageGroups imageGroups) {
        this.imageGroups = imageGroups;
    }

    public int getRemoteId() {
        return RemoteId;
    }

    public void setRemoteId(int remoteId) {
        RemoteId = remoteId;
    }
}
