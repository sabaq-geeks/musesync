package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class Videos extends BaseModel{
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField
    private String FileType;


    @ForeignCollectionField
    private Collection<ContentDetails> contentDetailList;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public Collection<ContentDetails> getContentDetailList() {
        return contentDetailList;
    }

    public void setContentDetailList(Collection<ContentDetails> contentDetailList) {
        this.contentDetailList = contentDetailList;
    }
}
