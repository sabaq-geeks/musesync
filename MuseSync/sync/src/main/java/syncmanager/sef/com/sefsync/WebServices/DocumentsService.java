package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.Documents;


public interface DocumentsService {
    @GET("/Documents")
    public void getDocuments(Callback<List<Documents>> callback);

    //Get Order record base on ID
    @GET("/Documents/{id}")
    public void getDocumentsById(@Path("id") Integer id, Callback<Documents> callback);

    //Delete Order record base on ID
    @DELETE("/Documents/{id}")
    public void deleteDocumentsById(@Path("id") Integer id, Callback<Documents> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/Documents/{id}")
    public void updateDocumentsById(@Path("id") Integer id, @Body Documents documents, Callback<Documents> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/Documents")
    public void addDocuments(@Body Documents documents, Callback<Documents> callback);

}
