package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class TeacherSessions {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "TeacherId")
    private Teachers teachers;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "AcademicSessionId")
    private AcademicSessions academicSession;

    @DatabaseField
    private Boolean Active;


}
