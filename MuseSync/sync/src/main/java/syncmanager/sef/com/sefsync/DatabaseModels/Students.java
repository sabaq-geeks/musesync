package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/26/2017.
 */
@DatabaseTable
public class Students {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField
    private String StudentCode;

    @DatabaseField
    private String LeagacyCode;

    @DatabaseField
    private String GrNumber;

    @DatabaseField
    private String SemisCode;

    @DatabaseField
    private String FirstName;

    @DatabaseField
    private String SecondName;

    @DatabaseField
    private String Surname;

    @DatabaseField
    private String GivenName;

    @DatabaseField
    private String FatherName;

    @DatabaseField
    private String FatherCnic;

    @DatabaseField
    private String ReligionId;

    @DatabaseField
    private String Gender;

    @DatabaseField
    private String DateOfBirth;

    @DatabaseField
    private String DateOfAdmission;

    @DatabaseField
    private String HealthInformation;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "UnionCouncilId")
    private UnionCouncil unionCouncils;

    private String UnionCouncilId;

    @DatabaseField
    private String Village;

    @DatabaseField
    private String HomeAddress;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "SchoolId")
    private Schools school;

    private int SchoolId;

    @DatabaseField
    private String LastSchoolAttended;

    @DatabaseField
    private String AdmissionRequiredFor;

    @DatabaseField
    private String LastClassAttended;

    @DatabaseField
    private String ParentContact;

    @DatabaseField
    private String AlternateContact;

    @DatabaseField
    private String SubsidyCategoryId;

    @DatabaseField
    private String ReasonForLeaving;

    @DatabaseField
    private String Left;

    @ForeignCollectionField
    private Collection<StudentSessions> studentSessionsList;


}
