package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class AcademicSessions extends BaseModel {
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private int Id;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "AcademicYearId")
    private AcademicYears academicYears;
///*
//    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "GradeId")
//    private Grades grades;*/

    @DatabaseField(columnName = "GradeId")
    private String grades;

   @DatabaseField
    private String Section;

    @DatabaseField(foreign=true,foreignAutoRefresh=true,columnName = "SchoolId")
    private Schools school;

    @DatabaseField
    private Boolean SessionOnGoing;

    @ForeignCollectionField
    private Collection<StudentSessions> studentSessionsList;

    @ForeignCollectionField
    private Collection<TeacherSessions> teacherSessionsList;

    @ForeignCollectionField
    private Collection<Quizs> quizsList;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public AcademicYears getAcademicYears() {
        return academicYears;
    }

    public void setAcademicYears(AcademicYears academicYears) {
        this.academicYears = academicYears;
    }

    /*public Grades getGrades() {
        return grades;
    }

    public void setGrades(Grades grades) {
        this.grades = grades;
    }
*/
    public String getSection() {
        return Section;
    }

    public void setSection(String section) {
        Section = section;
    }

    public Schools getSchool() {
        return school;
    }

    public void setSchool(Schools school) {
        this.school = school;
    }

    public Boolean getSessionOnGoing() {
        return SessionOnGoing;
    }

    public void setSessionOnGoing(Boolean sessionOnGoing) {
        SessionOnGoing = sessionOnGoing;
    }

    public Collection<StudentSessions> getStudentSessionsList() {
        return studentSessionsList;
    }

    public void setStudentSessionsList(Collection<StudentSessions> studentSessionsList) {
        this.studentSessionsList = studentSessionsList;
    }

    public Collection<TeacherSessions> getTeacherSessionsList() {
        return teacherSessionsList;
    }

    public void setTeacherSessionsList(Collection<TeacherSessions> teacherSessionsList) {
        this.teacherSessionsList = teacherSessionsList;
    }

    public Collection<Quizs> getQuizsList() {
        return quizsList;
    }

    public void setQuizsList(Collection<Quizs> quizsList) {
        this.quizsList = quizsList;
    }

    public int getRemoteId() {
        return RemoteId;
    }

    public void setRemoteId(int remoteId) {
        RemoteId = remoteId;
    }
}
