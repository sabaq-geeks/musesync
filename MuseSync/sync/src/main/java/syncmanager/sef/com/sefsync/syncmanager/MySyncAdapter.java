package syncmanager.sef.com.sefsync.syncmanager;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import syncmanager.sef.com.sefsync.Helper.GlobalHelper;


/**
 * Created by Hammad Ali on 12/2/2016.
 */

public class MySyncAdapter extends AbstractThreadedSyncAdapter {
    private final AccountManager mAccountManager;
            Context context;
    public static boolean pushed=false;

    public MySyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mAccountManager = AccountManager.get(context);
        this.context=context;
    }

    public static String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        Log.d("sefsyncing", "onPerformSync for account[" + account.name + "]");
        try {
            // Get the auth token for the current account
         //   String authToken = mAccountManager.blockingGetAuthToken(account, Account.AUTHTOKEN_TYPE_FULL_ACCESS, true);
            if(GlobalHelper.isStoragePermissionGranted(context)) {
                final SyncMethods syncMethod1 = new SyncMethods(context);
                //syncMethods.getSubjectsAndSaveInDb();
                // syncMethods.getExercisesAndSaveInDb_FirstTime();
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
                thread.start();
             /*   new Thread(new Runnable() {
                    @Override
                    public void run() {
                        syncMethod1.getSubjectsAndSaveInDb();

                    }
                }).start();*/
              /*  new Thread(new Runnable() {
                    @Override
                    public void run() {
                        syncMethod1.getTopicsAndSaveInDb();

                    }
                }).start();
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        syncMethod1.getGradesAndSaveInDb();
                    }
                }).start();*/
                //syncMethod1.checkAllFilesPathAndDownloadifNotThere();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("SyncAdapter:",""+e.getMessage());
        }
    }





}
