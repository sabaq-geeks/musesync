package syncmanager.sef.com.sefsync.WebServices;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import syncmanager.sef.com.sefsync.DatabaseModels.TalukaTehsil;


public interface TalukaTehsilService {
    @GET("/TalukaTehsil")
    public void getTalukaTehsil(Callback<List<TalukaTehsil>> callback);

    //Get Order record base on ID
    @GET("/TalukaTehsil/{id}")
    public void getTalukaTehsilById(@Path("id") Integer id, Callback<TalukaTehsil> callback);

    //Delete Order record base on ID
    @DELETE("/TalukaTehsil/{id}")
    public void deleteTalukaTehsilById(@Path("id") Integer id, Callback<TalukaTehsil> callback);

    //PUT Order record and post content in HTTP request BODY
    @PUT("/TalukaTehsil/{id}")
    public void updateTalukaTehsilById(@Path("id") Integer id, @Body TalukaTehsil talukatehsil, Callback<TalukaTehsil> callback);

    //Add Order record and post content in HTTP request BODY
    @POST("/TalukaTehsil")
    public void addTalukaTehsil(@Body TalukaTehsil talukatehsil, Callback<TalukaTehsil> callback);

}
