package syncmanager.sef.com.sefsync.Database;

/**
 * Created by Hammad Ali Khan on 5/4/2017.
 */

public class Constants {

    public static String TABLE_EXERCISE="exercises";
    public static String TABLE_EXERCISE_QUESTION="exercisequestions";
    public static String TABLE_EXERCISE_ANSWER="exerciseanswers";
    public static String COLUMN_IMAGEDOWNLOADED="imageDownloaded";
    public static String COLUMN_EXERCISE_QUESTION_ID ="id";
    public static String COLUMN_EXERCISEQUESTION_IMAGE_PATH ="ImagePath";
    public static String COLUMN_EXERCISEANSWER_IMAGE_PATH ="ImagePath";
    public static String COLUMN_EXERCISEQUESTION_SOUND_PATH ="audioPath";
    public static String COLUMN_EXERCISEANSWER_SOUND_PATH ="audioPath";
    public static String COLUMN_EXERCISE_INSTRUCTIONSOUND_PATH ="InstructionSoundPath";
    public static String REMOTEPATH_PREFIX ="/Content";


}
