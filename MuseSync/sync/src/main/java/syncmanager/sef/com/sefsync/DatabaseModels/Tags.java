package syncmanager.sef.com.sefsync.DatabaseModels;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Hammad Ali Khan on 4/25/2017.
 */
@DatabaseTable
public class Tags extends BaseModel{
    @DatabaseField
    public int RemoteId;

    @DatabaseField(canBeNull = false, id = true)
    private String Id;

    @ForeignCollectionField
    private Collection<ContentTags> contentTagList;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public Collection<ContentTags> getContentTagList() {
        return contentTagList;
    }

    public void setContentTagList(Collection<ContentTags> contentTagList) {
        this.contentTagList = contentTagList;
    }
}
