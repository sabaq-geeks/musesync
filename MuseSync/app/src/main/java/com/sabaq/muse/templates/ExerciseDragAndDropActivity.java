package com.sabaq.muse.templates;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sabaq.muse.R;
import com.sabaq.muse.activities.LessonMapActivity;
import com.sabaq.muse.activities.SubjectsActivity;
import com.sabaq.muse.datasource.ExerciseDataFetcher;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.ConvertDptoPx;
import com.sabaq.muse.utils.ExerciseUIUtils;
import com.sabaq.muse.utils.Helper;
import com.sabaq.muse.utils.ImageLoad;
import com.sabaq.muse.utils.MediaPlayerUtil;

import com.sabaq.muse.utils.ObjectTouchAndDragListener;
import com.sabaq.muse.viewModel.ExerciseAnswersViewModel;
import com.sabaq.muse.viewModel.ExerciseQuestionViewModel;
import com.sabaq.muse.viewModel.ExerciseViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseDragAndDropActivity extends ObjectTouchAndDragListener {
    private static MediaPlayer multiSoundPlayer = null;
    Context context;
    final int REMAINING_CHARACTERS_IN_LINE = 8;
    List<ExerciseQuestionViewModel> questionViewModels;
    private ExerciseViewModel exerciseViewModel;
    private boolean isSoundClicked = true;
    //todo change issoundclick
    int questionIndex = 0;

    @Override
    public void onObjectTouch(View view, MotionEvent motionEvent) {
        // Toast.makeText(context, "Check", Toast.LENGTH_SHORT).show();
        Log.d("MUSE", "check: child");
        if (view.getTag(R.string.EX_DRAG_N_DROP_OPTION_SOUND) != null) {
            String soundPath = view.getTag(R.string.EX_DRAG_N_DROP_OPTION_SOUND).toString();
            setAudio(soundPath);
        }
    }

    @Override
    public void onContainerDrag(View viewContainer, View droppedView) {
        checkObjectDroppedCondition(viewContainer, droppedView);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_drag_n_drop);
        startAnimation();
        context = this;
        AppController appController = new AppController();
        appController.CreateMaster(this);
        ExerciseDataFetcher exerciseDataFetcher = new ExerciseDataFetcher();
//        exerciseViewModel = exerciseDataFetcher.getExercisesFromDb(1531);
        //       exerciseViewModel = exerciseDataFetcher.getExercisesFromDb(22);
        Long exerciseId = Helper.CURRENT_CONTENTID;
        exerciseViewModel = exerciseDataFetcher.getExercisesFromDb(exerciseId);
        questionViewModels = exerciseViewModel.getExerciseQuestions();

        Boolean isHintAvailable = false;
        if (exerciseViewModel.getInstruction() != null || exerciseViewModel.getInstructionSoundPath() != null)
            isHintAvailable = true;
        getToolBar(exerciseViewModel.getExerciseTitle(), View.VISIBLE, View.VISIBLE, isHintAvailable != null ? View.VISIBLE : View.INVISIBLE, this);
        generateLayout();
        softKeyBoardDownSystemUiHide(R.id.main_container);
    }

    public void onClickHint(View view) {
        showHintDialog(this, exerciseViewModel.getInstruction(), exerciseViewModel.getInstructionSoundPath(), exerciseViewModel.getPath());
    }

    public void onClickCross(View view) {
        Intent intent = new Intent(ExerciseDragAndDropActivity.this, LessonMapActivity.class);
        startActivity(intent);
    }

    public void onClickHome(View view) {

        startActivity(new Intent(ExerciseDragAndDropActivity.this, SubjectsActivity.class));
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (multiSoundPlayer != null) {
            multiSoundPlayer.stop();
        }
    }

    private void setAudio(String audio) {
        if (isSoundClicked == true) {
            isSoundClicked = false;
            multiSoundPlayer = new MediaPlayerUtil().playSound(this, audio, "exercise", false);
            multiSoundPlayer.setOnCompletionListener(mp -> {
                isSoundClicked = true;
            });
        }
    }

    private void startAnimation() {
       setBackgroundBarAnimation(this, Helper.CURRENT_SUBJECTID);
    }

    private void generateLayout() {
        LinearLayout llQuestionContainer = findViewById(R.id.llQuestionsContainer);
        LinearLayout llOptionsContainer = findViewById(R.id.llOptionsContainer);

        if (llOptionsContainer.getChildCount() != 0 || llQuestionContainer.getChildCount() != 0) {
            llOptionsContainer.removeAllViews();
            llQuestionContainer.removeAllViews();
        }
        if (questionIndex <= questionViewModels.size() - 1)
            loadQuestionAndOptions(llQuestionContainer, llOptionsContainer);
//        else if (questionIndex == questionViewModels.size())
        //getResultScreen();
    }

    private void loadQuestionAndOptions(LinearLayout llQuestionContainer, LinearLayout llOptionsContainer) {
        // Question layout
        ExerciseQuestionViewModel questionViewModel = questionViewModels.get(questionIndex);
        String answer = questionViewModels.get(questionIndex).getExerciseAnswers().get(0).getAnswer();

        generateQuestion(questionViewModel, llQuestionContainer, answer);
        //Option layout
        generateOptions(questionViewModel, llOptionsContainer);
        //nav
        setNavigation();

    }

    private void setExQuestionInstructionAudioPath(final String instructionAudioPath) {
        ImageView btnExQuestionInstructionSound = findViewById(R.id.ivExQuesInstructionSound);

        btnExQuestionInstructionSound.bringToFront();
        if (instructionAudioPath == null)
            btnExQuestionInstructionSound.setVisibility(View.VISIBLE);

        btnExQuestionInstructionSound.setOnClickListener(v -> setAudio(instructionAudioPath));
    }

    private void setNavigation() {
        ImageView btnRight = findViewById(R.id.btnRightNav);
        ImageView btnLeft = findViewById(R.id.btnLeftNav);
        if (multiSoundPlayer != null) {
            multiSoundPlayer.stop();
            isSoundClicked = true;
        }
        if (questionIndex == 0)
            btnLeft.setVisibility(View.INVISIBLE);

        else if (questionIndex == questionViewModels.size() - 1)
            btnRight.setVisibility(View.INVISIBLE);

        else {
            btnRight.setVisibility(View.VISIBLE);
            btnLeft.setVisibility(View.VISIBLE);
        }

        btnRight.setOnClickListener(view -> {
            checkIfAnswerCorrect();
            loadNextQuestion(true);
            dismissMediaPlayer();
        });
        btnLeft.setOnClickListener(view -> loadNextQuestion(false));
    }

    private void dismissMediaPlayer() {
        if (multiSoundPlayer != null) {
            multiSoundPlayer.stop();
        }
    }

    private void checkIfAnswerCorrect() {
        ArrayList<ExerciseAnswersViewModel> answersViewModels = questionViewModels.get(questionIndex).getExerciseAnswers();
        for (int j = 0; j < answersViewModels.size(); j++) {
            ExerciseAnswersViewModel option = answersViewModels.get(j);
            if (option != null && option.getSelected() != null && option.getSelected() && option.getCorrectAnswer() && option.getOrder() == option.getCorrectOrder()) {
                questionViewModels.get(questionIndex).setCorrect(true);
                Toast.makeText(context, "Correct", Toast.LENGTH_SHORT).show();
            }

        }
    }

    /**
     * generate question Ui
     *
     * @param questionViewModel
     * @param llQuestionContainer
     */
    private void generateQuestion(ExerciseQuestionViewModel questionViewModel, LinearLayout llQuestionContainer, String answer) {
        final String imagePath;
        String text;
        imagePath = questionViewModel.getImagePath();
        text = questionViewModel.getQuestion();

        LinearLayout llQuestionTextContainer = new LinearLayout(context);
        llQuestionTextContainer.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams llQuestionTextContainerParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        llQuestionTextContainer.setLayoutParams(llQuestionTextContainerParams);
        llQuestionContainer.addView(llQuestionTextContainer);

        //set Ex Ques Audio Instruction
        if (questionViewModel.getInstructionAudioPath() != null)
            setExQuestionInstructionAudioPath(questionViewModel.getInstructionAudioPath());

        final String soundPath = questionViewModel.getAudioPath();

        if (soundPath != null) {
            llQuestionContainer.setOnClickListener(v -> setAudio(soundPath));
        }

        if (text.trim() != null && !text.trim().equals("-")) {

            String convertedQuestion = text.replace("$", " $ ");
            String[] splittedQuestionArray = convertedQuestion.split(" ");
            int lineCount = ((convertedQuestion + " " + answer).length() / 50) + 1;
            //Contains all the the answers being put in Edit Text Box
            //todo test with 2 $
            String[] answerText = new String[dollarCount(convertedQuestion)];
            for (int dollarCountIndex = 0; dollarCountIndex < dollarCount(convertedQuestion); dollarCountIndex++) {
                answerText[dollarCountIndex] = answer;
            }
            int breakLineLength = 50;
            int splittedQuestionArrayIndex = 0;
            LinearLayout llHorizontalContainer;
            //Defines working at which dollar sign , first or second
            int answerBox = 0;

            for (int lineCountIndex = 0; lineCountIndex < lineCount; lineCountIndex++) {

                //Defines how many Char in current horizontal Line
                int horizontalLineTextCount = 0;

                llHorizontalContainer = getllHorizontalContainer();
                llQuestionTextContainer.addView(llHorizontalContainer);
                for (; splittedQuestionArrayIndex < splittedQuestionArray.length; splittedQuestionArrayIndex++) {
                    //For Answer Edit Text Box
                    if (splittedQuestionArray[splittedQuestionArrayIndex].equals("$")) {
                        //Incase to be added in current line

                        if (breakLineLength - horizontalLineTextCount > REMAINING_CHARACTERS_IN_LINE) {
                            //todo lineCountIndex=0 to change
                            RelativeLayout llBlank = generateBlankForQuestion(questionViewModel, 0);
                            llHorizontalContainer.addView(llBlank);
                            horizontalLineTextCount += answerText[answerBox].length();

                        }
                        //If only 5 spaces left, then to be added in next line
                        else {
                            //yahan line break horahi ha
                            llHorizontalContainer = getllHorizontalContainer();
                            llQuestionTextContainer.addView(llHorizontalContainer);
                            llHorizontalContainer.addView(generateBlankForQuestion(questionViewModel, 0));
                            horizontalLineTextCount += answerText[answerBox].length();
                        }
                        answerBox++;
                        continue;
                    }
                    //For Simple Question Text
                    if (horizontalLineTextCount + splittedQuestionArray[splittedQuestionArrayIndex].length() < breakLineLength) {
                        splittedQuestionArray[splittedQuestionArrayIndex] = splittedQuestionArray[splittedQuestionArrayIndex] + " ";

                        llHorizontalContainer.addView(generateWordForQuestion(splittedQuestionArray[splittedQuestionArrayIndex]));
                        horizontalLineTextCount += splittedQuestionArray[splittedQuestionArrayIndex].length();
                    } else break;
                }
            }

        }

        ImageView imgQuestion = generateQuestionImage(imagePath, text);
        llQuestionContainer.addView(imgQuestion);
    }

    @NonNull
    private ImageView generateQuestionImage(final String imagePath, String text) {
        return generateQuestionIv(this, imagePath, text, null);
    }

    private int dollarCount(String question) {
        int counter = 0;
        for (int i = 0; i < question.length(); i++) {
            if (question.charAt(i) == '$') {
                counter++;
            }
        }
        return counter;
    }

    @NonNull
    private RelativeLayout generateBlankForQuestion(ExerciseQuestionViewModel questionViewModel, int i) {
        RelativeLayout llBlank = new RelativeLayout(context);
        llBlank.setOnDragListener(this);
        llBlank.setTag(R.string.EX_DRAG_N_DROP_LLBLANK_INDEX, i);
        for (int p = 0; p < questionViewModel.getExerciseAnswers().size(); p++) {
            // for checking correct answer i gave tag to the blank of correct answer and i'll match this tag on drop of option.
            ExerciseAnswersViewModel exerciseAnswer = questionViewModel.getExerciseAnswers().get(p);
            if (exerciseAnswer.getOrder() == i && exerciseAnswer.getCorrectAnswer())
                llBlank.setTag(R.string.EX_DRAG_N_DROP_OPTION_ANSWER, exerciseAnswer.getAnswer());
        }

        int dpWidth = (int) (context.getResources().getDimension(R.dimen._135sdp) / context.getResources().getDisplayMetrics().density);
        int dpHeight = (int) (context.getResources().getDimension(R.dimen._48sdp) / context.getResources().getDisplayMetrics().density);
        int widthPx = new ConvertDptoPx(context, dpWidth).dpToPx();
        int heightPx = new ConvertDptoPx(context, dpHeight).dpToPx();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(widthPx, heightPx);
        llBlank.setId(getResources().getInteger(R.integer.EX_DRAG_N_DROP_LLBLANK));
        llBlank.setGravity(RelativeLayout.CENTER_HORIZONTAL | RelativeLayout.CENTER_VERTICAL);

        ImageView line = new ImageView(context);
        line.setImageResource(R.drawable.shape_dash_boarder);
        LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        line.setLayoutParams(lineParams);
        lineParams.gravity = Gravity.CENTER;
        llBlank.addView(line);
        llBlank.setLayoutParams(layoutParams);
        llBlank.setTag(R.string.EX_DRAG_N_DROP_TAG_LAYOUT_TYPE, "llBlank");
        llBlank.setTag(R.string.EX_DRAG_N_DROP_TAG2, layoutParams);
        for (int l = 0; l < questionViewModel.getExerciseAnswers().size(); l++) {
            ExerciseAnswersViewModel exerciseAnswersViewModel = questionViewModel.getExerciseAnswers().get(l);
            //todo i=0 to change
            if (exerciseAnswersViewModel.getOrder() == 0 && exerciseAnswersViewModel.getSelectedView() != null) {

                setSelectedOptionInBlank(exerciseAnswersViewModel, llBlank);
            }

        }
        return llBlank;
    }

    /**
     * Generates word For question
     *
     * @param s word
     * @return textview for word
     */
    @NonNull
    private TextView generateWordForQuestion(String s) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 100);
        params.gravity = Gravity.CENTER;
        //todo check standard for font size for phone and tablet
        TextView tvWord;
        //if (Helper.isTablet(context))
        tvWord = ExerciseUIUtils.generateTextView(s, context, R.dimen._16sdp, params, Color.WHITE, View.TEXT_ALIGNMENT_CENTER, Gravity.CENTER);
        // else
        //   tvWord = ExerciseUIUtils.generateTextView(s, context, R.dimen._16sdp, params, Color.WHITE, View.TEXT_ALIGNMENT_CENTER, Gravity.CENTER);
        return tvWord;
    }

    /**
     * Generates new horizontal linear layout in vertical question text container
     *
     * @return
     */
    @NonNull
    private LinearLayout getllHorizontalContainer() {
        //todo make function in helper for ui
        LinearLayout llHorizontalContainer;
        llHorizontalContainer = new LinearLayout(context);
        LinearLayout.LayoutParams llHorizontalContainerlayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        llHorizontalContainerlayoutParams.gravity = Gravity.CENTER_VERTICAL;
        llHorizontalContainer.setGravity(Gravity.CENTER_VERTICAL);
//                        layoutParams.weight = 1;
        llHorizontalContainer.setLayoutParams(llHorizontalContainerlayoutParams);
        llHorizontalContainer.setOrientation(LinearLayout.HORIZONTAL);
        return llHorizontalContainer;
    }

    /*DeviceType :true = for tablet ,false =Phone*/
    private boolean isTablet() {
        return getResources().getBoolean(R.bool.isTablet);
    }

    private void setSelectedOptionInBlank(ExerciseAnswersViewModel exerciseAnswersViewModel, RelativeLayout llBlank) {
        View view = exerciseAnswersViewModel.getSelectedView();
        View tempView = view;

        ViewGroup viewParent = (ViewGroup) view.getParent();
        if (viewParent != null) {
            viewParent.removeView(view);
            llBlank.addView(tempView);
        }
    }

   /* public void getResultScreen() {
        final Dialog mDialog;
        mDialog = new Dialog(ExerciseMCQsActivity.this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_message);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView tvTotalQuestions, tvCorrectAnswers, tvIncorrectAnswers;
        int totalQuestion = questionViewModels.size();
        Button btnOkay;
        btnOkay = (Button) mDialog.findViewById(R.id.btnOk);
        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                startActivity(new Intent(ExerciseMCQsActivity.this, LessonMapActivity.class));
                finish();
            }
        });
        tvTotalQuestions = (TextView) mDialog.findViewById(R.id.tvTotalQuestion);
        tvCorrectAnswers = (TextView) mDialog.findViewById(R.id.tvCorrectAnswers);
        int correctAnswers = Integer.parseInt(getCorrectAnswers());
        int totalQuestions = questionViewModels.size();
        int incorrectAnswers = questionViewModels.size() - correctAnswers;
        tvCorrectAnswers.setText("Correct Answers:" + correctAnswers);
        tvIncorrectAnswers = (TextView) mDialog.findViewById(R.id.tvIncorrectAnswers);
        tvTotalQuestions.setText("Total Questions:" + totalQuestion);
        tvIncorrectAnswers.setText("Incorrect Answers:" + incorrectAnswers);

        mDialog.show();

    }*/

    /* private String getCorrectAnswers() {
         int correctAnswers = 0;
         for (int i = 0; i < questionViewModels.size(); i++) {
             ArrayList<ExerciseAnswersViewModel> options = questionViewModels.get(i).getExerciseAnswers();
             for (int j = 0; j < options.size(); j++) {
                 ExerciseAnswersViewModel option = options.get(j);
                 if (option.isCorrect() && option.isSelected())
                     correctAnswers++;

             }
         }
         return correctAnswers + "";
     }*/

    private void loadNextQuestion(boolean isAdded) {
        if (isAdded)
            questionIndex++;
        else
            questionIndex--;

        generateLayout();

    }

    /**
     * Shuffles the options. Generates Options in Option Container
     *
     * @param mcqQuestionViewModel
     * @param llOptionsContainer
     */
    private void generateOptions(ExerciseQuestionViewModel mcqQuestionViewModel, LinearLayout llOptionsContainer) {

        final ArrayList<ExerciseAnswersViewModel> options = mcqQuestionViewModel.getExerciseAnswers();
        if (mcqQuestionViewModel.getFirstTime())
            Collections.shuffle(options);

        mcqQuestionViewModel.setFirstTime(false);
        llOptionsContainer.setWeightSum(options.size());
        llOptionsContainer.setOnDragListener(this);
        Log.d("Muse", "generateOptions: option size " + options.size());
        final ArrayList<LinearLayout> llOptions = new ArrayList<>();

        for (int i = 0; i < options.size(); i++) {

            final ExerciseAnswersViewModel option = options.get(i);

            if (option.getSelectedView() == null) {
                ImageView btnSound;
                final LinearLayout llOption = generatellOptionUI(this);
                llOption.setBackgroundResource(R.drawable.button_normal);
                final int index = i;
                llOption.setTag(R.string.EX_DRAG_N_DROP_OPTION_ANSWER, options.get(index).getAnswer());
                llOptionsContainer.setTag(R.string.EX_DRAG_N_DROP_TAG_LAYOUT_TYPE, "llOption");
                llOptionsContainer.setTag(R.string.EX_DRAG_N_DROP_LAYOUT_PARAMS, llOption.getLayoutParams());
                llOption.setTag(R.string.EX_DRAG_N_DROP_OPTION_INDEX, i);
                llOption.setTag(R.string.EX_DRAG_N_DROP_OPTION_SOUND, option.getAudioPath());
                llOption.setOnTouchListener(this);

                llOption.setId(getResources().getInteger(R.integer.EX_DRAG_N_DROP_LLOPTION));

                if (option.getAudioPath() != null && option.getImagePath() == null && option.getAnswer() == null) {
                    btnSound = generateOptionSoundButton(option);
                    final String path = option.getAudioPath();
                    Log.d("MUSE", "getOptionSoundButton: AudioPath:" + path);
                    btnSound.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setAudio(path);
                        }
                    });
                    // llOption.setTag(index);
                    llOptions.add(llOption);
                    llOption.addView(btnSound);

                }
                if (option.getImagePath() != null) {
                    Log.d("MUSE", "generateOptions: option Image Path:" + option.getImagePath());
                    ImageView ivZoom = new ImageView(this);
                    LinearLayout.LayoutParams ivZoomParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                    ivZoomParams.weight = 1;
                    ivZoom.setLayoutParams(ivZoomParams);
                    ivZoom.setPadding(15, 15, 15, 15);
                    ivZoom.setImageResource(R.drawable.ic_zoom);
                    final String img = option.getImagePath();
                    ivZoom.setOnClickListener(v -> showImageInZoom(ExerciseDragAndDropActivity.this, option.getImagePath()));
                    String image = option.getImagePath();
                    ImageView ivOption = new ImageView(this);

                    LinearLayout.LayoutParams ivOptionParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                    ivOptionParams.weight = 2;
                    ivOption.setPadding(10, 10, 10, 10);
                    ivOption.setLayoutParams(ivOptionParams);
                    new ImageLoad(this)
                            .setImage(img, ivOption);
                    llOption.addView(ivZoom);
                    llOption.addView(ivOption);
                    llOption.setWeightSum(3);
                    llOptions.add(llOption);

                } else if (option.getAnswer() != null && !option.getAnswer().equals("-")) {

                    String optionText = option.getAnswer();
                    LinearLayout.LayoutParams tvOptionParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    tvOptionParams.gravity = Gravity.CENTER;
                    TextView tvOption = ExerciseUIUtils.generateTextView(optionText, context, R.dimen._16sdp, tvOptionParams, R.color.colorChapterTitle, View.TEXT_ALIGNMENT_CENTER, Gravity.CENTER);
                    tvOption.setLayoutParams(tvOptionParams);
                    llOption.addView(tvOption);
                    llOptions.add(llOption);
                }
                llOptionsContainer.addView(llOption);

            }
        }
    }


    private void selectOption(ArrayList<LinearLayout> llOptions, int index, ArrayList<ExerciseAnswersViewModel> options) {
        for (int i = 0; i < options.size(); i++) {
            if (index == i) {
                llOptions.get(i).setBackgroundResource(R.drawable.button_pressed);
                options.get(i).setSelected(true);
            } else {
                llOptions.get(i).setBackgroundResource(R.drawable.button_normal);
                options.get(i).setSelected(false);
            }
        }

    }

    /**
     * @param viewContainer view on which it is dropped
     * @param viewDropped   item which is dropped
     */
    private void checkObjectDroppedCondition(View viewContainer, View viewDropped) {
        ViewGroup viewGroup = (ViewGroup) viewContainer;
        if (viewDropped != null && viewContainer.getTag(R.string.EX_DRAG_N_DROP_TAG_LAYOUT_TYPE).equals("llOption")) {
            // If the option is dropped on Option container
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) viewContainer.getTag(R.string.EX_DRAG_N_DROP_LAYOUT_PARAMS);
            viewDropped.setLayoutParams(params);
            ExerciseAnswersViewModel exerciseAnswer = exerciseViewModel.getExerciseQuestions().get(questionIndex).getExerciseAnswers().get(Integer.parseInt(viewDropped.getTag(R.string.EX_DRAG_N_DROP_OPTION_INDEX).toString()));
            exerciseAnswer.setSelected(null);
            exerciseAnswer.setSelectedAnswer(null);
            exerciseAnswer.setSelectedView(null);
            exerciseAnswer.setOrder(0);

        } else if (viewContainer.getTag(R.string.EX_DRAG_N_DROP_TAG_LAYOUT_TYPE).equals("llBlank")) {
            // If the option is dropped on Blank container
            if (viewGroup.getChildCount() > 1 && viewGroup.findViewById(getResources().getInteger(R.integer.EX_DRAG_N_DROP_LLOPTION)) != null && (viewGroup.findViewById(getResources().getInteger(R.integer.EX_DRAG_N_DROP_LLOPTION))).getVisibility() == View.VISIBLE) {
                // If one option is placed already in blank and another option is also dropped on that blank, then remove the first option and placed in option container
                View view1 = viewGroup.findViewById(getResources().getInteger(R.integer.EX_DRAG_N_DROP_LLOPTION));
                //replace views
                ExerciseAnswersViewModel exerciseAnswer = exerciseViewModel.getExerciseQuestions().get(questionIndex).getExerciseAnswers().get(Integer.parseInt(view1.getTag(R.string.EX_DRAG_N_DROP_OPTION_INDEX).toString()));
                exerciseAnswer.setSelected(null);
                exerciseAnswer.setSelectedAnswer(null);
                exerciseAnswer.setSelectedView(null);
                exerciseAnswer.setOrder(0);

                Log.d("MUSE", "checkObjectDroppedCondition: " + "2 views");
                ViewGroup viewDroppedParent = (ViewGroup) viewDropped.getParent();
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) viewDroppedParent.getTag(R.string.EX_DRAG_N_DROP_LAYOUT_PARAMS);
                view1.setLayoutParams(params);
                viewGroup.removeView(view1);
                viewDroppedParent.addView(view1);

            }

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewContainer.getTag(R.string.EX_DRAG_N_DROP_TAG2);
            params.width = viewContainer.getWidth() - 25;
            params.height = viewContainer.getHeight() - 25;
            params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            viewDropped.setLayoutParams(params);

            if (viewDropped.getTag(R.string.EX_DRAG_N_DROP_OPTION_ANSWER).equals(viewGroup.getTag(R.string.EX_DRAG_N_DROP_OPTION_ANSWER))) {
                Toast.makeText(context, "Correct", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Incorrect", Toast.LENGTH_SHORT).show();
            }
            ExerciseAnswersViewModel exerciseAnswer = exerciseViewModel.getExerciseQuestions().get(questionIndex).getExerciseAnswers().get(Integer.parseInt(viewDropped.getTag(R.string.EX_DRAG_N_DROP_OPTION_INDEX).toString()));
            exerciseAnswer.setSelected(true);
            exerciseAnswer.setSelectedAnswer(viewDropped.getTag(R.string.EX_DRAG_N_DROP_OPTION_ANSWER).toString());
            exerciseAnswer.setSelectedView(viewDropped);
            exerciseAnswer.setOrder(Integer.parseInt(viewContainer.getTag(R.string.EX_DRAG_N_DROP_LLBLANK_INDEX).toString()));
        }

        if (viewDropped != null) {
            ViewGroup owner = (ViewGroup) viewDropped.getParent();
            owner.removeView(viewDropped);
            viewGroup.addView(viewDropped);
        }
        Log.d("MUSE", "checkObjectDroppedCondition: Matched ");

    }


}
