package com.sabaq.muse.utils;

import com.sabaq.muse.viewModel.LessonViewModel;

import java.util.Comparator;


/**
 *
 * It compares order of two lessonviewmodel objects
 */
public class LessonComparator implements Comparator<LessonViewModel>
 {
     // TODO: 9/27/2018 naming convention
     public int compare(LessonViewModel c1, LessonViewModel c2)
     {
         return ((Integer)c1.getOrder()).compareTo(c2.getOrder());
     }
 }