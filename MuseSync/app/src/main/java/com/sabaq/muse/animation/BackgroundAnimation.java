package com.sabaq.muse.animation;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.Display;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.sabaq.muse.R;
import com.sabaq.muse.utils.RandomNumber;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BackgroundAnimation {
    private Activity activity;
    private int shapeColor, backgroundColor;

    public BackgroundAnimation(Activity activity, int shapeColor, int backgroundColor) {
        this.activity = activity;
        this.shapeColor = shapeColor;
        this.backgroundColor = backgroundColor;
    }

    public void animateBackground() {
        Display display = activity.getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int maxY = size.y;

        activity.findViewById(R.id.rl_container).setBackgroundColor(backgroundColor);
        for (int i = 0; i < 10; i++) {
            animation(new RandomNumber().generateRandomNumber(90000, 40000), maxY, new RandomNumber().generateRandomNumber(-100, -800), generateRandomLayoutId(i), generateRandomLayoutId(i));
        }
        setCircleColor();
        setBarColor();
    }

    private void animation(final int duration, final int maxY, final int negMaxY, final int layoutId1, final int layoutId2) {
        TranslateAnimation moveDownwards = new TranslateAnimation(0, 0, negMaxY, maxY);

        Log.d("Animation", "onAnimationStart: new animation started First " + duration + " " + maxY);

        moveDownwards.setRepeatCount(Animation.INFINITE);
        moveDownwards.setDuration(duration);
        activity.findViewById(layoutId1).startAnimation(moveDownwards);
        activity.findViewById(layoutId2).startAnimation(moveDownwards);

        moveDownwards.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Log.d("Animation", "onAnimationStart: new animation started again");
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d("Animation", "onAnimationEnd:  animation End");
                animation.cancel();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                Log.d("Animation", "onAnimationRepeat: new animation started again " + duration + " " + maxY);
                TranslateAnimation moveDownwards = new TranslateAnimation(0, 0, -maxY, maxY + 100);
                moveDownwards.setRepeatCount(Animation.INFINITE);
                moveDownwards.setDuration(duration);
                activity.findViewById(layoutId1).startAnimation(moveDownwards);
                activity.findViewById(layoutId2).startAnimation(moveDownwards);
            }
        });
    }

    private void setCircleColor() {
        Drawable tempDrawable = activity.getResources().getDrawable(R.drawable.shape_circle);
        LayerDrawable bubble = (LayerDrawable) tempDrawable;     //  (cast to root element in xml)
        GradientDrawable solidColor = (GradientDrawable) bubble.findDrawableByLayerId(R.id.item_circle);
        solidColor.setColor(shapeColor);
        ((ImageView) activity.findViewById(R.id.ll_1_circle1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_2_circle1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_4_circle1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_6_circle1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_6_circle2)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_7_circle1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_8_circle1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_9_circle1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_10_circle1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_11_circle1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_12_circle1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_12_circle2)).setImageDrawable(tempDrawable);
    }

    private void setBarColor() {
        Drawable tempDrawable = activity.getResources().getDrawable(R.drawable.shape_round_bar);
        LayerDrawable bubble = (LayerDrawable) tempDrawable;     //  (cast to root element in xml)
        GradientDrawable solidColor = (GradientDrawable) bubble.findDrawableByLayerId(R.id.item_bar);
        solidColor.setColor(shapeColor);
        ((ImageView) activity.findViewById(R.id.ll_1_bar1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_2_bar1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_4_bar1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_6_bar1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_9_bar1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_10_bar1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_11_bar1)).setImageDrawable(tempDrawable);
        ((ImageView) activity.findViewById(R.id.ll_12_bar1)).setImageDrawable(tempDrawable);
    }


    private int generateRandomLayoutId(int position) {

        Integer[] layoutIDArray = {R.id.ll_1, R.id.ll_2, R.id.ll_4, R.id.ll_6, R.id.ll_7, R.id.ll_8, R.id.ll_9, R.id.ll_10, R.id.ll_11, R.id.ll_12};
        List<Integer> mListOfInt = new ArrayList<Integer>(Arrays.<Integer>asList(layoutIDArray));

        Collections.shuffle(mListOfInt);
        return layoutIDArray[position];
    }
}

