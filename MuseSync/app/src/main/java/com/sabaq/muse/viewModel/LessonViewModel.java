package com.sabaq.muse.viewModel;

import com.sabaq.muse.database.Exercise;
import com.sabaq.muse.database.Readaloud;
import com.sabaq.muse.database.Video;

public class LessonViewModel {


    private String Title;

    private int Order;
    private Long Id;
    public enum ContentType{
        Readaloud,Video,Exercise
    }
    private boolean isCompleted;

    private ContentType contentType;

   public enum FileType{
        Readaloud,
        Video
    }

    public String getExerciseType() {
        return ExerciseType;
    }

    public void setExerciseType(String exerciseType) {
        ExerciseType = exerciseType;
    }

    public String ExerciseType;

    public LessonViewModel(Exercise exercise) {
        this.Title = exercise.getExerciseTitle();
        this.Order = exercise.getOrder();
        this.contentType=ContentType.Exercise;
        this.Id=exercise.getId();
        this.ExerciseType=exercise.getExerciseTypeId();
    }

    public LessonViewModel(Readaloud readaloud) {
        this.Title = readaloud.getTitle();
        this.Order = readaloud.getOrder();
        this.contentType=ContentType.Readaloud;
        this.Id=readaloud.getId();
    }

    public LessonViewModel(Video video) {
        this.Title = video.getTitle();
        this.Order = video.getOrder();
        this.contentType=ContentType.Video;
        this.Id=video.getId();
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public void setUserType(ContentType input) {
        contentType = input;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }



    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getOrder() {
        return Order;
    }

    public void setOrder(int order) {
        Order = order;
    }
}
