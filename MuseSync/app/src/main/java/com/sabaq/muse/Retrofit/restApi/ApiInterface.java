package com.sabaq.muse.Retrofit.restApi;

import com.sabaq.muse.database.Document;
import com.sabaq.muse.database.Exercise;
import com.sabaq.muse.database.ExerciseAnswer;
import com.sabaq.muse.database.ExerciseQuestion;
import com.sabaq.muse.database.Grade;
import com.sabaq.muse.database.Readaloud;
import com.sabaq.muse.database.ReadaloudSlide;
import com.sabaq.muse.database.ReadaloudSlideAudio;
import com.sabaq.muse.database.School;
import com.sabaq.muse.database.Student;
import com.sabaq.muse.database.SubTopic;
import com.sabaq.muse.database.Topic;
import com.sabaq.muse.database.Video;
import com.sabaq.muse.database.Subject;
import com.sabaq.muse.viewModel.AllDataViewModel;
import com.sabaq.muse.viewModel.TabletViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by sabaq on 3/18/2018.
 * https://www.androidhive.info/2016/05/android-working-with-retrofit-http-library/
 */

public interface ApiInterface {
    @GET(" api/Content/GetSubjectsByDate/{date}")
    Call<List<Readaloud>> getSubjectsByDate(@Query("date") String date);

    @GET(" api/Content/GetReadaloudsByDate/{date}")
    Call<List<Readaloud>> GetReadaloudsByDate(@Query("date") String date);

    @GET("api/Content/GetTopicsByGradeSubject/")
    Call<List<Topic>> GetTopicsByGradeSubject(@Query("gradeId") String gradeId,@Query("subjectId") String subjectId);

    @GET("api/Content/GetAllGrades")
    Call<List<Grade>> GetAllGrades();

    @GET("api/Content/GetAllSubjects")
    Call<List<Subject>> GetAllSubjects();

    @GET("api/Content/getAllTopics")
    Call<List<Topic>> GetAllTopics();

    @GET("api/Content/GetAllSubtopics")
    Call<List<SubTopic>> GetAllSubTopics();

    @GET("api/Content/GetAllReadalouds")
    Call<List<Readaloud>> GetAllReadalouds();

    @GET("api/Content/GetAllReadaloudSlides")
    Call<List<ReadaloudSlide>> GetAllReadaloudSlides();

    @GET("api/Content/GetAllReadaloudSlideAudios")
    Call<List<ReadaloudSlideAudio>> GetAllReadaloudSlideAudios();

    @GET("api/Content/GetAllDocuments")
    Call<List<Document>> GetAllDocuments();

    @GET("api/Content/GetAllVideos")
    Call<List<Video>> GetAllVideos();

    @GET("api/Content/GetAllExercises")
    Call<List<Exercise>> GetAllExercises();

    @GET("api/Content/GetAllExerciseQuestions")
    Call<List<ExerciseQuestion>> GetAllExerciseQuestions();

    @GET("api/Content/GetExercisesByTopic")
    Call<List<Exercise>> GetExercisesByTopic(@Query("topicId") Long topicIc);


    @GET("api/Content/GetAllExerciseAnswers")
    Call<List<ExerciseAnswer>> GetAllExerciseAnswers();


    @POST("api/Schools/GetSchoolBySchoolCode")
    Call<School> GetSchool(@Body TabletViewModel TabletViewModel);

    @POST("api/Students/GetStudentBySchoolCode")
    Call<List<Student>> GetStudents(@Body TabletViewModel TabletViewModel);

    @GET("api/Content/GetCompleteDatabase")
    Call<AllDataViewModel> GetCompleteDatabase();

}
