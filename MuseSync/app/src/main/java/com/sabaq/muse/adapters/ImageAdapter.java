package com.sabaq.muse.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.sabaq.muse.R;
import com.sabaq.muse.templates.DnDObjectsMathOperations;

import java.util.ArrayList;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
 
  public ArrayList<Integer> objects;
 
    // Constructor
    public ImageAdapter(Context c,ArrayList<Integer> obj){
        mContext = c;
        this.objects=obj;
    }
 
    @Override
    public int getCount() {
        return objects.size();
    }
 
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return 0;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource(objects.get(position));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        DnDObjectsMathOperations dnd=new DnDObjectsMathOperations();

        imageView.setLayoutParams(new GridView.LayoutParams(70, 70));
        return imageView;
    }
 
}