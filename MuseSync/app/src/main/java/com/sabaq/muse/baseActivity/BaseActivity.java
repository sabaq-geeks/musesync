package com.sabaq.muse.baseActivity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.rd.PageIndicatorView;
import com.rd.animation.type.AnimationType;
import com.sabaq.muse.R;
import com.sabaq.muse.activities.SubjectsActivity;
import com.sabaq.muse.animation.BackgroundAnimation;
import com.sabaq.muse.utils.Helper;
import com.sabaq.muse.utils.RandomNumber;

import java.io.File;

import static com.sabaq.muse.utils.Helper.CURRENT_CHAPTER_INDEX;
import static com.sabaq.muse.utils.Helper.CURRENT_MAP_ITEM_INDEX;
import static com.sabaq.muse.utils.Helper.CURRENT_SUBJECTID;

public class BaseActivity extends AppCompatActivity {
    // Storage Permissions variables

    boolean isVisible;

    @Override
    protected void onResume() {
        super.onResume();
        View decorView = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public Drawable setProgressBar(String value) {
        switch (value) {
            case "English":
                return getResources().getDrawable(R.drawable.progressbar_green);
            case "Urdu":
                return getResources().getDrawable(R.drawable.progressbar_yellow);
            case "Math":
                return getResources().getDrawable(R.drawable.progressbar_blue);
            case "Science":
                return getResources().getDrawable(R.drawable.progressbar_pink);
            default:
        }
        return null;
    }


    protected void removeBarsFromNavigation(Dialog mDialog) {
        mDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);

//Set the dialog to immersive
        mDialog.getWindow().getDecorView().setSystemUiVisibility(
                getWindow().getDecorView().getSystemUiVisibility());
//Clear the not focusable flag from the window
        mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    public void setBackgroundBarAnimation(Activity activity, String intentValue) {
        // TODO: 9/27/2018 put colors in db
        BackgroundAnimation backgroundAnimation;
        switch (intentValue) {
            case "English":
                backgroundAnimation = new BackgroundAnimation(activity, getResources().getColor(R.color.colorChapterEnglishAnimation), getResources().getColor(R.color.colorChapterEnglishBg));
                backgroundAnimation.animateBackground();
                break;
            case "Urdu":
                backgroundAnimation = new BackgroundAnimation(activity, getResources().getColor(R.color.colorChapterUrduAnimation), getResources().getColor(R.color.colorChapterUrduBg));
                backgroundAnimation.animateBackground();
                break;
            case "Math":
                backgroundAnimation = new BackgroundAnimation(activity, getResources().getColor(R.color.colorChapterMathAnimation), getResources().getColor(R.color.colorChapterMathBg));
                backgroundAnimation.animateBackground();
                break;
            case "Science":
                backgroundAnimation = new BackgroundAnimation(activity, getResources().getColor(R.color.colorChapterScienceAnimation), getResources().getColor(R.color.colorChapterScienceBg));
                backgroundAnimation.animateBackground();
                break;
            default:
        }
    }

    public void onClickHome(View view) {
        startActivity(new Intent(this, SubjectsActivity.class));
        CURRENT_CHAPTER_INDEX = 0;
        CURRENT_MAP_ITEM_INDEX = 0;
        finish();
    }


    /**
     * PageIndicatorView library gives a lots of indicators animation now on every new instance it will generate new random number
     * then it will return AnimationType .
     */
    public PageIndicatorView setSpringDotsIndicatorColor(int color) {
        PageIndicatorView pageIndicatorView = findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setAnimationType(dotIndecatorAnimationType());

        switch (color) {
            case 0:
                pageIndicatorView.setSelectedColor(getResources().getColor(R.color.colorChapterEnglishBg));
                pageIndicatorView.setUnselectedColor(getResources().getColor(R.color.colorChapterEnglishAnimation));
                return pageIndicatorView;
            case 3:
                pageIndicatorView.setSelectedColor(getResources().getColor(R.color.colorChapterUrduBg));
                pageIndicatorView.setUnselectedColor(getResources().getColor(R.color.colorSubjectAnimation));
                return pageIndicatorView;
            case 1:
                pageIndicatorView.setSelectedColor(getResources().getColor(R.color.colorChapterMathBg));
                pageIndicatorView.setUnselectedColor(getResources().getColor(R.color.colorChapterMathAnimation));
                return pageIndicatorView;
            case 2:
                pageIndicatorView.setSelectedColor(getResources().getColor(R.color.colorChapterScienceBg));
                pageIndicatorView.setUnselectedColor(getResources().getColor(R.color.colorChapterScienceAnimation));
                return pageIndicatorView;
            default:
        }
        return pageIndicatorView;
    }

    public void getToolBar(String intentValue, int homeVisibility, int crossVisibility, int hintVisibility, Context context) {

        Toolbar mToolbar = findViewById(R.id.tool_bar);

        setSupportActionBar(mToolbar);
        setSupportActionBar(mToolbar);

        ((TextView) findViewById(R.id.toolBar_title)).setText(intentValue);
        ImageView ivHome = findViewById(R.id.iv_home);
        ivHome.setVisibility(homeVisibility);
        ivHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, SubjectsActivity.class));
                CURRENT_CHAPTER_INDEX = 0;
                CURRENT_MAP_ITEM_INDEX = 0;
            }
        });
        findViewById(R.id.iv_cancel).setVisibility(crossVisibility);
        findViewById(R.id.iv_hint).setVisibility(hintVisibility);

    }

    public void setViewDirection(View view) {
        switch (CURRENT_SUBJECTID) {
            case "Urdu":
                view.setRotationY(180);
                break;
        }
    }


    //persmission method.
    public static void verifyStoragePermissions(Activity activity) {
        String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        final int REQUEST_EXTERNAL_STORAGE = 1;
        // Check if we have read or write permission
        int writePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public void softKeyBoardDownSystemUiHide(int parentId) {
        final View activityRootView = findViewById(parentId);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                //r will be populated with the coordinates of your view that area still visible.
                activityRootView.getWindowVisibleDisplayFrame(r);

                int heightDiff = activityRootView.getRootView().getHeight() - (r.bottom - r.top);
                if (heightDiff > 70) { // if more than 100 pixels, its probably a keyboard...
                    hideSystemUI();
                }
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    // TODO: 11/8/2018 make one function for system ui
    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private AnimationType dotIndecatorAnimationType() {
        return AnimationType.values()[new RandomNumber().generateRandomNumber(9, 1)];
    }

    public void setSoftKeyBoardOffWhenActivityLaunch() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public boolean isFileExist(String filePath) {
        File file = new File(Helper.LOCAL_PATH + filePath);
        if (file.exists()) return true;
        else return false;
    }

}
