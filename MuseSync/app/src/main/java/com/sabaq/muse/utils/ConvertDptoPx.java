package com.sabaq.muse.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

public class ConvertDptoPx {

    Context context; // android.content.Context
    int dp;

    public ConvertDptoPx(Context context, int dp) {
        this.context = context;
        this.dp = dp;
    }

    public int dpToPx() {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
