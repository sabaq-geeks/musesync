package com.sabaq.muse.utils;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sabaq.muse.R;
import com.sabaq.muse.baseActivity.BaseExerciseActivity;
import com.sabaq.muse.templates.ExerciseDragAndDropActivity;

public abstract  class ObjectTouchAndDragListener extends BaseExerciseActivity implements View.OnTouchListener,View.OnDragListener {
    Context context;

    public ObjectTouchAndDragListener() {
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            // create it from the object's tag
            ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());
            String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
            ClipData data = new ClipData("label", mimeTypes, item);
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

            view.startDrag(data, //data to be dragged
                    shadowBuilder, //drag shadow
                    view, //local data about the drag and drop operation
                    0   //no needed flags
            );
            onObjectTouch(view,motionEvent);
            //view.setVisibility(View.INVISIBLE);
            return true;
        } else return false;
    }

    public abstract void onObjectTouch(View view,MotionEvent motionEvent);

    @Override
    public boolean onDrag(View v, DragEvent event) {
        View view = (View) event.getLocalState();
        // Handles each of the expected events
        switch (event.getAction()) {
            //signal for the start of a drag and drop operation.
            case DragEvent.ACTION_DRAG_STARTED:
                // do nothing
                if (view != null) {
                    view.setVisibility(View.INVISIBLE);
                    enableDisableOtherOptionsTouchListeners(view, false);
                }
                break;
            //the drag point has entered the bounding box of the View
            case DragEvent.ACTION_DRAG_ENTERED:
                // v.setBackground(targetShape);    //change the shape of the view
                break;
            //the user has moved the drag shadow outside the bounding box of the View
            case DragEvent.ACTION_DRAG_EXITED:
                // v.setBackground(normalShape);    //change the shape of the view back to normal
                break;
            //drag shadow has been released,the drag point is within the bounding box of the View
            case DragEvent.ACTION_DROP:
                // if the view is the correct view, we accept the drag item
                onContainerDrag(v,view);
                break;
            //the drag and drop operation has concluded.
            case DragEvent.ACTION_DRAG_ENDED:
                //  v.setBackground(normalShape);    //go back to normal shape
                if (view != null) {
                    view.setVisibility(View.VISIBLE);
                    enableDisableOtherOptionsTouchListeners(view, true);
                }
                break;
            default:
                break;
        }
        return true;
    }

    public abstract void onContainerDrag(View v,View view);

    private void enableDisableOtherOptionsTouchListeners(View touchedOption, Boolean toEnable) {
        ViewGroup viewGroup = findViewById(R.id.llOptionsContainer);
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (touchedOption != null) {
                View view = viewGroup.getChildAt(i);
                if (view.getTag(R.string.EX_DRAG_N_DROP_OPTION_INDEX) != null && touchedOption.getTag(R.string.EX_DRAG_N_DROP_OPTION_INDEX)!=null && touchedOption.getTag(R.string.EX_DRAG_N_DROP_OPTION_INDEX).toString() != view.getTag(R.string.EX_DRAG_N_DROP_OPTION_INDEX).toString()) {
                    if (!toEnable) {
                        view.setOnTouchListener(null);
                    } else {
                        view.setOnTouchListener(this);
                    }
                }
            } else {
                Toast.makeText(context, "Touched option null", Toast.LENGTH_SHORT).show();
            }
        }
    }

}