package com.sabaq.muse.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class MediaPlayerUtil {
    private static MediaPlayer mediaPlayer = new MediaPlayer();
    boolean isClicked;
    File fileSound;

    public MediaPlayer playSound(Context context, String audio, String contentType, boolean buttonClicked) {
        isClicked = buttonClicked;
        if (audio != null) {
            fileSound = new File(Helper.LOCAL_PATH, audio);
            if (fileSound.exists()) {
                //     Remove previous sound that why this is for readaloud
                if (contentType.equals("ReadAloud")) {
                    try {
                        mediaPlayer.release();
                        mediaPlayer = null;
                    } catch (Exception e) {
                        Log.i("error Media player", e.getMessage());
                    }
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setVolume(1, 1);
                    try {
                        mediaPlayer.setDataSource(fileSound.getAbsolutePath());
                        mediaPlayer.prepare();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mediaPlayer.setOnPreparedListener(mp -> mediaPlayer.start());
                } else {
                    mediaPlayer = playSoundManger(context, buttonClicked);
                }
            }else{
                Toast.makeText(context, "Audio not found", Toast.LENGTH_SHORT).show();
            }
        }

        return mediaPlayer;
    }

    //This function for exercises
    public MediaPlayer playSoundManger(Context context, boolean buttonClicked) {
        this.isClicked = buttonClicked;
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
        }
        mediaPlayer = MediaPlayer.create(context, Uri.fromFile(fileSound));
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(mp -> {
            isClicked = false;
            Log.d("MediaPlayerUtil", "Sound: false");
        });

        return mediaPlayer;

    }
}
