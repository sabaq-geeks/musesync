package com.sabaq.muse.greenDao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.sabaq.muse.database.DaoMaster;
import com.sabaq.muse.database.DaoSession;

public class AppController {
    // Todo static variable convention

    public static DaoSession daoSession;

    public static DaoSession getDaoSession() {
        return daoSession;
    }


    public void CreateMaster(Context context) {
        // Todo put in constructor
        // TODO: 9/27/2018 call this method in class which is extended by Application
        DatabaseOpenHelper helper = new DatabaseOpenHelper(context, "MUSE-Db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }
}
