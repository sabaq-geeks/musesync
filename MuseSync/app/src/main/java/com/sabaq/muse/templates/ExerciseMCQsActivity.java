package com.sabaq.muse.templates;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.sabaq.muse.R;
import com.sabaq.muse.activities.LessonMapActivity;
import com.sabaq.muse.activities.SubjectsActivity;
import com.sabaq.muse.baseActivity.BaseExerciseActivity;
import com.sabaq.muse.datasource.ExerciseDataFetcher;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.ExerciseUIUtils;
import com.sabaq.muse.utils.Helper;
import com.sabaq.muse.utils.ImageLoad;
import com.sabaq.muse.utils.MediaPlayerUtil;
import com.sabaq.muse.viewModel.ExerciseAnswersViewModel;
import com.sabaq.muse.viewModel.ExerciseQuestionViewModel;
import com.sabaq.muse.viewModel.ExerciseViewModel;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseMCQsActivity extends BaseExerciseActivity {
    private static MediaPlayer multiSoundPlayer = null;
    Context context;
    private boolean isSoundClicked = true;
    List<ExerciseQuestionViewModel> questionViewModels;
    private boolean allClicksDisable = true;
    private ExerciseViewModel exerciseViewModel;
    int questionIndex = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_mcqs);
        context = this;
        startAnimation();
        verifyStoragePermissions(this);
        AppController appController = new AppController();
        appController.CreateMaster(context);
        Long exerciseId = Helper.CURRENT_CONTENTID;
        ExerciseDataFetcher exerciseDataFetcher = new ExerciseDataFetcher();
//        exerciseViewModel = exerciseDataFetcher.getExercisesFromDb(1531);
        exerciseViewModel = exerciseDataFetcher.getExercisesFromDb(exerciseId);
        questionViewModels = exerciseViewModel.getExerciseQuestions();
        generateLayout();
        Boolean isHintAvailable = false;
        if (exerciseViewModel.getInstruction() != null || exerciseViewModel.getInstructionSoundPath() != null)
            isHintAvailable = true;
        getToolBar(exerciseViewModel.getExerciseTitle(), View.VISIBLE, View.VISIBLE, isHintAvailable != null ? View.VISIBLE : View.INVISIBLE, this);
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(
                visibility -> {
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
        );
        softKeyBoardDownSystemUiHide(R.id.main_container);
    }

    public void onClickHint(View view) {
        showHintDialog(this, exerciseViewModel.getInstruction(), exerciseViewModel.getInstructionSoundPath(), exerciseViewModel.getPath());
    }

    public void onClickCross(View view) {
        Intent intent = new Intent(ExerciseMCQsActivity.this, LessonMapActivity.class);
        startActivity(intent);
    }

    public void onClickHome(View view) {

        startActivity(new Intent(ExerciseMCQsActivity.this, SubjectsActivity.class));
        finish();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (multiSoundPlayer != null) {
            multiSoundPlayer.stop();
        }
    }

    private void startAnimation() {
        setBackgroundBarAnimation(this, Helper.CURRENT_SUBJECTID);
    }

    private void setAudio(String audio) {
        if (isSoundClicked == true) {
            multiSoundPlayer = new MediaPlayerUtil().playSound(this, audio, "exercise", false);
            isSoundClicked = false;
            multiSoundPlayer.setOnCompletionListener(mp -> {
                isSoundClicked = true;
            });
        }
    }

    private void playSound(String path, File url) {
        if (isSoundClicked) {
            if (multiSoundPlayer != null) {
                multiSoundPlayer.reset();
                multiSoundPlayer.release();
                isSoundClicked = false;
            }
            if (path != "" || !path.isEmpty())
                multiSoundPlayer = MediaPlayer.create(this, Uri.fromFile(new File(path)));
            if (url != null) multiSoundPlayer = MediaPlayer.create(this, Uri.fromFile(url));
            multiSoundPlayer.start();
            multiSoundPlayer.setOnCompletionListener(mp -> {
                isSoundClicked = true;

                Log.d("MCQ", "Sound: true");
            });
        }

    }

    private void generateLayout() {
        LinearLayout llQuestionContainer = findViewById(R.id.llQuestionsContainer);
        LinearLayout llOptionsContainer = findViewById(R.id.llOptionsContainer);
        llOptionsContainer.removeAllViews();
        llQuestionContainer.removeAllViews();
        if (questionIndex <= questionViewModels.size() - 1)
            loadQuestionAndOptions(llQuestionContainer, llOptionsContainer);
//        else if (questionIndex == questionViewModels.size())
        //getResultScreen();
    }

    private void loadQuestionAndOptions(LinearLayout llQuestionContainer, LinearLayout llOptionsContainer) {
        // Question layout
        ExerciseQuestionViewModel questionViewModel = questionViewModels.get(questionIndex);
        generateQuestion(questionViewModel, llQuestionContainer);
        //Option layout
        generateOptions(questionViewModel, llOptionsContainer);
        //nav
        setNavigation();

    }

    private void setExQuestionInstructionAudioPath(final String instructionAudioPath) {
        ImageView btnExQuestionInstructionSound = findViewById(R.id.ivExQuesInstructionSound);
        btnExQuestionInstructionSound.bringToFront();
        if (instructionAudioPath == null)
            btnExQuestionInstructionSound.setVisibility(View.INVISIBLE);
        btnExQuestionInstructionSound.setOnClickListener(v -> {
            setAudio(instructionAudioPath);
        });
    }

    private void setNavigation() {
        ImageView btnRight = findViewById(R.id.btnRightNav);
        ImageView btnLeft = findViewById(R.id.btnLeftNav);
        if (multiSoundPlayer != null) {
            multiSoundPlayer.stop();
            isSoundClicked = true;
        }
        if (questionIndex == 0)
            btnLeft.setVisibility(View.INVISIBLE);

        else if (questionIndex == questionViewModels.size() - 1)
            btnRight.setVisibility(View.INVISIBLE);

        else {
            btnRight.setVisibility(View.VISIBLE);
            btnLeft.setVisibility(View.VISIBLE);
        }

        btnRight.setOnClickListener(view -> {
            checkIfAnswerCorrect();
            loadNextQuestion(true);

        });
        btnLeft.setOnClickListener(view -> loadNextQuestion(false));
    }

    private void checkIfAnswerCorrect() {
        ArrayList<ExerciseAnswersViewModel> answersViewModels = questionViewModels.get(questionIndex).getExerciseAnswers();
        for (int j = 0; j < answersViewModels.size(); j++) {
            ExerciseAnswersViewModel option = answersViewModels.get(j);
            if (option != null && option.getSelected() != null && option.getSelected() && option.getCorrectAnswer()) {
                questionViewModels.get(questionIndex).setCorrect(true);
                Toast.makeText(context, "Correct", Toast.LENGTH_SHORT).show();
            }

        }
    }

    /**
     * generate question Ui
     *
     * @param questionViewModel
     * @param llQuestionContainer
     */
    private void generateQuestion(ExerciseQuestionViewModel questionViewModel, LinearLayout llQuestionContainer) {
        final String imagePath;
        String text;
        imagePath = questionViewModel.getImagePath();
        text = questionViewModel.getQuestion();

        //setAudioButton
        if (questionViewModel.getInstructionAudioPath() != null)
            setExQuestionInstructionAudioPath(Helper.LOCAL_PATH + '/' + questionViewModel.getInstructionAudioPath());

        final String soundPath = questionViewModel.getAudioPath();
        int weight = 0;

        if (text.trim() != null && !text.trim().equals("-")) {
            LinearLayout llQuestionTextContainer = new LinearLayout(context);
            llQuestionTextContainer.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams llQuestionTextContainerParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llQuestionTextContainer.setLayoutParams(llQuestionTextContainerParams);
            llQuestionContainer.addView(llQuestionTextContainer);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.LEFT;
            params.weight = 1;
            TextView tvQuestion = ExerciseUIUtils.generateTextView(text, context, R.dimen._16sdp, params, Color.WHITE, View.TEXT_ALIGNMENT_CENTER, Gravity.CENTER);
            weight += 1;
            tvQuestion.setOnClickListener(v -> {
                if (soundPath != null)
                    setAudio(soundPath);
            });

            llQuestionTextContainer.addView(tvQuestion);
        }
        if ((text.trim() == null || text.trim().equals("-")) && questionViewModel.getUrduImagePath() != null) {
            final ImageView ivText = new ImageView(context);
            LinearLayout.LayoutParams layoutParams;
            String imgText = questionViewModel.getUrduImagePath();
            if (imagePath == null)
                layoutParams = new LinearLayout.LayoutParams(600, 600);
            else
                layoutParams = new LinearLayout.LayoutParams(300, 300);
            layoutParams.gravity = Gravity.CENTER;
            layoutParams.setMargins(0, 20, 0, 0);
            layoutParams.weight = 1;
            ivText.setOnClickListener(v -> showImageInZoom(this, imgText));
            ivText.setLayoutParams(layoutParams);
            if (imgText != null) {
                new ImageLoad(this)
                        .setImage(imgText, ivText);
            }
            weight += 1;
            llQuestionContainer.addView(ivText);
        }

        if (imagePath != null) {
            final ImageView imgQuestion = generateQuestionIv(ExerciseMCQsActivity.this, imagePath, text, null);
            weight += 1;
            llQuestionContainer.addView(imgQuestion);
        }

        llQuestionContainer.setWeightSum(weight);


    }

    private void loadNextQuestion(boolean isAdded) {
        if (isAdded)
            questionIndex++;
        else
            questionIndex--;
        generateLayout();
    }

    private void generateOptions(ExerciseQuestionViewModel mcqQuestionViewModel, LinearLayout llOptionsContainer) {

        final ArrayList<ExerciseAnswersViewModel> options = mcqQuestionViewModel.getExerciseAnswers();
        if (mcqQuestionViewModel.getFirstTime())
            Collections.shuffle(options);
        mcqQuestionViewModel.setFirstTime(false);
        llOptionsContainer.setWeightSum(options.size());
        final ArrayList<LinearLayout> llOptions = new ArrayList<>();

        for (int i = 0; i < options.size(); i++) {
            final ExerciseAnswersViewModel option = options.get(i);

            if (option.isActive()) {
                ImageView btnSound;
                Boolean isSelected = false;
                isSelected = option.getSelected() != null && option.getSelected();
                final LinearLayout llOption = generatellOptionUI(this);

                if (isSelected)
                    llOption.setBackgroundResource(R.drawable.button_pressed);
                else
                    llOption.setBackgroundResource(R.drawable.button_normal);

                final int index = i;

                if (option.getAudioPath() != null && option.getImagePath() == null && option.getAnswer() == null) {
                    btnSound = generateOptionSoundButton(option);
                    final String path = option.getAudioPath();
                    Log.d("MUSE", "getOptionSoundButton: AudioPath:" + path);
                    btnSound.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setAudio(path);
                        }
                    });
                    llOption.setTag(index);
                    llOptions.add(llOption);
                    btnSound.setOnClickListener(v -> selectOption(llOptions, index, options));
                    llOption.addView(btnSound);

                }
                if (option.getImagePath() != null) {
                    ImageView ivZoom = new ImageView(this);
                    LinearLayout.LayoutParams ivZoomParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                    ivZoomParams.weight = 1;
                    ivZoom.setLayoutParams(ivZoomParams);
                    ivZoom.setPadding(0, 15, 0, 15);
                    ivZoom.setImageResource(R.drawable.ic_zoom);
                    final String img = option.getImagePath();
                    ivZoom.setOnClickListener(v -> showImageInZoom(this, img));
                    String image = option.getImagePath();
                    ImageView ivOption = new ImageView(this);
                    new ImageLoad(this)
                            .setImage(img, ivOption);
                    LinearLayout.LayoutParams ivOptionParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                    ivOptionParams.weight = 4;
                    ivOptionParams.gravity = Gravity.LEFT;
                    ivOption.setPadding(0, 10, 0, 10);
                    ivOption.setLayoutParams(ivOptionParams);
                    new ImageLoad(this)
                            .setImage(img, ivOption);
                    ivOption.setOnClickListener(v -> {
                        selectOption(llOptions, index, options);
                        if (option.getAudioPath() != null) {
                            setAudio(option.getAudioPath());
                        }
                    });
                    llOption.addView(ivZoom);
                    llOption.addView(ivOption);
                    llOption.setWeightSum(5);
                    llOptions.add(llOption);

                } else if (option.getAnswer() != null && !option.getAnswer().equals("-")) {

                    String optionText = option.getAnswer();
                    TextView tvOption = new TextView(context);
                    LinearLayout.LayoutParams tvOptionParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    tvOptionParams.gravity = Gravity.CENTER;
                    tvOption.setLayoutParams(tvOptionParams);
                    tvOption.setTextSize(25);
                    tvOption.setOnClickListener(v -> {
                        selectOption(llOptions, index, options);
                        if (option.getAudioPath() != null)
                            setAudio(option.getAudioPath());
                    });
                    tvOption.setTypeface(Typeface.DEFAULT_BOLD);
                    tvOption.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    tvOption.setText(optionText);
                    llOption.addView(tvOption);
                    llOptions.add(llOption);
                }
                llOptionsContainer.addView(llOption);

            } else if (!option.isActive()) {
                options.remove(i);
            }
        }

    }

    private void selectOption(ArrayList<LinearLayout> llOptions, int index, ArrayList<ExerciseAnswersViewModel> options) {
        for (int i = 0; i < options.size(); i++) {
            if (index == i) {
                llOptions.get(i).setBackgroundResource(R.drawable.button_pressed);
                options.get(i).setSelected(true);
            } else {
                llOptions.get(i).setBackgroundResource(R.drawable.button_normal);
                options.get(i).setSelected(false);
            }
        }

    }


}
