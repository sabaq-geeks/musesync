package com.sabaq.muse.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.sabaq.muse.animation.EnglishBgAnimationView;
import com.sabaq.muse.animation.MathBgAnimationView;
import com.sabaq.muse.animation.ScienceBgAnimationView;
import com.sabaq.muse.animation.UrduBgAnimationView;
import com.sabaq.muse.baseActivity.BaseActivity;
import com.sabaq.muse.R;
import com.sabaq.muse.adapters.ChapterAdapter;
import com.sabaq.muse.adapters.ItemClickListener;
import com.sabaq.muse.database.Topic;
import com.sabaq.muse.database.TopicDao;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.ConvertDptoPx;
import com.sabaq.muse.utils.Helper;
import com.sabaq.muse.viewModel.ChapterViewModel;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

import static com.sabaq.muse.utils.Helper.CURRENT_MAP_ITEM_INDEX;
import static com.sabaq.muse.utils.Helper.CURRENT_SUBJECTID;
import static com.sabaq.muse.utils.Helper.CURRENT_CHAPTER_INDEX;
import static com.sabaq.muse.utils.Helper.CURRRENT_COLOR;

public class ChapterActivity extends BaseActivity implements ItemClickListener {
    private String TAG = "ChapterActivity";

    String GradeId;
    EnglishBgAnimationView englishBgAnimationView;
    MathBgAnimationView mathBgAnimationView;
    ScienceBgAnimationView scienceBgAnimationView;
    UrduBgAnimationView urduBgAnimationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter);
        setChapterScreenRandomBg(Helper.CURRRENT_COLOR);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: " + CURRRENT_COLOR);
        setAnimationOnResume(CURRRENT_COLOR);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: " + CURRRENT_COLOR);
        setAnimationOnPause(CURRRENT_COLOR);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    private void initView() {
        GradeId = getResources().getString(R.string.Grade);
//        subjectIntentValue = getIntent().getStringExtra(getResources().getString(R.string.subject_name));

        getToolBar(CURRENT_SUBJECTID, View.VISIBLE, View.INVISIBLE, View.INVISIBLE, this);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        setViewDirection(recyclerView);
        List<ChapterViewModel> chapterList = new ArrayList<>();

        ChapterAdapter adapter = new ChapterAdapter(this, chapterList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, (new ConvertDptoPx(getApplicationContext(), 10)).dpToPx(), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);
        adapter.setClickListener(this);
        getAllTopics(adapter, chapterList);
        if (CURRENT_CHAPTER_INDEX != 0) {
//            recyclerView.post(() -> recyclerView.scrollToPosition(CURRENT_CHAPTER_INDEX));
//            recyclerView.scrollToPosition(CURRENT_CHAPTER_INDEX);
            ((LinearLayoutManager)recyclerView.getLayoutManager()).scrollToPositionWithOffset(CURRENT_CHAPTER_INDEX,200);
            Log.d(TAG, "scrollToPosition: "+ CURRENT_CHAPTER_INDEX);
        }
    }

    private void getAllTopics(ChapterAdapter adapter, List<ChapterViewModel> chapterList) {
        QueryBuilder<Topic> qbTopic = AppController.daoSession.getTopicDao().queryBuilder();

        List<Topic> topicList = qbTopic
                .where(qbTopic.and(TopicDao.Properties
                                .Active.eq(true),
                        TopicDao.Properties.GradeId.eq(GradeId), TopicDao
                                .Properties.SubjectId.eq(CURRENT_SUBJECTID))).orderAsc(TopicDao.Properties.Order
                ).list();

        //Checking if Topics already exist in Database
        if (topicList.size() > 0) {
            prepareChaptersData(topicList, adapter, chapterList);
        }
    }

    private void prepareChaptersData(List<Topic> topics, ChapterAdapter adapter, List<ChapterViewModel> chapterList) {
        int[] covers = new int[]{R.drawable.demo_image,};
        int image = 0;
        Log.d("ChapterViewModel", "prepareChaptersData: " + CURRENT_SUBJECTID);
        switch (CURRENT_SUBJECTID) {
            case "English":
                image = R.drawable.st1;
                break;
            case "Urdu":
                image = R.drawable.st3;
                break;
            case "Math":
                image = R.drawable.st4;
                break;
            case "Science":
                image = R.drawable.st2;


        }

        for (int i = 0; i < topics.size(); i++) {
            Topic topic = topics.get(i);
            ChapterViewModel chapterViewModel = new ChapterViewModel(topic.getId(), topic.getTopicName(), topic.getDescription(), image, setProgressBar(CURRENT_SUBJECTID));
            chapterList.add(chapterViewModel);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClickChapterItem(View view, int adapterPosition, List<ChapterViewModel> chapterList) {
        ChapterViewModel chapterViewModel = chapterList.get(adapterPosition);
        CURRENT_CHAPTER_INDEX = adapterPosition;
        Intent intent = new Intent(ChapterActivity.this, LessonMapActivity.class);
        Helper.CURRENT_TOPICNAME = chapterViewModel.getTitle();
        Helper.CURRENT_TOPICID = chapterViewModel.getTopicId();
        Log.d("Chapter", "onClickChapterItem: " + adapterPosition);
        startActivity(intent);
        finish();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    public void onClickHome(View view) {
        startActivity(new Intent(ChapterActivity.this, SubjectsActivity.class));
        CURRENT_CHAPTER_INDEX = 0;
        finish();
    }



    public void setChapterScreenRandomBg(int color) {
        Log.d(TAG, "setChapterScreenRandomBg: " + CURRRENT_COLOR);
        switch (color) {
            case 0:
                englishBgAnimationView = findViewById(R.id.animated_english);
                englishBgAnimationView.setVisibility(View.VISIBLE);
                break;
            case 1:
                mathBgAnimationView = findViewById(R.id.animated_math);
                mathBgAnimationView.setVisibility(View.VISIBLE);
                break;
            case 2:
                scienceBgAnimationView = findViewById(R.id.animated_science);
                scienceBgAnimationView.setVisibility(View.VISIBLE);
                break;
            case 3:
                urduBgAnimationView = findViewById(R.id.animated_urdu);
                urduBgAnimationView.setVisibility(View.VISIBLE);
                break;
            default:
        }
    }

    public void setAnimationOnResume(int color) {
        //    setChapterScreenRandomBg(color);
        switch (color) {
            case 0:
                englishBgAnimationView.resume();
                break;
            case 1:
                mathBgAnimationView.resume();
                break;
            case 2:
                scienceBgAnimationView.resume();
                break;
            case 3:
                urduBgAnimationView.resume();
                break;
            default:
        }
    }

    public void setAnimationOnPause(int color) {
        //    setChapterScreenRandomBg(color);
        switch (color) {
            case 0:
                englishBgAnimationView.pause();
                break;
            case 1:
                mathBgAnimationView.pause();
                break;
            case 2:
                scienceBgAnimationView.pause();
                break;
            case 3:
                urduBgAnimationView.pause();
                break;
            default:
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        CURRENT_CHAPTER_INDEX=0;
    }



}