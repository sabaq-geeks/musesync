package com.sabaq.muse.templates;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sabaq.muse.R;
import com.sabaq.muse.baseActivity.BaseExerciseActivity;
import com.sabaq.muse.utils.ObjectTouchAndDragListener;
import com.sabaq.muse.viewModel.ImageModel;

import java.util.ArrayList;
import java.util.Random;

public class DnDObjectsMathOperations extends ObjectTouchAndDragListener {
    private TextView tvTimer;
    private MyCountDownTimer countDownTimer;
    private byte millistimechange;
    private Runnable counter;
    private Handler handler = new Handler();
    private byte startTime = 30, interval = 1;
    int firstOperand = 2;
    int secondOperand = 3;
    char operator = '+';
    LinearLayout objectsContainer;
    ArrayList<ImageModel> gridContainer;
    LinearLayout btnTick;
    int objects[] = {R.drawable.object_dnd_addsub_1, R.drawable.object_dnd_addsub_2, R.drawable.object_dnd_addsub_3, R.drawable.object_dnd_addsub_4, R.drawable.object_dnd_addsub_5, R.drawable.object_dnd_addsub_6, R.drawable.object_dnd_addsub_7, R.drawable.object_dnd_addsub_8, R.drawable.object_dnd_addsub_9};
    int sum = 0;
    static int tagValue = 1;
    int questionIndex;
    int questionLimit = 10;
    Dialog mDialog;
    ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dn_dobjects_math_operations);
        initViews();
        showModeDialog();

    }

    @Override
    public void onObjectTouch(View view, MotionEvent motionEvent) {
    }

    @Override
    public void onContainerDrag(View viewContainer, View droppedView) {
        checkDroppedCondition(viewContainer, droppedView);
    }

    private void showModeDialog() {
        mDialog = showChooseModeDialog(this);
        Button btnTest = mDialog.findViewById(R.id.btnTest);
        Button btnPractice = mDialog.findViewById(R.id.btnPractice);
        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mode = BaseExerciseActivity.MODE.TEST;
                mDialog.dismiss();
                initMode();

            }
        });
        btnPractice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mode = BaseExerciseActivity.MODE.PRACTICE;
                mDialog.dismiss();
                initMode();
            }
        });
    }

    private void initMode() {
        gridContainer = new ArrayList<>();

        if (mode == MODE.PRACTICE) {
            tvTimer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    millistimechange = (byte) (startTime + millistimechange);
                    countDownTimer.Cancel();
                    countDownTimer.Start(millistimechange, interval);
                }
            });


            countDownTimer = new MyCountDownTimer();
            countDownTimer.Start(startTime, interval);
        } else if (mode == MODE.TEST) {

        }
        initQuestion();
        initObjects();
        btnTick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkQuestion();
            }
        });
    }

    private void initViews() {
        objectsContainer = findViewById(R.id.llOptionsContainer);
        tvTimer = findViewById(R.id.tv_timer);
        btnTick = findViewById(R.id.btnTick);
    }

    private void checkQuestion() {
        if (gridContainer.size() == sum) {
            // Toast.makeText(this, "Correct", Toast.LENGTH_LONG).show();
            if (mode == MODE.PRACTICE)
                showTickCross(true, this);

        } else {
            //Toast.makeText(this, "Incorrect", Toast.LENGTH_LONG).show();
            if (mode == MODE.PRACTICE)
                showTickCross(false, this);
        }
        if (mode == MODE.TEST)
            questionIndex++;

        if (mode == MODE.PRACTICE || questionIndex < questionLimit)
            initQuestion();
        else {
            //show dialog

        }
    }

    private void initQuestion() {
        int maxLimitOperand = 9;
        int minLimitOperand = 1;
        final int randomFirstOperand = new Random().nextInt(maxLimitOperand) + minLimitOperand;
        final int randomSecondOperand = new Random().nextInt(maxLimitOperand) + minLimitOperand;
        sum = randomFirstOperand + randomSecondOperand;
        TextView tvFirstOperand = findViewById(R.id.tvFirstOperand);
        tvFirstOperand.setText(randomFirstOperand + "");
        TextView tvSecondOperand = findViewById(R.id.tv_second_operand);
        tvSecondOperand.setText(randomSecondOperand + "");
        TextView tvOperand = findViewById(R.id.tvOperator);
        tvOperand.setText(operator + "");
        setupAnswerContainer();
    }

    private void setupAnswerContainer() {
        GridView gridAnswerContainer = findViewById(R.id.rlAnswerContainer);
        gridContainer.clear();
        gridAnswerContainer.setNumColumns(3);
        gridAnswerContainer.setTag(R.string.dnd_addsub_Container_Type, "gridContainer");
        gridAnswerContainer.setOnDragListener(this);
        imageAdapter = new ImageAdapter(DnDObjectsMathOperations.this, gridContainer);
        gridAnswerContainer.setAdapter(imageAdapter);
//        LinearLayout.LayoutParams llAnswerContainerParams=new LinearLayout.LayoutParams()
    }

    private void initObjects() {
        objectsContainer = findViewById(R.id.llOptionsContainer);
        ViewGroup.LayoutParams objectsContainerParams = objectsContainer.getLayoutParams();
        objectsContainer.setTag(R.string.dnd_addsub_Container_Type, "objectContainer");
        objectsContainer.setOnDragListener(this);
        objectsContainer.setWeightSum(5);
        for (int i = 0; i < 5; i++) {
            LinearLayout llObjectPlaceHolder = new LinearLayout(this);
            LinearLayout.LayoutParams llObjectPlaceHolderParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
            llObjectPlaceHolder.setLayoutParams(llObjectPlaceHolderParams);
            ImageView ivObject = new ImageView(this);
            LinearLayout.LayoutParams ivObjectParams = new LinearLayout.LayoutParams(150, 150);
            llObjectPlaceHolderParams.weight = 1;
            llObjectPlaceHolder.setGravity(Gravity.CENTER_HORIZONTAL);
            llObjectPlaceHolder.setOnTouchListener(this);
            ivObject.setTag("abc");
            ivObject.setTag(R.string.Dnd_addsub_image_index, i);
            llObjectPlaceHolder.setTag(R.string.EX_DRAG_N_DROP_OPTION_INDEX, i);
            llObjectPlaceHolderParams.setMargins(50, 0, 0, 0);
            ivObject.setBackgroundResource(objects[i]);
            ivObject.setLayoutParams(ivObjectParams);
            llObjectPlaceHolder.addView(ivObject);
            objectsContainer.addView(llObjectPlaceHolder);
        }
    }

    /**
     * @param viewContainer view on which it is dropped
     * @param droppedView   item which is dropped
     */

    void checkDroppedCondition(View viewContainer, View droppedView) {
        {
            imageAdapter.notifyDataSetChanged();
            if (droppedView != null) {
                if (viewContainer.getTag(R.string.dnd_addsub_Container_Type).toString() == "objectContainer") {
                    //   Toast.makeText(this, "objects", Toast.LENGTH_SHORT).show();
                    if (droppedView.getTag(R.string.Dnd_addsub_unique_object_id) != null)
                        removeImageViewFromGrid(droppedView);
                } else if (viewContainer.getTag(R.string.dnd_addsub_Container_Type).toString() == "gridContainer") {
                    //Toast.makeText(this, "grid", Toast.LENGTH_SHORT).show();
                    ImageView iv = new ImageView(DnDObjectsMathOperations.this);
                    ImageView droppedImage = null;
                    if (!(droppedView instanceof ImageView)) {
                        ViewGroup viewGroup = (ViewGroup) droppedView;

                        droppedImage = (ImageView) viewGroup.getChildAt(0);
                    } else {
                        droppedImage = (ImageView) droppedView;
                    }
                    Object imageIndexTag = droppedImage.getTag(R.string.Dnd_addsub_image_index);
                    if (imageIndexTag != null) {
                        int tag = Integer.parseInt(imageIndexTag.toString());
                        gridContainer.add(new ImageModel(objects[tag], tagValue, iv));
                        imageAdapter.notifyDataSetChanged();
                        tagValue++;
                    }
                }

            }
            Log.d("MUSE", "checkDroppedCondition: Matched ");


        }

    }

    private void removeImageViewFromGrid(View view) {
        for (int i = 0; i < gridContainer.size(); i++) {
            String imageViewTag = String.valueOf(gridContainer.get(i).getTag());
            if (view.getTag(R.string.Dnd_addsub_unique_object_id).toString() == imageViewTag) {
                gridContainer.remove(i);
                imageAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * Count down timer class
     */
    public class MyCountDownTimer {
        public MyCountDownTimer() {

        }

        /**
         * method call to start timer
         *
         * @param millisInFuture    the (@link byte) it is timer limit
         * @param countDownInterval the (@link byte) it is count down time interval
         */
        public void Start(final byte millisInFuture, final byte countDownInterval) {
            millistimechange = millisInFuture;
            counter = new Runnable() {

                public void run() {
                    if (millistimechange <= 0) {

                        countDownTimer.Cancel();
                        Toast.makeText(DnDObjectsMathOperations.this, "Time's up", Toast.LENGTH_SHORT).show();
                        //showDialog(R.layout.wrong_dialog, R.id.ivwrong, R.id.ivsadfish, R.id.rlwrongdialogscr, R.id.rlwrongdialogbg, false, (byte) 1, R.drawable.grp_wrong_popup_monster_timeup);

                    } else {
                        //GloabalHelper.SoundPoolPlay(sp,soundId,0);
                        tvTimer.setText("" + (millistimechange - 1));
                        millistimechange -= countDownInterval;
                        handler.postDelayed(this, countDownInterval * 1000);
                    }
                }
            };

            handler.postDelayed(counter, countDownInterval * 1000);
        }

        /**
         * method call to cancel timer
         */
        public void Cancel() {
            handler.removeCallbacks(counter);
        }
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ArrayList<ImageModel> objects;

        // Constructor
        public ImageAdapter(Context c, ArrayList<ImageModel> obj) {
            mContext = c;
            this.objects = obj;
        }

        @Override
        public int getCount() {
            return objects.size();
        }

        @Override
        public Object getItem(int position) {
            return objects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView = new ImageView(mContext);
            LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(150, 150);
            imageView.setLayoutParams(llParams);
            llParams.gravity = Gravity.CENTER;
            imageView.setImageResource(objects.get(position).getImage());
            imageView.setTag(R.string.Dnd_addsub_unique_object_id, objects.get(position).getTag());
//            view.setTag(R.string.Dnd_addsub_unique_object_id,tagValue);
            imageView.setOnTouchListener(DnDObjectsMathOperations.this);
            imageView.setTag("abc");

            imageView.setLayoutParams(new GridView.LayoutParams(70, 70));
            return imageView;
        }
    }
}
