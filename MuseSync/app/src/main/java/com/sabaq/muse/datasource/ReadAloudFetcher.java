package com.sabaq.muse.datasource;

import android.content.Context;

import com.sabaq.muse.database.ReadaloudSlide;
import com.sabaq.muse.database.ReadaloudSlideDao;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.InternetDetector;
import com.sabaq.muse.viewModel.ReadAloudViewModel;

import java.util.ArrayList;
import java.util.List;

import static com.sabaq.muse.utils.Helper.LIVE_PATH;


public class ReadAloudFetcher {
    private Context context;

    public ReadAloudFetcher(Context context) {
        this.context = context;
    }

    public List<ReadAloudViewModel> getReadAloudData(Long readAloudId) {
        if (!InternetDetector.checkIntern(context)) {
            return getFromDb(readAloudId);
        } else {
            /** Get Data from api*/
            return getFromApi(readAloudId);
        }
    }

    private List<ReadAloudViewModel> getFromApi(Long readAloudId) {


        List<ReadaloudSlide> readAloudSlide = AppController.daoSession.getReadaloudSlideDao()
                .queryBuilder().where(ReadaloudSlideDao.Properties.ReadaloudId.eq(readAloudId), ReadaloudSlideDao.Properties.Active.eq(true)).orderAsc(ReadaloudSlideDao.Properties.Order).list();
        List<ReadAloudViewModel> readAloudViewModelList = new ArrayList<>();
        for (int i = 0; i < readAloudSlide.size(); i++) {
            readAloudViewModelList.add(new ReadAloudViewModel(readAloudSlide.get(i).getImagePath(), readAloudSlide.get(i).getAudioPath()));
        }
        return readAloudViewModelList;
    }

    private List<ReadAloudViewModel> getFromDb(Long readAloudId) {
        AppController appController = new AppController();
        appController.CreateMaster(context);

        List<ReadaloudSlide> readAloudSlide = AppController.daoSession.getReadaloudSlideDao()
                .queryBuilder().where(ReadaloudSlideDao.Properties.ReadaloudId.eq(readAloudId), ReadaloudSlideDao.Properties.Active.eq(true)).orderAsc(ReadaloudSlideDao.Properties.Order).list();
        List<ReadAloudViewModel> readAloudViewModelList = new ArrayList<>();
        for (int i = 0; i < readAloudSlide.size(); i++) {
            readAloudViewModelList.add(new ReadAloudViewModel(readAloudSlide.get(i).getImagePath(),readAloudSlide.get(i).getAudioPath()));
        }
        return readAloudViewModelList;
    }

    private String convertPathToLocalPath(String path) {
        if (path != null) {
            String[] pathStrings = path.split("/");
            return pathStrings[pathStrings.length - 1];
        } else return "";
    }
}
