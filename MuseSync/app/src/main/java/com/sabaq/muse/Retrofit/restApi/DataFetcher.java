package com.sabaq.muse.Retrofit.restApi;

import android.content.Context;
import android.util.Log;

import com.sabaq.muse.R;
import com.sabaq.muse.database.DaoSession;
import com.sabaq.muse.database.Document;
import com.sabaq.muse.database.Exercise;
import com.sabaq.muse.database.ExerciseAnswer;
import com.sabaq.muse.database.ExerciseQuestion;
import com.sabaq.muse.database.Grade;
import com.sabaq.muse.database.Readaloud;
import com.sabaq.muse.database.ReadaloudSlide;
import com.sabaq.muse.database.ReadaloudSlideAudio;
import com.sabaq.muse.database.School;
import com.sabaq.muse.database.Student;
import com.sabaq.muse.database.SubTopic;
import com.sabaq.muse.database.Subject;
import com.sabaq.muse.database.Topic;
import com.sabaq.muse.database.Video;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.SharedPreference;
import com.sabaq.muse.viewModel.AllDataViewModel;
import com.sabaq.muse.viewModel.TabletViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

public class DataFetcher {
    private DaoSession daoSession;
    private ApiInterface mApiInterface;
    private SharedPreference sharedPreference;
    private Context context;

    public DataFetcher(Context context) {
        daoSession = AppController.daoSession;
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        this.context = context;
        sharedPreference = new SharedPreference(context);
    }

    /*if api response error so onFailure call , if else condition for mapping only as kashir said */

    public void GetAllGrades() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        mApiInterface.GetAllGrades().enqueue(new Callback<List<Grade>>() {
            @Override
            public void onResponse(Call<List<Grade>> call, Response<List<Grade>> response) {

                if (response.isSuccessful()) {
                    List<Grade> grades = response.body();
                    Log.d("AudioPlayer", "Grades loaded from API : " + grades.size());
                    daoSession.getGradeDao().insertOrReplaceInTx(grades);
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_GRADES_LOADED), true);

                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "Grades loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_GRADES_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<List<Grade>> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void GetAllTopics() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        mApiInterface.GetAllTopics().enqueue(new Callback<List<Topic>>() {
            @Override
            public void onResponse(Call<List<Topic>> call, Response<List<Topic>> response) {

                if (response.isSuccessful()) {
                    List<Topic> Topics = response.body();
                    Log.d("AudioPlayer", "Topics loaded from API : " + Topics.size());
                    daoSession.getTopicDao().insertOrReplaceInTx(Topics);
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_TOPICS_LOADED), true);

                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "Topics loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_TOPICS_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<List<Topic>> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void GetAllSubjects() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        mApiInterface.GetAllSubjects().enqueue(new Callback<List<Subject>>() {
            @Override
            public void onResponse(Call<List<Subject>> call, Response<List<Subject>> response) {

                if (response.isSuccessful()) {
                    List<Subject> Subjects = response.body();
                    Log.d("AudioPlayer", "Subjects loaded from API : " + Subjects.size());
                    daoSession.getSubjectDao().insertOrReplaceInTx(Subjects);
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_SUBJECT_LOADED), true);
                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "Subjects loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_SUBJECT_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<List<Subject>> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void GetAllSubTopics() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        mApiInterface.GetAllSubTopics().enqueue(new Callback<List<SubTopic>>() {
            @Override
            public void onResponse(Call<List<SubTopic>> call, Response<List<SubTopic>> response) {
                if (response.isSuccessful()) {
                    List<SubTopic> SubTopics = response.body();
                    Log.d("AudioPlayer", "SubTopics loaded from API : " + SubTopics.size());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_SUB_SUBJECT_LOADED), true);
                } else {
                    int statusCode = response.code();
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_SUB_SUBJECT_LOADED), false);
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<List<SubTopic>> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void GetAllReadalouds() {


        mApiInterface.GetAllReadalouds().enqueue(new Callback<List<Readaloud>>() {
            @Override
            public void onResponse(Call<List<Readaloud>> call, Response<List<Readaloud>> response) {

                if (response.isSuccessful()) {
                    List<Readaloud> Readalouds = response.body();
                    Log.d("AudioPlayer", "Readalouds loaded from API : " + Readalouds.size());
                    daoSession.getReadaloudDao().insertOrReplaceInTx(Readalouds);
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_READALOUDS_LOADED), true);

                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "Readalouds loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_READALOUDS_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<List<Readaloud>> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void GetAllReadaloudSlides() {


        mApiInterface.GetAllReadaloudSlides().enqueue(new Callback<List<ReadaloudSlide>>() {
            @Override
            public void onResponse(Call<List<ReadaloudSlide>> call, Response<List<ReadaloudSlide>> response) {

                if (response.isSuccessful()) {
                    List<ReadaloudSlide> ReadaloudSlides = response.body();
                    Log.d("AudioPlayer", "ReadaloudSlides loaded from API : " + ReadaloudSlides.size());
                    daoSession.getReadaloudSlideDao().insertOrReplaceInTx(ReadaloudSlides);
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_READALOUD_SLIDES_LOADED), true);

                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "ReadaloudSlides loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_READALOUD_SLIDES_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<List<ReadaloudSlide>> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void GetAllReadaloudSlideAudios() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        mApiInterface.GetAllReadaloudSlideAudios().enqueue(new Callback<List<ReadaloudSlideAudio>>() {
            @Override
            public void onResponse(Call<List<ReadaloudSlideAudio>> call, Response<List<ReadaloudSlideAudio>> response) {

                if (response.isSuccessful()) {
                    List<ReadaloudSlideAudio> ReadaloudSlideAudios = response.body();
                    Log.d("AudioPlayer", "ReadaloudSlideAudios loaded from API : " + ReadaloudSlideAudios.size());
                    daoSession.getReadaloudSlideAudioDao().insertOrReplaceInTx(ReadaloudSlideAudios);
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_READALOUD_SLIDES_AUDIO_LOADED), true);

                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "ReadaloudSlideAudios loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_READALOUD_SLIDES_AUDIO_LOADED), false);

                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<List<ReadaloudSlideAudio>> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void GetAllDocuments() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        mApiInterface.GetAllDocuments().enqueue(new Callback<List<Document>>() {
            @Override
            public void onResponse(Call<List<Document>> call, Response<List<Document>> response) {

                if (response.isSuccessful()) {
                    List<Document> Documents = response.body();
                    Log.d("AudioPlayer", "Documents loaded from API : " + Documents.size());
                    daoSession.getDocumentDao().insertOrReplaceInTx(Documents);
                    Log.d("AudioPlayer", "Documents Insertion Completed : " + Documents.size());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_DOCUMENT_LOADED), true);

                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "Documents loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_DOCUMENT_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<List<Document>> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void GetAllVideos() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        mApiInterface.GetAllVideos().enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response) {

                if (response.isSuccessful()) {
                    List<Video> Videos = response.body();
                    daoSession.getVideoDao().insertOrReplaceInTx(Videos);
                    Log.d("AudioPlayer", "Videos Insertion Completed : " + Videos.size());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_VIDEO_LOADED), true);

                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "Videos loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_VIDEO_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<List<Video>> call, Throwable t) {
                Log.d("AudioPlayer", "error loading from API");
            }
        });
    }

    public void GetAllExercises() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        Log.d(TAG, "GetAllExercises: ");
        mApiInterface.GetAllExercises().enqueue(new Callback<List<Exercise>>() {
            @Override
            public void onResponse(Call<List<Exercise>> call, Response<List<Exercise>> response) {

                if (response.isSuccessful()) {
                    List<Exercise> Exercises = response.body();
                    Log.d("AudioPlayer", "Exercises loaded from API : " + Exercises.size());
                    daoSession.getExerciseDao().insertOrReplaceInTx(Exercises);
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_EXERCISES_LOADED), true);

                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "Exercises loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_EXERCISES_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<List<Exercise>> call, Throwable t) {

                Log.d("AudioPlayer", "Exercises error loading from API" + t.getMessage());

            }
        });
    }

    public void GetAllExerciseQuestions() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        mApiInterface.GetAllExerciseQuestions().enqueue(new Callback<List<ExerciseQuestion>>() {
            @Override
            public void onResponse(Call<List<ExerciseQuestion>> call, Response<List<ExerciseQuestion>> response) {

                if (response.isSuccessful()) {
                    List<ExerciseQuestion> ExerciseQuestions = response.body();
                    Log.d("AudioPlayer", "ExerciseQuestions loaded from API : " + ExerciseQuestions.size());
                    daoSession.getExerciseQuestionDao().insertOrReplaceInTx(ExerciseQuestions);
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_EXERCISES_QUESTIONS_LOADED), true);

                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "ExerciseQuestions loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_EXERCISES_QUESTIONS_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<List<ExerciseQuestion>> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void GetAllExerciseAnswers() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        mApiInterface.GetAllExerciseAnswers().enqueue(new Callback<List<ExerciseAnswer>>() {
            @Override
            public void onResponse(Call<List<ExerciseAnswer>> call, Response<List<ExerciseAnswer>> response) {

                if (response.isSuccessful()) {
                    List<ExerciseAnswer> ExerciseAnswers = response.body();
                    Log.d("AudioPlayer", "ExerciseAnswers loaded from API : " + ExerciseAnswers.size());
                    daoSession.getExerciseAnswerDao().insertOrReplaceInTx(ExerciseAnswers);
                    Log.d("AudioPlayer", "ExerciseAnswers Insertion Completed : " + ExerciseAnswers.size());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_EXERCISES_ANSWERS_LOADED), true);
                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "ExerciseAnswers loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_EXERCISES_ANSWERS_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<List<ExerciseAnswer>> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void GetSchool() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        TabletViewModel tabletViewModel = new TabletViewModel();
        tabletViewModel.setMacAddress("abc");
        tabletViewModel.setSchoolCode("Test1");

        mApiInterface.GetSchool(tabletViewModel).enqueue(new Callback<School>() {
            @Override
            public void onResponse(Call<School> call, Response<School> response) {
                if (response.isSuccessful()) {
                    School school = response.body();
                    Log.d("AudioPlayer", "Schools loaded from API : " + school.getId());
                    daoSession.getSchoolDao().insertOrReplaceInTx(school);
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_SCHOOL_LOADED), true);
                    //.insertOrReplaceInTx(school);
                    // Log.d("AudioPlayer", "Students loaded from API : " + students.size());
                    //  daoSession.getStudentDao().insertOrReplaceInTx(students);
                } else {
                    Log.d("AudioPlayer", "Students loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_SCHOOL_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<School> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void GetStudents() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        TabletViewModel tabletViewModel = new TabletViewModel();
        tabletViewModel.setMacAddress("abc");
        tabletViewModel.setSchoolCode("Test1");

        mApiInterface.GetStudents(tabletViewModel).enqueue(new Callback<List<Student>>() {
            @Override
            public void onResponse(Call<List<Student>> call, Response<List<Student>> response) {
                if (response.isSuccessful()) {
                    List<Student> students = response.body();
                    Log.d("AudioPlayer", "Students loaded from API : " + students.size());
                    daoSession.getStudentDao().insertOrReplaceInTx(students);
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_STUDENT_LOADED), true);

                } else {
                    Log.d("AudioPlayer", "Students loaded from API : " + response.code());
                    sharedPreference.saveValueInSharedPreference(context.getResources().getString(R.string.PREF_IS_ALL_STUDENT_LOADED), false);
                }
            }

            @Override
            public void onFailure(Call<List<Student>> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API");

            }
        });
    }

    public void getAllData() {
        ApiInterface mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        Log.d("AudioPlayer", "Api calling Started ");
        mApiInterface.GetCompleteDatabase().enqueue(new Callback<AllDataViewModel>() {
            @Override
            public void onResponse(Call<AllDataViewModel> call, Response<AllDataViewModel> response) {

                if (response.isSuccessful()) {
                    Log.d("AudioPlayer", "Grades loaded from API : ");
                    AllDataViewModel allDataViewModels = response.body();
                    InsertAll(allDataViewModels);


                } else {
                    int statusCode = response.code();
                    Log.d("AudioPlayer", "Grades loaded from API : " + response.code());
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<AllDataViewModel> call, Throwable t) {

                Log.d("AudioPlayer", "error loading from API " + t.getCause() + " " + t.getMessage());

            }
        });
    }


    public void InsertAll(AllDataViewModel AllDataViewModel) {
        Log.d("AudioPlayer", "Insertion Started ");
        if (AllDataViewModel.getGrades() != null) {
            daoSession.getGradeDao().insertInTx(AllDataViewModel.getGrades());
        }
        if (AllDataViewModel.getDocuments() != null) {
            Log.d("AudioPlayer", "getDocuments Started " + AllDataViewModel.getDocuments().size());
            daoSession.getDocumentDao().insertInTx(AllDataViewModel.getDocuments());
        }
        if (AllDataViewModel.getReadalouds() != null) {
            Log.d("AudioPlayer", "getReadalouds Started " + AllDataViewModel.getReadalouds().size());
            daoSession.getReadaloudDao().insertInTx(AllDataViewModel.getReadalouds());
        }
        if (AllDataViewModel.getReadaloudSlides() != null) {
            Log.d("AudioPlayer", "getReadaloudSlides Started " + AllDataViewModel.getReadaloudSlides().size());
            daoSession.getReadaloudSlideDao().insertInTx(AllDataViewModel.getReadaloudSlides());
        }
        if (AllDataViewModel.getReadaloudDictionaries() != null) {
            Log.d("AudioPlayer", "getReadaloudDictionaries Started " + AllDataViewModel.getReadaloudDictionaries().size());
            daoSession.getReadaloudDictionaryDao().insertInTx(AllDataViewModel.getReadaloudDictionaries());
        }
        if (AllDataViewModel.getVideos() != null) {
            Log.d("AudioPlayer", "getVideos Started " + AllDataViewModel.getVideos().size());
            daoSession.getVideoDao().insertInTx(AllDataViewModel.getVideos());
        }
        if (AllDataViewModel.getExercises() != null) {
            Log.d("AudioPlayer", "getExercises Started " + AllDataViewModel.getExercises().size());
            daoSession.getExerciseDao().insertInTx(AllDataViewModel.getExercises());
        }
        if (AllDataViewModel.getExerciseAnswers() != null) {
            Log.d("AudioPlayer", "getExerciseAnswers Started " + AllDataViewModel.getExerciseAnswers().size());
            daoSession.getExerciseAnswerDao().insertInTx(AllDataViewModel.getExerciseAnswers());
        }
        if (AllDataViewModel.getExerciseQuestions() != null) {
            Log.d("AudioPlayer", "getExerciseQuestions Started " + AllDataViewModel.getExerciseQuestions().size());
            daoSession.getExerciseQuestionDao().insertInTx(AllDataViewModel.getExerciseQuestions());
        }
        //Log.d("AudioPlayer", "Insertion Started " + AllDataViewModel.getGrades().size());
        //daoSession.getSubjectDao().insertInTx(AllDataViewModel.getSubjects())   Fix Error

        if (AllDataViewModel.getTopics() != null) {
            Log.d("AudioPlayer", "getTopics Started " + AllDataViewModel.getTopics().size());
            daoSession.getTopicDao().insertInTx(AllDataViewModel.getTopics());
        }
        if (AllDataViewModel.getSubTopics() != null) {
            Log.d("AudioPlayer", "getSubTopics Started " + AllDataViewModel.getSubTopics().size());
            daoSession.getSubTopicDao().insertInTx(AllDataViewModel.getSubTopics());
        }
        Log.d("AudioPlayer", "Insertion Ended ");
    }

}
