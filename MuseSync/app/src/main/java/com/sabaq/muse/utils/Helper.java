package com.sabaq.muse.utils;

import android.content.Context;
import android.os.Environment;

import com.sabaq.muse.R;

public class Helper {
    public static String LIVE_PATH = "http://studio.sabaq.edu.pk";
    public static String LOCAL_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static Long CURRENT_TOPICID;
    public static Long CURRENT_CONTENTID;
    public static String CURRENT_TOPICNAME;
    public static String CURRENT_SUBJECTID;
    public static String CURRENT_LANGUAGE = "English";
    public static int CHAPTER_DECRIPTION_TEXT_SIZE = R.dimen._16sdp;
    public static int CHAPTER_TITLE_TEXT_SIZE = R.dimen._16sdp;
    public static int CURRRENT_COLOR;
    public static int CURRENT_CHAPTER_INDEX;
    public static int CURRENT_MAP_ITEM_INDEX;


    /*DeviceType :true = for tablet ,false =Phone*/
    public static boolean isTablet(Context context) {
        return context.getResources().getBoolean(R.bool.isTablet);
    }
}
