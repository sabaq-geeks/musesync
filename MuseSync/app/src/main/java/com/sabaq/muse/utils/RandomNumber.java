package com.sabaq.muse.utils;

import java.util.Random;

public class RandomNumber {
    public int generateRandomNumber(int max, int min) {
        int random = new Random().nextInt((max - min) + 1) + min;
        return random;
    }
}
