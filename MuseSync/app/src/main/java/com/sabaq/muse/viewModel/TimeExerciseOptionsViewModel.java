package com.sabaq.muse.viewModel;

public class TimeExerciseOptionsViewModel {

    public int hour;
    public int minutes;
    public  boolean isSelected;
    public boolean isCorrect;

    public TimeExerciseOptionsViewModel(int hour, int minutes) {
        this.hour = hour;
        this.minutes = minutes;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }


}
