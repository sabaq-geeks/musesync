package com.sabaq.muse.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.cunoraz.gifview.library.GifView;
import com.sabaq.muse.baseActivity.BaseActivity;
import com.sabaq.muse.R;
import com.sabaq.muse.Retrofit.restApi.DataFetcher;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.GenerateRandomColor;
import com.sabaq.muse.utils.SharedPreference;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        ImageView imageView = findViewById(R.id.iv_icon);
//
//        new GenerateRandomColor(this, findViewById(R.id.cl_main_container)).generateRandomBackgroundColor();
//        animationThread();
//        Glide.with(this)
//                .asGif()
//                .load(R.raw.image_gif)
//                .into(imageView);
//
//        GifView gifView1 = (GifView)findViewById(R.id.gif1);
//        gifView1.play();
//        gifView1.pause();
//        gifView1.setGifResource(R.mipmap.image_gif);
//        gifView1.getGifResource();
//        gifView1.setMovieTime(time);
//        gifView1.getMovie();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    private void startAnimation() {
//        LottieAnimationView animationView = findViewById(R.id.animation_view);
//        animationView.loop(true);
    }


    private void animationThread() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(8600);
                    AppController appController = new AppController();
                    appController.CreateMaster(SplashActivity.this);// set the duration of splash screen
                    startAnimation();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(SplashActivity.this, SubjectsActivity.class);
                    startActivity(intent);
                }
            }
        };
        timer.start();
    }

    private void insertDbDataFromApi(SharedPreference sharedPreference) {
        DataFetcher dataFetcher = new DataFetcher(this);
        dataFetcher.GetStudents();
        dataFetcher.GetSchool();
        dataFetcher.GetAllGrades();
        dataFetcher.GetAllTopics();
        dataFetcher.GetAllSubjects();
        dataFetcher.GetAllSubTopics();
        dataFetcher.GetAllReadalouds();
        dataFetcher.GetAllReadaloudSlides();
        dataFetcher.GetAllReadaloudSlideAudios();
        dataFetcher.GetAllVideos();
        dataFetcher.GetAllExercises();
        dataFetcher.GetAllExerciseQuestions();
        dataFetcher.GetAllExerciseAnswers();
        dataFetcher.GetAllDocuments();
        sharedPreference.saveValueInSharedPreference(sharedPreference.DataLoadedFromApi, true);
    }
}
