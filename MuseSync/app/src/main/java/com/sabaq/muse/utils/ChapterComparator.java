package com.sabaq.muse.utils;

import com.sabaq.muse.viewModel.ChapterViewModel;

import java.util.Comparator;


/**
 * It compares order of two lessonviewmodel objects
 */
public class ChapterComparator implements Comparator<ChapterViewModel> {
    // TODO: 9/27/2018 naming convention
    public int compare(ChapterViewModel c1, ChapterViewModel c2) {
        return ((Integer) c1.getOrder()).compareTo(c2.getOrder());
    }

}