package com.sabaq.muse.syncing;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import syncmanager.sef.com.sefsync.Helper.GlobalHelper;
import syncmanager.sef.com.sefsync.MainService;
import syncmanager.sef.com.sefsync.syncmanager.SyncMethods;

public class SyncActivity extends Activity {
    private Account mAccount;
    private static final String TAG = "tag";
     SyncMethods syncMethod1;
     SyncMethods syncMethod2;
    private int REQUEST_CODE=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(syncmanager.sef.com.sefsync.R.layout.activity_main);

        ////////////
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
        }
        CreateSyncAccount(this);
        startSync();

        startService(new Intent(this,MainService.class));
       finish();
    }

    BroadcastReceiver broadcastReceiver;




    @Override
    protected void onPause() {
        super.onPause();
      /*  try {
           syncMethod2.unregReceiver();
        }
        catch (Exception e){
            Log.d(TAG, "onPause: "+e.getMessage());
        }*/

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void startSync() {
     /*
         * Turn on periodic syncing
         *
         * SECONDS_PER_MINUTE: 60 SECONDS * MINUTES
         */
        mAccount = new Account(
                new GlobalHelper().ACCOUNT, new GlobalHelper().ACCOUNT_TYPE);
        ContentResolver.setSyncAutomatically(mAccount, new GlobalHelper().AUTHORITY, true);
        ContentResolver.addPeriodicSync(
                mAccount,
                new GlobalHelper().AUTHORITY,
                Bundle.EMPTY,
                new GlobalHelper().SYNC_INTERVAL);


    }

    /**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                new GlobalHelper().ACCOUNT, new GlobalHelper().ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            Log.i("Successful:", "Add Account");
        } else {
            Log.i("Error", "Add Account");
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
        }
        return newAccount;
    }


}
