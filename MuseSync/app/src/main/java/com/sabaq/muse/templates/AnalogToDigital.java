package com.sabaq.muse.templates;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sabaq.muse.R;
import com.sabaq.muse.database.ExerciseQuestion;
import com.sabaq.muse.viewModel.ExerciseAnswersViewModel;
import com.sabaq.muse.viewModel.ExerciseQuestionViewModel;
import com.sabaq.muse.viewModel.TimeExerciseOptionsViewModel;
import com.sabaq.muse.viewModel.TimeExerciseViewModel;


import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;
/*

public class AnalogToDigital extends BaseExerciseActivity {
    Context context;
    private int questionIndex=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analog_to_digital);
        context = this;

        mode=MODE.PRACTICE;

        initQuestion();
    }

    private void initQuestion() {
        int maxLimit = 9;
        int minLimit = 1;
        final int randomHour = new Random().nextInt(maxLimit) + minLimit;
        final int randomMinutes = new Random().nextInt(maxLimit) + minLimit;
        TimeExerciseViewModel questionViewModel=new TimeExerciseViewModel();
        questionViewModel.setHour(randomHour);
        questionViewModel.setQuestionAnalog(true);
        questionViewModel.setOptionsAnalog(false);
        initTime(randomHour,randomMinutes);
    }

    private void initTime(int randomHr,int randomMinutes) {
        Calendar calendar = Calendar.getInstance();
        //  calendar.add(Calendar.HOUR, -2);
        calendar.set(Calendar.HOUR_OF_DAY, randomHr);
        calendar.set(Calendar.MINUTE, randomMinutes);
        calendar.set(Calendar.SECOND, 00);
        initClock(calendar);
    }

    private void initClock(Calendar calendar) {
        CustomAnalogClock customAnalogClock =  findViewById(R.id.analog_clock);
        customAnalogClock.setAutoUpdate(false);
        customAnalogClock.setTime(calendar);
        customAnalogClock.setFace(R.drawable.clock_face);
    }

    private void generateLayout() {
        LinearLayout llQuestionContainer = findViewById(R.id.llQuestionsContainer);
        LinearLayout llOptionsContainer = findViewById(R.id.llOptionsContainer);
        llOptionsContainer.removeAllViews();
        llQuestionContainer.removeAllViews();
        if (questionIndex <= MAXQUESTIONS)
            loadQuestionAndOptions(llQuestionContainer, llOptionsContainer);
//        else if (questionIndex == questionViewModels.size())
        //getResultScreen();
    }

    private void loadQuestionAndOptions(LinearLayout llQuestionContainer, LinearLayout llOptionsContainer) {
        // Question layout
        ExerciseQuestionViewModel questionViewModel = questionViewModels.get(questionIndex);
        generateQuestion(questionViewModel, llQuestionContainer);
        //Option layout
        generateOptions(questionViewModel, llOptionsContainer);
        //nav
        setNavigation();

    }

    */
/**
     * generate question Ui
     *
     * @param questionViewModel
     * @param llQuestionContainer
     *//*

    private void generateQuestion(ExerciseQuestionViewModel questionViewModel, LinearLayout llQuestionContainer) {
        String imagePath;
        String text;
        imagePath = questionViewModel.getImagePath();
        text = questionViewModel.getQuestion();

        //setAudioButton
//        if (questionViewModel.getInstructionAudioPath() != null)
//            setExQuestionInstructionAudioPath(SOUND_PATH + '/' + questionViewModel.getInstructionAudioPath());

        final String soundPath = questionViewModel.getAudioPath();
        int weight = 0;

        if (text.trim() != null && !text.trim().equals("-")) {

            TextView tvQuestion = new TextView(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER;
            params.weight = 1;
            weight += 1;
            tvQuestion.setLayoutParams(params);
            tvQuestion.setText(text);
            tvQuestion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  if (soundPath != null)
                      //  playSound(SOUND_PATH + '/' + soundPath);
                }
            });
            tvQuestion.setMinimumWidth(200);
            tvQuestion.setTypeface(Typeface.DEFAULT_BOLD);
            tvQuestion.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            tvQuestion.setTextSize(25);
          */
/*  Typeface type = Typeface.createFromAsset(getAssets(), "Jameel_Noori_Nastaleeq.ttf");
            tvQuestion.setTypeface(type);*//*

            tvQuestion.setTextColor(Color.WHITE);
            llQuestionContainer.addView(tvQuestion);
        }
        if (imagePath != null) {

            ImageView imgQuestion = new ImageView(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.gravity = Gravity.CENTER;
            layoutParams.setMargins(0, 20, 0, 0);
            layoutParams.weight = 1;
            weight += 1;
            imgQuestion.setLayoutParams(layoutParams);
            imgQuestion.setImageURI(Uri.fromFile(new File(IMAGE_PATH + '/' + imagePath)));
            llQuestionContainer.addView(imgQuestion);

        }

        llQuestionContainer.setWeightSum(weight);


    }

    private void generateOptions(TimeExerciseViewModel mcqQuestionView, LinearLayout llOptionsContainer) {

        ArrayList<TimeExerciseOptionsViewModel> exerciseOptionsViewModels=new ArrayList<>();

        for (int i = 0; i <3; i++) {
        exerciseOptionsViewModels.add(new TimeExerciseOptionsViewModel(2,2));

        }

        final ArrayList<TimeExerciseOptionsViewModel> options = exerciseOptionsViewModels;
        Collections.shuffle(exerciseOptionsViewModels);
        llOptionsContainer.setWeightSum(options.size());
        Log.d("Muse", "generateOptions: option size " + options.size());
        final ArrayList<LinearLayout> llOptions = new ArrayList<>();

        for (int i = 0; i < options.size(); i++) {
            final TimeExerciseOptionsViewModel option = options.get(i);

            final LinearLayout llOption = getllOption();
            final int index = i;


            if (option.getHour() !=0) {

                int hour = option.getHour();
                int mins=option.getMinutes();
                TextView tvOption = new TextView(context);
                LinearLayout.LayoutParams tvOptionParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                tvOptionParams.gravity = Gravity.CENTER;
                tvOption.setLayoutParams(tvOptionParams);
                tvOption.setTextSize(25);
                tvOption.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectOption(llOptions, index, options);
                        //if (option.getAudioPath() != null)
                        //playSound(SOUND_PATH + "/" + option.getAudioPath());
                    }
                });
                tvOption.setTypeface(Typeface.DEFAULT_BOLD);
                tvOption.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                tvOption.setText(hour+":"+mins+" PM");
                llOption.addView(tvOption);
                llOptions.add(llOption);

            }
            llOptionsContainer.addView(llOption);

        }

    }

    private void selectOption(ArrayList<LinearLayout> llOptions, int index, ArrayList<TimeExerciseOptionsViewModel> options) {
        for (int i = 0; i < options.size(); i++) {
            if (index == i) {
                llOptions.get(i).setBackgroundResource(R.drawable.button_pressed);
                options.get(i).setSelected(true);
            } else {
                llOptions.get(i).setBackgroundResource(R.drawable.button_normal);
                options.get(i).setSelected(false);
            }
        }
    }

    @NonNull
    private LinearLayout getllOption() {
        LinearLayout llOption = new LinearLayout(context);
        llOption.setBackgroundResource(R.drawable.button_normal);
        llOption.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams llOptionparams = new LinearLayout.LayoutParams(0, 100);
        llOptionparams.gravity = Gravity.CENTER;
        llOptionparams.weight = 1;
        llOptionparams.setMarginStart(22);
        llOption.setLayoutParams(llOptionparams);
        return llOption;
    }


}
*/
