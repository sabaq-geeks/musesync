package com.sabaq.muse.viewModel;

import java.util.List;

public class TimeExerciseViewModel {

    public int hour;
    public int minutes;
    public boolean isQuestionAnalog;
    public boolean isOptionsAnalog;

    public List<TimeExerciseOptionsViewModel> optionsViewModels;


    public List<TimeExerciseOptionsViewModel> getOptionsViewModels() {
        return optionsViewModels;
    }

    public void setOptionsViewModels(List<TimeExerciseOptionsViewModel> optionsViewModels) {
        this.optionsViewModels = optionsViewModels;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public boolean isQuestionAnalog() {
        return isQuestionAnalog;
    }

    public void setQuestionAnalog(boolean questionAnalog) {
        isQuestionAnalog = questionAnalog;
    }

    public boolean isOptionsAnalog() {
        return isOptionsAnalog;
    }

    public void setOptionsAnalog(boolean optionsAnalog) {
        isOptionsAnalog = optionsAnalog;
    }
}
