package com.sabaq.muse.database;

import org.greenrobot.greendao.annotation.*;

import java.util.List;
import com.sabaq.muse.database.DaoSession;
import org.greenrobot.greendao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "ExerciseQuestions".
 */
@Entity(active = true, nameInDb = "ExerciseQuestions")
public class ExerciseQuestion {

    @Id(autoincrement = true)
    @Property(nameInDb = "Id")
    private Long Id;

    @Property(nameInDb = "Active")
    private Boolean Active;

    @Property(nameInDb = "CreatedDate")
    private java.util.Date CreatedDate;

    @Property(nameInDb = "CreatedBy")
    private String CreatedBy;

    @Property(nameInDb = "UpdatedDate")
    private java.util.Date UpdatedDate;

    @Property(nameInDb = "UpdatedBy")
    private String UpdatedBy;

    @Property(nameInDb = "ExerciseId")
    private Long ExerciseId;

    @Property(nameInDb = "ExerciseTypeId")
    private String ExerciseTypeId;

    @Property(nameInDb = "TranslationImagePath")
    private String TranslationImagePath;

    @Property(nameInDb = "Question")
    private String Question;

    @Property(nameInDb = "UrduTitleImagePath")
    private String UrduTitleImagePath;

    @Property(nameInDb = "UrduQuestion")
    private String UrduQuestion;

    @Property(nameInDb = "SindhiQuestion")
    private String SindhiQuestion;

    @Property(nameInDb = "ImagePath")
    private String ImagePath;

    @Property(nameInDb = "UrduImagePath")
    private String UrduImagePath;

    @Property(nameInDb = "AudioPath")
    private String AudioPath;

    @Property(nameInDb = "Instruction")
    private String Instruction;

    @Property(nameInDb = "InstructionAudioPath")
    private String InstructionAudioPath;

    @Property(nameInDb = "InstructionImagePath")
    private String InstructionImagePath;

    @Property(nameInDb = "ApplicationUserId")
    private String ApplicationUserId;

    @Property(nameInDb = "TranslationImageSize")
    private Float TranslationImageSize;

    @Property(nameInDb = "ImageSize")
    private Float ImageSize;

    @Property(nameInDb = "UrduImageSize")
    private Float UrduImageSize;

    @Property(nameInDb = "AudioSize")
    private Float AudioSize;

    @Property(nameInDb = "InstructionAudioSize")
    private Float InstructionAudioSize;

    @Property(nameInDb = "InstructionImageSize")
    private Float InstructionImageSize;

    @Property(nameInDb = "Order")
    private Integer Order;

    @Property(nameInDb = "TextboxShow")
    private Boolean TextboxShow;

    /** Used to resolve relations */
    @Generated
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated
    private transient ExerciseQuestionDao myDao;

    @ToOne(joinProperty = "ExerciseId")
    private Exercise exercise;

    @Generated
    private transient Long exercise__resolvedKey;

    @ToMany(joinProperties = {
        @JoinProperty(name = "Id", referencedName = "ExerciseQuestionId")
    })
    private List<ExerciseAnswer> exerciseAnswers;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public ExerciseQuestion() {
    }

    public ExerciseQuestion(Long Id) {
        this.Id = Id;
    }

    @Generated
    public ExerciseQuestion(Long Id, Boolean Active, java.util.Date CreatedDate, String CreatedBy, java.util.Date UpdatedDate, String UpdatedBy, Long ExerciseId, String ExerciseTypeId, String TranslationImagePath, String Question, String UrduTitleImagePath, String UrduQuestion, String SindhiQuestion, String ImagePath, String UrduImagePath, String AudioPath, String Instruction, String InstructionAudioPath, String InstructionImagePath, String ApplicationUserId, Float TranslationImageSize, Float ImageSize, Float UrduImageSize, Float AudioSize, Float InstructionAudioSize, Float InstructionImageSize, Integer Order, Boolean TextboxShow) {
        this.Id = Id;
        this.Active = Active;
        this.CreatedDate = CreatedDate;
        this.CreatedBy = CreatedBy;
        this.UpdatedDate = UpdatedDate;
        this.UpdatedBy = UpdatedBy;
        this.ExerciseId = ExerciseId;
        this.ExerciseTypeId = ExerciseTypeId;
        this.TranslationImagePath = TranslationImagePath;
        this.Question = Question;
        this.UrduTitleImagePath = UrduTitleImagePath;
        this.UrduQuestion = UrduQuestion;
        this.SindhiQuestion = SindhiQuestion;
        this.ImagePath = ImagePath;
        this.UrduImagePath = UrduImagePath;
        this.AudioPath = AudioPath;
        this.Instruction = Instruction;
        this.InstructionAudioPath = InstructionAudioPath;
        this.InstructionImagePath = InstructionImagePath;
        this.ApplicationUserId = ApplicationUserId;
        this.TranslationImageSize = TranslationImageSize;
        this.ImageSize = ImageSize;
        this.UrduImageSize = UrduImageSize;
        this.AudioSize = AudioSize;
        this.InstructionAudioSize = InstructionAudioSize;
        this.InstructionImageSize = InstructionImageSize;
        this.Order = Order;
        this.TextboxShow = TextboxShow;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getExerciseQuestionDao() : null;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean Active) {
        this.Active = Active;
    }

    public java.util.Date getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(java.util.Date CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public java.util.Date getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(java.util.Date UpdatedDate) {
        this.UpdatedDate = UpdatedDate;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String UpdatedBy) {
        this.UpdatedBy = UpdatedBy;
    }

    public Long getExerciseId() {
        return ExerciseId;
    }

    public void setExerciseId(Long ExerciseId) {
        this.ExerciseId = ExerciseId;
    }

    public String getExerciseTypeId() {
        return ExerciseTypeId;
    }

    public void setExerciseTypeId(String ExerciseTypeId) {
        this.ExerciseTypeId = ExerciseTypeId;
    }

    public String getTranslationImagePath() {
        return TranslationImagePath;
    }

    public void setTranslationImagePath(String TranslationImagePath) {
        this.TranslationImagePath = TranslationImagePath;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String Question) {
        this.Question = Question;
    }

    public String getUrduTitleImagePath() {
        return UrduTitleImagePath;
    }

    public void setUrduTitleImagePath(String UrduTitleImagePath) {
        this.UrduTitleImagePath = UrduTitleImagePath;
    }

    public String getUrduQuestion() {
        return UrduQuestion;
    }

    public void setUrduQuestion(String UrduQuestion) {
        this.UrduQuestion = UrduQuestion;
    }

    public String getSindhiQuestion() {
        return SindhiQuestion;
    }

    public void setSindhiQuestion(String SindhiQuestion) {
        this.SindhiQuestion = SindhiQuestion;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String ImagePath) {
        this.ImagePath = ImagePath;
    }

    public String getUrduImagePath() {
        return UrduImagePath;
    }

    public void setUrduImagePath(String UrduImagePath) {
        this.UrduImagePath = UrduImagePath;
    }

    public String getAudioPath() {
        return AudioPath;
    }

    public void setAudioPath(String AudioPath) {
        this.AudioPath = AudioPath;
    }

    public String getInstruction() {
        return Instruction;
    }

    public void setInstruction(String Instruction) {
        this.Instruction = Instruction;
    }

    public String getInstructionAudioPath() {
        return InstructionAudioPath;
    }

    public void setInstructionAudioPath(String InstructionAudioPath) {
        this.InstructionAudioPath = InstructionAudioPath;
    }

    public String getInstructionImagePath() {
        return InstructionImagePath;
    }

    public void setInstructionImagePath(String InstructionImagePath) {
        this.InstructionImagePath = InstructionImagePath;
    }

    public String getApplicationUserId() {
        return ApplicationUserId;
    }

    public void setApplicationUserId(String ApplicationUserId) {
        this.ApplicationUserId = ApplicationUserId;
    }

    public Float getTranslationImageSize() {
        return TranslationImageSize;
    }

    public void setTranslationImageSize(Float TranslationImageSize) {
        this.TranslationImageSize = TranslationImageSize;
    }

    public Float getImageSize() {
        return ImageSize;
    }

    public void setImageSize(Float ImageSize) {
        this.ImageSize = ImageSize;
    }

    public Float getUrduImageSize() {
        return UrduImageSize;
    }

    public void setUrduImageSize(Float UrduImageSize) {
        this.UrduImageSize = UrduImageSize;
    }

    public Float getAudioSize() {
        return AudioSize;
    }

    public void setAudioSize(Float AudioSize) {
        this.AudioSize = AudioSize;
    }

    public Float getInstructionAudioSize() {
        return InstructionAudioSize;
    }

    public void setInstructionAudioSize(Float InstructionAudioSize) {
        this.InstructionAudioSize = InstructionAudioSize;
    }

    public Float getInstructionImageSize() {
        return InstructionImageSize;
    }

    public void setInstructionImageSize(Float InstructionImageSize) {
        this.InstructionImageSize = InstructionImageSize;
    }

    public Integer getOrder() {
        return Order;
    }

    public void setOrder(Integer Order) {
        this.Order = Order;
    }

    public Boolean getTextboxShow() {
        return TextboxShow;
    }

    public void setTextboxShow(Boolean TextboxShow) {
        this.TextboxShow = TextboxShow;
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public Exercise getExercise() {
        Long __key = this.ExerciseId;
        if (exercise__resolvedKey == null || !exercise__resolvedKey.equals(__key)) {
            __throwIfDetached();
            ExerciseDao targetDao = daoSession.getExerciseDao();
            Exercise exerciseNew = targetDao.load(__key);
            synchronized (this) {
                exercise = exerciseNew;
            	exercise__resolvedKey = __key;
            }
        }
        return exercise;
    }

    @Generated
    public void setExercise(Exercise exercise) {
        synchronized (this) {
            this.exercise = exercise;
            ExerciseId = exercise == null ? null : exercise.getId();
            exercise__resolvedKey = ExerciseId;
        }
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    @Generated
    public List<ExerciseAnswer> getExerciseAnswers() {
        if (exerciseAnswers == null) {
            __throwIfDetached();
            ExerciseAnswerDao targetDao = daoSession.getExerciseAnswerDao();
            List<ExerciseAnswer> exerciseAnswersNew = targetDao._queryExerciseQuestion_ExerciseAnswers(Id);
            synchronized (this) {
                if(exerciseAnswers == null) {
                    exerciseAnswers = exerciseAnswersNew;
                }
            }
        }
        return exerciseAnswers;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated
    public synchronized void resetExerciseAnswers() {
        exerciseAnswers = null;
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void delete() {
        __throwIfDetached();
        myDao.delete(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void update() {
        __throwIfDetached();
        myDao.update(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void refresh() {
        __throwIfDetached();
        myDao.refresh(this);
    }

    @Generated
    private void __throwIfDetached() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
