package com.sabaq.muse.templates;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sabaq.muse.R;
import com.sabaq.muse.activities.LessonMapActivity;
import com.sabaq.muse.activities.SubjectsActivity;
import com.sabaq.muse.baseActivity.BaseExerciseActivity;
import com.sabaq.muse.datasource.ExerciseDataFetcher;
import com.sabaq.muse.utils.ConvertDptoPx;
import com.sabaq.muse.utils.ExerciseUIUtils;
import com.sabaq.muse.utils.Helper;
import com.sabaq.muse.utils.ImageLoad;
import com.sabaq.muse.utils.MediaPlayerUtil;
import com.sabaq.muse.viewModel.ExerciseAnswersViewModel;
import com.sabaq.muse.viewModel.ExerciseQuestionViewModel;
import com.sabaq.muse.viewModel.ExerciseViewModel;

import java.io.File;
import java.util.List;

import static com.sabaq.muse.utils.Helper.LOCAL_PATH;

public class FillInTheBlankInputEnglish extends BaseExerciseActivity {
    List<ExerciseQuestionViewModel> questionViewModels;
    ExerciseViewModel exerciseViewModel;
    String answer;
    private static MediaPlayer multiSoundPlayer = null;
    private boolean isSoundClicked = true;
    int questionIndex = 0;
     EditText etAnswerInput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_in_the_blank_input_english);
        Long exerciseId = Helper.CURRENT_CONTENTID;
        dataFetchFromDb(exerciseId);
        getToolBar(exerciseViewModel.getExerciseTitle(), View.VISIBLE, View.VISIBLE, View.VISIBLE, this);
        Toolbar toolbar = findViewById(R.id.tool_bar);
        setBackgroundBarAnimation(this, Helper.CURRENT_SUBJECTID);
        loadLayout();
        softKeyBoardDownSystemUiHide(R.id.cl_container);
        setSoftKeyBoardOffWhenActivityLaunch();
        isTablet();
        hideToolbarOnVisibleKeyboard(toolbar);

    }

    public void onClickHome(View view) {

        startActivity(new Intent(FillInTheBlankInputEnglish.this, SubjectsActivity.class));
        finish();
    }

    public void onClickDone(View view) {
        startActivity(new Intent(FillInTheBlankInputEnglish.this, LessonMapActivity.class));
    }

    public void onClickHint(View view) {
        showHintDialog(this, exerciseViewModel.getInstruction(), exerciseViewModel.getInstructionSoundPath(), exerciseViewModel.getPath());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (multiSoundPlayer != null) {
            multiSoundPlayer.stop();
            isSoundClicked = true;
        }
    }

    private void hideToolbarOnVisibleKeyboard(Toolbar toolbar) {
        final View activityRootView = findViewById(R.id.cl_container);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect rectangle = new Rect();
            //r will be populated with the coordinates of your view that area still visible.
            activityRootView.getWindowVisibleDisplayFrame(rectangle);

            int heightDiff = activityRootView.getRootView().getHeight() - (rectangle.bottom - rectangle.top);
            if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
                toolbar.setVisibility(View.INVISIBLE);
            } else {
                toolbar.setVisibility(View.VISIBLE);
            }
        });
    }

    private void dataFetchFromDb(Long exerciseId) {
        ExerciseDataFetcher exerciseDataFetcher = new ExerciseDataFetcher();
        exerciseViewModel = exerciseDataFetcher.getExercisesFromDb(exerciseId);
        questionViewModels = exerciseViewModel.getExerciseQuestions();
    }

    private LinearLayout generateContainer() {
        LinearLayout llHorizontalContainer = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llHorizontalContainer.setGravity(Gravity.CENTER | Gravity.LEFT);
        llHorizontalContainer.setLayoutParams(layoutParams);
        return llHorizontalContainer;
    }

    private TextView generateTextView(String text) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER | Gravity.LEFT;
        TextView tvQuestionWord = ExerciseUIUtils.generateTextView(text + " ", FillInTheBlankInputEnglish.this, R.dimen._16sdp, params, Color.WHITE, View.TEXT_ALIGNMENT_CENTER, Gravity.CENTER);
        return tvQuestionWord;
    }

    private LinearLayout generateLinearEditText(String text, List<ExerciseAnswersViewModel> answersViewModelsList) {
        LinearLayout llEditText = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Log.d("Question", "generateQuestion: " + text);
       etAnswerInput = generateEditText();
        etAnswerInput.setText(loadAnswer(0));
//        if (questionIndex > 0) {
//            answersViewModelsList.get(0).setSelectedAnswer(etAnswerInput.getText().toString());
//        }
        etAnswerInput.setOnFocusChangeListener((view, hasFocus) -> {
            Log.d("onFocusChange", questionIndex + "");
            /*When First question generate it will not show*/

            answersViewModelsList.get(0).setSelectedAnswer(etAnswerInput.getText().toString());
            String a = answersViewModelsList.get(0).getSelectedAnswer();
        });
        etAnswerInput.setOnClickListener(v -> {

        });
        llEditText.addView(etAnswerInput);
        llEditText.setLayoutParams(layoutParams);
        return llEditText;
    }

    private void generateQuestion(ExerciseQuestionViewModel questionViewModel, LinearLayout llQuestionContainer) {
        final String imagePath;
        final String audioPath;
        final String questionInstructionPath;
        File fileSound = null;
        File fileInstructionSound = null;
        String text;
        imagePath = questionViewModel.getImagePath();
        audioPath = questionViewModel.getAudioPath();
        questionInstructionPath = questionViewModel.getInstructionAudioPath();
        text = questionViewModel.getQuestion();

        if (!TextUtils.isEmpty(audioPath)) {
            fileSound = new File(LOCAL_PATH, audioPath);
        }
        if (!TextUtils.isEmpty(questionInstructionPath)) {
            fileInstructionSound = new File(LOCAL_PATH, questionInstructionPath);
        }

        //setAudioButton
        if (!TextUtils.isEmpty(questionInstructionPath) && fileInstructionSound.exists()) {
            setExQuestionInstructionAudioPath(questionInstructionPath);
        }

        int weight = 0;
        String convertedQuestion = text.replace("$", " $ ");
        String[] splittedQuestionArray = convertedQuestion.split(" ");

        int lineCount = ((convertedQuestion + " " + answer).length() / 40) + 1;

        //Contains all the the answers being put in Edit Text Box
        String[] answerText = new String[dollarCount(convertedQuestion)];
        for (int i = 0; i < dollarCount(convertedQuestion); i++) {
            answerText[i] = answer;
        }

        LinearLayout llHorizontalContainer = null;
        int breakLineLength = 40;
        int k = 0;

        //Defines working at which dollar sign , first or second
        int answerBox = 0;

        for (int i = 0; i < lineCount; i++) {
            //Defines how many Char in current horizontal Line
            int horizontalLineTextCount = 0;

            //Add Horizontal Container to the View
            llHorizontalContainer = generateContainer();
            llQuestionContainer.addView(llHorizontalContainer);
            final File finalFileSound = fileSound;
            llQuestionContainer.setOnClickListener(v -> {
                if (!TextUtils.isEmpty(audioPath) && finalFileSound.exists()) {
                    setAudio(audioPath);
                }
            });

            for (; k < splittedQuestionArray.length; k++) {
                //For Answer Edit Text Box
                if (splittedQuestionArray[k].equals("$")) {
                    //Incase to be added in current line
                    if (breakLineLength - horizontalLineTextCount > 5) {
                        llHorizontalContainer.addView(generateLinearEditText(answerText[answerBox], questionViewModel.getExerciseAnswers()));
                        horizontalLineTextCount += answerText[answerBox].length();
                    }
                    //If only 5 spaces left, then to be added in next line
                    else {
                        llHorizontalContainer = generateContainer();
                        llQuestionContainer.addView(llHorizontalContainer);
                        llHorizontalContainer.addView(generateLinearEditText(answerText[answerBox], questionViewModel.getExerciseAnswers()));
                        horizontalLineTextCount += answerText[answerBox].length();
                    }
                    answerBox++;
                    continue;
                }
                //For Simple Question Text
                if (horizontalLineTextCount + splittedQuestionArray[k].length() < breakLineLength) {
                    splittedQuestionArray[k] = splittedQuestionArray[k] + " ";

                    llHorizontalContainer.addView(generateTextView(splittedQuestionArray[k]));
                    horizontalLineTextCount += splittedQuestionArray[k].length();
                } else break;
            }
        }

        if (imagePath != null) {

            ImageView imgQuestion = new ImageView(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.gravity = Gravity.CENTER;
            layoutParams.setMargins(0, 20, 0, 0);
            layoutParams.weight = 1;
            weight += 1;
            imgQuestion.setLayoutParams(layoutParams);
            new ImageLoad(this)
                    .setImage(imagePath, imgQuestion);
            imgQuestion.setOnClickListener(view -> showImageInZoom(FillInTheBlankInputEnglish.this, imagePath));
            llQuestionContainer.addView(imgQuestion);
        }
        llQuestionContainer.setWeightSum(weight);
    }

    private int dollarCount(String question) {
        int counter = 0;
        for (int i = 0; i < question.length(); i++) {
            if (question.charAt(i) == '$') {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Max length of edit text input= answer.length() +5
     */
    @NonNull
    private EditText generateEditText() {
        EditText etQuestion = new EditText(this);
        int etheight;
        if (isTablet())
            etheight = 70;
        else etheight = 50;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, new ConvertDptoPx(this, etheight).dpToPx());
        params.gravity = Gravity.TOP;

        etQuestion.setMaxHeight(new ConvertDptoPx(this, 150).dpToPx());
        etQuestion.setMinWidth(new ConvertDptoPx(this, 150).dpToPx());
        etQuestion.setGravity(Gravity.CENTER);
        etQuestion.setLayoutParams(params);

        int maxLength = getAnswerLength(answer);
        etQuestion.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
        etQuestion.setTypeface(Typeface.create("sans-serif-black", Typeface.NORMAL));
        etQuestion.setBackground(getResources().getDrawable(R.drawable.shape_dash_boarder));
        etQuestion.setTextColor(Color.WHITE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            etQuestion.setLetterSpacing(0.02f);
        }
        if (isTablet())
            etQuestion.setTextSize(40);
        else etQuestion.setTextSize(20);
        etQuestion.setTextColor(Color.WHITE);

        return etQuestion;
    }

    private void setExQuestionInstructionAudioPath(final String instructionAudioPath) {
        LinearLayout btnExQuestionInstructionSound = findViewById(R.id.ll_speaker);
        btnExQuestionInstructionSound.setVisibility(View.VISIBLE);
        btnExQuestionInstructionSound.bringToFront();

        btnExQuestionInstructionSound.setOnClickListener(view ->
                setAudio(instructionAudioPath));
    }

    private void loadLayout() {
        ExerciseQuestionViewModel questionViewModel = questionViewModels.get(questionIndex);
        answer = questionViewModels.get(questionIndex).getExerciseAnswers().get(0).getAnswer();
        LinearLayout llQuestionContainer = findViewById(R.id.ll_question_container);
        llQuestionContainer.removeAllViews();
        generateQuestion(questionViewModel, llQuestionContainer);
        if (questionIndex <= questionViewModels.size() - 1)
            setNavigation(exerciseViewModel, multiSoundPlayer, questionIndex);
    }

    private int getAnswerLength(String answer) {
        return answer.length();
    }

    /*DeviceType :true = for tablet ,false = for Phone*/
    private boolean isTablet() {
        return getResources().getBoolean(R.bool.isTablet);
    }

    private void setAudio(String audio) {
        if (isSoundClicked == true) {
            multiSoundPlayer = new MediaPlayerUtil().playSound(this, audio, "exercise", false);
            isSoundClicked = false;
            multiSoundPlayer.setOnCompletionListener(mp -> isSoundClicked = true);
        }
    }

//    private void setNavigation(List<ExerciseQuestionViewModel> questionViewModels) {
//        ImageView btnRight = findViewById(R.id.btnRightNav);
//        ImageView btnLeft = findViewById(R.id.btnLeftNav);
//        if (multiSoundPlayer != null) {
//            multiSoundPlayer.stop();
//            isSoundClicked = true;
//        }
//        if (questionIndex == 0)
//            btnLeft.setVisibility(View.INVISIBLE);
//
//        else if (questionIndex == questionViewModels.size() - 1) {
//
//            btnRight.setVisibility(View.INVISIBLE);
//            (findViewById(R.id.button)).setVisibility(View.VISIBLE);
//        } else {
//            btnRight.setVisibility(View.VISIBLE);
//            btnLeft.setVisibility(View.VISIBLE);
//            (findViewById(R.id.button)).setVisibility(View.INVISIBLE);
//        }
//
//        btnRight.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                checkIfAnswerCorrect();
//                checkIfAnswerCorrect(exerciseViewModel,answer,questionIndex);
//                loadNextQuestion(true);
//            }
//        });
//        btnLeft.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                loadNextQuestion(false);
//
//            }
//        });
//    }

    public void onClickRight(View view) {
        setNavigation(exerciseViewModel, multiSoundPlayer, questionIndex);
        checkIfAnswerCorrect(exerciseViewModel,etAnswerInput.getText().toString(), questionIndex);
        etAnswerInput.setText("");
        loadNextQuestion(true);
    }

    public void onClickLeft(View view) {
        setNavigation(exerciseViewModel, multiSoundPlayer, questionIndex);
        checkIfAnswerCorrect(exerciseViewModel, loadAnswer(questionIndex-1), questionIndex);
        loadNextQuestion(false);
    }

    private void loadNextQuestion(boolean isAdded) {
        if (isAdded)
            questionIndex++;
        else
            questionIndex--;

        loadLayout();

    }

    public void onClickCross(View view) {

        Intent intent = new Intent(FillInTheBlankInputEnglish.this, LessonMapActivity.class);
        startActivity(intent);
    }

    private String loadAnswer(int questionIndex) {
        List<ExerciseQuestionViewModel> questionViewModels = exerciseViewModel.getExerciseQuestions();
        if (questionViewModels.get(questionIndex).getExerciseAnswers().get(0).getSelectedAnswer() != null) {
            return questionViewModels.get(questionIndex).getExerciseAnswers().get(0).getSelectedAnswer();
        }
        return "";
    }

//    private void checkIfAnswerCorrect() {
//        ArrayList<ExerciseAnswersViewModel> answersViewModels = questionViewModels.get(questionIndex).getExerciseAnswers();
//        for (int j = 0; j < answersViewModels.size(); j++) {
//            ExerciseAnswersViewModel answer = answersViewModels.get(j);
//
//            if (answer != null && answer.getSelectedAnswer() != null && (answer.getAnswer().equals(answer.getSelectedAnswer()))) {
//                questionViewModels.get(questionIndex).setCorrect(true);
//                Toast.makeText(this, "Correct", Toast.LENGTH_SHORT).show();
//            }
//
//        }
//    }

}
