package com.sabaq.muse.utils;
/**
 * Created by Muneeba.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.widget.Toast;

public class InternetDetector {
    private static Context context;
    static ConnectivityManager cm;
    public static String ERROR_INTERNET = "Please connect with internet";

    public InternetDetector(Context c) {
        context = c;
    }

    public static boolean checkIntern(Context c) {
        if (InternetDetector.detectInternet(c)) {
            return true;
        }
        Toast.makeText((Context)c, (CharSequence)ERROR_INTERNET, Toast.LENGTH_SHORT).show();
        return false;
    }

    public static boolean detectInternet(Context ctx) {
        context = ctx;
        cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > 22) {
            return InternetDetector.detectApiG22();
        }
        return InternetDetector.detectApiL22();
    }

    private static boolean detectApiG22() {
        cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null && info.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    private static boolean detectApiL22() {
        NetworkInfo[] info = cm.getAllNetworkInfo();
        if (info != null) {
            for (int i = 0; i < info.length; ++i) {
                if (info[i].getState() != NetworkInfo.State.CONNECTED) continue;
                return true;
            }
        }
        return false;
    }

    public boolean isNetworkAvailable() {
        cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        return false;
    }
}