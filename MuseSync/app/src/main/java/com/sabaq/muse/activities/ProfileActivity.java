package com.sabaq.muse.activities;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sabaq.muse.baseActivity.BaseActivity;
import com.sabaq.muse.R;
import com.sabaq.muse.adapters.SubjectProgressAdapter;
import com.sabaq.muse.database.School;
import com.sabaq.muse.database.SchoolDao;
import com.sabaq.muse.database.Student;
import com.sabaq.muse.database.StudentDao;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.ConvertDptoPx;
import com.sabaq.muse.utils.GenerateRandomColor;
import com.sabaq.muse.viewModel.SubjectProgress;

import java.util.ArrayList;
import java.util.List;

public class ProfileActivity extends BaseActivity {

    private static final String TAG = "muse";
    private ArrayList<SubjectProgress> progressList;
    private SubjectProgressAdapter adapter;
    // TODO: 9/27/2018 remove global var
    private TextView tvStudentName;
    private TextView tvSchoolName;
    private TextView tvStudentId;
    private String schoolCode;
    private int studentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        /*schoolCode=getIntent().getStringExtra("SchoolCode");
        studentId=getIntent().getIntExtra("StudentId",45);*/
        schoolCode = "TEST1";
        studentId = 45;
        initViews();
        setStudentInfo();
        initSubjectProgressList();
        getToolBar();
    }


    private void setStudentInfo() {

        List<School> schools = AppController.daoSession.getSchoolDao().queryBuilder()
                .where(SchoolDao.Properties.SchoolCode.eq(schoolCode)).list();
        if (schools != null && schools.size() > 0) {
            School school = schools.get(0);
            Student student = AppController.daoSession.getStudentDao().queryBuilder().where(StudentDao.Properties.Id.eq(studentId)).list().get(0);
            tvSchoolName.setText(school.getName());
            tvStudentId.setText(student.getId() + "");
            tvStudentName.setText(student.getFirstName());
            Log.d(TAG, "setStudentInfo: school " + student.getFirstName());
        } else {
            Log.d(TAG, "setStudentInfo: data null " + schools.size());

        }

    }

    private void initViews() {
        tvStudentName = findViewById(R.id.tvStudentName);
        tvSchoolName = findViewById(R.id.tvSchoolname);
        tvStudentId = findViewById(R.id.tvId);
    }


    /**
     * Calculates subject wise progress
     */
    private void initSubjectProgressList() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        progressList = new ArrayList<>();
        SubjectProgress subjectProgress1 = new SubjectProgress("Math", 25, 100, setProgressBar("Math"), R.color.colorChapterMathBg);
        SubjectProgress subjectProgress4 = new SubjectProgress("Urdu", 25, 100, setProgressBar("Urdu"), R.color.colorChapterUrduBg);
        SubjectProgress subjectProgress3 = new SubjectProgress("Science", 55, 100, setProgressBar("Science"), R.color.colorChapterScienceBg);
        SubjectProgress subjectProgress2 = new SubjectProgress("English", 35, 100, setProgressBar("English"), R.color.colorChapterEnglishBg);

        progressList.add(subjectProgress1);
        progressList.add(subjectProgress2);
        progressList.add(subjectProgress3);
        progressList.add(subjectProgress4);
        adapter = new SubjectProgressAdapter(this, progressList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, (new ConvertDptoPx(getApplicationContext(), 2)).dpToPx(), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);
    }

    public void getToolBar() {

        Toolbar mToolbar = findViewById(R.id.tool_bar);
        ImageView home = findViewById(R.id.iv_home);
        ImageView cross = findViewById(R.id.iv_cancel);
        setSupportActionBar(mToolbar);

        setSupportActionBar(mToolbar);
        ((TextView) findViewById(R.id.toolBar_title)).setVisibility(View.INVISIBLE);
        home.setImageResource(R.drawable.ic_edit);
        cross.setImageResource(R.drawable.x_grey);
        ((TextView) findViewById(R.id.toolBar_title)).setText(getIntent().getStringExtra(getString(R.string.topic_name)));
        home.setVisibility(View.VISIBLE);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, ProfileEditActivity.class);
                intent.putExtra("StudentId", studentId);
                startActivity(intent);
            }
        });

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   Intent intent = new Intent(ProfileActivity.this, SubjectsActivity.class);
                startActivity(intent);*/
                onBackPressed();
            }
        });
        cross.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setStudentInfo();
        Log.d(TAG, "onResume: Profile");
    }

    public void onClickProfileColorChange(View view) {
        new GenerateRandomColor(this, findViewById(R.id.ivProfileFaceColor)).generateRandomBackgroundColor();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }


}
