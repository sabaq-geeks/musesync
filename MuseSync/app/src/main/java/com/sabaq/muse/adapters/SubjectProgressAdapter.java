package com.sabaq.muse.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sabaq.muse.R;
import com.sabaq.muse.viewModel.SubjectProgress;

import java.util.List;

public class SubjectProgressAdapter extends RecyclerView.Adapter<SubjectProgressAdapter.MyViewHolder> {

    private Context mContext;
    private List<SubjectProgress> subjectProgressList;
    private ItemClickListener clickListener;


    public class MyViewHolder extends RecyclerView.ViewHolder  {
        public TextView tvSubjectName, tvScore;
        ProgressBar progressbar;

        MyViewHolder(View view) {
            super(view);
            tvSubjectName = (TextView) view.findViewById(R.id.tvSubject);
            tvScore = (TextView) view.findViewById(R.id.tvScore);
            progressbar = (ProgressBar) view.findViewById(R.id.progressbar);

        }



    }


    public SubjectProgressAdapter(Context mContext, List<SubjectProgress> subjectProgressList) {
        this.mContext = mContext;
        this.subjectProgressList = subjectProgressList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_subject_progress_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        SubjectProgress subjectProgress = subjectProgressList.get(position);
        holder.tvSubjectName.setText(subjectProgress.getSubjectName());
        holder.tvScore.setText(subjectProgress.getScore() + "/" + subjectProgress.getOutOf());
        //holder.description.setText(subjectProgress.getDescription());
        holder.progressbar.setProgress(40);
        holder.tvSubjectName.setTextColor(mContext.getResources().getColor(subjectProgress.getColorSubject()));
        holder.progressbar.setProgressDrawable(subjectProgress.getProgressBarColor());
    }

    @Override
    public int getItemCount() {
        return subjectProgressList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;

    }


}