package com.sabaq.muse.utils;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by Ishaq Ahmed Khan on 3/30/2017.
 */

public class SharedPreference {
    private final String PREFS_NAME = "Prefs_MUSE";
    private SharedPreferences settings;
    private Context context;
    private SharedPreferences.Editor editor;
    public String check_login = "check_login";
    public String user_name = "user_name";
    public String access_token = "access_token";
    public String expires_in = "expires_in";
    public String token_type = "token_type";
    public String ApplicationUserId = "ApplicationUserId";
    public String language = "language";
    public String forceSync = "forceSync";
    public String map_position = "mapPosition";


    public String DataLoadedFromApi = "DataLoadedFromApi";


    public SharedPreference(Context context) {
        this.context = context;
    }

    //Save String value in shared preference
    public void saveValueInSharedPreference(String key, String value) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    //Save boolean value in shared preference
    public void saveValueInSharedPreference(String key, boolean value) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    //Save int value in shared preference
    public void saveValueInSharedPreference(String key, int value) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    //Save long value in shared preference
    public void saveValueInSharedPreference(String key, long value) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    //Save float value in shared preference
    public void saveValueInSharedPreference(String key, float value) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    //Get String value from shared preference
    public String getStringValue(String key) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return settings.getString(key, "");
    }

    //Get boolean value from shared preference
    public boolean getBooleanValue(String key) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return settings.getBoolean(key, false);
    }

    //Get int value from shared preference
    public int getIntValue(String key) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return settings.getInt(key, 0);
    }

    //Get long value from shared preference
    public long getLongValue(String key) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return settings.getLong(key, 0);
    }

    //Get float value from shared preference
    public float getFloatValue(String key) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return settings.getFloat(key, 0f);
    }

    //To remove all values from preferences use editor.clear() method as shown below.
    public void clearSharedPreference() {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.clear();
        editor.commit();
    }

    //To remove a specific key-value pair use editor.remove(KEY) method as shown below.
    public void removeValue(String key) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.remove(key);
        editor.commit();
    }

/*    public void saveValueInSharedPreference(String key, MonitoringForms monitoringForms) {
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putString(key, monitoringForms.toString());
        editor.commit();
    }*/

}
