package com.sabaq.muse.viewModel;

import com.sabaq.muse.database.Exercise;
import com.sabaq.muse.database.ExerciseQuestion;
import com.sabaq.muse.database.SubTopic;
import com.sabaq.muse.database.Topic;

import java.util.ArrayList;
import java.util.List;

public class ExerciseViewModel {
    private Long Id;

    private String ExerciseTitle;

    private String UrduTitle;

    private String UrduTitleImagePath;

    private String SindhiTitle;

    private String Instruction;

    private String InstructionSoundPath;

    private String TranslationImagePath;

    private String UrduInstructionImage;

    private String Path;

    private Float UrduTitleImageSize;

    private Float InstructionSoundSize;

    private Float TranslationImageSize;

    private Float UrduInstructionImageSize;

    private Integer Order;

    private Integer TotalMarks;

    private Integer Timer;

    private Boolean Reviewed;

    private Boolean IsZoom;

    private Boolean IsSquare;

    private Boolean IsQuiz;

    private Boolean IsCompleted;

    private String GradeId;

    private String SubjectId;

    private Long TopicId;

    private Long SubTopicId;

    private Long ExerciseId;

    private Long ReadaloudId;

    private String ExerciseTypeId;

    private Topic topic;

    private transient Long topic__resolvedKey;

    private SubTopic subTopic;

    private transient Long subTopic__resolvedKey;

    private List<ExerciseQuestionViewModel> exerciseQuestions;

    public ExerciseViewModel(Exercise exercise,ArrayList<ExerciseQuestionViewModel> exerciseQuestions) {
        Id = exercise.getId();
        ExerciseTitle = exercise.getExerciseTitle();
        UrduTitle = exercise.getUrduTitle();
        UrduTitleImagePath = exercise.getUrduTitleImagePath();
        SindhiTitle = exercise.getSindhiTitle();
        Instruction = exercise.getInstruction();
        InstructionSoundPath = exercise.getInstructionSoundPath();
        TranslationImagePath = exercise.getTranslationImagePath();
        UrduInstructionImage = exercise.getUrduInstructionImage();
        Path = exercise.getPath();
        UrduTitleImageSize = exercise.getUrduTitleImageSize();
        InstructionSoundSize = exercise.getInstructionSoundSize();
        TranslationImageSize = exercise.getTranslationImageSize();
        UrduInstructionImageSize = exercise.getUrduInstructionImageSize();
        Order = exercise.getOrder();
        TotalMarks = exercise.getTotalMarks();
        Timer = exercise.getTimer();
        Reviewed = exercise.getReviewed();
        IsZoom = exercise.getIsZoom();
        IsSquare = exercise.getIsSquare();
        IsQuiz = exercise.getIsQuiz();
        IsCompleted = exercise.getIsCompleted();
        GradeId = exercise.getGradeId();
        SubjectId = exercise.getSubjectId();
        TopicId = exercise.getTopicId();
        SubTopicId = exercise.getSubTopicId();
        ExerciseId = exercise.getExerciseId();
        ReadaloudId = exercise.getReadaloudId();
        ExerciseTypeId = exercise.getExerciseTypeId();
        this.topic = exercise.getTopic();
        this.topic__resolvedKey = exercise.getTopicId();
        this.subTopic = exercise.getSubTopic();
        this.subTopic__resolvedKey = exercise.getSubTopicId();
        this.exerciseQuestions = exerciseQuestions;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getExerciseTitle() {
        return ExerciseTitle;
    }

    public void setExerciseTitle(String exerciseTitle) {
        ExerciseTitle = exerciseTitle;
    }

    public String getUrduTitle() {
        return UrduTitle;
    }

    public void setUrduTitle(String urduTitle) {
        UrduTitle = urduTitle;
    }

    public String getUrduTitleImagePath() {
        return UrduTitleImagePath;
    }

    public void setUrduTitleImagePath(String urduTitleImagePath) {
        UrduTitleImagePath = urduTitleImagePath;
    }

    public String getSindhiTitle() {
        return SindhiTitle;
    }

    public void setSindhiTitle(String sindhiTitle) {
        SindhiTitle = sindhiTitle;
    }

    public String getInstruction() {
        return Instruction;
    }

    public void setInstruction(String instruction) {
        Instruction = instruction;
    }

    public String getInstructionSoundPath() {
        return InstructionSoundPath;
    }

    public void setInstructionSoundPath(String instructionSoundPath) {
        InstructionSoundPath = instructionSoundPath;
    }

    public String getTranslationImagePath() {
        return TranslationImagePath;
    }

    public void setTranslationImagePath(String translationImagePath) {
        TranslationImagePath = translationImagePath;
    }

    public String getUrduInstructionImage() {
        return UrduInstructionImage;
    }

    public void setUrduInstructionImage(String urduInstructionImage) {
        UrduInstructionImage = urduInstructionImage;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }

    public Float getUrduTitleImageSize() {
        return UrduTitleImageSize;
    }

    public void setUrduTitleImageSize(Float urduTitleImageSize) {
        UrduTitleImageSize = urduTitleImageSize;
    }

    public Float getInstructionSoundSize() {
        return InstructionSoundSize;
    }

    public void setInstructionSoundSize(Float instructionSoundSize) {
        InstructionSoundSize = instructionSoundSize;
    }

    public Float getTranslationImageSize() {
        return TranslationImageSize;
    }

    public void setTranslationImageSize(Float translationImageSize) {
        TranslationImageSize = translationImageSize;
    }

    public Float getUrduInstructionImageSize() {
        return UrduInstructionImageSize;
    }

    public void setUrduInstructionImageSize(Float urduInstructionImageSize) {
        UrduInstructionImageSize = urduInstructionImageSize;
    }

    public Integer getOrder() {
        return Order;
    }

    public void setOrder(Integer order) {
        Order = order;
    }

    public Integer getTotalMarks() {
        return TotalMarks;
    }

    public void setTotalMarks(Integer totalMarks) {
        TotalMarks = totalMarks;
    }

    public Integer getTimer() {
        return Timer;
    }

    public void setTimer(Integer timer) {
        Timer = timer;
    }

    public Boolean getReviewed() {
        return Reviewed;
    }

    public void setReviewed(Boolean reviewed) {
        Reviewed = reviewed;
    }

    public Boolean getZoom() {
        return IsZoom;
    }

    public void setZoom(Boolean zoom) {
        IsZoom = zoom;
    }

    public Boolean getSquare() {
        return IsSquare;
    }

    public void setSquare(Boolean square) {
        IsSquare = square;
    }

    public Boolean getQuiz() {
        return IsQuiz;
    }

    public void setQuiz(Boolean quiz) {
        IsQuiz = quiz;
    }

    public Boolean getCompleted() {
        return IsCompleted;
    }

    public void setCompleted(Boolean completed) {
        IsCompleted = completed;
    }

    public String getGradeId() {
        return GradeId;
    }

    public void setGradeId(String gradeId) {
        GradeId = gradeId;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

    public Long getTopicId() {
        return TopicId;
    }

    public void setTopicId(Long topicId) {
        TopicId = topicId;
    }

    public Long getSubTopicId() {
        return SubTopicId;
    }

    public void setSubTopicId(Long subTopicId) {
        SubTopicId = subTopicId;
    }

    public Long getExerciseId() {
        return ExerciseId;
    }

    public void setExerciseId(Long exerciseId) {
        ExerciseId = exerciseId;
    }

    public Long getReadaloudId() {
        return ReadaloudId;
    }

    public void setReadaloudId(Long readaloudId) {
        ReadaloudId = readaloudId;
    }

    public String getExerciseTypeId() {
        return ExerciseTypeId;
    }

    public void setExerciseTypeId(String exerciseTypeId) {
        ExerciseTypeId = exerciseTypeId;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Long getTopic__resolvedKey() {
        return topic__resolvedKey;
    }

    public void setTopic__resolvedKey(Long topic__resolvedKey) {
        this.topic__resolvedKey = topic__resolvedKey;
    }

    public SubTopic getSubTopic() {
        return subTopic;
    }

    public void setSubTopic(SubTopic subTopic) {
        this.subTopic = subTopic;
    }

    public Long getSubTopic__resolvedKey() {
        return subTopic__resolvedKey;
    }

    public void setSubTopic__resolvedKey(Long subTopic__resolvedKey) {
        this.subTopic__resolvedKey = subTopic__resolvedKey;
    }

    public List<ExerciseQuestionViewModel> getExerciseQuestions() {
        return exerciseQuestions;
    }

    public void setExerciseQuestions(ArrayList<ExerciseQuestionViewModel> exerciseQuestions) {
        this.exerciseQuestions = exerciseQuestions;
    }
}
