package com.sabaq.muse.activities;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.rd.PageIndicatorView;
import com.sabaq.muse.R;
import com.sabaq.muse.baseActivity.BaseActivity;
import com.sabaq.muse.datasource.ReadAloudFetcher;
import com.sabaq.muse.utils.Helper;
import com.sabaq.muse.utils.ImageLoad;
import com.sabaq.muse.utils.MediaPlayerUtil;
import com.sabaq.muse.viewModel.ReadAloudViewModel;

import java.util.List;

import static android.support.constraint.Constraints.TAG;
import static com.sabaq.muse.utils.Helper.CURRENT_CONTENTID;
import static com.sabaq.muse.utils.Helper.CURRENT_SUBJECTID;

public class ReadAloudActivity extends BaseActivity {

    ViewPager viewPager;
    ImagePagerAdapter adapter;
    Long readAloudId;
    Button btnRight, btnLeft;
    private static MediaPlayer multiSoundPlayer = null;
    List<ReadAloudViewModel> readAloudViewModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_readaloud);
        initViews();
        init();
    }

    private void initViews() {
        btnRight = findViewById(R.id.btnRightNav);
        btnLeft = findViewById(R.id.btnLeftNav);
    }

    public void onClickLeft(View view) {
        setNavigation(viewPager.getCurrentItem());
        if (!CURRENT_SUBJECTID.equals("Urdu")) {
            if (viewPager.getCurrentItem() != 0)
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
            Log.d(TAG, "onClickLeft: " + viewPager.getCurrentItem());
            viewPager.setOnPageChangeListener(adapter.change);
        } else {
            if (viewPager.getCurrentItem() != adapter.readAloudViewModelList.size())
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            viewPager.setOnPageChangeListener(adapter.change);
            Log.d(TAG, "onClickLeft: " + viewPager.getCurrentItem());
        }
    }

    public void onClickRight(View view) {
        setNavigation(viewPager.getCurrentItem());

        if (!CURRENT_SUBJECTID.equals("Urdu")) {
            if (viewPager.getCurrentItem() != adapter.readAloudViewModelList.size())
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            viewPager.setOnPageChangeListener(adapter.change);
            Log.d(TAG, "onClickLeft: " + viewPager.getCurrentItem());
        } else {
            if (viewPager.getCurrentItem() != 0)
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
            Log.d(TAG, "onClickLeft: " + viewPager.getCurrentItem());
            viewPager.setOnPageChangeListener(adapter.change);
        }
    }

    public class ImagePagerAdapter extends PagerAdapter {


        List<ReadAloudViewModel> readAloudViewModelList;

        ImagePagerAdapter(List<ReadAloudViewModel> readAloudViewModelList) {
            this.readAloudViewModelList = readAloudViewModelList;
        }


        @Override
        public int getCount() {
            return readAloudViewModelList.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            Context context = ReadAloudActivity.this;
            ImageView imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            setViewDirection(imageView);
            new ImageLoad(ReadAloudActivity.this)
                    .setImage(readAloudViewModelList.get(position).getImagePath(), imageView);
            container.addView(imageView, 0);

            return imageView;
        }

        ViewPager.OnPageChangeListener change = new ViewPager.OnPageChangeListener() {
            //declare key
            Boolean first = true;

            @Override
            public void onPageSelected(int position) {
                setAudio(readAloudViewModelList.get(position).getAudioPath());
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d(ACTIVITY_SERVICE, "onPageScrolled" + position + " " + positionOffset + "  " + positionOffsetPixels);
                if (first && positionOffset == 0 && positionOffsetPixels == 0) {
                    onPageSelected(0);
                    first = false;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.d(ACTIVITY_SERVICE, "onPageScrollStateChanged" + state);
            }
        };

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((ImageView) object);
        }
    }

    private void init() {
        getToolBar("", View.VISIBLE, View.VISIBLE, View.INVISIBLE, this);
        viewPager = findViewById(R.id.viewPager);
        readAloudId = CURRENT_CONTENTID;
        mapDataInToViewPager(getData());
        setNavigation(viewPager.getCurrentItem());
    }

    private void mapDataInToViewPager(List<ReadAloudViewModel> readAloudList) {
        adapter = new ImagePagerAdapter(readAloudList);
        viewPager.setAdapter(adapter);
        setViewDirection(viewPager);
        setViewPagerSwiping();

        PageIndicatorView pageIndicatorView = setSpringDotsIndicatorColor(Helper.CURRRENT_COLOR);
        setViewDirection(pageIndicatorView);
    }

    private void setViewPagerSwiping() {
        if (viewPager.getCurrentItem() == 0) {
            viewPager.setOnPageChangeListener(adapter.change);
        } else if (viewPager.getCurrentItem() > 0)
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        if (viewPager.getCurrentItem() != adapter.readAloudViewModelList.size() && viewPager.getCurrentItem() != 0)
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
        viewPager.setOnPageChangeListener(adapter.change);
    }

    //TODO remove this func
    private List<ReadAloudViewModel> getData() {


        ReadAloudFetcher readAloudDatasource = new ReadAloudFetcher(this);
        readAloudViewModelList = readAloudDatasource.getReadAloudData(readAloudId);
        return readAloudViewModelList;
    }


    private void setAudio(String audio) {
        setAudioFromDb(audio);
    }

    private void setAudioFromDb(String audio) {
        multiSoundPlayer = new MediaPlayerUtil().playSound(this, audio, "ReadAloud", false);
    }

    private void setNavigation(int pageIndex) {

        if (pageIndex == 0 && CURRENT_SUBJECTID.equals("Urdu"))
            btnRight.setVisibility(View.INVISIBLE);
        else if (pageIndex == 0 && !CURRENT_SUBJECTID.equals("Urdu"))
            btnLeft.setVisibility(View.INVISIBLE);
        else if (pageIndex == readAloudViewModelList.size() - 1 && CURRENT_SUBJECTID.equals("Urdu"))
            btnLeft.setVisibility(View.INVISIBLE);
        else if (pageIndex == readAloudViewModelList.size() - 1 && !CURRENT_SUBJECTID.equals("Urdu"))
            btnRight.setVisibility(View.INVISIBLE);
        else {
            btnRight.setVisibility(View.VISIBLE);
            btnLeft.setVisibility(View.VISIBLE);
        }

    }

    public void onClickCross(View view) {
        Intent intent = new Intent(ReadAloudActivity.this, LessonMapActivity.class);
        startActivity(intent);
        finish();
    }

    public void onClickHome(View view) {
        startActivity(new Intent(ReadAloudActivity.this, SubjectsActivity.class));
        finish();
    }
}
