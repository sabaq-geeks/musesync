package com.sabaq.muse.baseActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sabaq.muse.R;
import com.sabaq.muse.utils.ImageLoad;
import com.sabaq.muse.utils.MediaPlayerUtil;
import com.sabaq.muse.viewModel.ExerciseAnswersViewModel;
import com.sabaq.muse.viewModel.ExerciseQuestionViewModel;
import com.sabaq.muse.viewModel.ExerciseViewModel;

import java.util.List;
import java.util.Objects;

public class BaseExerciseActivity extends BaseActivity {
    private static MediaPlayer mediaPlayer = new MediaPlayer();
    private boolean isSoundClicked = true;
    private byte correctAnswerCount;

    public enum MODE {
        PRACTICE,
        TEST
    }

    public MODE mode = null;

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            isSoundClicked = true;
        }
    }

    public void showTickCross(Boolean isCorrect, Context context) {
        final Dialog mDialog;
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_tick_cross);
        mDialog.setCancelable(true);
        mDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ImageView ivStatus;
        ivStatus = mDialog.findViewById(R.id.btnStatus);
        if (isCorrect)
            ivStatus.setImageResource(R.drawable.right_answer);
        else if (!isCorrect)
            ivStatus.setImageResource(R.drawable.wrong_answer);
        mDialog.show();
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (mDialog.isShowing()) {
                    mDialog.dismiss();
                }
            }
        };
        mDialog.setOnDismissListener(dialog -> handler.removeCallbacks(runnable));
        handler.postDelayed(runnable, 1000);
    }

    protected Dialog showChooseModeDialog(Context context) {
        final Dialog mDialog;
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_choose_mode);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        mDialog.show();

        return mDialog;
    }

    @NonNull
    protected LinearLayout generatellOptionUI(Activity context) {
        LinearLayout llOption = new LinearLayout(context);
        llOption.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams llOptionparams = new LinearLayout.LayoutParams(0, getResources().getDimensionPixelSize(R.dimen._35sdp));
        llOptionparams.gravity = Gravity.CENTER;
        llOptionparams.weight = 1;
        llOptionparams.setMarginStart(22);
        llOption.setLayoutParams(llOptionparams);
        return llOption;
    }

    protected ImageView generateQuestionIv(Activity context, String imagePath, String text, LinearLayout.LayoutParams params) {
        final ImageView imgQuestion = new ImageView(context);
        LinearLayout.LayoutParams ivParams = null;
        int dimen = 0;

        if (params == null) {
            if (text.equals(null) || text.trim().equals("-"))
                ivParams = new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen._80sdp), getResources().getDimensionPixelSize(R.dimen._80sdp));
            else if (text.length() < 50)
                ivParams = new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen._65sdp), getResources().getDimensionPixelSize(R.dimen._65sdp));
            else if (text.length() > 50)
                ivParams = new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen._40sdp), getResources().getDimensionPixelSize(R.dimen._40sdp));
            ivParams.gravity = Gravity.CENTER;
            ivParams.setMargins(0, getDpFromDimens(context, R.dimen._8sdp), 0, 0);
            ivParams.weight = 1;
        } else {
            ivParams = params;
        }
        imgQuestion.setOnClickListener(v ->
                showImageInZoom(context, imagePath));
        imgQuestion.setLayoutParams(ivParams);
        new ImageLoad(this)
                .setImage(imagePath, imgQuestion);
        //imgQuestion.setImageResource(R.drawable.demo_image);
        return imgQuestion;
    }

    private int getDpFromDimens(Activity context, int dimen) {
        return (int) (context.getResources().getDimension(dimen) / context.getResources().getDisplayMetrics().density);
    }

    public void showImageInZoom(Activity context, String image) {
        final Dialog mDialog;
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_zoom_image);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        ImageView ivImage = mDialog.findViewById(R.id.ivImage);
        new ImageLoad(context)
                .setImage(image, ivImage);
        ImageView btnOkay;
        btnOkay = mDialog.findViewById(R.id.btnCross);
        btnOkay.setOnClickListener(v -> mDialog.dismiss());
        removeBarsFromNavigation(mDialog);
        mDialog.show();
    }

    public MediaPlayer showHintDialog(Context context, String instructionText, String instructionSound, String instructionImage) {
        final Dialog mDialog;
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCancelable(false);

        if (instructionImage != "" && instructionImage != null && isFileExist(instructionImage)) {
            mDialog.setContentView(R.layout.dialog_exercise_instruction_image);
            ImageView ivImage = mDialog.findViewById(R.id.iv_instruction_image);
            ivImage.setVisibility(View.VISIBLE);
            new ImageLoad(this)
                    .setImage(instructionImage, ivImage);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(mDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        }
        mDialog.setContentView(R.layout.dialog_exercise_instruction);
        if (instructionText != "") {
            ((TextView) mDialog.findViewById(R.id.tv_ex_instruction)).setText(instructionText);
        }
        if (instructionSound != "" && isFileExist(instructionSound)) {
            ImageView ivInstructionSound = mDialog.findViewById(R.id.btnSound);
            ivInstructionSound.setVisibility(View.VISIBLE);
            ivInstructionSound.setOnClickListener(view ->
            {
                if (isSoundClicked == true) {
                    mediaPlayer = new MediaPlayerUtil().playSound(this, instructionSound, "exercise", false);
                    isSoundClicked = false;
                    mediaPlayer.setOnCompletionListener(mp -> isSoundClicked = true);
                }
            });
        }

        mDialog.findViewById(R.id.btnCross)
                .setOnClickListener(v -> {
                    mDialog.dismiss();
                    if (mediaPlayer != null) {
                        mediaPlayer.stop();
                        isSoundClicked = true;
                    }
                });
        mDialog.show();
        return mediaPlayer;
    }

    public void setNavigation(ExerciseViewModel exerciseViewModel, MediaPlayer multiSoundPlayer, int questionIndex) {
        List<ExerciseQuestionViewModel> questionViewModels = exerciseViewModel.getExerciseQuestions();
        ImageView btnRight = findViewById(R.id.btnRightNav);
        ImageView btnLeft = findViewById(R.id.btnLeftNav);
        if (multiSoundPlayer != null) {
            multiSoundPlayer.stop();
            isSoundClicked = true;
        }
        setButtonVisibilityOnNavigation(exerciseViewModel, questionIndex, questionViewModels, btnRight, btnLeft);

        // setAnswerInViewModel;
        //setAnswerIntoAnswerViewModel(exerciseViewModel, questionIndex, givenAnswer);
        // FillInTheBlankInputMath.this.editTextAnswerField.setText("");
//        questionIndex = loadNextQuestion(isRight, questionIndex);
//        return questionIndex;
    }

    private void setButtonVisibilityOnNavigation(ExerciseViewModel exerciseViewModel, int questionIndex, List<ExerciseQuestionViewModel> questionViewModels, ImageView btnRight, ImageView btnLeft) {
        if (questionIndex == 0)
            btnLeft.setVisibility(View.INVISIBLE);

        else if (questionIndex == questionViewModels.size() - 1) {
            btnRight.setVisibility(View.INVISIBLE);
            setResult(exerciseViewModel, correctAnswerCount);
        } else {
            btnRight.setVisibility(View.VISIBLE);
            btnLeft.setVisibility(View.VISIBLE);
        }
    }

    public void setResult(ExerciseViewModel exerciseViewModel, int correctAnswerCount) {
        int totalQuestion = exerciseViewModel.getExerciseQuestions().size();
        int CorrectAnswer = correctAnswerCount;
        int wrongAnswer = totalQuestion - correctAnswerCount;
    }

    public void setAnswerIntoAnswerViewModel(ExerciseViewModel exerciseViewModel, int questionIndex, String givenAnswer) {
        exerciseViewModel.getExerciseQuestions().get(questionIndex).getExerciseAnswers().get(0).setSelectedAnswer(givenAnswer);
    }

    public void checkIfAnswerCorrect(ExerciseViewModel exerciseViewModel, String givenAnswer, int questionIndex) {

        String correctAnswer = exerciseViewModel.getExerciseQuestions().get(questionIndex).getExerciseAnswers().get(0).getAnswer().replace(",", "");
        correctAnswer = correctAnswer.trim().toLowerCase();
        givenAnswer= givenAnswer.trim().toLowerCase();
        if (givenAnswer != null && !givenAnswer.isEmpty()) {
            if (correctAnswer.equals(givenAnswer)) {
                Toast.makeText(this, "Correct", Toast.LENGTH_SHORT).show();
                correctAnswerCount++;
            } else
                Toast.makeText(this, "Wrong", Toast.LENGTH_SHORT).show();
        }
    }

    @NonNull
    protected ImageView generateOptionSoundButton(final ExerciseAnswersViewModel option) {
        ImageView btnSound;
        btnSound = new ImageView(this);
        LinearLayout.LayoutParams btnSoundParams = new LinearLayout.LayoutParams(50, 50);
        btnSoundParams.gravity = Gravity.CENTER;
        btnSoundParams.setMargins(40, 20, 20, 20);
        btnSound.setLayoutParams(btnSoundParams);
        btnSound.setBackgroundResource(R.drawable.ic_volume_up);

        return btnSound;
    }
}


