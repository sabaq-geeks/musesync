package com.sabaq.muse.animation;

import android.animation.TimeAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.sabaq.muse.R;

import java.util.Random;

import static com.sabaq.muse.utils.Helper.CURRRENT_COLOR;

/**
 * Continuous animation where stars slide from the bottom to the top
 * Created by Patrick Ivarsson on 7/23/17.
 */
public class MathBgAnimationView extends View {


    /**
     * Class representing the state of a star
     */
    private class First {
        private float x;
        private float y;
        private float scale;
        private float alpha;
        private float speed;
    }

    private class Second {
        private float x;
        private float y;
        private float scale;
        private float alpha;
        private float speed;
    }

    private class Third {
        private float x;
        private float y;
        private float scale;
        private float alpha;
        private float speed;
    }

    private final int BASE_SPEED_DP_PER_S = 200;
    private final int COUNT = 12;
    private final int SEED = 1337;

    /**
     * The minimum scale of a star
     */
    private final float SCALE_MIN_PART = 0.45f;
    /**
     * How much of the scale that's based on randomness
     */
    private final float SCALE_RANDOM_PART = 0.55f;
    /**
     * How much of the alpha that's based on the scale of the star
     */
    private final float ALPHA_SCALE_PART = 0.5f;
    /**
     * How much of the alpha that's based on randomness
     */
    private final float ALPHA_RANDOM_PART = 0.5f;

    private final First[] mFirsts = new First[COUNT];
    private final Second[] mSecond = new Second[COUNT];
    private final Third[] mThird = new Third[COUNT];

    private final Random mRnd = new Random(SEED);

    private TimeAnimator mTimeAnimator;
    private Drawable firstDrawable, secondDrawable, thirdDrawable;
    private float mBaseSpeed;
    private float firstBaseSize, secondBaseSize, thirdBaseSize;
    private long mCurrentPlayTime;
    final First first =  new First();
    final Second second =  new Second();
    final Third third =  new Third();
    /**
     * @see View#View(Context)
     */
    public MathBgAnimationView(Context context) {
        super(context);
        init();
    }

    /**
     * @see View#View(Context, AttributeSet)
     */
    public MathBgAnimationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * @see View#View(Context, AttributeSet, int)
     */
    public MathBgAnimationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        firstDrawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_equal_math);
        secondDrawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_minus_math);
        thirdDrawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_plus_math);
        thirdDrawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_multiple_math);

        firstBaseSize = Math.max(firstDrawable.getIntrinsicWidth(), firstDrawable.getIntrinsicHeight()) / 1f;
        Log.d("INIT", "init: " + firstDrawable.getIntrinsicWidth() + " " + firstDrawable.getIntrinsicHeight());
        secondBaseSize = Math.max(secondDrawable.getIntrinsicWidth(), secondDrawable.getIntrinsicHeight()) / 1f;
        Log.d("INIT", "init1: " + secondDrawable.getIntrinsicWidth() + " " + secondDrawable.getIntrinsicHeight());
        thirdBaseSize = Math.max(thirdDrawable.getIntrinsicWidth(), thirdDrawable.getIntrinsicHeight()) / 1f;
        Log.d("INIT", "init2: " + thirdDrawable.getIntrinsicWidth() + " " + thirdDrawable.getIntrinsicHeight());

        mBaseSpeed = BASE_SPEED_DP_PER_S * getResources().getDisplayMetrics().density;
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldw, int oldh) {
        super.onSizeChanged(width, height, oldw, oldh);

        // The starting position is dependent on the size of the view,
        // which is why the model is initialized here, when the view is measured.
        for (int i = 0; i < mFirsts.length; i++) {
            final First first = new First();
            initializeFirst(first, width, height);
            mFirsts[i] = first;
        }
        for (int i = 0; i < mSecond.length; i++) {
            final Second second = new Second();
            initializeScond(second, width, height);
            mSecond[i] = second;
        }
        for (int i = 0; i < mThird.length; i++) {
            final Third third = new Third();
            initializeThird(third, width, height);
            mThird[i] = third;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        final int viewHeight = getHeight();
        int index = 0;

        for (final First first : mFirsts) {
            // Ignore the first if it's outside of the view bounds
            final float firstSize = first.scale * firstBaseSize;
            final float thirdSize = first.scale * secondBaseSize;

            if (first.y + firstSize < 0 || first.y - firstSize > viewHeight) {
                continue;
            }

            if (first.y + thirdSize < 0 || first.y - thirdSize > viewHeight) {
                continue;
            }

            // Save the current canvas state
            final int save = canvas.save();

            // Move the canvas to the center of the first
            canvas.translate(first.x, first.y);

            // Rotate the canvas based on how far the first has moved
            final float progress = (first.y + firstSize) / viewHeight;
            canvas.rotate(360 * progress);

            // Prepare the size and alpha of the drawable
            final int size = Math.round(firstSize);

            firstDrawable.setBounds(-size, -size, size, size);
            firstDrawable.setAlpha(Math.round(255 * first.alpha));

            firstDrawable.draw(canvas);

            // Restore the canvas to it's previous position and rotation
            canvas.restoreToCount(save);
        }
        /*Second*/
        for (final Second second : mSecond) {
            // Ignore the first if it's outside of the view bounds
            final float secondSize = second.scale * secondBaseSize;

            if (second.y + secondSize < 0 || second.y - secondSize > viewHeight) {
                continue;
            }

            // Save the current canvas state
            final int save = canvas.save();

            // Move the canvas to the center of the second
            canvas.translate(second.x, second.y);

            // Rotate the canvas based on how far the second has moved
            final float progress = (second.y + secondSize) / viewHeight;
            canvas.rotate(360 * progress);

            // Prepare the size and alpha of the drawable
            final int size = Math.round(secondSize);

            secondDrawable.setBounds(-size, -size, size, size);
            secondDrawable.setAlpha(Math.round(255 * second.alpha));

            secondDrawable.draw(canvas);

            // Restore the canvas to it's previous position and rotation
            canvas.restoreToCount(save);
        }

        for (final Third third : mThird) {
            // Ignore the first if it's outside of the view bounds
            final float thirdSize = third.scale * thirdBaseSize;

            if (third.y + thirdSize < 0 || third.y - thirdSize > viewHeight) {
                continue;
            }

            // Save the current canvas state
            final int save = canvas.save();

            // Move the canvas to the center of the third
            canvas.translate(third.x, third.y);

            // Rotate the canvas based on how far the third has moved
            final float progress = (third.y + thirdSize) / viewHeight;
            canvas.rotate(360 * progress);

            // Prepare the size and alpha of the drawable
            final int size = Math.round(thirdSize);

            thirdDrawable.setBounds(-size, -size, size, size);
            thirdDrawable.setAlpha(Math.round(255 * third.alpha));

            thirdDrawable.draw(canvas);

            // Restore the canvas to it's previous position and rotation
            canvas.restoreToCount(save);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        mTimeAnimator = new TimeAnimator();
        mTimeAnimator.setTimeListener(new TimeAnimator.TimeListener() {
            @Override
            public void onTimeUpdate(TimeAnimator animation, long totalTime, long deltaTime) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (!isLaidOut()) {
                        // Ignore all calls before the view has been measured and laid out.
                        return;
                    }
                }
                if (CURRRENT_COLOR == 1) {
                    updateState(deltaTime);
                }
                invalidate();
            }
        });
        mTimeAnimator.start();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mTimeAnimator.cancel();
        mTimeAnimator.setTimeListener(null);
        mTimeAnimator.removeAllListeners();
        mTimeAnimator = null;
    }

    /**
     * Pause the animation if it's running
     */
    public void pause() {
        if (mTimeAnimator != null && mTimeAnimator.isRunning()) {
            // Store the current play time for later.
            mCurrentPlayTime = mTimeAnimator.getCurrentPlayTime();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mTimeAnimator.pause();
            }
        }
    }

    /**
     * Resume the animation if not already running
     */
    public void resume() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (mTimeAnimator != null && mTimeAnimator.isPaused()) {
                mTimeAnimator.start();
                // Why set the current play time?
                // TimeAnimator uses timestamps internally to determine the delta given
                // in the TimeListener. When resumed, the next delta received will the whole
                // pause duration, which might cause a huge jank in the animation.
                // By setting the current play time, it will pick of where it left off.
                mTimeAnimator.setCurrentPlayTime(mCurrentPlayTime);
            }
        }
    }

    /**
     * Progress the animation by moving the stars based on the elapsed time
     *
     * @param deltaMs time delta since the last frame, in millis
     */
    private void updateState(float deltaMs) {
        // Converting to seconds since PX/S constants are easier to understand
        final float deltaSeconds = deltaMs / 1000f;
        final int viewWidth = getWidth();
        final int viewHeight = getHeight();

        for (final First first : mFirsts) {
            if (first != null) {  // Move the first based on the elapsed time and it's speed
                if (first.y != 0) first.y -= first.speed * deltaSeconds;

                // If the first is completely outside of the view bounds after
                // updating it's position, recycle it.
                final float size1 = first.scale * firstBaseSize;

                if (first.y + size1 < 0) {
                    initializeFirst(first, viewWidth, viewHeight);
                }
            }
        }
        /*Second*/
        for (final Second second : mSecond) {
            if (second != null) {      // Move the first based on the elapsed time and it's speed
                second.y -= second.speed * deltaSeconds;

                // If the second is completely outside of the view bounds after
                // updating it's position, recycle it.
                final float size2 = second.scale * secondBaseSize;

                if (second.y + size2 < 0) {
                    initializeScond(second, viewWidth, viewHeight);
                }
            }
        }
        /*Third*/
        for (final Third third : mThird) {
            if (third != null) {  // Move the first based on the elapsed time and it's speed
                third.y -= third.speed * deltaSeconds;

                // If the third is completely outside of the view bounds after
                // updating it's position, recycle it.
                final float size3 = third.scale * thirdBaseSize;

                if (third.y + size3 < 0) {
                    initializeThird(third, viewWidth, viewHeight);
                }
            }
        }
    }

    /**
     * Initialize the given first by randomizing it's position, scale and alpha
     *
     * @param first      the first to initialize
     * @param viewWidth  the view width
     * @param viewHeight the view height
     */
    private void initializeFirst(First first, int viewWidth, int viewHeight) {
        // Set the scale based on a min value and a random multiplier
        first.scale = SCALE_MIN_PART + SCALE_RANDOM_PART * mRnd.nextFloat();

        // Set X to a random value within the width of the view
        first.x = viewWidth * mRnd.nextFloat();

        // Set the Y position
        // Start at the bottom of the view
        first.y = viewHeight;
        // The Y value is in the center of the first, add the size
        // to make sure it starts outside of the view bound
        first.y += first.scale * firstBaseSize;
        first.y += first.scale * secondBaseSize;
        first.y += first.scale * thirdBaseSize;
        // Add a random offset to create a small delay before the
        // first appears again.
        first.y += viewHeight * mRnd.nextFloat() / 4f;

        // The alpha is determined by the scale of the first and a random multiplier.
        first.alpha = ALPHA_SCALE_PART * first.scale + ALPHA_RANDOM_PART * mRnd.nextFloat();
        // The bigger and brighter a first is, the faster it moves
        first.speed = mBaseSpeed * first.alpha * first.scale;

    }

    private void initializeScond(Second second, int viewWidth, int viewHeight) {
        /*Second*/
        second.scale = SCALE_MIN_PART + SCALE_RANDOM_PART * mRnd.nextFloat();
        second.x = viewWidth * mRnd.nextFloat();
        second.y = viewHeight;
        second.y += second.scale * secondBaseSize;

        second.y += viewHeight * mRnd.nextFloat() / 4f;
        second.alpha = ALPHA_SCALE_PART * second.scale + ALPHA_RANDOM_PART * mRnd.nextFloat();
        second.speed = mBaseSpeed * second.alpha * second.scale;

    }

    private void initializeThird(Third third, int viewWidth, int viewHeight) {
        /*Third*/
        third.scale = SCALE_MIN_PART + SCALE_RANDOM_PART * mRnd.nextFloat();
        third.x = viewWidth * mRnd.nextFloat();
        third.y = viewHeight;
        third.y += third.scale * thirdBaseSize;
        third.y += viewHeight * mRnd.nextFloat() / 4f;
        third.alpha = ALPHA_SCALE_PART * third.scale + ALPHA_RANDOM_PART * mRnd.nextFloat();
        third.speed = mBaseSpeed * third.alpha * third.scale;
    }
}

