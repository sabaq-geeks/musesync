package com.sabaq.muse.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;


import com.bumptech.glide.Glide;
import com.sabaq.muse.baseActivity.BaseActivity;
import com.sabaq.muse.R;
import com.sabaq.muse.utils.Helper;
import com.sabaq.muse.viewModel.LessonViewModel;
import com.sabaq.muse.database.Exercise;
import com.sabaq.muse.database.ExerciseDao;
import com.sabaq.muse.database.Readaloud;
import com.sabaq.muse.database.ReadaloudDao;
import com.sabaq.muse.database.Video;
import com.sabaq.muse.database.VideoDao;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.LessonComparator;
import com.sabaq.muse.utils.MapUtils;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.sabaq.muse.utils.Helper.CURRENT_MAP_ITEM_INDEX;
import static com.sabaq.muse.utils.Helper.CURRENT_SUBJECTID;
import static com.sabaq.muse.utils.Helper.CURRENT_CHAPTER_INDEX;

public class LessonMapActivity extends BaseActivity {
    LinearLayout llMain, llSubContainer;
    //  Long TopicIdFromIntent = (long) 0;
    // String subjectNameFromIntent;
    HorizontalScrollView horizontalScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (CURRENT_SUBJECTID!=null && CURRENT_SUBJECTID.equals("Urdu")) {
            setContentView(R.layout.activity_lesson_map_urdu);
        } else {
            setContentView(R.layout.activity_lesson_map);
        }

        initViews();
        GetAllLessons();

        horizontalScrollView.post(() ->
                horizontalScrollView.scrollTo(Helper.CURRENT_MAP_ITEM_INDEX,0));
    }

    private void initViews() {
        setBackgroundBarAnimation(this, CURRENT_SUBJECTID);
        llMain = findViewById(R.id.layout);
        ImageView ivLessonImage = findViewById(R.id.lessonImage);
        horizontalScrollView = findViewById(R.id.hs_container);
        setLayoutDirection(ivLessonImage);
        llMain.setGravity(Gravity.CENTER);
        llSubContainer = findViewById(R.id.layout);
        int image = 0;
        switch (CURRENT_SUBJECTID) {
            case "English":
                image = R.drawable.st1;
                break;
            case "Urdu":
                image = R.drawable.st3;
                break;
            case "Math":
                image = R.drawable.st4;
                break;
            case "Science":
                image = R.drawable.st2;
                break;
        }

        Glide.with(LessonMapActivity.this).load(image).into(ivLessonImage);
        getToolBar(Helper.CURRENT_TOPICNAME, View.VISIBLE, View.VISIBLE, View.INVISIBLE, this);

    }

    private void setLayoutDirection(ImageView ivLessonImage) {
        setViewDirection(llMain);
        setViewDirection(ivLessonImage);
        if (CURRENT_SUBJECTID.equals("Urdu")) {
            HorizontalScrollView horizontalScrollView = findViewById(R.id.hs_container);
            horizontalScrollView.post(() -> horizontalScrollView.fullScroll(ScrollView.FOCUS_RIGHT));
        }
    }


    public void GetAllLessons() {
        QueryBuilder<Exercise> qbExercises = AppController.daoSession.getExerciseDao().queryBuilder();
        QueryBuilder<Readaloud> qbReadalouds = AppController.daoSession.getReadaloudDao().queryBuilder();
        QueryBuilder<Video> qbVideos = AppController.daoSession.getVideoDao().queryBuilder();
        List<LessonViewModel> lessons = new ArrayList<>();
        Log.d("TopicId Lessonmap", "GetAllLessons: " + Helper.CURRENT_TOPICID);
        List<Exercise> lessonsExercises = qbExercises.where(qbExercises.and(ExerciseDao.Properties.Active.eq(true), ExerciseDao.Properties.TopicId.eq(Helper.CURRENT_TOPICID))).list();
        List<Readaloud> lessonsReadalouds = qbReadalouds.where(qbReadalouds.and(ReadaloudDao.Properties.Active.eq(true), ReadaloudDao.Properties.TopicId.eq(Helper.CURRENT_TOPICID))).list();
        List<Video> lessonsVideos = qbVideos.where(qbVideos.and(VideoDao.Properties.Active.eq(true), VideoDao.Properties.TopicId.eq(Helper.CURRENT_TOPICID))).list();

        lessons.addAll(castExercisesToViewModelList(lessonsExercises));
        lessons.addAll(castVideosToViewModelList(lessonsVideos));
        lessons.addAll(castReadaloudsToViewModelList(lessonsReadalouds));
        Collections.sort(lessons, new LessonComparator());
        if (lessons.size() > 0)
            llMain = MapUtils.generateRoadMap(LessonMapActivity.this, lessons, llMain, horizontalScrollView);
    }

    private List<LessonViewModel> castExercisesToViewModelList(List<Exercise> lessonsExercises) {
        List<LessonViewModel> lessonViewModels = new ArrayList<>();
        for (int i = 0; i < lessonsExercises.size(); i++) {
            LessonViewModel lessonViewModel = new LessonViewModel(lessonsExercises.get(i));
            lessonViewModels.add(lessonViewModel);
        }

        return lessonViewModels;
    }

    private List<LessonViewModel> castReadaloudsToViewModelList(List<Readaloud> lessonsReadalouds) {
        List<LessonViewModel> lessonViewModels = new ArrayList<>();
        for (int i = 0; i < lessonsReadalouds.size(); i++) {
            LessonViewModel lessonViewModel = new LessonViewModel(lessonsReadalouds.get(i));
            lessonViewModels.add(lessonViewModel);
        }
        return lessonViewModels;
    }

    private List<LessonViewModel> castVideosToViewModelList(List<Video> lessonsVideos) {
        List<LessonViewModel> lessonViewModels = new ArrayList<>();
        for (int i = 0; i < lessonsVideos.size(); i++) {
            LessonViewModel lessonViewModel = new LessonViewModel(lessonsVideos.get(i));
            lessonViewModels.add(lessonViewModel);
        }

        return lessonViewModels;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onClickCross(View view) {
        Intent intent = new Intent(LessonMapActivity.this, ChapterActivity.class);
        startActivity(intent);
        CURRENT_MAP_ITEM_INDEX=0;
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        CURRENT_MAP_ITEM_INDEX=0;
    }

    public void onClickHome(View view) {
        startActivity(new Intent(LessonMapActivity.this, SubjectsActivity.class));
        CURRENT_CHAPTER_INDEX = 0;
        CURRENT_MAP_ITEM_INDEX=0;
        finish();
    }

}
