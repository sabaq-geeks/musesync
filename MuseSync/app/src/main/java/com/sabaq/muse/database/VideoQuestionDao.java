package com.sabaq.muse.database;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.SqlUtils;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "VideoQuestions".
*/
public class VideoQuestionDao extends AbstractDao<VideoQuestion, Long> {

    public static final String TABLENAME = "VideoQuestions";

    /**
     * Properties of entity VideoQuestion.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "Id", true, "Id");
        public final static Property Active = new Property(1, Boolean.class, "Active", false, "Active");
        public final static Property CreatedDate = new Property(2, java.util.Date.class, "CreatedDate", false, "CreatedDate");
        public final static Property CreatedBy = new Property(3, String.class, "CreatedBy", false, "CreatedBy");
        public final static Property UpdatedDate = new Property(4, java.util.Date.class, "UpdatedDate", false, "UpdatedDate");
        public final static Property UpdatedBy = new Property(5, String.class, "UpdatedBy", false, "UpdatedBy");
        public final static Property IsAnswerCompulsory = new Property(6, Boolean.class, "IsAnswerCompulsory", false, "IsAnswerCompulsory");
        public final static Property Order = new Property(7, Integer.class, "Order", false, "Order");
        public final static Property PlayTime = new Property(8, Integer.class, "PlayTime", false, "PlayTime");
        public final static Property ForwardTime = new Property(9, Integer.class, "ForwardTime", false, "ForwardTime");
        public final static Property VideoId = new Property(10, Long.class, "VideoId", false, "VideoId");
    }

    private DaoSession daoSession;

    private Query<VideoQuestion> video_VideoQuestionsQuery;

    public VideoQuestionDao(DaoConfig config) {
        super(config);
    }
    
    public VideoQuestionDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"VideoQuestions\" (" + //
                "\"Id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: Id
                "\"Active\" INTEGER," + // 1: Active
                "\"CreatedDate\" INTEGER," + // 2: CreatedDate
                "\"CreatedBy\" TEXT," + // 3: CreatedBy
                "\"UpdatedDate\" INTEGER," + // 4: UpdatedDate
                "\"UpdatedBy\" TEXT," + // 5: UpdatedBy
                "\"IsAnswerCompulsory\" INTEGER," + // 6: IsAnswerCompulsory
                "\"Order\" INTEGER," + // 7: Order
                "\"PlayTime\" INTEGER," + // 8: PlayTime
                "\"ForwardTime\" INTEGER," + // 9: ForwardTime
                "\"VideoId\" INTEGER);"); // 10: VideoId
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"VideoQuestions\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, VideoQuestion entity) {
        stmt.clearBindings();
 
        Long Id = entity.getId();
        if (Id != null) {
            stmt.bindLong(1, Id);
        }
 
        Boolean Active = entity.getActive();
        if (Active != null) {
            stmt.bindLong(2, Active ? 1L: 0L);
        }
 
        java.util.Date CreatedDate = entity.getCreatedDate();
        if (CreatedDate != null) {
            stmt.bindLong(3, CreatedDate.getTime());
        }
 
        String CreatedBy = entity.getCreatedBy();
        if (CreatedBy != null) {
            stmt.bindString(4, CreatedBy);
        }
 
        java.util.Date UpdatedDate = entity.getUpdatedDate();
        if (UpdatedDate != null) {
            stmt.bindLong(5, UpdatedDate.getTime());
        }
 
        String UpdatedBy = entity.getUpdatedBy();
        if (UpdatedBy != null) {
            stmt.bindString(6, UpdatedBy);
        }
 
        Boolean IsAnswerCompulsory = entity.getIsAnswerCompulsory();
        if (IsAnswerCompulsory != null) {
            stmt.bindLong(7, IsAnswerCompulsory ? 1L: 0L);
        }
 
        Integer Order = entity.getOrder();
        if (Order != null) {
            stmt.bindLong(8, Order);
        }
 
        Integer PlayTime = entity.getPlayTime();
        if (PlayTime != null) {
            stmt.bindLong(9, PlayTime);
        }
 
        Integer ForwardTime = entity.getForwardTime();
        if (ForwardTime != null) {
            stmt.bindLong(10, ForwardTime);
        }
 
        Long VideoId = entity.getVideoId();
        if (VideoId != null) {
            stmt.bindLong(11, VideoId);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, VideoQuestion entity) {
        stmt.clearBindings();
 
        Long Id = entity.getId();
        if (Id != null) {
            stmt.bindLong(1, Id);
        }
 
        Boolean Active = entity.getActive();
        if (Active != null) {
            stmt.bindLong(2, Active ? 1L: 0L);
        }
 
        java.util.Date CreatedDate = entity.getCreatedDate();
        if (CreatedDate != null) {
            stmt.bindLong(3, CreatedDate.getTime());
        }
 
        String CreatedBy = entity.getCreatedBy();
        if (CreatedBy != null) {
            stmt.bindString(4, CreatedBy);
        }
 
        java.util.Date UpdatedDate = entity.getUpdatedDate();
        if (UpdatedDate != null) {
            stmt.bindLong(5, UpdatedDate.getTime());
        }
 
        String UpdatedBy = entity.getUpdatedBy();
        if (UpdatedBy != null) {
            stmt.bindString(6, UpdatedBy);
        }
 
        Boolean IsAnswerCompulsory = entity.getIsAnswerCompulsory();
        if (IsAnswerCompulsory != null) {
            stmt.bindLong(7, IsAnswerCompulsory ? 1L: 0L);
        }
 
        Integer Order = entity.getOrder();
        if (Order != null) {
            stmt.bindLong(8, Order);
        }
 
        Integer PlayTime = entity.getPlayTime();
        if (PlayTime != null) {
            stmt.bindLong(9, PlayTime);
        }
 
        Integer ForwardTime = entity.getForwardTime();
        if (ForwardTime != null) {
            stmt.bindLong(10, ForwardTime);
        }
 
        Long VideoId = entity.getVideoId();
        if (VideoId != null) {
            stmt.bindLong(11, VideoId);
        }
    }

    @Override
    protected final void attachEntity(VideoQuestion entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public VideoQuestion readEntity(Cursor cursor, int offset) {
        VideoQuestion entity = new VideoQuestion( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // Id
            cursor.isNull(offset + 1) ? null : cursor.getShort(offset + 1) != 0, // Active
            cursor.isNull(offset + 2) ? null : new java.util.Date(cursor.getLong(offset + 2)), // CreatedDate
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // CreatedBy
            cursor.isNull(offset + 4) ? null : new java.util.Date(cursor.getLong(offset + 4)), // UpdatedDate
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // UpdatedBy
            cursor.isNull(offset + 6) ? null : cursor.getShort(offset + 6) != 0, // IsAnswerCompulsory
            cursor.isNull(offset + 7) ? null : cursor.getInt(offset + 7), // Order
            cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8), // PlayTime
            cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9), // ForwardTime
            cursor.isNull(offset + 10) ? null : cursor.getLong(offset + 10) // VideoId
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, VideoQuestion entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setActive(cursor.isNull(offset + 1) ? null : cursor.getShort(offset + 1) != 0);
        entity.setCreatedDate(cursor.isNull(offset + 2) ? null : new java.util.Date(cursor.getLong(offset + 2)));
        entity.setCreatedBy(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setUpdatedDate(cursor.isNull(offset + 4) ? null : new java.util.Date(cursor.getLong(offset + 4)));
        entity.setUpdatedBy(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setIsAnswerCompulsory(cursor.isNull(offset + 6) ? null : cursor.getShort(offset + 6) != 0);
        entity.setOrder(cursor.isNull(offset + 7) ? null : cursor.getInt(offset + 7));
        entity.setPlayTime(cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8));
        entity.setForwardTime(cursor.isNull(offset + 9) ? null : cursor.getInt(offset + 9));
        entity.setVideoId(cursor.isNull(offset + 10) ? null : cursor.getLong(offset + 10));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(VideoQuestion entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(VideoQuestion entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(VideoQuestion entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "videoQuestions" to-many relationship of Video. */
    public List<VideoQuestion> _queryVideo_VideoQuestions(Long VideoId) {
        synchronized (this) {
            if (video_VideoQuestionsQuery == null) {
                QueryBuilder<VideoQuestion> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.VideoId.eq(null));
                video_VideoQuestionsQuery = queryBuilder.build();
            }
        }
        Query<VideoQuestion> query = video_VideoQuestionsQuery.forCurrentThread();
        query.setParameter(0, VideoId);
        return query.list();
    }

    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getVideoDao().getAllColumns());
            builder.append(" FROM VideoQuestions T");
            builder.append(" LEFT JOIN Videos T0 ON T.\"VideoId\"=T0.\"Id\"");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected VideoQuestion loadCurrentDeep(Cursor cursor, boolean lock) {
        VideoQuestion entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        Video video = loadCurrentOther(daoSession.getVideoDao(), cursor, offset);
        entity.setVideo(video);

        return entity;    
    }

    public VideoQuestion loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<VideoQuestion> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<VideoQuestion> list = new ArrayList<VideoQuestion>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<VideoQuestion> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<VideoQuestion> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
