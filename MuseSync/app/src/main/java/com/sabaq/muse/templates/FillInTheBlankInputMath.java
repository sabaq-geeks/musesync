package com.sabaq.muse.templates;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sabaq.muse.R;
import com.sabaq.muse.activities.LessonMapActivity;
import com.sabaq.muse.baseActivity.BaseExerciseActivity;
import com.sabaq.muse.datasource.ExerciseDataFetcher;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.ConvertDptoPx;
import com.sabaq.muse.utils.ExerciseUIUtils;
import com.sabaq.muse.utils.Helper;
import com.sabaq.muse.utils.MediaPlayerUtil;
import com.sabaq.muse.viewModel.ExerciseAnswersViewModel;
import com.sabaq.muse.viewModel.ExerciseQuestionViewModel;
import com.sabaq.muse.viewModel.ExerciseViewModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.sabaq.muse.utils.Helper.LOCAL_PATH;

public class FillInTheBlankInputMath extends BaseExerciseActivity {
    private MediaPlayer multiSoundPlayer = null;
    private boolean isSoundClicked = true;
    private String answer, buttonText = "", stConcatenated = "";
    private int questionIndex = 0;
    private byte correctAnswerCount = 0;
    ExerciseViewModel exerciseViewModel;
    EditText editTextAnswerField;
    ArrayList<String> inputArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_in_the_blank_input_math);
        Long exerciseId = Helper.CURRENT_CONTENTID;
        dataFetchFromDb(exerciseId);
        getToolBar(exerciseViewModel.getExerciseTitle(), View.VISIBLE, View.VISIBLE, View.VISIBLE, this);
//        setBackgroundBarAnimation(this, Helper.CURRENT_SUBJECTID);
        setBackgroundBarAnimation(this, "Math");
        loadLayout();
        softKeyBoardDownSystemUiHide(R.id.cl_container);
        setSoftKeyBoardOffWhenActivityLaunch();
        isTablet();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (multiSoundPlayer != null) {
            multiSoundPlayer.stop();
        }
    }

    public void onClickCross(View view) {
        Intent intent = new Intent(FillInTheBlankInputMath.this, LessonMapActivity.class);
        startActivity(intent);
    }

    public void onClickKeyboard(View view) {
        Button b = (Button) view;
        buttonText = b.getText().toString();
        int textLength = inputArray.size();
        int answerLength = getAnswerLength(answer);
        if (textLength < answerLength) {
            textLength++;
            inputArray.add(buttonText);
            inputInEditText(editTextAnswerField);
        }
    }

    public void onClickClear(View view) {
        if (editTextAnswerField != null && inputArray.size() > 0) {
            stConcatenated = onClear(stConcatenated);
            editTextAnswerField.setText(stConcatenated);
        }
    }

    public void onClickDot(View view) {
        if (stConcatenated.contains("."))
//            findViewById(R.id.btn_dot).setEnabled(false);
//        else {
//            findViewById(R.id.btn_dot).setEnabled(true);
            onClickKeyboard(view);
//        }
    }

    public void onClickHint(View view) {
        showHintDialog(this, exerciseViewModel.getInstruction(), exerciseViewModel.getInstructionSoundPath(), exerciseViewModel.getPath());
    }

    private void dataFetchFromDb(Long exerciseId) {
        ExerciseDataFetcher exerciseDataFetcher = new ExerciseDataFetcher();
        exerciseViewModel = exerciseDataFetcher.getExercisesFromDb(21);
    }

    private LinearLayout generateContainer() {
        LinearLayout llHorizontalContainer = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llHorizontalContainer.setGravity(Gravity.LEFT);
        llHorizontalContainer.setLayoutParams(layoutParams);
        return llHorizontalContainer;
    }

    private TextView generateTextView(String text) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.LEFT;
        TextView tvQuestionWord = ExerciseUIUtils.generateTextView(text, FillInTheBlankInputMath.this, R.dimen._16sdp, params, Color.WHITE, View.TEXT_ALIGNMENT_CENTER, Gravity.CENTER);
        return tvQuestionWord;
    }

    private LinearLayout generateLinearEditText(String text, List<ExerciseAnswersViewModel> answersViewModelsList) {
        LinearLayout llEditText = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Log.d("Question", "generateQuestion: " + text);
        final EditText etAnswerInput = generateEditText();
        etAnswerInput.setText(loadAnswer());

        editTextAnswerField = etAnswerInput;
        etAnswerInput.setOnFocusChangeListener((view, b) -> {
            Log.d("onFocusChange", questionIndex + "");
            /*When First question generate it will not show*/
            if (questionIndex > 0) {
                answersViewModelsList.get(0).setSelectedAnswer(etAnswerInput.getText().toString());
            }
        });
        llEditText.addView(etAnswerInput);
        llEditText.setLayoutParams(layoutParams);
        return llEditText;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void generateQuestion(ExerciseQuestionViewModel questionViewModel, LinearLayout llQuestionContainer) {
        final String imagePath;
        final String audioPath;
        final String questionInstructionPath;
        File fileSound = null;
        File fileInstructionSound = null;
        String text;
        imagePath = questionViewModel.getImagePath();
        audioPath = questionViewModel.getAudioPath();
        questionInstructionPath = questionViewModel.getInstructionAudioPath();
        text = questionViewModel.getQuestion();

        if (!TextUtils.isEmpty(audioPath)) {
            fileSound = new File(LOCAL_PATH, audioPath);
        }
        if (!TextUtils.isEmpty(questionInstructionPath)) {
            fileInstructionSound = new File(LOCAL_PATH, questionInstructionPath);
        }

        //setAudioButton
        if (!TextUtils.isEmpty(questionInstructionPath) && fileInstructionSound.exists()) {
            setExQuestionInstructionAudioPath(questionInstructionPath);
        }
        String convertedQuestion = text.replace("$", " $ ");
        String[] splittedQuestionArray = convertedQuestion.split(" ");
        //previous length 40
        int lineCount = ((convertedQuestion + " " + answer).length() / 50) + 1;

        //Contains all the the answers being put in Edit Text Box
        String[] answerText = new String[dollarCount(convertedQuestion)];
        for (int i = 0; i < dollarCount(convertedQuestion); i++) {
            answerText[i] = answer;
        }

        LinearLayout llHorizontalContainer;
        //previous length 40
        int breakLineLength = 50;
        int k = 0;

        //Defines working at which dollar sign , first or second
        int answerBox = 0;

        for (int i = 0; i < lineCount; i++) {
            //Defines how many Char in current horizontal Line
            int horizontalLineTextCount = 0;

            //Add Horizontal Container to the View
            llHorizontalContainer = generateContainer();
            llQuestionContainer.addView(llHorizontalContainer);
            final File finalFileSound = fileSound;
            llQuestionContainer.setOnClickListener(v -> {
                if (!TextUtils.isEmpty(audioPath) && finalFileSound.exists()) {
                    setAudio(audioPath);
                }
            });

            for (; k < splittedQuestionArray.length; k++) {
                //For Answer Edit Text Box
                if (splittedQuestionArray[k].equals("$")) {
                    //In case to be added in current line
                    if (breakLineLength - horizontalLineTextCount > 5) {
                        llHorizontalContainer.addView(generateLinearEditText(answerText[answerBox], questionViewModel.getExerciseAnswers()));
                        horizontalLineTextCount += answerText[answerBox].length();
                    }
                    //If only 5 spaces left, then to be added in next line
                    else {
                        llHorizontalContainer = generateContainer();
                        llQuestionContainer.addView(llHorizontalContainer);
                        llHorizontalContainer.addView(generateLinearEditText(answerText[answerBox], questionViewModel.getExerciseAnswers()));
                        horizontalLineTextCount += answerText[answerBox].length();
                    }
                    answerBox++;
                    continue;
                }
                //For Simple Question Text
                if (horizontalLineTextCount + splittedQuestionArray[k].length() < breakLineLength) {
                    splittedQuestionArray[k] = splittedQuestionArray[k] + " ";

                    llHorizontalContainer.addView(generateTextView(splittedQuestionArray[k]));
                    horizontalLineTextCount += splittedQuestionArray[k].length();
                } else break;
            }
        }

        if (imagePath == null) {
            ImageView imgQuestion = generateQuestionIv(this, imagePath, text, null);
            ((LinearLayout) findViewById(R.id.ll_image_container)).addView(imgQuestion);
            imgQuestion.setOnClickListener(view -> showImageInZoom(this, imagePath));
        }
    }

    private int dollarCount(String question) {
        int counter = 0;
        for (int i = 0; i < question.length(); i++) {
            if (question.charAt(i) == '$') {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Max length of edit text input= answer.length() +5
     */
    @NonNull
    private EditText generateEditText() {
        EditText etQuestion = new EditText(this);
        int etHeight;
        if (isTablet())
            etHeight = 70;
        else etHeight = 50;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, new ConvertDptoPx(this, etHeight).dpToPx());
        params.gravity = Gravity.TOP;

        etQuestion.setMaxHeight(new ConvertDptoPx(this, 150).dpToPx());
        etQuestion.setMinWidth(new ConvertDptoPx(this, 150).dpToPx());
        etQuestion.setGravity(Gravity.CENTER);
        etQuestion.setLayoutParams(params);

        int maxLength = getAnswerLength(answer);
        etQuestion.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
        etQuestion.setTypeface(Typeface.create("sans-serif-black", Typeface.NORMAL));
        etQuestion.setBackground(getResources().getDrawable(R.drawable.shape_dash_boarder));
        etQuestion.setTextColor(Color.WHITE);
        etQuestion.setFocusable(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            etQuestion.setLetterSpacing(0.02f);
        }
        if (isTablet())
            etQuestion.setTextSize(40);
        else etQuestion.setTextSize(20);
        etQuestion.setTextColor(Color.WHITE);
        return etQuestion;
    }

    private void setExQuestionInstructionAudioPath(final String instructionAudioPath) {
        LinearLayout btnExQuestionInstructionSound = findViewById(R.id.ll_speaker);
        btnExQuestionInstructionSound.setVisibility(View.VISIBLE);
        btnExQuestionInstructionSound.setOnClickListener(v -> setAudio(instructionAudioPath));

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void loadLayout() {
        List<ExerciseQuestionViewModel> questionViewModels = exerciseViewModel.getExerciseQuestions();
        ExerciseQuestionViewModel questionViewModel = questionViewModels.get(questionIndex);
        answer = questionViewModels.get(questionIndex).getExerciseAnswers().get(0).getAnswer();
        LinearLayout llQuestionContainer = findViewById(R.id.ll_question_container);
        llQuestionContainer.removeAllViews();
        ((LinearLayout) findViewById(R.id.ll_image_container)).removeAllViews();
        generateQuestion(questionViewModel, llQuestionContainer);
        if (questionIndex <= questionViewModels.size() - 1) {
            setNavigation(exerciseViewModel, multiSoundPlayer, questionIndex);
        }
//            setNavigation(questionViewModels);

        setClearLongPress();
    }

    private int getAnswerLength(String answer) {
        return answer.length();
    }

    /*DeviceType :true = for tablet ,false =Phone*/
    private boolean isTablet() {
        return getResources().getBoolean(R.bool.isTablet);
    }

    private void setAudio(String audio) {
        if (isSoundClicked == true) {
            multiSoundPlayer = new MediaPlayerUtil().playSound(this, audio, "exercise", false);
            isSoundClicked = false;
            multiSoundPlayer.setOnCompletionListener(mp -> {
                isSoundClicked = true;
            });
        }
    }

//    private void setNavigation(List<ExerciseQuestionViewModel> questionViewModels) {
//        ImageView btnRight = findViewById(R.id.btnRightNav);
//        ImageView btnLeft = findViewById(R.id.btnLeftNav);
//        if (multiSoundPlayer != null) {
//            multiSoundPlayer.stop();
//            isSoundClicked = true;
//        }
//        if (questionIndex == 0)
//            btnLeft.setVisibility(View.INVISIBLE);
//
//        else if (questionIndex == questionViewModels.size() - 1) {
//            btnRight.setVisibility(View.INVISIBLE);
//            setResult();
//        } else {
//            btnRight.setVisibility(View.VISIBLE);
//            btnLeft.setVisibility(View.VISIBLE);
//        }
//
//        btnRight.setOnClickListener(view -> {
//            //  setAnswerInViewModel;
//            setAnswerIntoAnswerViewModel();
//            checkIfAnswerCorrect();
//            //   FillInTheBlankInputMath.this.editTextAnswerField.setText("");
//            inputArray = new ArrayList<>();
//            loadNextQuestion(true);
//
//        });
//        btnLeft.setOnClickListener(view -> {
//            loadNextQuestion(false);
//
//
//        });
//    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void loadNextQuestion(boolean isAdded) {
        if (isAdded)
            ++questionIndex;
        else
            --questionIndex;

        loadLayout();

    }

    private String loadAnswer() {
        List<ExerciseQuestionViewModel> questionViewModels = exerciseViewModel.getExerciseQuestions();
        if (questionViewModels.get(questionIndex).getExerciseAnswers().get(0).getSelectedAnswer() != null) {
            return questionViewModels.get(questionIndex).getExerciseAnswers().get(0).getSelectedAnswer();
        }
        return "";
    }

    private void checkIfAnswerCorrect() {
        String givenAnswer = stConcatenated;
        String correctAnswer = exerciseViewModel.getExerciseQuestions().get(questionIndex).getExerciseAnswers().get(0).getAnswer().replace(",", "");
        correctAnswer = correctAnswer.trim();
        if (stConcatenated != null && !stConcatenated.isEmpty()) {
            if (correctAnswer.equals(givenAnswer)) {
                Toast.makeText(this, "Correct", Toast.LENGTH_SHORT).show();
                correctAnswerCount++;
            } else
                Toast.makeText(this, "Wrong", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void inputInEditText(EditText editText) {
        if (editText != null) {
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < inputArray.size(); i++) {
                strBuilder.append(inputArray.get(i));
            }
            stConcatenated = strBuilder.toString();
            editText.setText(stConcatenated);
        }
    }

    private String onClear(String answer) {
        if (answer != null && answer.length() > 0) {
            answer = answer.substring(0, answer.length() - 1);
        }
        inputArray.remove(inputArray.size() - 1);
        return answer;
    }

    private void setAnswerIntoAnswerViewModel() {
        exerciseViewModel.getExerciseQuestions().get(questionIndex).getExerciseAnswers().get(0).setSelectedAnswer(stConcatenated);

    }

    private void setClearLongPress() {
        findViewById(R.id.btn_clear).setOnLongClickListener(v -> {
            stConcatenated = "";
            editTextAnswerField.setText("");
            inputArray = new ArrayList<>();
            findViewById(R.id.btn_dot).setEnabled(true);
            return false;
        });
    }

    public void onClickRight(View view) {
        setNavigation(exerciseViewModel, multiSoundPlayer, questionIndex);
        loadNextQuestion(true);
        inputArray = new ArrayList<>();
    }

    public void onClickLeft(View view) {
        setNavigation(exerciseViewModel, multiSoundPlayer, questionIndex);
        loadNextQuestion(false);
    }

}
