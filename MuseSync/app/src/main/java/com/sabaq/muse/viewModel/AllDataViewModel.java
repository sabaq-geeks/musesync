package com.sabaq.muse.viewModel;

import com.sabaq.muse.database.Document;
import com.sabaq.muse.database.Exercise;
import com.sabaq.muse.database.ExerciseAnswer;
import com.sabaq.muse.database.ExerciseQuestion;
import com.sabaq.muse.database.Grade;
import com.sabaq.muse.database.Readaloud;
import com.sabaq.muse.database.ReadaloudDictionary;
import com.sabaq.muse.database.ReadaloudSlide;
import com.sabaq.muse.database.SubTopic;
import com.sabaq.muse.database.Topic;
import com.sabaq.muse.database.Video;

import java.util.List;

public class AllDataViewModel {
    private List<Video> videos;
    private List<Readaloud> readalouds;
    private List<ReadaloudDictionary> readaloudDictionaries;
    private List<ReadaloudSlide> readaloudSlides;
    private List<ExerciseAnswer> exerciseAnswers;
    private List<ExerciseQuestion> exerciseQuestions;
    private List<Exercise> exercises;
    private List<Document> documents;
    private List<Grade> grades;
    private List<Subject> subjects;
    private List<Topic> topics;
    private List<SubTopic> subTopics;


    public AllDataViewModel(List<Video> videos, List<Readaloud> readalouds, List<ExerciseAnswer> exerciseAnswers, List<ExerciseQuestion> exerciseQuestions, List<ReadaloudDictionary> readaloudDictionaries, List<ReadaloudSlide> readaloudSlides, List<Exercise> exercises, List<Document> documents, List<Grade> grades, List<Subject> subjects, List<Topic> topics, List<SubTopic> subTopics) {
        this.videos = videos;
        this.readalouds = readalouds;
        this.exercises = exercises;
        this.documents = documents;
        this.grades = grades;
        this.subjects = subjects;
        this.topics = topics;
        this.subTopics = subTopics;
        this.exerciseAnswers=exerciseAnswers;
        this.exerciseQuestions=exerciseQuestions;
        this.readaloudDictionaries=readaloudDictionaries;
        this.readaloudSlides=readaloudSlides;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    public List<Readaloud> getReadalouds() {
        return readalouds;
    }

    public void setReadalouds(List<Readaloud> readalouds) {
        this.readalouds = readalouds;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    public List<Grade> getGrades() {
        return grades;
    }

    public void setGrades(List<Grade> grades) {
        this.grades = grades;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public List<SubTopic> getSubTopics() {
        return subTopics;
    }

    public void setSubTopics(List<SubTopic> subTopics) {
        this.subTopics = subTopics;
    }

    public List<ReadaloudDictionary> getReadaloudDictionaries() {
        return readaloudDictionaries;
    }

    public void setReadaloudDictionaries(List<ReadaloudDictionary> readaloudDictionaries) {
        this.readaloudDictionaries = readaloudDictionaries;
    }

    public List<ReadaloudSlide> getReadaloudSlides() {
        return readaloudSlides;
    }

    public void setReadaloudSlides(List<ReadaloudSlide> readaloudSlides) {
        this.readaloudSlides = readaloudSlides;
    }

    public List<ExerciseAnswer> getExerciseAnswers() {
        return exerciseAnswers;
    }

    public void setExerciseAnswers(List<ExerciseAnswer> exerciseAnswers) {
        this.exerciseAnswers = exerciseAnswers;
    }

    public List<ExerciseQuestion> getExerciseQuestions() {
        return exerciseQuestions;
    }

    public void setExerciseQuestions(List<ExerciseQuestion> exerciseQuestions) {
        this.exerciseQuestions = exerciseQuestions;
    }
}