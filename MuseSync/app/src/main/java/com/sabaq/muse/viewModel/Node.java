package com.sabaq.muse.viewModel;

public class Node {


    private int PreviousThread;
    private int NextThread;
    private int margin;

    public Node(int previousThread, int nextThread, int margin) {
        PreviousThread = previousThread;
        NextThread = nextThread;
        this.margin = margin;
    }

    public int getPreviousThread() {
        return PreviousThread;
    }

    public void setPreviousThread(int previousThread) {
        PreviousThread = previousThread;
    }

    public int getNextThread() {
        return NextThread;
    }

    public void setNextThread(int nextThread) {
        NextThread = nextThread;
    }

    public int getMargin() {
        return margin;
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }
}

