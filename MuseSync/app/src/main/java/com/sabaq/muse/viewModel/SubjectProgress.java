package com.sabaq.muse.viewModel;

import android.graphics.drawable.Drawable;

public class SubjectProgress {


    private String SubjectName;
    private int Score;
    private int OutOf;
    // TODO: 9/27/2018 code convention
    private Drawable progressBarColor;
    private int colorSubject;
    public SubjectProgress(String subjectName, int score, int outOf, Drawable progressBarColor,int colorSubject) {
        SubjectName = subjectName;
        Score = score;
        OutOf = outOf;
        this.progressBarColor = progressBarColor;
        this.colorSubject=colorSubject;
    }

    public Drawable getProgressBarColor() {
        return progressBarColor;
    }

    public void setProgressBarColor(Drawable progressBarColor) {
        this.progressBarColor = progressBarColor;
    }

    public int getColorSubject() {
        return colorSubject;
    }

    public void setColorSubject(int colorSubject) {
        this.colorSubject = colorSubject;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }

    public int getOutOf() {
        return OutOf;
    }

    public void setOutOf(int outOf) {
        OutOf = outOf;
    }
}
