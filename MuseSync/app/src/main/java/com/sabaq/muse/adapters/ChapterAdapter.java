package com.sabaq.muse.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sabaq.muse.R;
import com.sabaq.muse.baseActivity.BaseActivity;
import com.sabaq.muse.utils.ExerciseUIUtils;
import com.sabaq.muse.viewModel.ChapterViewModel;

import java.util.List;

import static com.sabaq.muse.utils.Helper.CURRENT_SUBJECTID;

public class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.MyViewHolder> {

    private Context mContext;
    private List<ChapterViewModel> chapterList;
    private ItemClickListener clickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, description;
        ImageView thumbnail;
        ProgressBar progressbar;

        MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.tvChapterTitle);
            description = view.findViewById(R.id.tvChapterDescription);
            thumbnail = view.findViewById(R.id.ivChapterThumbnail);
            progressbar = view.findViewById(R.id.progressbar);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                clickListener.onClickChapterItem(view, getAdapterPosition(), chapterList);
        }

    }

    public ChapterAdapter(Context mContext, List<ChapterViewModel> chapterList) {
        this.mContext = mContext;
        this.chapterList = chapterList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = setView(parent);

        return new MyViewHolder(itemView);
    }

    private View setView(ViewGroup parent) {
        int layoutId;
        if (CURRENT_SUBJECTID.equals(mContext.getString(R.string.subject_id_urdu)))
            layoutId = R.layout.chapter_item_urdu;
        else layoutId = R.layout.chapter_item;
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(layoutId, parent, false);
        if (CURRENT_SUBJECTID.equals(mContext.getString(R.string.subject_id_urdu)))
            itemView.setRotationY(180);
        return itemView;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        ChapterViewModel chapterViewModel = chapterList.get(position);
//        holder.title = ExerciseUIUtils.setTextStyle(mContext, holder.title, R.dimen._16sdp, Color.WHITE);
        holder.title.setText(chapterViewModel.getTitle());
        //holder.title.setText(chapterViewModel.getTitle());
        if (chapterViewModel.getDescription() != null && !chapterViewModel.getDescription().equals("") && !chapterViewModel.getDescription().isEmpty() && !chapterViewModel.getDescription().trim().equals("."))
            holder.description.setText(chapterViewModel.getDescription());

        Glide.with(mContext).load(chapterViewModel.getThumbnail()).into(holder.thumbnail);
        holder.progressbar.setProgressDrawable(chapterViewModel.getProgressBarColor());
    }

    @Override
    public int getItemCount() {
        return chapterList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


}