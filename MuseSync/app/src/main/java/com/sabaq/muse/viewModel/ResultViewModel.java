package com.sabaq.muse.viewModel;

public class ResultViewModel {

    private int TotalQuestion;
    private int CorrectAnswer;
    private int WrongAnswer;

    public ResultViewModel(int totalQuestion, int correctAnswer, int wrongAnswer) {
        TotalQuestion = totalQuestion;
        CorrectAnswer = correctAnswer;
        WrongAnswer = wrongAnswer;
    }

    public int getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public int getCorrectAnswer() {
        return CorrectAnswer;
    }

    public void setCorrectAnswer(int correctAnswer) {
        CorrectAnswer = correctAnswer;
    }

    public int getWrongAnswer() {
        return WrongAnswer;
    }

    public void setWrongAnswer(int wrongAnswer) {
        WrongAnswer = wrongAnswer;
    }
}
