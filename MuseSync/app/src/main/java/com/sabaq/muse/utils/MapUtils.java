package com.sabaq.muse.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sabaq.muse.R;
import com.sabaq.muse.activities.ReadAloudActivity;
import com.sabaq.muse.activities.VideoPlayerActivity;
import com.sabaq.muse.templates.ExerciseDragAndDropActivity;
import com.sabaq.muse.templates.ExerciseMCQsActivity;
import com.sabaq.muse.templates.FillInTheBlankInputEnglish;
import com.sabaq.muse.templates.FillInTheBlankInputMath;
import com.sabaq.muse.viewModel.LessonViewModel;
import com.sabaq.muse.viewModel.Node;

import java.util.ArrayList;
import java.util.List;

import info.abdolahi.CircularMusicProgressBar;

import static android.view.View.TEXT_ALIGNMENT_CENTER;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.sabaq.muse.utils.Helper.CURRENT_MAP_ITEM_INDEX;
import static com.sabaq.muse.utils.Helper.CURRENT_SUBJECTID;

public class MapUtils {


    private static ArrayList<Node> nodes;
    private static String TAG = "MapUtils";
    /*
     * It generates lesson map
     *
     * @param context
     * @param lessonViewModels list of all lessons to be displayed
     * @param llMain           in which map to be added
     * @return
     */
    /*public static LinearLayout generateRoadMap(Context context, List<LessonViewModel> lessonViewModels, LinearLayout llMain) {
        initNodes();
        int nodeCount = 0;
        final float scale = context.getResources().getDisplayMetrics().density;
        Log.d(TAG, "generateRoadMap: "+lessonViewModels.size());
        for (int i = 0; i < 26; i++) {
            int dp = nodes.get(nodeCount).getMargin();
            int pixels = (int) (dp * scale + 0.5f);
            LessonViewModel lessonViewModel =  lessonViewModels.get(0);

            LinearLayout llSubContainer = new LinearLayout(context);
            llSubContainer.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams llSubContainerLayoutparams = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
            llSubContainerLayoutparams.gravity = Gravity.CENTER;
            llSubContainer.setLayoutParams(llSubContainerLayoutparams);

            //creating circle
            ImageView circle = generateCircle(context,lessonViewModels,i,pixels);
            CircularMusicProgressBar circleProgress = generateCircleProgressBar(1, circle, 80, context);

            //creating title
            TextView title = generateTitle(i+"", context,circle);
            LinearLayout nodesContainerLayout = new LinearLayout(context);
            nodesContainerLayout.setOrientation(LinearLayout.VERTICAL);


            LinearLayout.LayoutParams circleTitleLayoutparams = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
            circleTitleLayoutparams.gravity = Gravity.CENTER;

            circle.setLayoutParams(circleTitleLayoutparams);
            title.setLayoutParams(circleTitleLayoutparams);
            LinearLayout.LayoutParams nodesContainerLayoutLParams = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);

            nodesContainerLayoutLParams.setMargins(0, pixels, 0, 0);
            nodesContainerLayout.setLayoutParams(nodesContainerLayoutLParams);
               {
                nodesContainerLayout.addView(circle);
            }
            nodesContainerLayout.addView(title);


            llSubContainer.addView(nodesContainerLayout);
            ImageView thread = generateThread(nodeCount, context,circle);
            llSubContainer.addView(thread);
            // creating thread
            if (i == (26 - 1)) {
                thread.setVisibility(View.INVISIBLE);
            }
            if (nodeCount == 19) {
                nodeCount = 0;
            } else {
                nodeCount++;
            }

            llMain.addView(llSubContainer);
        }
        return llMain;
    }*/


    /**
     * It generates lesson map
     *
     * @param context
     * @param lessonViewModels list of all lessons to be displayed
     * @param llMain           in which map to be added
     * @return
     */
    public static LinearLayout generateRoadMap(Context context, List<LessonViewModel> lessonViewModels, LinearLayout llMain, HorizontalScrollView horizontalScrollView) {
        initNodes();
        int nodeCount = 0;
        final float scale = context.getResources().getDisplayMetrics().density;
        Log.d(TAG, "generateRoadMap: " + lessonViewModels.size());
        for (int i = 0; i < lessonViewModels.size(); i++) {
            int dp = nodes.get(nodeCount).getMargin();
            int pixels = (int) (dp * scale + 0.5f);
            LessonViewModel lessonViewModel = lessonViewModels.get(i);

            RelativeLayout llSubContainer = new RelativeLayout(context);
            RelativeLayout.LayoutParams llSubContainerLayoutparams = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
            llSubContainer.setLayoutParams(llSubContainerLayoutparams)  ;

            //creating circle
            ImageView circle = generateCircle(context, lessonViewModels, i, pixels,horizontalScrollView);
            circle.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            CircularMusicProgressBar circleProgress = generateCircleProgressBar(1, circle, 80, context);
//            horizontalScrollView[i].getParent().requestChildFocus(horizontalScrollView[i], horizontalScrollView[i]);
            //creating title
            TextView title = generateTitle(lessonViewModel.getTitle(), context, circle);
            setViewDirection(circle, title);
            LinearLayout.LayoutParams nodesContainerLayoutLParams = new LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);

            nodesContainerLayoutLParams.setMargins(0, pixels, 0, 0);

           /* if ((i == 4 || i == 8 || i == 7)) {
                nodesContainerLayout.addView(circleProgress);
            } else*/
            {
                llSubContainer.addView(circle);
            }
            llSubContainer.addView(title);
            // nodesContainerLayout.setId(i);

            ImageView thread = generateThread(nodeCount, context, circle);
            llSubContainer.addView(thread);

            // creating thread
            if (i == (lessonViewModels.size() - 1)) {
                thread.setVisibility(View.INVISIBLE);
            }

            if (nodeCount == nodes.size() - 1) {
                nodeCount = 0;
            } else {
                nodeCount++;
            }

            llMain.addView(llSubContainer);
        }

        return llMain;
    }

    private static void setViewDirection(ImageView circle, TextView title) {
        if (CURRENT_SUBJECTID.equals("Urdu")) {
            circle.setRotationY(180);
            title.setRotationY(180);
        }
    }



    public static int getMapCircleBg() {
        switch (Helper.CURRRENT_COLOR) {
            case 0:
                return R.drawable.circle_map_green;
            case 1:
                return R.drawable.circle_map_blue;
            case 2:
                return R.drawable.circle_map_pink;
            case 3:
                return R.drawable.circle_map_yellow;

        }
        return 0;

    }


    /**
     * @param nodeCount the count of present node
     * @param context
     * @return returns a thread image
     */

    @NonNull
    private static ImageView generateThread(int nodeCount, Context context, View view) {
        ImageView thread = new ImageView(context);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        params.addRule(RelativeLayout.RIGHT_OF, view.getId());
        //params.gravity = Gravity.TOP;
        thread.setLayoutParams(params);
//        thread.setBackgroundColor(Color.YELLOW);
        thread.setImageResource(nodes.get(nodeCount).getNextThread());
        return thread;
    }

    /**
     * It initializes nodes
     */

    private static void initNodes() {
        nodes = new ArrayList<Node>();
        Node node1 = new Node(0, R.drawable.n1, 30);
        Node node2 = new Node(0, R.drawable.n2, 170);
        Node node3 = new Node(0, R.drawable.n3, 110);
        Node node4 = new Node(0, R.drawable.n4, 20);
        Node node5 = new Node(0, R.drawable.n5, 175);
        Node node6 = new Node(0, R.drawable.n6, 130);
        Node node7 = new Node(0, R.drawable.n7, 30);
        Node node8 = new Node(0, R.drawable.n8, 185);
        Node node9 = new Node(0, R.drawable.n9, 120);
      /*  Node node10 = new Node(0, R.drawable.n10, 130);
        Node node11 = new Node(0, R.drawable.n11, 20);
        Node node12 = new Node(0, R.drawable.n12, 230);
        Node node13 = new Node(0, R.drawable.n13, 210);*/
     /*   Node node14 = new Node(0, R.drawable.n14, 140);
        Node node15 = new Node(0, R.drawable.n15, 160);
        Node node16 = new Node(0, R.drawable.n16, 180);
        Node node17 = new Node(0, R.drawable.n17, 30);
        Node node18 = new Node(0, R.drawable.n18, 200);
        Node node19 = new Node(0, R.drawable.n19, 150);
        Node node20 = new Node(0, R.drawable.n20, 210);*/
        nodes.add(node1);
        nodes.add(node2);
        nodes.add(node3);
        nodes.add(node4);
        nodes.add(node5);
        nodes.add(node6);
        nodes.add(node7);
        nodes.add(node8);
        nodes.add(node9);
       /* nodes.add(node10);
        nodes.add(node11);*/
     //   nodes.add(node12);
      //  nodes.add(node13);
      /*  nodes.add(node14);
        nodes.add(node15);
        nodes.add(node16);
        nodes.add(node17);
        nodes.add(node18);
        nodes.add(node19);
        nodes.add(node20);*/
    }

    @NonNull
    private static TextView generateTitle(String titleName, Context context, View view) {
        TextView title = new TextView(context);
//        title.setBackgroundColor(Color.BLUE);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (view.getWidth() * scale + 0.5f);
        params.setMargins(0, 10, pixels, 0);
        params.addRule(RelativeLayout.BELOW, view.getId());
        params.addRule(RelativeLayout.ALIGN_LEFT, view.getId());
        title.setLayoutParams(params);
        title.setText(setTextLimitInLine(titleName));

        title.setTextSize(16);
        title.setTextColor(Color.WHITE);
        title.setGravity(TEXT_ALIGNMENT_CENTER);
        title.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        return title;
    }

    private static String setTextLimitInLine(String titleName) {
        String s = titleName;
        int CHAR_COUNT_LIMIT = 10;
        String[] arr = s.split(" ");
        int count = 1;

        String text = "";
        for (String word : arr) {
            char[] tempArray = word.toCharArray();

            if (count + word.length() > CHAR_COUNT_LIMIT) {
                text = text + "\n";
                count = 0;
            }
            count = count + word.length();

            text = text + " " + word;


        }
        return text;
    }

    /**
     * @param context
     * @param horizontalScrollView
     * @return an imageview containing a circle
     */

    @NonNull
    private static ImageView generateCircle(Context context, List<LessonViewModel> lessonViewModels, int i, int pixelsFromTop, HorizontalScrollView horizontalScrollView) {
        ImageView circle = new ImageView(context);
        RelativeLayout.LayoutParams circleParams = new RelativeLayout.LayoutParams(90, 90);

        circle.setLayoutParams(circleParams);
        circle.setId(i + 1);

        circleParams.setMargins(0, pixelsFromTop, 0, 0);

        setOnClickListener(circle, context, lessonViewModels, i,horizontalScrollView);
        Log.d(TAG, "generateCircle: " + i);
        switch (lessonViewModels.get(i).getContentType()) {
            case Readaloud:
                circle.setImageResource(R.drawable.ic_readaloud);
                break;
            case Video:
                circle.setImageResource(R.drawable.ic_video);
                break;
            case Exercise:
                circle.setImageResource(R.drawable.ic_exercise);
                break;

        }
//        circle.setPadding(10, 10, 10, 10);
        circle.setScaleType(ImageView.ScaleType.FIT_XY);
        circle.setBackground(context.getResources().getDrawable(getMapCircleBg()));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            circle.setTransitionName("simple_activity_transition");
        }
        return circle;
    }

    private static void setOnClickListener(final ImageView circle, final Context context, final List<LessonViewModel> lessonViewModels, final int position, HorizontalScrollView horizontalScrollView) {
        circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CURRENT_MAP_ITEM_INDEX = position;
                Log.d(TAG, "MAPCLICK: "+position);
                Helper.CURRENT_MAP_ITEM_INDEX=horizontalScrollView.getScrollX();
                switch (lessonViewModels.get(position).getContentType()) {
                    case Video:
                        if (checkIfAllPreviousLessonsCompleted(lessonViewModels, position)) {
                            startLesson(context, lessonViewModels, position, VideoPlayerActivity.class);
                        } else {
                            //showDialog(context, lessonViewModels, position, VideoPlayerActivity.class);
                            startLesson(context, lessonViewModels, position, VideoPlayerActivity.class);
                        }
                        break;
                    case Readaloud:
                        if (checkIfAllPreviousLessonsCompleted(lessonViewModels, position)) {
                            startLesson(context, lessonViewModels, position, ReadAloudActivity.class);
                        } else {
                            //showDialog(context, lessonViewModels, position, ReadAloudActivity.class);
                            startLesson(context, lessonViewModels, position, ReadAloudActivity.class);
                        }
                        break;

                    case Exercise:
                        Class activity = getExerciseClass(lessonViewModels.get(position));
                        if (checkIfAllPreviousLessonsCompleted(lessonViewModels, position)) {

                            startLesson(context, lessonViewModels, position, activity);
                        } else {
                            // showDialog(context, lessonViewModels, position, activity);
                            startLesson(context, lessonViewModels, position, activity);
                        }
                        break;
                }
            }
        });
    }

    private static Class getExerciseClass(LessonViewModel lessonViewModel) {
        switch (lessonViewModel.getExerciseType()) {
            case "MCQs":
                return ExerciseMCQsActivity.class;
            case "Fill in the blanks (Input English)":
                return FillInTheBlankInputEnglish.class;
            case "Fill in the Blanks (Input)":
                return FillInTheBlankInputMath.class;
            case "Fill in the Blanks (Drag-Drop)":
                return ExerciseDragAndDropActivity.class;
        }


        return null;
    }

    private static void startLesson(Context context, List<LessonViewModel> lessonViewModels, int position, Class c) {
        if (c != null) {
            Intent intent = new Intent(context, c);
            Log.d(TAG, "onClick: ContentId" + lessonViewModels.get(position).getId());
            Helper.CURRENT_CONTENTID = lessonViewModels.get(position).getId();
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "Not Found.", Toast.LENGTH_SHORT).show();
        }
    }


    public static void showDialog(final Context context, final List<LessonViewModel> lessonViewModels, final int position, final Class c) {
        final Dialog mDialog;
        mDialog = new Dialog(context);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_message);
        mDialog.setCancelable(false);
        mDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);

//Show the dialog!
        mDialog.show();

//Set the dialog to immersive
        mDialog.getWindow().getDecorView().setSystemUiVisibility(
                ((Activity) context).getWindow().getDecorView().getSystemUiVisibility());

//Clear the not focusable flag from the window
        mDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        mDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView tvMessage;
        Button btnYes;
        btnYes = mDialog.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                startLesson(context, lessonViewModels, position, c);

            }
        });
        Button btnNo;
        btnNo = mDialog.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

            }
        });
        ImageView btnCross = mDialog.findViewById(R.id.btnCross);
        btnCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        tvMessage = mDialog.findViewById(R.id.tvMessage);
        tvMessage.setText("It seems that you haven't completed previous lessons.Do you want to continue?");
        mDialog.show();

    }

    private static boolean checkIfAllPreviousLessonsCompleted(List<LessonViewModel> lessonViewModels, int position) {
        for (int i = position - 1; i >= 0; i++) {
            if (!lessonViewModels.get(i).isCompleted())
                return false;
        }

        return true;
    }


    @SuppressLint("ClickableViewAccessibility")
    private static void setOnClickListener(final CircularMusicProgressBar circularProgressBar) {
        circularProgressBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return false;
            }
        });
    }

    /**
     * Generates a circular progress bar
     *
     * @param index
     * @param circle
     * @param progressValue progress
     * @param context
     * @return
     */
    private static CircularMusicProgressBar generateCircleProgressBar(int index, ImageView circle, int progressValue, Context context) {

        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (80 * scale + 0.5f);

        CircularMusicProgressBar circularProgressBar = new CircularMusicProgressBar(context);
        circularProgressBar.setLayoutParams(new LinearLayout.LayoutParams(pixels, pixels));

        circularProgressBar.setBorderColor(context.getResources().getColor(R.color.colorAccent));
        circularProgressBar.setBorderWidth(10);
        circularProgressBar.setImageDrawable(context.getResources().getDrawable(R.drawable.demo_circle));
        circularProgressBar.setBackground(context.getResources().getDrawable(R.drawable.demo_circle));
        circularProgressBar.setBorderProgressColor(context.getResources().getColor(R.color.colorChapterUrduBg));
        circularProgressBar.setValue(progressValue);
        // setOnClickListener(circularProgressBar);
        setOnClickListener(circularProgressBar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            circularProgressBar.setTransitionName("simple_activity_transition");
        }
        return circularProgressBar;
    }
}
