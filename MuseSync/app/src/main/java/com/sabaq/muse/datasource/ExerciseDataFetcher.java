package com.sabaq.muse.datasource;

import com.sabaq.muse.database.Exercise;
import com.sabaq.muse.database.ExerciseAnswer;
import com.sabaq.muse.database.ExerciseDao;
import com.sabaq.muse.database.ExerciseQuestion;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.viewModel.ExerciseAnswersViewModel;
import com.sabaq.muse.viewModel.ExerciseQuestionViewModel;
import com.sabaq.muse.viewModel.ExerciseViewModel;

import java.util.ArrayList;
import java.util.List;

public class ExerciseDataFetcher {


    public ExerciseViewModel getExercisesFromDb(long exerciseId) {
        Exercise exercise = AppController.daoSession.getExerciseDao().queryBuilder()
                .where(ExerciseDao.Properties.Id.eq(exerciseId)).list().get(0);
        return filterOutInActiveQuestionOptions(setExerciseViewModel(exercise));

    }

    public ExerciseViewModel getExercisesByTopicIdFromDb(int topicId) {
        Exercise exercise = AppController.daoSession.getExerciseDao().queryBuilder()
                .where(ExerciseDao.Properties.TopicId.eq(topicId)).list().get(0);
        return setExerciseViewModel(exercise);

    }

    private ExerciseViewModel filterOutInActiveQuestionOptions(ExerciseViewModel exerciseViewModel) {
        List<ExerciseQuestionViewModel> questionViewModels=exerciseViewModel.getExerciseQuestions();
        boolean isQuestionDeleted=false;
        for (int i = 0; i <questionViewModels.size() ; i++) {
            if(!questionViewModels.get(i).getActive()) {
                isQuestionDeleted=true;
                questionViewModels.remove(i);
            }
            if(!isQuestionDeleted){
                List<ExerciseAnswersViewModel> answersViewModels=questionViewModels.get(i).getExerciseAnswers();
                for (int j = 0; j < answersViewModels.size(); j++) {
                    if(!answersViewModels.get(j).isActive()){
                        answersViewModels.remove(j);
                    }
                }
            }
        }
        return exerciseViewModel;
    }

    private ExerciseViewModel setExerciseViewModel(Exercise exercise) {
        ArrayList<ExerciseQuestionViewModel> questionViewModels = new ArrayList<>();
        ExerciseQuestionViewModel questionViewModel = null;

        for (int i = 0; i < exercise.getExerciseQuestions().size(); i++) {

            ArrayList<ExerciseAnswersViewModel> exerciseAnswersViewModels = new ArrayList<>();
            ExerciseQuestion exerciseQuestion = exercise.getExerciseQuestions().get(i);

            for (int j = 0; j < exercise.getExerciseQuestions().get(i).getExerciseAnswers().size(); j++) {

                ExerciseAnswer exerciseAnswer = exerciseQuestion.getExerciseAnswers().get(j);
                ExerciseAnswersViewModel exerciseAnswersViewModel = new ExerciseAnswersViewModel(exerciseAnswer);
                exerciseAnswersViewModels.add(exerciseAnswersViewModel);

            }
            questionViewModel = new ExerciseQuestionViewModel(exercise.getExerciseQuestions().get(i), exerciseAnswersViewModels);
            questionViewModels.add(questionViewModel);
        }

        ExerciseViewModel exerciseViewModel = new ExerciseViewModel(exercise, questionViewModels);

        return exerciseViewModel;
    }


}
