package com.sabaq.muse.viewModel;

import android.widget.ImageView;

public class ImageModel {

    public int image;
    public int tag;
    public ImageView iv;
    public String containerFromTag;
    public int imageIndex;

    public ImageModel(int image, int tag, ImageView iv, String containerFromTag, int imageIndex) {
        this.image = image;
        this.tag = tag;
        this.iv = iv;
        this.containerFromTag = containerFromTag;
        this.imageIndex = imageIndex;
    }

    public ImageModel(int image) {
        this.image = image;
    }

    public ImageModel(int image, int tag) {
        this.image = image;
        this.tag = tag;
    }

    public ImageModel(int image, int tag, ImageView iv) {
        this.image = image;
        this.tag = tag;
        this.iv = iv;
    }

    public ImageModel(int image, int tag, ImageView iv, String containerFromTag) {
        this.image = image;
        this.tag = tag;
        this.iv = iv;
        this.containerFromTag = containerFromTag;
    }


    public int getImageIndex() {
        return imageIndex;
    }

    public void setImageIndex(int imageIndex) {
        this.imageIndex = imageIndex;
    }

    public String getContainerFromTag() {
        return containerFromTag;
    }

    public void setContainerFromTag(String containerFromTag) {
        this.containerFromTag = containerFromTag;
    }

    public ImageView getIv() {
        return iv;
    }

    public void setIv(ImageView iv) {
        this.iv = iv;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
