package com.sabaq.muse.utils;

import android.content.Context;
import android.view.View;

import com.sabaq.muse.R;

import java.util.Random;

public class GenerateRandomColor {
    private Context context;
    private View view;

    public GenerateRandomColor(Context context, View view) {
        this.context = context;
        this.view = view;
    }

    public void generateRandomBackgroundColor() {
        Integer[] backgroundColorArray = {R.color.colorChapterEnglishBg, R.color.colorChapterUrduBg, R.color.colorChapterScienceBg, R.color.colorChapterMathBg};
        view.setBackgroundColor(context.getResources().getColor(backgroundColorArray[new Random().nextInt((3 - 1) + 1) + 1]));
    }
}
