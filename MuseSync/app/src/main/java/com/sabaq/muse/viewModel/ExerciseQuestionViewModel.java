package com.sabaq.muse.viewModel;

import com.sabaq.muse.database.ExerciseQuestion;

import java.util.ArrayList;

public class ExerciseQuestionViewModel {

    private Long Id;

    private Long ExerciseId;

    private String ExerciseTypeId;

    private String TranslationImagePath;

    private String Question;

    private String UrduTitleImagePath;

    private String UrduQuestion;

    private String SindhiQuestion;

    private String ImagePath;

    private String UrduImagePath;

    private String AudioPath;

    private String Instruction;

    private String InstructionAudioPath;

    private String InstructionImagePath;

    private String ApplicationUserId;

    private Float TranslationImageSize;

    private Float ImageSize;

    private Float UrduImageSize;

    private Float AudioSize;

    private Float InstructionAudioSize;

    private Float InstructionImageSize;

    private Integer Order;

    private Boolean TextboxShow;

    private Boolean IsCorrect;

    private Boolean isFirstTime;

    private Boolean Active;

    private ArrayList<ExerciseAnswersViewModel> exerciseAnswers;

    public ExerciseQuestionViewModel(){}

    public ExerciseQuestionViewModel(ExerciseQuestion exerciseQuestion, ArrayList<ExerciseAnswersViewModel> exerciseAnswers) {
        Id = exerciseQuestion.getId();
        ExerciseId = exerciseQuestion.getExerciseId();
        ExerciseTypeId = exerciseQuestion.getExerciseTypeId();
        TranslationImagePath = exerciseQuestion.getTranslationImagePath();
        Question = exerciseQuestion.getQuestion();
        UrduTitleImagePath = exerciseQuestion.getUrduTitleImagePath();
        UrduQuestion = exerciseQuestion.getUrduQuestion();
        SindhiQuestion = exerciseQuestion.getSindhiQuestion();
        ImagePath = exerciseQuestion.getImagePath();
        UrduImagePath = exerciseQuestion.getUrduImagePath();
        AudioPath = exerciseQuestion.getAudioPath();
        Instruction = exerciseQuestion.getInstruction();
        InstructionAudioPath = exerciseQuestion.getInstructionAudioPath();
        InstructionImagePath = exerciseQuestion.getInstructionImagePath();
        ApplicationUserId = exerciseQuestion.getApplicationUserId();
        TranslationImageSize = exerciseQuestion.getTranslationImageSize();
        ImageSize = exerciseQuestion.getImageSize();
        UrduImageSize = exerciseQuestion.getUrduImageSize();
        AudioSize = exerciseQuestion.getAudioSize();
        InstructionAudioSize = exerciseQuestion.getInstructionAudioSize();
        InstructionImageSize = exerciseQuestion.getInstructionImageSize();
        Order = exerciseQuestion.getOrder();
        TextboxShow = exerciseQuestion.getTextboxShow();
        this.exerciseAnswers = exerciseAnswers;
        this.isFirstTime=true;
        this.Active=exerciseQuestion.getActive();
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean active) {
        Active = active;
    }

    public Boolean getFirstTime() {
        return isFirstTime;
    }

    public void setFirstTime(Boolean firstTime) {
        isFirstTime = firstTime;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Boolean getCorrect() {
        return IsCorrect;
    }

    public void setCorrect(Boolean correct) {
        IsCorrect = correct;
    }

    public Long getExerciseId() {
        return ExerciseId;
    }

    public void setExerciseId(Long exerciseId) {
        ExerciseId = exerciseId;
    }

    public String getExerciseTypeId() {
        return ExerciseTypeId;
    }

    public void setExerciseTypeId(String exerciseTypeId) {
        ExerciseTypeId = exerciseTypeId;
    }

    public String getTranslationImagePath() {
        return TranslationImagePath;
    }

    public void setTranslationImagePath(String translationImagePath) {
        TranslationImagePath = translationImagePath;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getUrduTitleImagePath() {
        return UrduTitleImagePath;
    }

    public void setUrduTitleImagePath(String urduTitleImagePath) {
        UrduTitleImagePath = urduTitleImagePath;
    }

    public String getUrduQuestion() {
        return UrduQuestion;
    }

    public void setUrduQuestion(String urduQuestion) {
        UrduQuestion = urduQuestion;
    }

    public String getSindhiQuestion() {
        return SindhiQuestion;
    }

    public void setSindhiQuestion(String sindhiQuestion) {
        SindhiQuestion = sindhiQuestion;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getUrduImagePath() {
        return UrduImagePath;
    }

    public void setUrduImagePath(String urduImagePath) {
        UrduImagePath = urduImagePath;
    }

    public String getAudioPath() {
        return AudioPath;
    }

    public void setAudioPath(String audioPath) {
        AudioPath = audioPath;
    }

    public String getInstruction() {
        return Instruction;
    }

    public void setInstruction(String instruction) {
        Instruction = instruction;
    }

    public String getInstructionAudioPath() {
        return InstructionAudioPath;
    }

    public void setInstructionAudioPath(String instructionAudioPath) {
        InstructionAudioPath = instructionAudioPath;
    }

    public String getInstructionImagePath() {
        return InstructionImagePath;
    }

    public void setInstructionImagePath(String instructionImagePath) {
        InstructionImagePath = instructionImagePath;
    }

    public String getApplicationUserId() {
        return ApplicationUserId;
    }

    public void setApplicationUserId(String applicationUserId) {
        ApplicationUserId = applicationUserId;
    }

    public Float getTranslationImageSize() {
        return TranslationImageSize;
    }

    public void setTranslationImageSize(Float translationImageSize) {
        TranslationImageSize = translationImageSize;
    }

    public Float getImageSize() {
        return ImageSize;
    }

    public void setImageSize(Float imageSize) {
        ImageSize = imageSize;
    }

    public Float getUrduImageSize() {
        return UrduImageSize;
    }

    public void setUrduImageSize(Float urduImageSize) {
        UrduImageSize = urduImageSize;
    }

    public Float getAudioSize() {
        return AudioSize;
    }

    public void setAudioSize(Float audioSize) {
        AudioSize = audioSize;
    }

    public Float getInstructionAudioSize() {
        return InstructionAudioSize;
    }

    public void setInstructionAudioSize(Float instructionAudioSize) {
        InstructionAudioSize = instructionAudioSize;
    }

    public Float getInstructionImageSize() {
        return InstructionImageSize;
    }

    public void setInstructionImageSize(Float instructionImageSize) {
        InstructionImageSize = instructionImageSize;
    }

    public Integer getOrder() {
        return Order;
    }

    public void setOrder(Integer order) {
        Order = order;
    }

    public Boolean getTextboxShow() {
        return TextboxShow;
    }

    public void setTextboxShow(Boolean textboxShow) {
        TextboxShow = textboxShow;
    }

    public ArrayList<ExerciseAnswersViewModel> getExerciseAnswers() {
        return exerciseAnswers;
    }

    public void setExerciseAnswers(ArrayList<ExerciseAnswersViewModel> exerciseAnswers) {
        this.exerciseAnswers = exerciseAnswers;
    }
}
