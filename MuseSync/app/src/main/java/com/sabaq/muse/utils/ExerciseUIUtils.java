package com.sabaq.muse.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sabaq.muse.R;

public class ExerciseUIUtils {

    @NonNull
    public static TextView generateTextView(String text, Context context, int textSize, LinearLayout.LayoutParams params, int textColor, int textAlignment, int gravity) {
        TextView textView = new TextView(context);
        if (Helper.CURRENT_SUBJECTID == "Urdu") {
            Typeface type = Typeface.createFromAsset(context.getAssets(), "Jameel_Noori_Nastaleeq.ttf");
            textView.setTypeface(type);
        } else {
            textView.setTypeface(Typeface.create("sans-serif-black", Typeface.BOLD));
        }
        textView.setGravity(gravity);
        textView.setText(text);

        textView.setTextAlignment(textAlignment);
        textView.setLayoutParams(params);
        textView.setTextColor(textColor);
        //todo check standard for font size for phone and tablet
     /*   int screenSize = context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                textSize = 36;
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                textSize = 18;
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                textSize = 16;
                break;
            default:
                textSize = 15;
        }*/
//        int valueInPixels = (int) context.getResources().getDimension(R.dimen._11sdp);
        int dp = (int) (context.getResources().getDimension(textSize) / context.getResources().getDisplayMetrics().density);
        textView.setTextSize(dp);

        return textView;
    }


    @NonNull
    private LinearLayout getllHorizontalContainer(LinearLayout llQuestionTextContainer, Context context) {
        //todo make function in helper for ui
        LinearLayout llHorizontalContainer;
        llHorizontalContainer = new LinearLayout(context);

        LinearLayout.LayoutParams llHorizontalContainerlayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        llHorizontalContainerlayoutParams.gravity = Gravity.CENTER_VERTICAL;
        llHorizontalContainer.setGravity(Gravity.CENTER_VERTICAL);
//                        layoutParams.weight = 1;
        llHorizontalContainer.setLayoutParams(llHorizontalContainerlayoutParams);
        llHorizontalContainer.setOrientation(LinearLayout.HORIZONTAL);
        return llHorizontalContainer;
    }

    @NonNull
    private LinearLayout getLinearLayout(LinearLayout llQuestionTextContainer, Context context, LinearLayout.LayoutParams params, int gravity, int orientation) {
        //todo make function in helper for ui
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setGravity(gravity);
        linearLayout.setLayoutParams(params);
        linearLayout.setOrientation(orientation);
        llQuestionTextContainer.addView(linearLayout);
        return linearLayout;
    }

    public static TextView setTextStyle(Context context, TextView textView, int textSize, int textColor) {
//        if (CURRENT)
        textView.setTypeface(Typeface.create("sans-serif-black", Typeface.BOLD));
        textView.setTextColor(textColor);
        int dp = (int) (context.getResources().getDimension(textSize) / context.getResources().getDisplayMetrics().density);
        textView.setTextSize(dp);
        return textView;
    }

}
