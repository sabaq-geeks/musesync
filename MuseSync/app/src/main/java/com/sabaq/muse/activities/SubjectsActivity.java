package com.sabaq.muse.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sabaq.muse.R;
import com.sabaq.muse.animation.BackgroundAnimation;
import com.sabaq.muse.baseActivity.BaseActivity;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.Helper;
import com.sabaq.muse.viewModel.Subject;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubjectsActivity extends BaseActivity {
    private static final String TAG = "MUSE";
    LinearLayout containerBtnSubjects;
    // Storage Permissions variables
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);
        verifyStoragePermissions(this);
        initViews();
        AppController appController = new AppController();
        appController.CreateMaster(this);
        getAllSubjects();

//        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//        WifiInfo wInfo = wifiManager.getConnectionInfo();

//        if (sharedPreference.getBooleanValue(sharedPreference.DataLoadedFromApi))
    }



    private void initViews() {
        containerBtnSubjects = findViewById(R.id.containerbtnSubjects);
        startAnimation();
    }

    private void getAllSubjects() {

        List<com.sabaq.muse.database.Subject> subjects = AppController.daoSession.getSubjectDao().loadAll();
        if (subjects.size() > 0) {
            getSubjectList(subjects);
        } else {
            Toast.makeText(this, "subject is null", Toast.LENGTH_SHORT).show();
        }
    }

    private void getSubjectList(List<com.sabaq.muse.database.Subject> dbSubjects) {

        ArrayList<Subject> subjects = new ArrayList<>();
        for (int i = 0; i < dbSubjects.size(); i++) {
            String Id = dbSubjects.get(i).getId();
            int drawableId = getResources().getIdentifier("com.sabaq.muse:drawable/ic_" + Id.toLowerCase(), null, null);
            Subject subject = new Subject(Id, "", getResources().getDrawable(drawableId));
            subjects.add(subject);
        }
        generateSubjectsButtons(subjects);
    }

    private void generateSubjectsButtons(final ArrayList<Subject> subjects) {
        // Todo make container var here
        int colorCount = 0;
        containerBtnSubjects.setWeightSum(subjects.size());
        for (int i = 0; i < subjects.size(); i++) {
            ImageView ivSubject = new ImageView(this);
            LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            llParams.weight = 1;
            llParams.gravity = Gravity.CENTER;
            ivSubject.setLayoutParams(llParams);
            ivSubject.setImageDrawable(subjects.get(i).getDrawableIcon());
            ivSubject.setId(i);
            final int subjectIndex = i;
            final int color = colorCount;
            colorCount++;
            ivSubject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SubjectsActivity.this, ChapterActivity.class);
                    Helper.CURRENT_SUBJECTID = subjects.get(subjectIndex).getId();
                    Helper.CURRRENT_COLOR = color;
                    Log.d(TAG, "CURRRENT_COLOR: " + color);
                    //intent.putExtra(getString(R.string.subject_name), subjects.get(subjectIndex).getId());
                    startActivity(intent);
                }
            });
//            animationThread(i, ivSubject);
            containerBtnSubjects.addView(ivSubject);
        }
    }

    private void setSubjectIconAnimation(int i, ImageView ivSubject) {

        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(1000 * (i + 2));

        AnimationSet animation = new AnimationSet(false); //change to false
        animation.addAnimation(fadeIn);
        ivSubject.setAnimation(animation);
    }

    private void animationThread(final int index, final ImageView ivSubject) {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(10);   // set the duration of splash screen
                    setSubjectIconAnimation(index, ivSubject);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        timer.start();
    }

    private void shakeAnimation() {
        final ImageView ivProfile = findViewById(R.id.btnProfile);
        final Animation mShakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake_animation);
        ivProfile.startAnimation(mShakeAnimation);
        mShakeAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
//                        R.anim.blink_animation);
//                ivProfile.startAnimation(animFadeIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void startAnimation() {
        shakeAnimation();
        BackgroundAnimation backgroundAnimation = new BackgroundAnimation(this, getResources().getColor(R.color.colorSubjectAnimation), getResources().getColor(R.color.colorSubjectBg));
        backgroundAnimation.animateBackground();
    }

    public void onClickProfile(View view) {
        startActivity(new Intent(SubjectsActivity.this, ProfileActivity.class));
    }


    //persmission method.
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have read or write permission
        int writePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

}

