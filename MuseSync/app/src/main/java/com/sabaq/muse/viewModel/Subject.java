package com.sabaq.muse.viewModel;

import android.graphics.drawable.Drawable;

public class Subject {

    private String Id;
    private String IconPath;
    private Drawable DrawableIcon;

    public Subject(String id, String iconPath, Drawable drawableIcon) {
        Id = id;
        IconPath = iconPath;
        DrawableIcon = drawableIcon;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getIconPath() {
        return IconPath;
    }

    public void setIconPath(String iconPath) {
        IconPath = iconPath;
    }

    public Drawable getDrawableIcon() {
        return DrawableIcon;
    }

    public void setDrawableIcon(Drawable drawableIcon) {
        this.DrawableIcon = drawableIcon;
    }
}
