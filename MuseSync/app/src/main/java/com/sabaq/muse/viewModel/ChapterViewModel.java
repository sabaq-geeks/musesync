package com.sabaq.muse.viewModel;

import android.graphics.drawable.Drawable;

public class ChapterViewModel {
    private Long topicId;
    private String title;
    private String description;
    private int thumbnail;
    private boolean isDownloaded;
    private Drawable progressBarColor;
    private int Order;

    public ChapterViewModel(Long topicId, String title, String description, int thumbnail, Drawable progressBarColor) {
        this.topicId = topicId;
        this.title = title;
        this.description = description;
        this.thumbnail = thumbnail;
        this.progressBarColor = progressBarColor;
    }

    public int getOrder() {
        return Order;
    }

    public void setOrder(int order) {
        Order = order;
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public void setDownloaded(boolean downloaded) {
        isDownloaded = downloaded;
    }

    public Drawable getProgressBarColor() {
        return progressBarColor;
    }

    public void setProgressBarColor(Drawable progressBarColor) {
        this.progressBarColor = progressBarColor;
    }
}