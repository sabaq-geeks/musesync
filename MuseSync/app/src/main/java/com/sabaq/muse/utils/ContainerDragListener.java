package com.sabaq.muse.utils;

import android.view.DragEvent;
import android.view.View;

import com.sabaq.muse.baseActivity.BaseExerciseActivity;

public abstract class ContainerDragListener   extends BaseExerciseActivity implements View.OnDragListener {

    public ContainerDragListener() {

    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        View view = (View) event.getLocalState();
        // Handles each of the expected events
        switch (event.getAction()) {
            //signal for the start of a drag and drop operation.
            case DragEvent.ACTION_DRAG_STARTED:
                // do nothing
                break;
            //the drag point has entered the bounding box of the View
            case DragEvent.ACTION_DRAG_ENTERED:
                // v.setBackground(targetShape);    //change the shape of the view
                break;
            //the user has moved the drag shadow outside the bounding box of the View
            case DragEvent.ACTION_DRAG_EXITED:
                // v.setBackground(normalShape);    //change the shape of the view back to normal
                break;
            //drag shadow has been released,the drag point is within the bounding box of the View
            case DragEvent.ACTION_DROP:
                // if the view is the correct view, we accept the drag item

                break;
            //the drag and drop operation has concluded.
            case DragEvent.ACTION_DRAG_ENDED:
                //  v.setBackground(normalShape);    //go back to normal shape

                break;
            default:
                break;
        }
        return true;
    }

    public abstract void onContainerDrag(View v,View view);



}
