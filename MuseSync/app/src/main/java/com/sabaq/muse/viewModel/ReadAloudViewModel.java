package com.sabaq.muse.viewModel;

public class ReadAloudViewModel {
    private int image;
    private int sound;

    private String imagePath;
    private String audioPath;

    public ReadAloudViewModel(int image, int sound) {
        this.image = image;
        this.sound = sound;
    }

    public ReadAloudViewModel(String imagePath, String audioPath) {
        this.imagePath = imagePath;
        this.audioPath = audioPath;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getSound() {
        return sound;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getAudioPath() {
        return audioPath;
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }
}
