package com.sabaq.muse.adapters;

import android.view.View;

import com.sabaq.muse.viewModel.ChapterViewModel;

import java.util.List;

public interface ItemClickListener {
    void onClickChapterItem(View view, int adapterPosition, List<ChapterViewModel> chapterList);
}
