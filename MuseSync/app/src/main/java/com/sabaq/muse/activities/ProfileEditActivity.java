package com.sabaq.muse.activities;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sabaq.muse.baseActivity.BaseActivity;
import com.sabaq.muse.R;
import com.sabaq.muse.adapters.SubjectProgressAdapter;
import com.sabaq.muse.database.School;
import com.sabaq.muse.database.Student;
import com.sabaq.muse.database.StudentDao;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.ConvertDptoPx;
import com.sabaq.muse.viewModel.SubjectProgress;

import java.util.ArrayList;

public class ProfileEditActivity extends BaseActivity {

    private static final String TAG = "Muse";
    private ArrayList<SubjectProgress> progressList;
    private SubjectProgressAdapter adapter;
    int studentId;
    Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        studentId = getIntent().getIntExtra("StudentId", 45);

        initSubjectProgressList();
        getToolBar();
        setStudentInfo();
    }

    private void setStudentInfo() {
        EditText etStudentName = findViewById(R.id.etStudentName);
        TextView tvSchoolName = findViewById(R.id.tvSchoolname);
        TextView tvStudentId = findViewById(R.id.tvId);

        School school = AppController.daoSession.getSchoolDao().queryBuilder()
                .list().get(0);
        StudentDao studentDao = AppController.daoSession.getStudentDao();
        student = studentDao.queryBuilder().where(StudentDao.Properties.Id.eq(studentId)).list().get(0);
        tvSchoolName.setText(school.getName());
        tvStudentId.setText(student.getId() + "");
        etStudentName.setText(student.getFirstName());

    }

    /**
     * Calculates subject wise progress
     */
    private void initSubjectProgressList() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        progressList = new ArrayList<>();
        SubjectProgress subjectProgress1 = new SubjectProgress("Math", 25, 100, setProgressBar("Math"), R.color.colorChapterMathBg);
        SubjectProgress subjectProgress4 = new SubjectProgress("Urdu", 25, 100, setProgressBar("Urdu"), R.color.colorChapterUrduBg);
        SubjectProgress subjectProgress3 = new SubjectProgress("Science", 55, 100, setProgressBar("Science"), R.color.colorChapterScienceBg);
        SubjectProgress subjectProgress2 = new SubjectProgress("English", 35, 100, setProgressBar("English"), R.color.colorChapterEnglishBg);

        progressList.add(subjectProgress1);
        progressList.add(subjectProgress2);
        progressList.add(subjectProgress3);
        progressList.add(subjectProgress4);
        adapter = new SubjectProgressAdapter(this, progressList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addItemDecoration(new ProfileEditActivity.GridSpacingItemDecoration(1, (new ConvertDptoPx(getApplicationContext(), 2)).dpToPx(), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);
    }

    public void getToolBar() {

        Toolbar mToolbar = findViewById(R.id.tool_bar);
        ImageView home = findViewById(R.id.iv_home);
        ImageView cross = findViewById(R.id.iv_cancel);
        setSupportActionBar(mToolbar);

        setSupportActionBar(mToolbar);
        ((TextView) findViewById(R.id.toolBar_title)).setVisibility(View.INVISIBLE);
        home.setVisibility(View.VISIBLE);
        home.setImageResource(R.drawable.ic_save_grey);
        cross.setImageResource(R.drawable.x_grey);
        ((TextView) findViewById(R.id.toolBar_title)).setText(getIntent().getStringExtra(getString(R.string.topic_name)));
        home.setVisibility(View.VISIBLE);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(ProfileActivity.this, SubjectsActivity.class));
                editStudent();
                onBackPressed();
            }
        });
        cross.setVisibility(View.VISIBLE);

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  Intent intent = new Intent(ProfileEditActivity.this, SubjectsActivity.class);

                startActivity(intent);*/
              onBackPressed();
            }
        });
        cross.setVisibility(View.VISIBLE);
    }

    private void editStudent() {
        EditText etStudentName = findViewById(R.id.etStudentName);
        String studentName = etStudentName.getText().toString();
        StudentDao studentDao = AppController.daoSession.getStudentDao();
        String firstName = studentName.trim();
        Student student1 = studentDao.queryBuilder().where(StudentDao.Properties.Id.eq(studentId)).list().get(0);
        if (firstName != "" && firstName != null)
            student1.setFirstName(firstName);
        studentDao.update(student1);

        Log.d(TAG, "editStudent: " + student1.getFirstName());
        Log.d(TAG, "editStudent et: " + etStudentName.getText().toString());
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        // When the window loses focus (e.g. the action overflow is shown),
        // cancel any pending hide action. When the window gains focus,
        // hide the system UI.
        if (hasFocus) {
            delayedHide(300);
        } else {
            mHideHandler.removeMessages(0);
        }
    }

    private void hideSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void showSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private final Handler mHideHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            hideSystemUI();
        }
    };

    private void delayedHide(int delayMillis) {
        mHideHandler.removeMessages(0);
        mHideHandler.sendEmptyMessageDelayed(0, delayMillis);
    }

}
