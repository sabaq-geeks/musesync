package com.sabaq.muse.viewModel;

import android.view.View;

import com.sabaq.muse.database.ExerciseAnswer;

public class ExerciseAnswersViewModel {

    private Long Id;

    private Long ExerciseQuestionId;

    private String Answer;

    private String ImagePath;

    private String AudioPath;

    private String SindhiAnswer;

    private String UrduAnswer;

    private String MatchingAnswer;

    private String MatchingImagePath;

    private String MatchingAudioPath;

    private Float ImageSize;

    private Float AudioSize;

    private Float MatchingImageSize;

    private Float MatchingAudioSize;

    private Integer CorrectOrder;

    private Boolean CorrectAnswer;

    private Boolean IsSelected;

    private String SelectedAnswer;

    private View selectedView;

    private int Order;

private  boolean Active;

    public int getOrder() {
        return Order;
    }

    public void setOrder(int order) {
        Order = order;
    }

    public Boolean getSelected() {
        return IsSelected;
    }

    public void setSelected(Boolean selected) {
        IsSelected = selected;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public ExerciseAnswersViewModel(ExerciseAnswer exerciseAnswer) {
        Id = exerciseAnswer.getId();
        ExerciseQuestionId = exerciseAnswer.getExerciseQuestionId();
        Answer = exerciseAnswer.getAnswer();
        ImagePath = exerciseAnswer.getImagePath();
        AudioPath = exerciseAnswer.getAudioPath();
        SindhiAnswer = exerciseAnswer.getSindhiAnswer();
        UrduAnswer = exerciseAnswer.getUrduAnswer();
        MatchingAnswer = exerciseAnswer.getMatchingAnswer();
        MatchingImagePath = exerciseAnswer.getMatchingImagePath();
        MatchingAudioPath = exerciseAnswer.getMatchingAudioPath();
        ImageSize = exerciseAnswer.getImageSize();
        AudioSize = exerciseAnswer.getAudioSize();
        MatchingImageSize = exerciseAnswer.getMatchingImageSize();
        MatchingAudioSize = exerciseAnswer.getMatchingAudioSize();
        CorrectOrder = exerciseAnswer.getCorrectOrder();
        CorrectAnswer = exerciseAnswer.getCorrectAnswer();
        Active=exerciseAnswer.getActive();
    }

    public Long getExerciseQuestionId() {
        return ExerciseQuestionId;
    }

    public void setExerciseQuestionId(Long exerciseQuestionId) {
        ExerciseQuestionId = exerciseQuestionId;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getAudioPath() {
        return AudioPath;
    }

    public void setAudioPath(String audioPath) {
        AudioPath = audioPath;
    }

    public String getSindhiAnswer() {
        return SindhiAnswer;
    }

    public void setSindhiAnswer(String sindhiAnswer) {
        SindhiAnswer = sindhiAnswer;
    }

    public String getUrduAnswer() {
        return UrduAnswer;
    }

    public void setUrduAnswer(String urduAnswer) {
        UrduAnswer = urduAnswer;
    }

    public String getMatchingAnswer() {
        return MatchingAnswer;
    }

    public void setMatchingAnswer(String matchingAnswer) {
        MatchingAnswer = matchingAnswer;
    }

    public String getMatchingImagePath() {
        return MatchingImagePath;
    }

    public void setMatchingImagePath(String matchingImagePath) {
        MatchingImagePath = matchingImagePath;
    }

    public String getMatchingAudioPath() {
        return MatchingAudioPath;
    }

    public void setMatchingAudioPath(String matchingAudioPath) {
        MatchingAudioPath = matchingAudioPath;
    }

    public Float getImageSize() {
        return ImageSize;
    }

    public void setImageSize(Float imageSize) {
        ImageSize = imageSize;
    }

    public Float getAudioSize() {
        return AudioSize;
    }

    public void setAudioSize(Float audioSize) {
        AudioSize = audioSize;
    }

    public Float getMatchingImageSize() {
        return MatchingImageSize;
    }

    public void setMatchingImageSize(Float matchingImageSize) {
        MatchingImageSize = matchingImageSize;
    }

    public Float getMatchingAudioSize() {
        return MatchingAudioSize;
    }

    public void setMatchingAudioSize(Float matchingAudioSize) {
        MatchingAudioSize = matchingAudioSize;
    }

    public Integer getCorrectOrder() {
        return CorrectOrder;
    }

    public void setCorrectOrder(Integer correctOrder) {
        CorrectOrder = correctOrder;
    }

    public Boolean getCorrectAnswer() {
        return CorrectAnswer;
    }

    public void setCorrectAnswer(Boolean correctAnswer) {
        CorrectAnswer = correctAnswer;
    }

    public String getSelectedAnswer() {
        return SelectedAnswer;
    }

    public void setSelectedAnswer(String selectedAnswer) {
        SelectedAnswer = selectedAnswer;
    }

    public View getSelectedView() {
        return selectedView;
    }

    public void setSelectedView(View selectedView) {
        this.selectedView = selectedView;
    }
}
