package com.sabaq.muse.utils;

import android.content.Context;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;

public class ImageLoad {
    Context context;

    public ImageLoad(Context context) {
        this.context = context;
    }

    public void setImage(String imagePath, ImageView imageView) {
        imageFromLocalStorage(context, imagePath, imageView);
    }

    private void imageFromLocalStorage(Context context, String imagePath, ImageView imageView) {
        if (imagePath != null) {
            File fileImage = new File(Helper.LOCAL_PATH, imagePath);
            if (fileImage.exists()) {
                Glide.with(context)
                        .load(fileImage)
                        .into(imageView);
            } else if (InternetDetector.detectInternet(context)) {
                imageFromApi(context, imagePath, imageView);
            } else {
                imageView.setImageBitmap(null);
                Toast.makeText(context, "Image not exist", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void imageFromApi(Context context, String imageUrl, ImageView imageView) {
        Glide.with(context)
                .load(Helper.LIVE_PATH + imageUrl)
                .into(imageView);
    }
}
