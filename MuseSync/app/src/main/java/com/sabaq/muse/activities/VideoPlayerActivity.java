package com.sabaq.muse.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;
import com.sabaq.muse.baseActivity.BaseActivity;
import com.sabaq.muse.R;
import com.sabaq.muse.database.Video;
import com.sabaq.muse.database.VideoDao;
import com.sabaq.muse.greenDao.AppController;
import com.sabaq.muse.utils.Helper;
import com.sabaq.muse.utils.InternetDetector;

import org.greenrobot.greendao.query.QueryBuilder;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

public class VideoPlayerActivity extends BaseActivity implements BetterVideoCallback {
    /**
     * Called when the activity is first created.
     */
    private ActionBarDrawerToggle drawerToggle;
    private BetterVideoPlayer player;
    LinearLayout llToolbar;
    boolean fetchContentFromLive = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.video_player);


        //Helper.CURRENT_CONTENTID= Long.valueOf(78);
        Long videoId = Helper.CURRENT_CONTENTID;
        Log.d("VideoPlayer", "onCreate: VideoId:" + videoId);

        Uri uri = getUri(videoId);

        initToolbar();

        initPlayer(uri);


    }


    android.support.v7.widget.Toolbar toolbar;

    private void initToolbar() {
        getToolBar("", View.VISIBLE, View.VISIBLE, View.INVISIBLE, this);
//        llToolbar = findViewById(R.id.tool_bar);
        llToolbar = findViewById(R.id.ll_tool_bar);
        llToolbar.setBackgroundColor(Color.TRANSPARENT);
        llToolbar.bringToFront();
    }

    private void initPlayer(Uri uri) {
        player = findViewById(R.id.player);
        player.setAutoPlay(true);
        player.setCallback(VideoPlayerActivity.this);

        // Sets the source to the HTTP URL held in the TEST_URL variable.
        // To play files, you can use Uri.fromFile(new File("..."))
        player.setSource(uri);
        player.enableSwipeGestures(getWindow());
    }

    private Uri getUri(Long videoId) {
        QueryBuilder<Video> qbVideo = AppController.daoSession.getVideoDao().queryBuilder().where(VideoDao.Properties.Id.eq(videoId));
        Video video = qbVideo.list().get(0);
        return getUri(video);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Make sure the player stops playing if the user presses the home button.
        player.pause();
    }

    // Methods for the implemented EasyVideoCallback

    @Override
    public void onStarted(BetterVideoPlayer player) {
        //Log.i(TAG, "Started");
    }

    @Override
    public void onPaused(BetterVideoPlayer player) {
        //Log.i(TAG, "Paused");
    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {
        //Log.i(TAG, "Preparing");
    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {
        //Log.i(TAG, "Prepared");
    }

    @Override
    public void onBuffering(int percent) {
        //Log.i(TAG, "Buffering " + percent);
    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {
        Log.i("MUSE", "Error " + e.getMessage());
    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {
        //Log.i(TAG, "Completed");
        startActivity(new Intent(VideoPlayerActivity.this, LessonMapActivity.class));
    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {
        if (isShowing) {
            llToolbar.setVisibility(View.VISIBLE);
        } else
            llToolbar.setVisibility(View.INVISIBLE);
    }

    @Nullable
    private Uri getUri(Video video) {
        String englishVideoPath = video.getPath();
        String urduVideoPath = video.getUrduPath();
        String sindhiVideoPath = video.getSindhiPath();
        Uri uri = null;
        if (englishVideoPath != null && englishVideoPath!="") {
            ifFileExist(englishVideoPath);
            englishVideoPath = removeSpacesFromPath(englishVideoPath);
             if (!fetchContentFromLive) {
                englishVideoPath = (Helper.LOCAL_PATH) + englishVideoPath;
             }
            else if (InternetDetector.checkIntern(VideoPlayerActivity.this) && fetchContentFromLive)
            englishVideoPath = (Helper.LIVE_PATH) + englishVideoPath;
            else {
                 Toast.makeText(this, "Not Found", Toast.LENGTH_SHORT).show();
             }

            uri = Uri.parse(englishVideoPath);
            Log.d("VideoActivity", "onCreate: EP:" + englishVideoPath);

        } else if (urduVideoPath != null && urduVideoPath!="") {
            ifFileExist(urduVideoPath);
            urduVideoPath = removeSpacesFromPath(urduVideoPath);
            if(!fetchContentFromLive){
                urduVideoPath = (Helper.LOCAL_PATH) + urduVideoPath;
            }
            else if (InternetDetector.checkIntern(VideoPlayerActivity.this) && fetchContentFromLive)
                urduVideoPath = (Helper.LIVE_PATH) + urduVideoPath;
            else {
                Toast.makeText(this, "Not Found", Toast.LENGTH_SHORT).show();
            }
           // urduVideoPath = (fetchContentFromLive ? Helper.LIVE_PATH : Helper.LOCAL_PATH) + urduVideoPath;
            uri = Uri.parse(urduVideoPath);
            Log.d("VideoActivity", "onCreate: UP:" + urduVideoPath + " URI:" + uri);

        } else if (sindhiVideoPath != null && sindhiVideoPath!="") {
            ifFileExist(sindhiVideoPath);
            sindhiVideoPath = removeSpacesFromPath(sindhiVideoPath);
            sindhiVideoPath = (fetchContentFromLive ? Helper.LIVE_PATH : Helper.LOCAL_PATH) + sindhiVideoPath;
            if(!fetchContentFromLive){
                sindhiVideoPath = (Helper.LOCAL_PATH) + sindhiVideoPath;
            }
            else if (InternetDetector.checkIntern(VideoPlayerActivity.this) && fetchContentFromLive)
                sindhiVideoPath = (Helper.LIVE_PATH) + sindhiVideoPath;
            else {
                Toast.makeText(this, "Not Found", Toast.LENGTH_SHORT).show();
            }
            uri = Uri.parse(sindhiVideoPath);
            Log.d("VideoActivity", "onCreate: SP:" + sindhiVideoPath);
        }
        return uri;
    }

    @NonNull
    private String removeSpacesFromPath(String englishVideoPath) {
        try {

            URI uri2 = new URI(englishVideoPath.replace(" ", "%20"));
            englishVideoPath = uri2.toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return englishVideoPath;
    }

    private void ifFileExist(String englishVideoPath) {
        File videoPath = new File(Helper.LOCAL_PATH, englishVideoPath);
        if (videoPath.exists()) {
            fetchContentFromLive = false;
        } else {
            fetchContentFromLive = true;
        }
    }


    public void onClickCross(View view) {
        Intent intent = new Intent(VideoPlayerActivity.this, LessonMapActivity.class);
        startActivity(intent);
        finish();
    }

    public void onClickHome(View view) {

        startActivity(new Intent(VideoPlayerActivity.this, SubjectsActivity.class));
        finish();
    }
}