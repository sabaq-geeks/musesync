package com.sabaq.muse.database;

import org.greenrobot.greendao.annotation.*;

import java.util.List;
import com.sabaq.muse.database.DaoSession;
import org.greenrobot.greendao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "Readalouds".
 */
@Entity(active = true, nameInDb = "Readalouds")
public class Readaloud {

    @Id(autoincrement = true)
    @Property(nameInDb = "Id")
    private Long Id;

    @Property(nameInDb = "Active")
    private Boolean Active;

    @Property(nameInDb = "CreatedDate")
    private java.util.Date CreatedDate;

    @Property(nameInDb = "CreatedBy")
    private String CreatedBy;

    @Property(nameInDb = "UpdatedDate")
    private java.util.Date UpdatedDate;

    @Property(nameInDb = "UpdatedBy")
    private String UpdatedBy;

    @Property(nameInDb = "Title")
    private String Title;

    @Property(nameInDb = "UrduTitleImagePath")
    private String UrduTitleImagePath;

    @Property(nameInDb = "Description")
    private String Description;

    @Property(nameInDb = "UrduTitle")
    private String UrduTitle;

    @Property(nameInDb = "UrduPath")
    private String UrduPath;

    @Property(nameInDb = "CompleteConvention")
    private Boolean CompleteConvention;

    @Property(nameInDb = "Reviewed")
    private Boolean Reviewed;

    @Property(nameInDb = "Order")
    private Integer Order;

    @Property(nameInDb = "UrduTitleImageSize")
    private Float UrduTitleImageSize;

    @Property(nameInDb = "UrduFileSize")
    private Float UrduFileSize;

    @Property(nameInDb = "SindhiFileSize")
    private Float SindhiFileSize;

    @Property(nameInDb = "FileSize")
    private Float FileSize;

    @Property(nameInDb = "IsCompleted")
    private Boolean IsCompleted;

    @Property(nameInDb = "GradeId")
    private String GradeId;

    @Property(nameInDb = "SubjectId")
    private String SubjectId;

    @Property(nameInDb = "TopicId")
    private Long TopicId;

    @Property(nameInDb = "SubTopicId")
    private Long SubTopicId;

    @Property(nameInDb = "ReadaloudId")
    private Long ReadaloudId;

    @Property(nameInDb = "ExerciseId")
    private Long ExerciseId;

    @Property(nameInDb = "ReadaloudTypeId")
    private String ReadaloudTypeId;

    /** Used to resolve relations */
    @Generated
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated
    private transient ReadaloudDao myDao;

    @ToOne(joinProperty = "TopicId")
    private Topic topic;

    @Generated
    private transient Long topic__resolvedKey;

    @ToOne(joinProperty = "SubTopicId")
    private SubTopic subTopic;

    @Generated
    private transient Long subTopic__resolvedKey;

    @ToMany(joinProperties = {
        @JoinProperty(name = "Id", referencedName = "ReadaloudId")
    })
    private List<ReadaloudSlide> readaloudSlides;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public Readaloud() {
    }

    public Readaloud(Long Id) {
        this.Id = Id;
    }

    @Generated
    public Readaloud(Long Id, Boolean Active, java.util.Date CreatedDate, String CreatedBy, java.util.Date UpdatedDate, String UpdatedBy, String Title, String UrduTitleImagePath, String Description, String UrduTitle, String UrduPath, Boolean CompleteConvention, Boolean Reviewed, Integer Order, Float UrduTitleImageSize, Float UrduFileSize, Float SindhiFileSize, Float FileSize, Boolean IsCompleted, String GradeId, String SubjectId, Long TopicId, Long SubTopicId, Long ReadaloudId, Long ExerciseId, String ReadaloudTypeId) {
        this.Id = Id;
        this.Active = Active;
        this.CreatedDate = CreatedDate;
        this.CreatedBy = CreatedBy;
        this.UpdatedDate = UpdatedDate;
        this.UpdatedBy = UpdatedBy;
        this.Title = Title;
        this.UrduTitleImagePath = UrduTitleImagePath;
        this.Description = Description;
        this.UrduTitle = UrduTitle;
        this.UrduPath = UrduPath;
        this.CompleteConvention = CompleteConvention;
        this.Reviewed = Reviewed;
        this.Order = Order;
        this.UrduTitleImageSize = UrduTitleImageSize;
        this.UrduFileSize = UrduFileSize;
        this.SindhiFileSize = SindhiFileSize;
        this.FileSize = FileSize;
        this.IsCompleted = IsCompleted;
        this.GradeId = GradeId;
        this.SubjectId = SubjectId;
        this.TopicId = TopicId;
        this.SubTopicId = SubTopicId;
        this.ReadaloudId = ReadaloudId;
        this.ExerciseId = ExerciseId;
        this.ReadaloudTypeId = ReadaloudTypeId;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getReadaloudDao() : null;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Boolean getActive() {
        return Active;
    }

    public void setActive(Boolean Active) {
        this.Active = Active;
    }

    public java.util.Date getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(java.util.Date CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public java.util.Date getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(java.util.Date UpdatedDate) {
        this.UpdatedDate = UpdatedDate;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String UpdatedBy) {
        this.UpdatedBy = UpdatedBy;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getUrduTitleImagePath() {
        return UrduTitleImagePath;
    }

    public void setUrduTitleImagePath(String UrduTitleImagePath) {
        this.UrduTitleImagePath = UrduTitleImagePath;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getUrduTitle() {
        return UrduTitle;
    }

    public void setUrduTitle(String UrduTitle) {
        this.UrduTitle = UrduTitle;
    }

    public String getUrduPath() {
        return UrduPath;
    }

    public void setUrduPath(String UrduPath) {
        this.UrduPath = UrduPath;
    }

    public Boolean getCompleteConvention() {
        return CompleteConvention;
    }

    public void setCompleteConvention(Boolean CompleteConvention) {
        this.CompleteConvention = CompleteConvention;
    }

    public Boolean getReviewed() {
        return Reviewed;
    }

    public void setReviewed(Boolean Reviewed) {
        this.Reviewed = Reviewed;
    }

    public Integer getOrder() {
        return Order;
    }

    public void setOrder(Integer Order) {
        this.Order = Order;
    }

    public Float getUrduTitleImageSize() {
        return UrduTitleImageSize;
    }

    public void setUrduTitleImageSize(Float UrduTitleImageSize) {
        this.UrduTitleImageSize = UrduTitleImageSize;
    }

    public Float getUrduFileSize() {
        return UrduFileSize;
    }

    public void setUrduFileSize(Float UrduFileSize) {
        this.UrduFileSize = UrduFileSize;
    }

    public Float getSindhiFileSize() {
        return SindhiFileSize;
    }

    public void setSindhiFileSize(Float SindhiFileSize) {
        this.SindhiFileSize = SindhiFileSize;
    }

    public Float getFileSize() {
        return FileSize;
    }

    public void setFileSize(Float FileSize) {
        this.FileSize = FileSize;
    }

    public Boolean getIsCompleted() {
        return IsCompleted;
    }

    public void setIsCompleted(Boolean IsCompleted) {
        this.IsCompleted = IsCompleted;
    }

    public String getGradeId() {
        return GradeId;
    }

    public void setGradeId(String GradeId) {
        this.GradeId = GradeId;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(String SubjectId) {
        this.SubjectId = SubjectId;
    }

    public Long getTopicId() {
        return TopicId;
    }

    public void setTopicId(Long TopicId) {
        this.TopicId = TopicId;
    }

    public Long getSubTopicId() {
        return SubTopicId;
    }

    public void setSubTopicId(Long SubTopicId) {
        this.SubTopicId = SubTopicId;
    }

    public Long getReadaloudId() {
        return ReadaloudId;
    }

    public void setReadaloudId(Long ReadaloudId) {
        this.ReadaloudId = ReadaloudId;
    }

    public Long getExerciseId() {
        return ExerciseId;
    }

    public void setExerciseId(Long ExerciseId) {
        this.ExerciseId = ExerciseId;
    }

    public String getReadaloudTypeId() {
        return ReadaloudTypeId;
    }

    public void setReadaloudTypeId(String ReadaloudTypeId) {
        this.ReadaloudTypeId = ReadaloudTypeId;
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public Topic getTopic() {
        Long __key = this.TopicId;
        if (topic__resolvedKey == null || !topic__resolvedKey.equals(__key)) {
            __throwIfDetached();
            TopicDao targetDao = daoSession.getTopicDao();
            Topic topicNew = targetDao.load(__key);
            synchronized (this) {
                topic = topicNew;
            	topic__resolvedKey = __key;
            }
        }
        return topic;
    }

    @Generated
    public void setTopic(Topic topic) {
        synchronized (this) {
            this.topic = topic;
            TopicId = topic == null ? null : topic.getId();
            topic__resolvedKey = TopicId;
        }
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public SubTopic getSubTopic() {
        Long __key = this.SubTopicId;
        if (subTopic__resolvedKey == null || !subTopic__resolvedKey.equals(__key)) {
            __throwIfDetached();
            SubTopicDao targetDao = daoSession.getSubTopicDao();
            SubTopic subTopicNew = targetDao.load(__key);
            synchronized (this) {
                subTopic = subTopicNew;
            	subTopic__resolvedKey = __key;
            }
        }
        return subTopic;
    }

    @Generated
    public void setSubTopic(SubTopic subTopic) {
        synchronized (this) {
            this.subTopic = subTopic;
            SubTopicId = subTopic == null ? null : subTopic.getId();
            subTopic__resolvedKey = SubTopicId;
        }
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    @Generated
    public List<ReadaloudSlide> getReadaloudSlides() {
        if (readaloudSlides == null) {
            __throwIfDetached();
            ReadaloudSlideDao targetDao = daoSession.getReadaloudSlideDao();
            List<ReadaloudSlide> readaloudSlidesNew = targetDao._queryReadaloud_ReadaloudSlides(Id);
            synchronized (this) {
                if(readaloudSlides == null) {
                    readaloudSlides = readaloudSlidesNew;
                }
            }
        }
        return readaloudSlides;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated
    public synchronized void resetReadaloudSlides() {
        readaloudSlides = null;
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void delete() {
        __throwIfDetached();
        myDao.delete(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void update() {
        __throwIfDetached();
        myDao.update(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void refresh() {
        __throwIfDetached();
        myDao.refresh(this);
    }

    @Generated
    private void __throwIfDetached() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
